﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class MyFedScript : MonoBehaviour {

	public Text FedNameText;
	public Text FedBonusText;
	public Image FedImage;

	public Text CommishNameText;
	public Text CommishBonusText;
	public Image CommishImage;

	public Mask ratings;

	private float ratingsWidth;
	private GameObject UIManager;
	private GameObject HUD;

	public void Start(){
       
	}
	// Use this for initialization
	void OnEnable () {
		if(ratingsWidth == 0){
			ratingsWidth = ratings.rectTransform.rect.width;
		}

		UIManager = GameObject.Find("UIManager");
		HUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;
		HUD.SetActive(false);

		ratings.rectTransform.sizeDelta = new Vector2(ratingsWidth * GamePlaySession.instance.GetAverageRating(),
		                                              ratings.rectTransform.rect.height  );
		FedNameText.text = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.GetSelectedFederation()].NameString;
		FedBonusText.text = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.GetSelectedFederation()].BonusString;
		FedImage.sprite = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.GetSelectedFederation()].littleSprite;

		CommishNameText.text = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.GetSelectedComissioner()].NameString;
		CommishBonusText.text = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.GetSelectedComissioner()].BonusString;
		CommishImage.sprite = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.GetSelectedComissioner()].littleSprite;

	}
	
	// Update is called once per frame
	void OnDisable() {
		HUD.SetActive(true);
	}
}
