﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferenceManager : MonoBehaviour 
{
	public static ReferenceManager instance;

	public GameObject UIManager;

	void Awake()
	{
		instance = this;
		DontDestroyOnLoad (this.gameObject);
	}
}
