﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SkitSegmentSetup : SegmentSetup {
	
	public override bool CheckReady() {
		bool isReady = true;

		if (cards[0] != null) {
			// We have a card set now, so check whether we have a minimum amount of wrestlers
			SkitCardObject skit = (SkitCardObject) cards[0];

			if (skit != null) {
				for (int i = 1;i <= skit.wrestlerMin;i++) {
					if (cards[i] == null)
						isReady = false;
				}
			} else {
				isReady = false;
			}
		} else {
			// We don't have a main segment card set yet
			isReady = false;
		}
		
		return isReady;
	}
	
	public override void SetFlavorButtons(List<FlavorCardObject> flavors) {
		int[] flavorOptions = new int[] {6};
		foreach (int option in flavorOptions) {
			Button button = optionButtons[option].GetComponentInChildren<Button>();
			if (button != null) {
				bool hasFlavor = false;
				
				CardSelector selector = selectors[option];
				
				foreach (FlavorCardObject flavor in flavors) {
					if (CheckCardPlayable(flavor, selector)) {
						hasFlavor = true;
					}
				}
				
				if (hasFlavor) {
					button.interactable = true;
				} else {
					button.interactable = false;
				}
			}
		}
	}
	
	public override void ResetWrestlerButtons() {
		for (int i = 1;i <= 5;i++) {
			if (selectors[i] != null) {
				GameObject holder = selectors[i].transform.parent.gameObject;
				if (holder != null) {
					holder.SetActive(true);
				}
			}
		}
	}
	
	public override void SetWrestlerButtons() {
		for (int i = 1;i <= 5;i++) {
			if (selectors[i] != null && optionButtons[i] != null) {
				GameObject holder = selectors[i].transform.parent.gameObject;

				Button button = optionButtons[i].GetComponent<Button>();
				if (button == null) {
					button = optionButtons[i].GetComponentInChildren<Button>();
				}
				
				if (holder != null && button != null) {
					if (cards[0] == null) {
						holder.SetActive(true);
						button.interactable = false;
					} else {
						SkitCardObject skit = (SkitCardObject) cards[0];
						
						if (skit != null) {
							if (i <= skit.wrestlerMax) {
								holder.SetActive(true);
								button.interactable = true;
							} else {
								holder.SetActive(false);
								button.interactable = false;
							}
						} else {
							holder.SetActive(true);
							button.interactable = false;
						}
					}
				}
			}
		}
	}
	
	public override void CheckCardCompatibility(int index) {
		SegmentObject segment = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID].segments[segmentDisplay.segmentID];

		//Debug.Log("Cards Count = " + cards.Length.ToString());
		
		int wrestler1Index = 1;
		int wrestler2Index = 2;
		int wrestler3Index = 3;
		int wrestler4Index = 4;
		int wrestler5Index = 5;
		int flavorIndex = 6;
		
		if (cards[flavorIndex] != null) {
			WrestlerCardObject[] wrestlers = new WrestlerCardObject[5];
			
			if (cards[wrestler1Index] != null)
				wrestlers[0] = (WrestlerCardObject) cards[wrestler1Index];
			if (cards[wrestler2Index] != null)
				wrestlers[1] = (WrestlerCardObject) cards[wrestler2Index];
			if (cards[wrestler3Index] != null)
				wrestlers[2] = (WrestlerCardObject) cards[wrestler3Index];
			if (cards[wrestler4Index] != null)
				wrestlers[3] = (WrestlerCardObject) cards[wrestler4Index];
			if (cards[wrestler5Index] != null)
				wrestlers[4] = (WrestlerCardObject) cards[wrestler5Index];
			
			FlavorCardObject flavor = (FlavorCardObject) cards[flavorIndex];
			
			if (wrestlers.Length > 0 && flavor.cardClass == "Manager") {
				// We have a manager card selected, so let's make sure it is compatible in alignment
				bool mismatch = true;
				
				foreach (WrestlerCardObject wrestler in wrestlers) {
					if (wrestler != null) {
						if (wrestler.cardAlignment.ToUpper() == flavor.characterAlignment.ToUpper()) {
							// The alignment of the cards is a mismatch
							mismatch = false;
						}
					}
				}
				
				if (mismatch) {
					// We had a mismatch so deselect the other card
					if (index != flavorIndex) {
						cards[flavorIndex] = null;
					} else {
						cards[wrestler1Index] = null;
						cards[wrestler2Index] = null;
						cards[wrestler3Index] = null;
						cards[wrestler4Index] = null;
						cards[wrestler5Index] = null;
					}
					SetData(segmentDisplay);
				}
			}
		}
	}
}
