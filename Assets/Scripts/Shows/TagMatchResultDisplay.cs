﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TagMatchResultDisplay : ResultDisplay {
	
	public Text segmentTitle;
	public Image wrestlerHeadshot1;
	public Image wrestlerHeadshot2;
	public Image wrestlerHeadshot3;
	public Image wrestlerHeadshot4;
	public Image wrestlerBorder1;
	public Image wrestlerBorder2;
	public Image wrestlerBorder3;
	public Image wrestlerBorder4;
	public Text wrestlerWinner1;
	public Text wrestlerWinner2;
	public Text wrestlerWinner3;
	public Text wrestlerWinner4;
	public Text wrestlerSkill1;
	public Text wrestlerSkill2;
	public Text wrestlerSkill3;
	public Text wrestlerSkill4;
	public Image segmentRating;
	public Text segmentText;
	
	public override void UpdateResultData() {
		if (segment != null) {
			// Tell the segment to get the segment results!
			segment.GetResults(segment);
			
			// Display the results!
			segmentTitle.text = segment.segmentTitle;
			
			wrestlerHeadshot1.sprite = GamePlaySession.instance.CheckCard(segment.team1IDs[0]).headshotImage;
			wrestlerHeadshot2.sprite = GamePlaySession.instance.CheckCard(segment.team1IDs[1]).headshotImage;
			wrestlerHeadshot3.sprite = GamePlaySession.instance.CheckCard(segment.team2IDs[0]).headshotImage;
			wrestlerHeadshot4.sprite = GamePlaySession.instance.CheckCard(segment.team2IDs[1]).headshotImage;

			if (segment.winnerIDs[0] == segment.team1IDs[0]) {
				wrestlerWinner1.text = "Winner!";
				wrestlerWinner2.text = "Winner!";
				wrestlerWinner3.text = "";
				wrestlerWinner4.text = "";
			} else {
				wrestlerWinner1.text = "";
				wrestlerWinner2.text = "";
				wrestlerWinner3.text = "Winner!";
				wrestlerWinner4.text = "Winner!";
			}
			
			if (segment.team1SkillChange[0] > 0) {
				wrestlerSkill1.text = "+" + segment.team1SkillChange[0].ToString() + " SKILL!";
			} else {
				wrestlerSkill1.text = "";
			}
			if (segment.team1SkillChange[1] > 0) {
				wrestlerSkill2.text = "+" + segment.team1SkillChange[1].ToString() + " SKILL!";
			} else {
				wrestlerSkill2.text = "";
			}
			
			if (segment.team2SkillChange[0] > 0) {
				wrestlerSkill3.text = "+" + segment.team2SkillChange[0].ToString() + " SKILL!";
			} else {
				wrestlerSkill3.text = "";
			}
			if (segment.team2SkillChange[1] > 0) {
				wrestlerSkill4.text = "+" + segment.team2SkillChange[1].ToString() + " SKILL!";
			} else {
				wrestlerSkill4.text = "";
			}
			
			segmentRating.fillAmount = segment.GetStarRating();

			WrestlerCardObject w = (WrestlerCardObject)GamePlaySession.instance.CheckCard (segment.winnerIDs [Random.Range (0, segment.winnerIDs.Length - 1)]);

			segmentText.text = w.victoryLine;

			if (segment.winnerIDs[0] == segment.team1IDs[0]) 
			{
				wrestlerBorder1.color = Color.yellow;
				wrestlerBorder2.color = Color.yellow;
				wrestlerBorder3.color = Color.white;
				wrestlerBorder4.color = Color.white;
				wrestlerWinner1.color = Color.yellow;
				wrestlerWinner2.color = Color.yellow;
				wrestlerWinner3.color = Color.white;
				wrestlerWinner4.color = Color.white;
				wrestlerSkill1.color = Color.yellow;
				wrestlerSkill2.color = Color.yellow;
				wrestlerSkill3.color = Color.white;
				wrestlerSkill4.color = Color.white;
			} 
			else 
			{
				wrestlerBorder1.color = Color.white;
				wrestlerBorder2.color = Color.white;
				wrestlerBorder3.color = Color.yellow;
				wrestlerBorder4.color = Color.yellow;
				wrestlerWinner1.color = Color.white;
				wrestlerWinner2.color = Color.white;
				wrestlerWinner3.color = Color.yellow;
				wrestlerWinner4.color = Color.yellow;
				wrestlerSkill1.color = Color.white;
				wrestlerSkill2.color = Color.white;
				wrestlerSkill3.color = Color.yellow;
				wrestlerSkill4.color = Color.yellow;
			}
		}
	}
}
