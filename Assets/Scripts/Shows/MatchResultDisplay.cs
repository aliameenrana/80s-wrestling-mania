﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MatchResultDisplay : ResultDisplay {

	public Text segmentTitle;
	public Image wrestlerHeadshot1;
	public Image wrestlerHeadshot2;
	public Image wrestlerBorder1;
	public Image wrestlerBorder2;
	public Text wrestlerWinner1;
	public Text wrestlerWinner2;
	public Text wrestlerSkill1;
	public Text wrestlerSkill2;
	public Image segmentRating;
	public Text segmentText;

	public override void UpdateResultData() {
		if (segment != null ) {
			// Tell the segment to get the segment results!
			segment.GetResults(segment);

			// Display the results!
			segmentTitle.text = segment.segmentTitle;

            //Debug.Log(segment.team1[0].CardNumber);
            //Debug.Log(segment.team2[0].CardNumber);

			wrestlerHeadshot1.sprite = GamePlaySession.instance.CheckCard(segment.team1IDs[0]).headshotImage;
			wrestlerHeadshot2.sprite = GamePlaySession.instance.CheckCard(segment.team2IDs[0]).headshotImage;

            if (segment.DisQualify == 0 || segment.DisQualify == 2 || segment.DisQualify == 5)
            {
                if (segment.DisQualify == 0)
                {
                    if (segment.winnerIDs[0] == segment.team1IDs[0])
                    {
                        wrestlerWinner1.text = "Winner!";
                        wrestlerWinner2.text = "";
                    }
                    else
                    {
                        wrestlerWinner1.text = "";
                        wrestlerWinner2.text = "Winner!";
                    }
                }
                else if (segment.DisQualify == 2)
                {
                    if (segment.winnerIDs[0] == segment.team1IDs[0])
                    {
                        wrestlerWinner1.text = "Winner!";
                        wrestlerWinner2.text = "Countout!";
                    }
                    else
                    {
                        wrestlerWinner1.text = "Countout!";
                        wrestlerWinner2.text = "Winner!";
                    }
                }
                else if (segment.DisQualify == 5)
                {
                    if (segment.winnerIDs[0] == segment.team1IDs[0])
                    {
                        wrestlerWinner1.text = "Winner!";
                        wrestlerWinner2.text = "DisQualified!";
                    }
                    else
                    {
                        wrestlerWinner1.text = "DisQualified!";
                        wrestlerWinner2.text = "Winner!";
                    }
                }
            }
            else if ((segment.DisQualify != 0) && (segment.DisQualify == 1 || segment.DisQualify == 3 || segment.DisQualify == 4))
            {
                //Double DQ, Double Count Out or Time Limit Draw
                if (segment.DisQualify == 3)
                {
                    wrestlerWinner1.text = "DisQualified!";
                    wrestlerWinner2.text = "DisQualified!";
                }
                else if (segment.DisQualify == 1)
                {
                    wrestlerWinner1.text = "Draw!";
                    wrestlerWinner2.text = "Draw!";
                }
                else if (segment.DisQualify == 4)
                {
                    wrestlerWinner1.text = "Countout!";
                    wrestlerWinner2.text = "Countout!";
                }
            }

            segmentRating.fillAmount = segment.GetStarRating();

            if (segment.team1SkillChange[0] > 0)
            {
                wrestlerSkill1.text = "+" + segment.team1SkillChange[0].ToString() + " SKILL!";
            }
            else
            {
                wrestlerSkill1.text = "";
            }

            if (segment.team2SkillChange[0] > 0)
            {
                wrestlerSkill2.text = "+" + segment.team2SkillChange[0].ToString() + " SKILL!";
            }
            else
            {
                wrestlerSkill2.text = "";
            }
            switch (segment.DisQualify)
            {
                case 1:
                    segmentText.text = "Time Limit Draw!";
                    if (segment.winnerIDs[0] == segment.team1IDs[0])
                    {
                        wrestlerBorder1.color = Color.white;
                        wrestlerBorder2.color = Color.white;
                        wrestlerWinner1.color = Color.white;
                        wrestlerWinner2.color = Color.white;
                        wrestlerSkill1.color = Color.white;
                        wrestlerSkill2.color = Color.white;
                    }
                    else
                    {
                        wrestlerBorder1.color = Color.white;
                        wrestlerBorder2.color = Color.white;
                        wrestlerWinner1.color = Color.white;
                        wrestlerWinner2.color = Color.white;
                        wrestlerSkill1.color = Color.white;
                        wrestlerSkill2.color = Color.white;
                    }
                    break;
                case 2:
                    segmentText.text = "Count out!";
                    if (segment.winnerIDs[0] == segment.team1IDs[0])
                    {
                        wrestlerBorder1.color = Color.yellow;
                        wrestlerBorder2.color = Color.white;
                        wrestlerWinner1.color = Color.yellow;
                        wrestlerWinner2.color = Color.white;
                        wrestlerSkill1.color = Color.yellow;
                        wrestlerSkill2.color = Color.white;
                    }
                    else
                    {
                        wrestlerBorder1.color = Color.white;
                        wrestlerBorder2.color = Color.yellow;
                        wrestlerWinner1.color = Color.white;
                        wrestlerWinner2.color = Color.yellow;
                        wrestlerSkill1.color = Color.white;
                        wrestlerSkill2.color = Color.yellow;
                    }
                    break;
                case 3:
                    segmentText.text = "Double Disqualification!";
                    if (segment.winnerIDs[0] == segment.team1IDs[0])
                    {
                        wrestlerBorder1.color = Color.white;
                        wrestlerBorder2.color = Color.white;
                        wrestlerWinner1.color = Color.white;
                        wrestlerWinner2.color = Color.white;
                        wrestlerSkill1.color = Color.white;
                        wrestlerSkill2.color = Color.white;
                    }
                    else
                    {
                        wrestlerBorder1.color = Color.white;
                        wrestlerBorder2.color = Color.white;
                        wrestlerWinner1.color = Color.white;
                        wrestlerWinner2.color = Color.white;
                        wrestlerSkill1.color = Color.white;
                        wrestlerSkill2.color = Color.white;
                    }
                    break;
                case 4:
                    segmentText.text = "Double Count out!";
                    if (segment.winnerIDs[0] == segment.team1IDs[0])
                    {
                        wrestlerBorder1.color = Color.white;
                        wrestlerBorder2.color = Color.white;
                        wrestlerWinner1.color = Color.white;
                        wrestlerWinner2.color = Color.white;
                        wrestlerSkill1.color = Color.white;
                        wrestlerSkill2.color = Color.white;
                    }
                    else
                    {
                        wrestlerBorder1.color = Color.white;
                        wrestlerBorder2.color = Color.white;
                        wrestlerWinner1.color = Color.white;
                        wrestlerWinner2.color = Color.white;
                        wrestlerSkill1.color = Color.white;
                        wrestlerSkill2.color = Color.white;
                    }
                    break;
                case 5:
                    segmentText.text = "Disqualification!";
                    if (segment.winnerIDs[0] == segment.team1IDs[0])
                    {
                        wrestlerBorder1.color = Color.yellow;
                        wrestlerBorder2.color = Color.white;
                        wrestlerWinner1.color = Color.yellow;
                        wrestlerWinner2.color = Color.white;
                        wrestlerSkill1.color = Color.yellow;
                        wrestlerSkill2.color = Color.white;
                    }
                    else
                    {
                        wrestlerBorder1.color = Color.white;
                        wrestlerBorder2.color = Color.yellow;
                        wrestlerWinner1.color = Color.white;
                        wrestlerWinner2.color = Color.yellow;
                        wrestlerSkill1.color = Color.white;
                        wrestlerSkill2.color = Color.yellow;
                    }
                    break;
				case 0:
					WrestlerCardObject w = (WrestlerCardObject)GamePlaySession.instance.CheckCard (segment.winnerIDs [Random.Range (0, segment.winnerIDs.Length - 1)]);
					segmentText.text = w.victoryLine;
                    //segmentText.text = segment.winners[Random.Range(0, segment.winners.Length - 1)].victoryLine;
                    if (segment.winnerIDs[0] == segment.team1IDs[0])
                    {
                        wrestlerBorder1.color = Color.yellow;
                        wrestlerBorder2.color = Color.white;
                        wrestlerWinner1.color = Color.yellow;
                        wrestlerWinner2.color = Color.white;
                        wrestlerSkill1.color = Color.yellow;
                        wrestlerSkill2.color = Color.white;
                    }
                    else
                    {
                        wrestlerBorder1.color = Color.white;
                        wrestlerBorder2.color = Color.yellow;
                        wrestlerWinner1.color = Color.white;
                        wrestlerWinner2.color = Color.yellow;
                        wrestlerSkill1.color = Color.white;
                        wrestlerSkill2.color = Color.yellow;
                    }
                    break;
            }
		}
	}
}
