﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SegmentDisplay : MonoBehaviour {

	public ShowSetup showSetup;
	public int segmentID;

	public Button setupButton;
	public Text setupButtonText;
	
	public Color titleSegmentNormalColor = Color.white;
	public Color titleSegmentHighlightColor = Color.white;
	public Color titleSegmentPressedColor = Color.white;

	public GameObject pendingState;
	public GameObject activeState;
	
	protected GameObject UIManager;

	public virtual void ShowSegment() {
		if (pendingState == null || activeState == null)
			return;

		SegmentObject segment = GamePlaySession.instance.myShows[showSetup.currentWeek.showID].segments[segmentID];

		if (segment != null) {
			if (segment.isPendingSetup) {
				if (segment.isFixed)
					segment.isPendingSetup = false;

				GamePlaySession.instance.myShows[showSetup.currentWeek.showID].segments[segmentID].ProcessCardIDs();
			}

			UpdateSegmentData();
		}

		if (segment.isPendingSetup) {
			pendingState.SetActive(true);
			activeState.SetActive(false);
		} else {
			pendingState.SetActive(false);
			activeState.SetActive(true);
		}

		Button button = activeState.GetComponent<Button>();
		if (button != null) {
			if (segment.isFixed) {
				button.interactable = false;
			} else {
				button.interactable = true;
			}
		}
	}

	public virtual void SetSegmentData(CardObject[] cards) {
		SegmentObject segment = GamePlaySession.instance.myShows[showSetup.currentWeek.showID].segments[segmentID];

		if (segment != null) {
			segment.cards = cards;
			segment.isPendingSetup = false;
		}

		UpdateSegmentData();
	}

	public virtual void EditSegmentSetup() {
		SegmentObject segment = GamePlaySession.instance.myShows[showSetup.currentWeek.showID].segments[segmentID];

		if (segment != null && !segment.isFixed)
			OpenSegmentSetup();
	}

	public virtual void OpenSegmentSetup()
    {
		GamePlaySession.instance.currentSegment = GamePlaySession.instance.myShows[showSetup.currentWeek.showID].segments[segmentID];
	}
	public virtual void UpdateSegmentData() {}
}
