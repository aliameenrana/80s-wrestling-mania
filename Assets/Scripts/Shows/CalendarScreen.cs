﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class CalendarScreen : MonoBehaviour {
	public WeekDisplay[] weekDisplays;

	public DatePanel datePanel;

	public Animator shopScreen;

	public Color completeShowColor;
	public Color nextShowColor;
	public Color nextShowColor90s;
	public Color lockedShowColor;

	public List<Button> nonTutorialButtons;

	public PopUpMessage initialPopUp;
	
	public int flavorTutorialWeek;
	public PopUpMessage flavorTutorialMessage;

	public int resultTutorialWeek;
	public PopUpMessage resultTutorialMessage;

	public int storeTutorialWeek;
	public PopUpMessage[] giftMessages;
	public PopUpMessage continueShopTutorialMessage;

	public GameObject UIManager;
	public GameObject playerHUD;

    
	bool screenOverride;
	private int lastCash;
	private int lastTokens;

	void Update() {
		if (GamePlaySession.instance == null)
			return;

		//CheckTutorialPopups();

		if (nonTutorialButtons != null && nonTutorialButtons.Count>0) 
		{
			if (GamePlaySession.instance.GetStorePurchaseTutorialClear() || GamePlaySession.instance.GetNewGamePlus()) 
			{
				foreach (var item in nonTutorialButtons) 
				{
					item.interactable = true;
				}
			} 
			else 
			{
				foreach (var item in nonTutorialButtons) 
				{
					item.interactable = false;
				}
			}
		}

		if (playerHUD != null && playerHUD.activeInHierarchy) {
			if (GamePlaySession.instance.GetMyCash() != lastCash || GamePlaySession.instance.GetMyTokens() != lastTokens) {
				UIManager.GetComponent<UIManagerScript>().UpdateHUD();
				lastCash = GamePlaySession.instance.GetMyCash();
				lastTokens = GamePlaySession.instance.GetMyTokens();
			}
		}
	}

	void CheckTutorialPopups(bool updateRatings) 
	{
		screenOverride = false;
		if (!GamePlaySession.instance.GetInTutorialMode())
			return;

		if (GamePlaySession.instance.myShows.Count >= flavorTutorialWeek && GamePlaySession.instance.myShows[flavorTutorialWeek - 1].showFinished) 
		{
            //if ((!GamePlaySession.instance.flavorTutorialClear && !GamePlaySession.instance.newGamePlus) || GamePlaySession.instance.CheckCard(199) == null) {
			if (!GamePlaySession.instance.GetFlavorTutorialClear() || GamePlaySession.instance.CheckCard(199) == null)
            {
                if (flavorTutorialMessage != null && !string.IsNullOrEmpty (flavorTutorialMessage.message)) 
				{
					int year = int.Parse(GamePlaySession.instance.myCalendar.years [GamePlaySession.instance.GetCurrentYear()].yearName);
					Debug.Log (year);
					if (year == 1990) 
					{
						flavorTutorialMessage.gift.cardIDs [2] = 600;
					}

					PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
					
					if (popUpWindow != null) 
					{
						if (updateRatings) 
						{
							popUpWindow.OpenPopUpWindow (new PopUpMessage[] { GetTVRatings (), flavorTutorialMessage },null);
						} 
						else
						{
							popUpWindow.OpenPopUpWindow (new PopUpMessage[] { flavorTutorialMessage}, null);
						}
					}
				}
				
				GamePlaySession.instance.SetFlavorTutorialClear(true);
				screenOverride = true;
				return;
			}
		}

		if (GamePlaySession.instance.myShows.Count >= resultTutorialWeek && GamePlaySession.instance.myShows[resultTutorialWeek - 1].showFinished) 
		{
            //if (!GamePlaySession.instance.resultTutorialClear && !GamePlaySession.instance.newGamePlus) {

			if (!GamePlaySession.instance.GetResultTutorialClear())
            {
                if (resultTutorialMessage != null && !string.IsNullOrEmpty(resultTutorialMessage.message)) 
				{
					PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
					
					if (popUpWindow != null && shopScreen != null) 
					{
						popUpWindow.OpenPopUpWindow (new PopUpMessage[] { resultTutorialMessage}, this.GetComponent<Animator>());
					}
				}

				GamePlaySession.instance.SetResultTutorialClear(true);
				screenOverride = true;
				return;
			}
		}

		if (GamePlaySession.instance.myShows.Count >= storeTutorialWeek && GamePlaySession.instance.myShows[storeTutorialWeek - 1].showFinished) {
			if (!GamePlaySession.instance.GetNewGamePlus()) 
            { 
				if (!GamePlaySession.instance.GetStorePurchaseTutorialClear()) 
				{
					if (playerHUD != null)
						playerHUD.SetActive(false);

					if (giftMessages.Length > 0) 
					{
						PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
						
						if (popUpWindow != null && shopScreen != null) 
						{
							if (updateRatings) 
							{
								PopUpMessage[] giftsAndRatings = new PopUpMessage[3];
								giftsAndRatings [0] = GetTVRatings ();
								giftsAndRatings [1] = giftMessages [0];
								giftsAndRatings [2] = giftMessages [1];
								popUpWindow.OpenPopUpWindow(giftsAndRatings, shopScreen);	
							} 
							else
							{
								popUpWindow.OpenPopUpWindow(giftMessages, shopScreen);	
							}
						}
					}

					GamePlaySession.instance.SetStorePurchaseTutorialClear(true);
					screenOverride = true;
					return;
				} 
				else if (GamePlaySession.instance.GetStorePurchaseTutorialClear() && !GamePlaySession.instance.GetStoreTutorialClear()) 
				{
					if (playerHUD != null)
						playerHUD.SetActive(false);

					PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
					
					if (popUpWindow != null && shopScreen != null) {
						popUpWindow.OpenPopUpWindow(new PopUpMessage[] {continueShopTutorialMessage}, shopScreen);
					}
				}
			}
		}
	}

	public void OpenCalendar(bool UpdateRatings)
    {
		
		if (playerHUD != null)
			playerHUD.SetActive(true);

        playerHUD.GetComponent<HUDScript>().SetCommishImage();
        playerHUD.GetComponent<HUDScript>().SetFedImage();
        playerHUD.GetComponent<HUDScript>().UpdateHUD();

		UpdateDatePanel();
		CheckWeekButtons();

		UIManager.GetComponent<PanelRatings> ().UpdateRatingsDisplay (UpdateRatings);

        if (ScreenHandler.current != null)
			ScreenHandler.current.OpenPanel(this.gameObject.GetComponent<Animator>());
		
		CheckTutorialPopups(UpdateRatings);
		if (screenOverride)
			return;

		if (UpdateRatings && !screenOverride) 
		{
			ShowTVRatings ();
		}
	}

	public void UpdateDatePanel()
    {
		if (datePanel == null)
			return;

		datePanel.UpdateDateDisplay();
	}
	
	public void CheckWeekButtons()
    {
		for(int i = 0;i < weekDisplays.Length;i++)
        {
			WeekItem currentWeek = GamePlaySession.instance.GetWeekInCurrentMonth(i);
			if (currentWeek != null)
            {
				weekDisplays[i].week = currentWeek;

                
				if (GamePlaySession.instance.GetNewGamePlus() && GamePlaySession.instance.GetIsNewPlayer() && !GamePlaySession.instance.GetInTutorialMode() && GamePlaySession.instance.GetCurrentMonth() == 0)
                {
                    if (currentWeek.showTypeID == 0)
                    {
                        currentWeek.showTypeID = 5;
                    }
                    else if (currentWeek.showTypeID == 1)
                    {
                        currentWeek.showTypeID = 8;
                    }
                    else if (currentWeek.showTypeID == 2)
                    {
                        currentWeek.showTypeID = 11;
                    }
                    else if (currentWeek.showTypeID == 3)
                    {
                        currentWeek.showTypeID = 14;
                    }
                }

                int thisShowID = currentWeek.showID;

                
                
                // Get the current week variable and images
                ShowObject thisWeek = null;
				if (GamePlaySession.instance.myShows.Count > thisShowID)
                {
					thisWeek = GamePlaySession.instance.myShows[thisShowID];
				}
                
				Image borderImage = weekDisplays[i].gameObject.GetComponent<Image>();
				Image iconImage = weekDisplays[i].showIcon;
                                            
				if (thisShowID == 0)
                {
					// We're the first show, so default to active and check if we've completed it yet
					weekDisplays[i].weekButton.interactable = true;

					if (thisWeek != null && thisWeek.showFinished)
                    {
						// Show is finished
						borderImage.color = completeShowColor;
						iconImage.color = completeShowColor;
					}
                    else
                    {
						// Show isn't yet finished
						borderImage.color = nextShowColor;
						iconImage.color = nextShowColor;

					}
				} else {
					// We're not the first show, so check last week's show for completion
					int lastShowID = thisShowID - 1;
                    
                    if (lastShowID >= GamePlaySession.instance.myShows.Count)
                    {
						// If there's no show yet for the previous week, lock this week
						weekDisplays[i].weekButton.interactable = false;

						borderImage.color = lockedShowColor;
						iconImage.color = lockedShowColor;
					}
                    else
                    {
						// Check if previous week's show is finished
						ShowObject lastWeek = GamePlaySession.instance.myShows[lastShowID];
						if (lastWeek.showFinished) {
							weekDisplays[i].weekButton.interactable = true;

							// Is there a show entry for this week?
							if (thisWeek != null && thisWeek.showFinished) {
								// Show is finished
								borderImage.color = completeShowColor;
								iconImage.color = completeShowColor;
							} else {
								// Show isn't yet finished
								borderImage.color = nextShowColor;
								iconImage.color = nextShowColor;

							}
						} else {
							// Last week's show isn't finished yet
							weekDisplays[i].weekButton.interactable = false;

							borderImage.color = lockedShowColor;
							iconImage.color = lockedShowColor;
						}
					}
				}

				// Update this week's display
				weekDisplays[i].UpdateShowDisplay();
			}
		}
	}


	void ShowTVRatings()
	{
		PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript> ().popUpWindow;
		if (GamePlaySession.instance.myCalendar.years [GamePlaySession.instance.GetCurrentYear()].yearName == "1990") 
		{
			if (GamePlaySession.instance.GetCurrentMonth() == 1 && !GamePlaySession.instance.GetFantasyPopUpShown()) 
			{
				
				WeekItem firstWeek = GamePlaySession.instance.GetWeekInCurrentMonth(0);
				PopUpMessage message90s = new PopUpMessage();
				message90s.message = "Welcome to the 90s! \n it's a whole new decade and Mr. Fantasy is taking over! \n if he wasn't already your commissioner, he is now! \n The first order of business in this extreme era is rebranding your fed. Check out your new fed logo!";
				message90s.FantasyPopup = true;
				GamePlaySession.instance.SetSelectedComissioner(2);

				UIManager.GetComponent<UIManagerScript>().UpdateTheme();
				if (popUpWindow != null)
				{
					popUpWindow.OpenPopUpWindow(new PopUpMessage[] { GetTVRatings(), message90s }, UIManager.GetComponent<UIManagerScript>().MainCalendarScreen, true);
					BonusValue bonus = new BonusValue ();
					bonus.bType = BonusValue.BonusType.PLUS_CASH;
					bonus.value = 500;
					GamePlaySession.instance.myBonuses.Add (bonus);
					GamePlaySession.instance.SetBonusValues (GamePlaySession.instance.myBonuses);

					UIManager.GetComponent<UIManagerScript> ().UpdateHUDDelayed ();

				}
				GamePlaySession.instance.SetFantasyPopUpShown(true);
				return;
			}
		}

		//PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
		if (popUpWindow != null)
		{
			popUpWindow.OpenPopUpWindow(new PopUpMessage[] { GetTVRatings() }, this.gameObject.GetComponent<Animator>(), true);
		}
	}
	PopUpMessage GetTVRatings()
	{
		PopUpMessage message = new PopUpMessage();
		string myPosition = "";

		int i = 1;
		foreach (var item in GamePlaySession.instance.RatingPositionHolder)
		{
			if (item == GamePlaySession.instance.GetSelectedFederation())
			{
				myPosition = (i).ToString();
				break;
			}
			i++;
		}

		int reward = (4-i) * 100;
		GamePlaySession.instance.SetMyCash(GamePlaySession.instance.GetMyCash() + reward);
		message.message = "Your Fed was #"+myPosition+ " in this week's TV Ratings!\n\n\n Reward: "+"<color=#00ff00>$"+reward.ToString()+"</color>";
		return message;
	}
}
