using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SegmentSetup : MonoBehaviour {
	
	public int numberOfOptions;
	
	public GameObject[] optionButtons;
	public Graphic[] logoImages;
	
	public Button bookItButton;
	
	public CardSelector[] selectors;

	public ScrollRect optionScroll;
	
	public CardSelectScreenScript cardViewer;

    bool tutFeudPopUpShown, tutTagPopUpShown;

	[HideInInspector]
	public SegmentDisplay segmentDisplay;
	[HideInInspector]
	public CardObject[] cards;
	
	void Awake() {
		if (cards == null)
			cards = new CardObject[numberOfOptions];
	}
	void OnEnable()
	{
		if (UIManagerScript.instance.Theme90s) 
		{
			optionScroll.GetComponent<ScrollRect> ().verticalScrollbar.gameObject.transform.GetChild (0).GetChild (0).GetComponent<Image> ().color = Color.red;		
		}
	}
	
	void Update() 
	{
		// Check if the start show button should be on or off
		if (bookItButton != null) {
			if (CheckReady()) {
				bookItButton.interactable = true;
			} else {
				bookItButton.interactable = false;
			}
		}
		CheckButtons();
	}

	public virtual bool CheckReady() 
	{
		return true;
	}

	public void CheckUnlocks()
    {
		NewCardsObject newCards = CheckTagFeudPair();
		
		if (newCards != null)
        {
			//Debug.Log("We have cards to give!");
			// We have new cards to unlock, so let's handle it
			if (cardViewer != null)
            {
				//Debug.Log("And a card viewer!");
				cardViewer.OpenCardView(newCards, this.gameObject.GetComponent<Animator>());
			}
		}
	}

	public virtual void CheckCardCompatibility(int index) {}
	
	void CheckButtons() {
		if (GamePlaySession.instance == null)
			return;

		List<FlavorCardObject> flavors = new List<FlavorCardObject>();

		if (segmentDisplay != null) {
			SegmentObject segment = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID].segments[segmentDisplay.segmentID];
			GameSetupItems setupItems = GameObject.Find("UIManager").GetComponent<GameSetupItems>();
			
			if (segment.availableCardIDs.Length > 0 && !GamePlaySession.instance.GetNewGamePlus()) {
				foreach (int cardNum in segment.availableCardIDs) {
					if (setupItems.flavors.cards.ContainsKey(cardNum)) {
						if (GamePlaySession.instance.CheckCard(cardNum) != null && CheckCardPlayable(GamePlaySession.instance.CheckCard(cardNum))) {
							flavors.Add((FlavorCardObject) GamePlaySession.instance.CheckCard(cardNum));
						}
					}
				}
			} else {
				foreach (CardObject card in setupItems.flavors.cardArray) {
					if (GamePlaySession.instance.CheckCard(card.CardNumber) != null && CheckCardPlayable(GamePlaySession.instance.CheckCard(card.CardNumber))) {
						flavors.Add((FlavorCardObject) GamePlaySession.instance.CheckCard(card.CardNumber));
					}
				}
			}
		}

		SetFlavorButtons(flavors);
		SetWrestlerButtons();
	}
	
	public virtual void SetFlavorButtons(List<FlavorCardObject> flavors) {}
	public virtual void SetWrestlerButtons() {}
	public virtual void ResetWrestlerButtons() {}
	
	public bool CheckCardPlayable(CardObject card, CardSelector selector = null) {		
		SegmentObject segment = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID].segments[segmentDisplay.segmentID];
		
		bool isPlayable = true;

		if (segment != null)
        {
			// Check if the card is in the list of available cards if one is provided
			if (segment.availableCardIDs.Length > 0)
            {
				bool cardAvailable = false;
				foreach (int checkID in segment.availableCardIDs)
                {
					if (card.CardNumber == checkID)
                    {
						cardAvailable = true;
					}
				}

				if (!cardAvailable)
                {
					isPlayable = false;
				}
			}

			// Check if the card is playable based on selector
			if (selector != null)
            {
				if (selector.hideTypes.Contains(card.myCardType))
                {
					isPlayable = false;
				}
			}

			// Check if the card/character has already been played
			// Skip this check if we're dealing with a match card
			if (card.myCardType != CardType.MATCH)
            {
				// Iterate through all the segments in the current show
				ShowObject show = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID];

				//foreach (SegmentObject checkSegment in show.segments) {
				for (int r = 0; r < show.segments.Length; r++) {
					SegmentObject checkSegment = show.segments[r];
					if (card.myCardType == CardType.WRESTLER) {
						// Check character number of segment cards
						WrestlerCardObject wrestler = (WrestlerCardObject) card;

						for (int i = 0; i < checkSegment.cards.Length; i++) {
							CardObject checkCard = checkSegment.cards[i];
							if (checkCard != null && checkCard.myCardType == CardType.WRESTLER) {
								WrestlerCardObject checkWrestler = (WrestlerCardObject) checkCard;
								if (wrestler.characterNumber == checkWrestler.characterNumber && wrestler.characterNumber!=0) {
									if (selector != null) {
										if (selector.optionIndex != i || segmentDisplay.segmentID != r)
											isPlayable = false;
									} else {
										isPlayable = false;
									}
								}
							}
						}

						// Also check if there's a title match in the show that the character is involved with
						if (checkSegment.isTitleMatch) {
							if (checkSegment.titleMatchType == SegmentObject.TitleMatchType.WORLD) {
								// World champion match
								if (GamePlaySession.instance.worldChampion.characterNumber == wrestler.characterNumber)
									isPlayable = false;
							} else if (checkSegment.titleMatchType == SegmentObject.TitleMatchType.CONTINENTAL) {
								// Continental champion match
								if (GamePlaySession.instance.titleChampion.characterNumber == wrestler.characterNumber)
									isPlayable = false;
							} else if (checkSegment.titleMatchType == SegmentObject.TitleMatchType.TAG) {
								// Tag title match
								if (GamePlaySession.instance.tagChampions[0].characterNumber == wrestler.characterNumber || GamePlaySession.instance.tagChampions[1].characterNumber == wrestler.characterNumber)
									isPlayable = false;
							}
						}
					} else {
                        // Check card number of segment cards
                        if (checkSegment.cards != null)
                        {
                            for (int i = 0; i < checkSegment.cards.Length; i++)
                            {
                                CardObject checkCard = checkSegment.cards[i];
                                if (checkCard != null && card.CardNumber == checkCard.CardNumber)
                                {
                                    if (selector != null)
                                    {
                                        if (selector.optionIndex != i || segmentDisplay.segmentID != r)
                                            isPlayable = false;
                                    }
                                    else
                                    {
                                        isPlayable = false;
                                    }
                                }
                            }
                        }
						

						// Check if the card is a tag team or feud
						if (card.myCardType == CardType.FEUD || card.myCardType == CardType.TAG_TEAM) {
							if (!CheckTagFeudPlayable((FlavorCardObject) card, selector))
								isPlayable = false;
						}

						// Check if card is a manager
						if (card.myCardType == CardType.MANAGER) {
							if (!CheckManagerPlayable((FlavorCardObject) card, selector))
								isPlayable = false;
						}
					}
				}
			}
		}
		
		return isPlayable;
	}

	public bool CheckManagerPlayable(FlavorCardObject card, CardSelector selector = null) {
		bool isPlayable = false;
		
		SegmentObject segment = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID].segments[segmentDisplay.segmentID];
		
		List<WrestlerCardObject> team1 = new List<WrestlerCardObject>();
		List<WrestlerCardObject> team2 = new List<WrestlerCardObject>();
        if (segment.cards != null)
        {
            if (segment.segmentType == SegmentObject.SegmentType.MATCH)
            {
                // Match
                if (segment.cards[2] != null)
                    team1.Add((WrestlerCardObject)segment.cards[2]);
                if (segment.cards[4] != null)
                    team2.Add((WrestlerCardObject)segment.cards[4]);
            }
            else if (segment.segmentType == SegmentObject.SegmentType.TAG_MATCH)
            {
                // Tag Match
                if (segment.cards[2] != null)
                    team1.Add((WrestlerCardObject)segment.cards[2]);
                if (segment.cards[3] != null)
                    team1.Add((WrestlerCardObject)segment.cards[3]);
                if (segment.cards[5] != null)
                    team2.Add((WrestlerCardObject)segment.cards[5]);
                if (segment.cards[6] != null)
                    team2.Add((WrestlerCardObject)segment.cards[6]);
            }
            else
            {
                // Mic Spot or Skit
                if (segment.cards[1] != null)
                    team1.Add((WrestlerCardObject)segment.cards[1]);
                if (segment.cards[2] != null)
                    team1.Add((WrestlerCardObject)segment.cards[2]);
                if (segment.cards[3] != null)
                    team1.Add((WrestlerCardObject)segment.cards[3]);
                if (segment.cards[4] != null)
                    team1.Add((WrestlerCardObject)segment.cards[4]);
                if (segment.cards[5] != null)
                    team1.Add((WrestlerCardObject)segment.cards[5]);
            }
        }

		// Check manager alignments
		if (selector == null) 
		{
			try
			{
				if (team1.Exists(x => x.cardAlignment.ToUpper() == card.characterAlignment.ToUpper()) || team2.Exists(x => x.cardAlignment.ToUpper() == card.characterAlignment.ToUpper())) 
				{
					// Contains a match
					isPlayable = true;
				}
			}
			catch 
			{
				isPlayable = true;
			}
		} 
		else 
		{
			// Check selector name to determine which team to check
			if (selector.name.Contains("Team1")) 
			{
				if (team1.Exists(x => x.cardAlignment.ToUpper() == card.characterAlignment.ToUpper())) {
					// Team 1 contains a match
					isPlayable = true;
				}
			} 
			else if (selector.name.Contains("Team2")) 
			{
				if (team2.Exists(x => x.cardAlignment.ToUpper() == card.characterAlignment.ToUpper())) {
					// Team 2 contains a match
					isPlayable = true;
				}
			} 
			else 
			{
				// Might be a skit/mic spot flavor button
				if (team1.Exists(x => x.cardAlignment.ToUpper() == card.characterAlignment.ToUpper()) || team2.Exists(x => x.cardAlignment.ToUpper() == card.characterAlignment.ToUpper())) 
				{
					// Contains a match
					isPlayable = true;
				}
			}
		}

		return isPlayable;
	}

	public bool CheckTagFeudPlayable(FlavorCardObject card, CardSelector selector = null) {
		bool isPlayable = false;

		SegmentObject segment = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID].segments[segmentDisplay.segmentID];
		
		List<WrestlerCardObject> team1 = new List<WrestlerCardObject>();
		List<WrestlerCardObject> team2 = new List<WrestlerCardObject>();
        if (segment.cards != null)
        {
            if (segment.segmentType == SegmentObject.SegmentType.MATCH)
            {
                if (segment.cards[2] != null)
                    team1.Add((WrestlerCardObject)segment.cards[2]);
                if (segment.cards[4] != null)
                    team2.Add((WrestlerCardObject)segment.cards[4]);
            }
            else if (segment.segmentType == SegmentObject.SegmentType.TAG_MATCH)
            {
                if (segment.cards[2] != null)
                    team1.Add((WrestlerCardObject)segment.cards[2]);
                if (segment.cards[3] != null)
                    team1.Add((WrestlerCardObject)segment.cards[3]);
                if (segment.cards[5] != null)
                    team2.Add((WrestlerCardObject)segment.cards[5]);
                if (segment.cards[6] != null)
                    team2.Add((WrestlerCardObject)segment.cards[6]);
            }
        }
		if (card.myCardType == CardType.FEUD) {
			if (team1.Exists(x => x.CardNumber == card.unlockRequirements.CardA) && team2.Exists(x => x.CardNumber == card.unlockRequirements.CardB)) {
				// Contains a match
				isPlayable = true;
			} else if (team2.Exists(x => x.CardNumber == card.unlockRequirements.CardA) && team1.Exists(x => x.CardNumber == card.unlockRequirements.CardB)) {
				// Contains a match
				isPlayable = true;
			}
		} else if (card.myCardType == CardType.TAG_TEAM) {
			if (selector == null) {
				// Check either team if no selector specified
				if (team1.Exists(x => x.CardNumber == card.unlockRequirements.CardA) && team1.Exists(x => x.CardNumber == card.unlockRequirements.CardB)) {
					// Team 1 contains a match
					isPlayable = true;
				} else if (team2.Exists(x => x.CardNumber == card.unlockRequirements.CardA) && team2.Exists(x => x.CardNumber == card.unlockRequirements.CardB)) {
					// Team 2 contains a match
					isPlayable = true;
				}
			} else {
				// Check selector name to determine which team to check
				if (selector.name.Contains("Team1")) {
					if (team1.Exists(x => x.CardNumber == card.unlockRequirements.CardA) && team1.Exists(x => x.CardNumber == card.unlockRequirements.CardB)) {
						// Team 1 contains a match
						isPlayable = true;
					}
				} else if (selector.name.Contains("Team2")) {
					if (team2.Exists(x => x.CardNumber == card.unlockRequirements.CardA) && team2.Exists(x => x.CardNumber == card.unlockRequirements.CardB)) {
						// Team 2 contains a match
						isPlayable = true;
					}
				}
			}
		}

		return isPlayable;
	}

	public bool CheckTagTeam(int teamNum, FlavorCardObject flavor) {
		bool isPlayable = false;

		SegmentObject segment = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID].segments[segmentDisplay.segmentID];
		
		List<WrestlerCardObject> team = new List<WrestlerCardObject>();

		if (teamNum == 1) {
			if (segment.cards[2] != null)
				team.Add((WrestlerCardObject) segment.cards[2]);
			if (segment.cards[3] != null)
				team.Add((WrestlerCardObject) segment.cards[3]);
		} else {
			if (segment.cards[5] != null)
				team.Add((WrestlerCardObject) segment.cards[5]);
			if (segment.cards[6] != null)
				team.Add((WrestlerCardObject) segment.cards[6]);
		}

		// Check team
		if (team.Exists(x => x.CardNumber == flavor.unlockRequirements.CardA) && team.Exists(x => x.CardNumber == flavor.unlockRequirements.CardB)) {
			// Team 1 contains a match
			isPlayable = true;
		}

		return isPlayable;
	}
	
	public void BookSegment()
    {
		segmentDisplay.SetSegmentData(cards);
        if (DuplicateSegment())
        {
            Debug.Log("Duplicate");
            GamePlaySession.instance.currentSegment.Duplicate = true;
            segmentDisplay.showSetup.ReturnToPanel(true);
        }
        else
        {
            segmentDisplay.showSetup.ReturnToPanel();
        }
		ReturnToShowNoReset();
	}

    public bool DuplicateSegment()
    {
        
        int w = segmentDisplay.showSetup.currentWeek.showID - 1;
        if (w >= 0)
        {
			if (GamePlaySession.instance.myShows [w].segments != null) 
			{
				if (GamePlaySession.instance.myShows[w].segments.Length > 0)
				{
					foreach (SegmentObject item in GamePlaySession.instance.myShows[w].segments)
					{
						if (item.Equals(GamePlaySession.instance.currentSegment))
						{
							return true;
						}
						else
						{
							return false;
						}
					}
				}
			}
            return false;
        }
        else
        {
            return false;
        }
    }

	
	public NewCardsObject CheckTagFeudPair()
    {
		NewCardsObject newCards = null;

		GameSetupItems items = GameObject.Find("UIManager").GetComponent<GameSetupItems>();

		if (items != null)
        {
			// Check for tutorial weeks
			if (GamePlaySession.instance.GetCurrentYear() == 0 && GamePlaySession.instance.GetCurrentMonth() == 0 && GamePlaySession.instance.GetInTutorialMode())
            {
				int unlockWeek = segmentDisplay.showSetup.teamFeudTutorialWeek;
				if (unlockWeek < 4)
					unlockWeek -= 1;

				if (segmentDisplay.showSetup.currentWeek.weekNum == unlockWeek)
                {
					// It's week 3 of the tutorial!

					// Now let's check that we're ready to book, thus the player has selected the cards
					if (CheckReady())
                    {
						if (segmentDisplay.segmentID == 3)
                        {
                            // Verify we don't already have this card unlocked
                            if (!tutTagPopUpShown)
                            {
                                //Tag Team tutorial
                                newCards = new NewCardsObject();
                                newCards.cards.Add(items.flavors.cards[205].Clone());

                                newCards.message = segmentDisplay.showSetup.tagTeamTutorialMessage;
                                tutTagPopUpShown = true;
                            }
                        }
                        else if (segmentDisplay.segmentID == 4)
                        {
                            // Verify we don't already have this card unlocked
                            if (!tutFeudPopUpShown)
                            {
                                // Feud tutorial
                                newCards = new NewCardsObject();
								newCards.cards.Add(items.flavors.cards[219].Clone());

								newCards.message = segmentDisplay.showSetup.feudTutorialMessage;
                                tutFeudPopUpShown = true;
							}
						}
					}
				}
			}

			// If we didn't have a tutorial hand out, let's check for a normal unlock
			if (newCards == null) {
				// Check for segment data
				SegmentObject segment = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID].segments[segmentDisplay.segmentID];
				
				if (segment != null) {
					if (segment.segmentType == SegmentObject.SegmentType.MATCH || segment.segmentType == SegmentObject.SegmentType.TAG_MATCH) {
						List<WrestlerCardObject> team1 = new List<WrestlerCardObject>();
						List<WrestlerCardObject> team2 = new List<WrestlerCardObject>();
						
						if (segment.segmentType == SegmentObject.SegmentType.MATCH) {
							if (segment.cards[2] != null)
								team1.Add((WrestlerCardObject) segment.cards[2]);
							if (segment.cards[4] != null)
								team2.Add((WrestlerCardObject) segment.cards[4]);
						} else if (segment.segmentType == SegmentObject.SegmentType.TAG_MATCH) {
							if (segment.cards[2] != null)
								team1.Add((WrestlerCardObject) segment.cards[2]);
							if (segment.cards[3] != null)
								team1.Add((WrestlerCardObject) segment.cards[3]);
							if (segment.cards[5] != null)
								team2.Add((WrestlerCardObject) segment.cards[5]);
							if (segment.cards[6] != null)
								team2.Add((WrestlerCardObject) segment.cards[6]);
						}
						
						bool unlockedTeam = false;
						bool unlockedFeud = false;
						
						List<CardObject> cardsUnlocked = new List<CardObject>();
						
						foreach (FlavorCardObject card in items.flavors.cardArray) {
							bool hasMatch = false;

							// Verify we don't already own this card
							if (GamePlaySession.instance.CheckCard(card.CardNumber) == null) {
								if (card.cardClass == "Tag Team") {
									if (team1.Exists(x => x.CardNumber == card.unlockRequirements.CardA) && team1.Exists(x => x.CardNumber == card.unlockRequirements.CardB)) {
										// Team 1 contains a match
										hasMatch = true;
									} else if (team2.Exists(x => x.CardNumber == card.unlockRequirements.CardA) && team2.Exists(x => x.CardNumber == card.unlockRequirements.CardB)) {
										// Team 2 contains a match
										hasMatch = true;
									}
								} else if (card.cardClass == "Feud") {
									if (team1.Exists(x => x.CardNumber == card.unlockRequirements.CardA) && team2.Exists(x => x.CardNumber == card.unlockRequirements.CardB)) {
										// Contains a match
										hasMatch = true;
									} else if (team2.Exists(x => x.CardNumber == card.unlockRequirements.CardA) && team1.Exists(x => x.CardNumber == card.unlockRequirements.CardB)) {
										// Contains a match
										hasMatch = true;
									}
								}
							}

							if (hasMatch) {
								if (GamePlaySession.instance.CheckCard(card.CardNumber) == null) {
									cardsUnlocked.Add(card.Clone());
									
									if (card.cardClass == "Feud") {
										// We unlocked a feud so note it
										unlockedFeud = true;
									} else if (card.cardClass == "Tag Team") {
										// We unlocked a tag team so note it
										unlockedTeam = true;
									}
								}
							}
						}
						
						// Now we create a new card object if we have cards to give the player
						if (cardsUnlocked.Count > 0) {
							newCards = new NewCardsObject();
							newCards.cards = cardsUnlocked;
							
							if (unlockedFeud && unlockedTeam) {
								newCards.message = segmentDisplay.showSetup.teamFeudUnlockMessage;
							} else if (unlockedFeud) {
								newCards.message = segmentDisplay.showSetup.feudUnlockMessage;
							} else if (unlockedTeam) {
								newCards.message = segmentDisplay.showSetup.teamUnlockMessage;
							}
						}
					}
				}
			}
		}
		
		return newCards;
	}

	public void ReturnToShow() {
		UIManagerScript UIManager = GameObject.Find("UIManager").GetComponent<UIManagerScript>();
		Animator showSetupScreen = UIManager.ShowSetupScreen;

		//ResetSegment();

		optionScroll.verticalNormalizedPosition = 1f;
		
		if (ScreenHandler.current != null && showSetupScreen != null)
			ScreenHandler.current.OpenPanel(showSetupScreen);
	}
	
	public void ReturnToShowNoReset() {
		UIManagerScript UIManager = GameObject.Find("UIManager").GetComponent<UIManagerScript>();
		Animator showSetupScreen = UIManager.ShowSetupScreen;
		
		optionScroll.verticalNormalizedPosition = 1f;
		
		if (ScreenHandler.current != null && showSetupScreen != null)
			ScreenHandler.current.OpenPanel(showSetupScreen);
	}

	public void ResetSegment() {
		UIManagerScript UIManager = GameObject.Find("UIManager").GetComponent<UIManagerScript>();
		SegmentObject segment = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID].segments[segmentDisplay.segmentID];
		GameSetupItems items = UIManager.gameObject.GetComponent<GameSetupItems>();
		
		if (items != null) {
			SegmentObject segmentTemplate = items.ShowTypes[segmentDisplay.showSetup.currentWeek.showTypeID].Clone().segments[segmentDisplay.segmentID];
			
			segmentTemplate.ProcessCardIDs();
			segment.cards = segmentTemplate.cards;
			segment.isPendingSetup = true;
		}
		
		segmentDisplay.UpdateSegmentData();
		segmentDisplay.showSetup.UpdateShowOutput();
	}
	
	public virtual void UpdateOption(int index, CardObject card)
    {
		if (index < numberOfOptions)
			cards[index] = card;
        //Debug.Log(card.CardNumber);
		SetData(segmentDisplay);
		CheckCardCompatibility(index);
		CheckUnlocks();
	}


	public virtual void UpdateDisplay() {
		if (cards == null){
			return;
		}

		for (int i = 0;i < numberOfOptions;i++) {
			if (cards[i] != null) {
				// Clone the card - this avoids the wrestling match logo bug on continue
				CardObject card = cards[i].Clone();

				if (optionButtons[i] != null) {
					optionButtons[i].SetActive(false);
				}
				if (logoImages[i] != null)
                {
					//logoImages[i].sprite = cards[i].logoImage;
					//if logoImage doesnt exist just put the card name here in Text
					GameObject logoImageObject = logoImages[i].gameObject;
					if(card.logoImage == null)
                    {

						if(logoImages[i].gameObject.GetComponent<Image>() != null)
                        {
							DestroyImmediate(logoImages[i].gameObject.GetComponent<Image>());
							logoImageObject.AddComponent<Text>();
						}

						logoImageObject.GetComponent<Text>().font = GameObject.Find("UIManager").GetComponent<UIHelper>().gameFont;
						logoImageObject.GetComponent<Text>().fontSize = 60;
						logoImageObject.GetComponent<Text>().resizeTextForBestFit = true;
						logoImageObject.GetComponent<Text>().text = card.name.Trim();
						logoImageObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
						logoImages[i] = logoImageObject.GetComponent<Text>();

						if (card.myCardType == CardType.SPONSOR) {
							logoImages[i].color = Color.green;
						} else {
							logoImages[i].color = Color.white;
						}
					}
					else{
						if(logoImages[i].gameObject.GetComponent<Text>() != null){
							DestroyImmediate(logoImages[i].gameObject.GetComponent<Text>());
							logoImageObject.gameObject.AddComponent<Image>();
						}

						logoImageObject.GetComponent<Image>().sprite = card.logoImage;
						logoImages[i] = logoImageObject.GetComponent<Image>();
					}
					logoImages[i].gameObject.SetActive(true);

					// Check if the edit button should be interactable
					Button button = logoImages[i].gameObject.GetComponent<Button>();
					if (button != null) {
						UIManagerScript UIManager = GameObject.Find("UIManager").GetComponent<UIManagerScript>();
						GameSetupItems items = UIManager.gameObject.GetComponent<GameSetupItems>();
						SegmentObject segmentTemplate = items.ShowTypes[segmentDisplay.showSetup.currentWeek.showTypeID].Clone().segments[segmentDisplay.segmentID];

						bool canEdit = true;

						if (segmentTemplate.cardIDs.Length > i) {
							if (segmentTemplate.cardIDs[i] != null && segmentTemplate.cardIDs[i] > 0)
								canEdit = false;
						}

						if (segmentTemplate.isTitleMatch) {
							if (segmentTemplate.segmentType == SegmentObject.SegmentType.MATCH) {
								if (i == 2)
									canEdit = false;
							} else if (segmentTemplate.segmentType == SegmentObject.SegmentType.TAG_MATCH) {
								if (i == 2 || i == 3)
									canEdit = false;
							}
						}

						if (canEdit) {
							button.interactable = true;
						} else {
							button.interactable = false;
						}
					}
				}
			} else {
				if (optionButtons[i] != null) {
					optionButtons[i].SetActive(true);
				}
				if (logoImages[i] != null) {
					logoImages[i].gameObject.SetActive(false);
				}
			}
		}
	}
	
	public virtual void SetData(SegmentDisplay display) {
		segmentDisplay = display;

		SegmentObject segment = GamePlaySession.instance.myShows[display.showSetup.currentWeek.showID].segments[display.segmentID];

		ResetWrestlerButtons();
		SetTitleCards(segment);

		if (segment.cards != null)
        {
			cards = segment.cards;
		}
        else
        {
			cards = new CardObject[numberOfOptions];
		}


		for (int i = 0;i < optionButtons.Length;i++)
        {
			// Get the button object and selector
			GameObject buttonObject = optionButtons[i];
			optionButtons[i].SetActive(true);
			Button button = buttonObject.GetComponent<Button>();
			if (button == null)
            {
				button = buttonObject.GetComponentInChildren<Button>();
			}

			CardSelector selector = selectors[i];

			if (selector != null)
            {
				// Set up the selector
				selector.cards.Clear();
				foreach (CardObject card in GamePlaySession.instance.myCards)
                {
					if (CheckCardPlayable(card, selector))
                    {
						selector.cards.Add(card);
					}
				}

				// Check if this button should be locked
				bool lockButton = false;

				// Is it a locked option in the current segment?
				foreach (int lockID in segment.lockedOptions) {
					if (lockID == i)
						lockButton = true;
				}

				// Do we have no cards to use for this option?
				if (selector.cards.Count == 0) {
					lockButton = true;
				}

                
				// Is this a single wrestler skit or mic spot?
				if (cards[0] != null) {                    
					if (segment.segmentType == SegmentObject.SegmentType.SKIT && i == 2) {
						SkitCardObject skit = (SkitCardObject) cards[0];

						if (skit.wrestlerMax == 1) {
							lockButton = true;
						}
					} else if (segment.segmentType == SegmentObject.SegmentType.MIC_SPOT && i == 2) {
						MicSpotCardObject micSpot = (MicSpotCardObject) cards[0];
						
						if (micSpot.wrestlerMax == 1) {
							lockButton = true;
						}
					}
				}
				
				if (button != null) {
					if (lockButton) {
						button.interactable = false;
					} else {
						button.interactable = true;
					}
				}
			}
		}
		
		UpdateDisplay();

        //if (segment.popupMessage != null && !string.IsNullOrEmpty(segment.popupMessage.message) && !segment.popUpClear && !GamePlaySession.instance.newGamePlus) {
		if (segment.popupMessage != null && !string.IsNullOrEmpty(segment.popupMessage.message) && !segment.popUpClear && GamePlaySession.instance.GetInTutorialMode())
        {
            PopUpWindow popUpWindow = GameObject.Find("UIManager").GetComponent<UIManagerScript>().popUpWindow;

			if (popUpWindow != null) {
				popUpWindow.OpenPopUpWindow(new PopUpMessage[] {segment.popupMessage}, this.GetComponent<Animator>());
			}

			segment.popUpClear = true;
		}
	}

	public virtual void SetTitleCards(SegmentObject segment) {}
}
