using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ResultsTypeDisplay 
{
	public SegmentObject.SegmentType resultType;
	public ResultDisplay resultDisplay;
}

public class ShowResults : MonoBehaviour 
{
	public ResultsTypeDisplay[] resultsTypeDisplays;

	public WeekItem weekData;
	public ShowObject showData;

	public Text showTitle;
	public Text venueName;
	public Text attendanceResult;
	public Text earningsValue;
	public Image ratingsValue;
	
	public CardSelectScreenScript cardViewer;
	
	public GameObject resultAnchor;
	public ScrollRect resultScroll;

	public PopUpMessage firstShowMessage;
	public PopUpMessage merchUnlockMessage;

	public Dictionary<int, ResultDisplay> results = new Dictionary<int, ResultDisplay>();

	[HideInInspector]
	public bool oldResult;
	
	private Hashtable resultTypeHash;
	
	private GameObject UIManager;
	private GameObject playerHUD;

    void Awake()
    {
        UIManager = GameObject.Find("UIManager");
    }

    void GenerateHashtable() {
		// Generate a hashtable of card back prefabs if one isn't present
		if (resultTypeHash != null)
			return;
		
		resultTypeHash = new Hashtable();
		foreach (ResultsTypeDisplay display in resultsTypeDisplays) {
			resultTypeHash.Add(display.resultType, display.resultDisplay);
		}
	}

	public void SetResultsData(WeekItem week) {
		weekData = week;
		showData = GamePlaySession.instance.myShows[weekData.showID];

		WipeResultsOutput();
		UpdateResultsOutput();
	}

	public void WipeResultsOutput() {
		foreach (KeyValuePair<int, ResultDisplay> display in results) {
			Object.Destroy(display.Value.gameObject);
		}
		results.Clear();
	}

	public void UpdateResultsOutput() 
	{
		if (showData == null)
			return;

		//GamePlaySession.instance.SaveAll();
        showTitle.text = showData.displayTitle;
		venueName.text = GamePlaySession.instance.CheckCard(showData.venueCard).name;
		attendanceResult.text = showData.showAttendance.attandanceString;

		float modifier = showData.showAttendance.attendanceModifier;
		if (modifier >= 1f) {
			attendanceResult.color = Color.magenta;
		} else if (modifier >= .75f) {
			attendanceResult.color = Color.green;
		} else if (modifier >= .5f) {
			attendanceResult.color = Color.yellow;
		} else if (modifier >= .25f) {
			attendanceResult.color = Color.red;
		} else {
			attendanceResult.color = Color.white;
		}

		earningsValue.text = "$" + showData.showEarnings.ToString();
		if (showData.showEarnings >= 0) {
			earningsValue.color = Color.green;
		} else {
			earningsValue.color = Color.red;
		}

		ratingsValue.fillAmount = showData.GetStarRating();
		
		// Generate our hashtable of segment displays
		GenerateHashtable();
		
		// Process segment display objects
		for (int i = 0;i < showData.segments.Length;i++) {
			SegmentObject segment = showData.segments[i];
			
			if (resultTypeHash.ContainsKey(segment.segmentType)) {
				if (results.ContainsKey(i)) {
					Object.Destroy(results[i].gameObject);
					results.Remove(i);
				}
				
				ResultDisplay display = ResultDisplay.Instantiate(resultTypeHash[segment.segmentType] as ResultDisplay, Vector3.zero, new Quaternion()) as ResultDisplay;
				
				// Set the parent to the segment anchor and show display
				display.transform.SetParent(resultAnchor.transform, false);
				display.show = showData;
				display.segment = segment;
				display.UpdateResultData();
				
				results.Add(i, display);
			}
		}
	}

    public void CloseBack()
    {
        if (FinishResults())
        {
            return;
        }

		bool updateRatings;
		if (oldResult) 
		{
			updateRatings = false;
		} 
		else 
		{
			updateRatings = true;
			SaveShowProgressToServer ();
		}

        //UIManager = GameObject.Find("UIManager");
		UIManager.GetComponent<UIManagerScript>().GoToCalendar(updateRatings);
	}

	public void SaveShowProgressToServer()
	{
		int count = Random.Range(1, 100000);
//		foreach (var item in GamePlaySession.instance.myShows) 
//		{
//			if (item.showFinished)
//				count++;	
//		}

		Dictionary<string,object> baseData = showData.GetShowBaseData ();
		baseData.Add ("uniqueID", count);
		List<Dictionary<string,object>> segmentDatas = new List<Dictionary<string, object>> ();
		foreach (var item in showData.segments) 
		{
			Dictionary<string,object> segmentData = item.GetSegmentData ();
			segmentData.Add ("uniqueID", count);
			segmentDatas.Add (segmentData);
		}

		ParseManager.CreateShowData (baseData,null, segmentDatas);
	}


	public void CloseHome()
    {
		if (FinishResults())
			return;

		if(!oldResult)
		{
			SaveShowProgressToServer ();
		}
		//UIManager = GameObject.Find("UIManager");
		UIManager.GetComponent<UIManagerScript>().HomeButton();
	}

	public bool FinishResults()
    {
		bool screenOverride = false;
		//UIManager = GameObject.Find("UIManager");
		GameSetupItems items = UIManager.GetComponent<GameSetupItems>();
		playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;

		//ALI AMEEN - Testing
		//screenOverride = CheckYearUnlocks();

		// If this is the last week of a month, advance the calendar
		if (weekData.weekNum == 4 && !oldResult)
        {
			int lastYear = GamePlaySession.instance.GetCurrentYear();
			GamePlaySession.instance.AdvanceMonth();
			
			if (GamePlaySession.instance.GetGameCompleted())
            {
				screenOverride = true;
				
				//UIManager = GameObject.Find("UIManager");
				UIManagerScript UIManagerScript = UIManager.GetComponent<UIManagerScript>();

				UIManagerScript.endGameScreen.OpenScreen();
			}
            else
            {
				if (lastYear < GamePlaySession.instance.GetCurrentYear())
                {
                    // Check for year end unlocks
                    screenOverride = CheckYearUnlocks();
                }
			}
		}

		if (!screenOverride)
        {
			screenOverride = CheckMerchUnlocks ();
		
			if (playerHUD != null)
				playerHUD.GetComponent<HUDScript> ().UpdateHUD ();
		}

		resultScroll.verticalNormalizedPosition = 1f;

		return screenOverride;
	}


	public bool CheckYearUnlocks()
    {
		bool screenOverride = false;
		
		//UIManager = GameObject.Find("UIManager");
		GameSetupItems items = UIManager.GetComponent<GameSetupItems>();
		PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
		
		int year = int.Parse(GamePlaySession.instance.myCalendar.years [GamePlaySession.instance.GetCurrentYear()].yearName);

		int lastYear = int.Parse (GamePlaySession.instance.myCalendar.years [GamePlaySession.instance.myCalendar.years.Length - 1].yearName);

		GamePlaySession.instance.SetMyLevel(GamePlaySession.instance.GetMyLevel() + 1);

		PopUpMessage message = new PopUpMessage();
		if(year >= lastYear)
        {
			message.message = "You've completed the last year!" + "!\nYou have been gifted with a <color=#d29314>Token</color>!";
		}
		else
        {
			message.message = "It's " + (int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName)).ToString() + "!\nYour rank has increased to Level " + GamePlaySession.instance.GetMyLevel().ToString() + "!\nYou have been gifted with a <color=#d29314>Token</color>!";
		}

		message.gift = new GiftObject();
		message.gift.tokens = 1;
		message.isNewYear = true;

		int[] giftCards = new int[2];

		switch (year - 1)
        {
		case 1980:
			giftCards[0] = 180;
			giftCards[1] = 234;
			break;
		case 1981:
			giftCards[0] = 181;
			giftCards[1] = 235;
			break;
		case 1982:
			giftCards[0] = 182;
			giftCards[1] = 236;
			break;
		case 1983:
			giftCards[0] = 183;
			giftCards[1] = 237;
			break;
		case 1984:
			giftCards[0] = 184;
			giftCards[1] = 238;
			break;
		case 1985:
			giftCards[0] = 185;
			giftCards[1] = 239;
			break;
		case 1986:
			giftCards[0] = 186;
			giftCards[1] = 240;
			break;
		case 1987:
			giftCards[0] = 187;
			giftCards[1] = 241;
			break;
		case 1988:
			giftCards[0] = 188;
			giftCards[1] = 242;
			break;
		case 1989:
			giftCards[0] = 600;
			giftCards[1] = -1;
			break;
		case 1990:
			giftCards[0] = 601;
			giftCards[1] = -1;
			break;
		case 1991:
			giftCards[0] = 602;
			giftCards[1] = -1;
			break;
		case 1992:
			giftCards[0] = 603;
			giftCards[1] = -1;
			break;
		case 1993:
			giftCards[0] = 604;
			giftCards[1] = -1;
			break;
		case 1994:
			giftCards[0] = 605;
			giftCards[1] = -1;
			break;
		case 1995:
			giftCards[0] = 606;
			giftCards[1] = -1;
			break;
		case 1996:
			giftCards[0] = 607;
			giftCards[1] = -1;
			break;
		case 1997:
			giftCards[0] = 608;
			giftCards[1] = -1;
			break;
		case 1998:
			giftCards[0] = 609;
			giftCards[1] = -1;
			break;
		case 1999:
			giftCards[0] = 233;
			giftCards[1] = -1;
			break;
		default:
			giftCards[0] = -1;
			giftCards[1] = -1;	
			break;
		}

		// If we already have the first card, we can assume the player has cleared this year once before so we shouldn't give duplicate cards
		if (giftCards[0] == -1)
        {

		}
		else if(GamePlaySession.instance.CheckCard(giftCards[0]) == null)
        {
			if (year < 1990) 
			{
				message.gift.cardMessage = "Also, here is a new <color=#ff44d3>Venue</color> and <color=#00ff00>Sponsor</color> for your shows!";
			} 
			else 
			{
				message.gift.cardMessage = "Also, here is a new <color=#00ff00>Sponsor</color> for your shows!";
			}
			message.gift.cardIDs = giftCards;
		}

        if (popUpWindow != null)
        {
            popUpWindow.OpenPopUpWindow(new PopUpMessage[] { message }, UIManager.GetComponent<UIManagerScript>().MainCalendarScreen, true);
            screenOverride = true;
        }

        return screenOverride;
    }
	
	public bool CheckMerchUnlocks()
    {
		bool screenOverride = false;
		
		//UIManager = GameObject.Find("UIManager");
		GameSetupItems items = UIManager.GetComponent<GameSetupItems>();
		
		NewCardsObject newCards = new NewCardsObject();
		newCards.message = merchUnlockMessage;
		foreach (FlavorCardObject flavor in items.flavors.cardArray) {
			// Check if flavor is a merchandise card
			if (flavor.cardClass == "Merchandise") {
				// Make sure we don't already have this card
				if (GamePlaySession.instance.CheckCard(flavor.CardNumber) == null) {
					CardObject card = GamePlaySession.instance.CheckCard(flavor.unlockRequirements.MaxOut);
					if (card != null) {
                        //We have the character associated with this merchandise so let's check if we maxed their stats
                        try
                        {
                            WrestlerCardObject wrestler = (WrestlerCardObject)card;
                            if (wrestler.micCurrent >= wrestler.stats.MicMax && wrestler.popCurrent >= wrestler.stats.PopMax && wrestler.skillCurrent >= wrestler.stats.SkillMax)
                            {
                                newCards.cards.Add(flavor.Clone());
                            }
                        }
                        catch (System.InvalidCastException e)
                        {
                            Debug.Log(e.Message);
                        }
                    }
				}
			}
		}
		
		if (newCards.cards.Count > 0) {
			if (cardViewer != null) {
				cardViewer.OpenCardView(newCards);
				screenOverride = true;
			}
		}

		return screenOverride;
	}
}
