﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Linq;

public class GamePlaySessionSaveable 
{
	//[System.NonSerialized]
	[XmlIgnore, System.NonSerialized]
	public static GamePlaySession instance = null;
	public int selectedFederation;
	public int selectedComissioner;

	[System.NonSerialized]
	[XmlIgnore]
	public GameSetupItems GSI;

	[System.NonSerialized]
	[XmlIgnore]
	public const short MAX_BLURBS = 500;

	public int myCash;
	public int myTokens;
	public int myLevel;
	public int shownBlurbCount;

	public bool isNewPlayer;
	public bool inTutorialMode;
	public bool flavorTutorialClear;
	public bool calendarTutorialClear;
	public bool resultTutorialClear;
	public bool storePurchaseTutorialClear;
	public bool storeRefreshTutorialClear;
	public bool storeTutorialClear;
	public bool transitionMessageClear;

	public int storeCardsBought;
	public bool[] storePurchases;

	public bool newGamePlus;

	public int currentYear;
	public int currentMonth;
	public int numberOfYears;

	public int worldChampCardNumber;
	public int titleChampCardNumber;
	public int[] tagChampCardNumbers;

	public int worldChampLastCardNumber;
	public int titleChampLastCardNumber;
	public int[] tagChampLastCardNumbers;

	public List<int> RatingPositionHolder;
	public List<float> FedRatings;

	public List<CardObject>[] Awards;

	public bool firstShopCards;
	public DateTime lastShopRefresh;

	public bool gameCompleted;
	public bool fantasyPopUpShown;
	public bool transferData;

	public List<int> myCardIDs;
	public List<List<int>> myWrestlers;

	[XmlArray("MyBonuses"),XmlArrayItem("Bonus")]
	public List<BonusValue> myBonuses;

	[XmlArray("MyShows"),XmlArrayItem("Show")]
	public List<ShowObject> myShows;

	[XmlElement("MyCalendar")]
	public CalendarObject myCalendar;

	[XmlArray("ShownBlurbs"),XmlArrayItem("Blurb")]
	public List<int> shownBlurbs;

	[XmlArray("StoreCards"),XmlArrayItem("Card")]
	public List<CardObject> storeCards = new List<CardObject>();

	public float averageRating;

	public GamePlaySessionSaveable()
	{
		selectedFederation = -1;
		selectedComissioner = -1;
		myBonuses = new List<BonusValue>();
		isNewPlayer = true;
		inTutorialMode = true;
		storeCardsBought = 0;
		storePurchases = new bool[3];
		averageRating = 0f;
		myLevel = 0;

		lastShopRefresh = DateTime.Now;

		myCardIDs = new List<int> ();
		shownBlurbs = new List<int>();

		myShows = new List<ShowObject>();
		myBonuses = new List<BonusValue>();

		myCalendar = new CalendarObject();
		numberOfYears = myCalendar.GetNumberOfYears();
		myCalendar = myCalendar.GenerateCalendar();

		tagChampCardNumbers = new int[2];
		tagChampLastCardNumbers = new int[2];

		storeCards = new List<CardObject>();

		RatingPositionHolder = new List<int>();
		FedRatings = new List<float>();

		Awards = new List<CardObject>[20];
		fantasyPopUpShown = false;
	}


	public GamePlaySessionSaveable MakeSaveable(GamePlaySession session)
	{
		GamePlaySessionSaveable ss = new GamePlaySessionSaveable ();

		//Iterates through all the cards in GamePlaySession and saves all their Card IDs in the myCardIDs list. 
		//If a card is also a wrestler type, makes it into a List of ints and saves it into myWrestlers list so we can later De-Serialize it and make into a wrestler card
		for (int i = 0; i < session.myCards.Count; i++) 
		{
			ss.myCardIDs.Add (session.myCards [i].CardNumber);
			if (session.myCards [i].myCardType == CardType.WRESTLER) 
			{
				ss.myWrestlers.Add (session.myCards [i].SerializeReady ());
			}
		}

		//Copying over the ints 
		ss.myCash = session.GetMyCash();
		ss.myTokens = session.GetMyTokens();
		ss.myLevel = session.GetMyLevel();
		ss.shownBlurbCount = session.GetShownBlurbCount();

		return ss;
	}

}
