﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MatchSegmentDisplay : SegmentDisplay {

	public Text matchTitle;
	public Text wrestlerName1;
	public Text wrestlerName2;
	public Image wrestlerHeadshot1;
	public Image wrestlerHeadshot2;
	
	public override void OpenSegmentSetup()
    {
		base.OpenSegmentSetup();

		UIManager = GameObject.Find("UIManager");
		Animator matchScreen = UIManager.GetComponent<UIManagerScript>().MatchSetupScreen;
		MatchSegmentSetup matchSetup = matchScreen.gameObject.GetComponent<MatchSegmentSetup>();

		if (ScreenHandler.current != null && matchScreen != null)
			ScreenHandler.current.OpenPanel(matchScreen);
		
		matchSetup.SetData(this);
	}

	public override void UpdateSegmentData() {
		SegmentObject segment = GamePlaySession.instance.myShows[showSetup.currentWeek.showID].segments[segmentID];

		if (segment != null) {
			if (segment.isTitleMatch && setupButton != null && setupButtonText != null) {
				ColorBlock titleColorBlock = setupButton.colors;
				titleColorBlock.normalColor = titleSegmentNormalColor;
				titleColorBlock.highlightedColor = titleSegmentHighlightColor;
				titleColorBlock.pressedColor = titleSegmentPressedColor;

				switch (segment.titleMatchType) {
				case SegmentObject.TitleMatchType.CONTINENTAL:
					setupButton.colors = titleColorBlock;
					setupButtonText.text = "+TV Title Match!";
					break;
				case SegmentObject.TitleMatchType.WORLD:
					setupButton.colors = titleColorBlock;
					setupButtonText.text = "+World Title Match!";
					break;
				}
			}
			if (segment.cards != null) {
				if (matchTitle != null) {
					if (segment.isTitleMatch) {
						switch (segment.titleMatchType) {
						case SegmentObject.TitleMatchType.CONTINENTAL:
							matchTitle.text = "TV Title Match";
							break;
						case SegmentObject.TitleMatchType.WORLD:
							matchTitle.text = "World Title Match";
							break;
						}
					} else {
						matchTitle.text = "Match";
					}
				}

				CardObject wrestlerCard1 = segment.cards[2];
				CardObject wrestlerCard2 = segment.cards[4];

				if (wrestlerCard1 != null) {
					wrestlerName1.text = wrestlerCard1.name;
					wrestlerHeadshot1.sprite = GamePlaySession.instance.CheckCard(wrestlerCard1.CardNumber).headshotImage;
				}
				if (wrestlerCard2 != null) {
					wrestlerName2.text = wrestlerCard2.name;
					wrestlerHeadshot2.sprite = GamePlaySession.instance.CheckCard(wrestlerCard2.CardNumber).headshotImage;
				}
			}
		}
	}
}
