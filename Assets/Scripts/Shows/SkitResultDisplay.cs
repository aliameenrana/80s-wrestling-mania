﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SkitResultDisplay : ResultDisplay {
	
	public Text segmentTitle;
	public Image segmentIcon;
	public Image[] wrestlerHeadshots;
	public Text[] wrestlerSkills;
	public Image segmentRating;
	public Text segmentText;
	
	public override void UpdateResultData() {
		if (segment != null) 
		{
			
			// Tell the segment to get the segment results!
			segment.GetResults(segment);
			
			// Adapt result box height
			LayoutElement layout = this.gameObject.GetComponent<LayoutElement>();
			if (layout != null) {
				if (segment.team1IDs.Length < 2) 
				{
					layout.preferredHeight = 300;
				} else {
					layout.preferredHeight = 400;
				}
			}

			// Display the results!
			segmentTitle.text = segment.segmentTitle;

			for (int i = 0;i <= 4;i++) 
			{
				if (segment.team1IDs.Length > i) 
				{
					if (segment.team1IDs[i] != null) 
					{
						wrestlerSkills[i].transform.parent.gameObject.SetActive(true);

						wrestlerHeadshots[i].sprite = GamePlaySession.instance.CheckCard(segment.team1IDs[i]).headshotImage;

						if (segment.team1SkillChange[i] > 0) 
						{
							wrestlerSkills[i].text = "+" + segment.team1SkillChange[i].ToString() + " POP!";
						} 
						else 
						{
							wrestlerSkills[i].text = "";
						}
					} 
					else 
					{
						wrestlerSkills[i].transform.parent.gameObject.SetActive(false);
					}
				} 
				else 
				{
					wrestlerSkills[i].transform.parent.gameObject.SetActive(false);
				}
			}
			
			segmentRating.fillAmount = segment.GetStarRating();

			SkitCardObject skit;

			if (segment.cards == null) 
			{
				if ((SkitCardObject)GamePlaySession.instance.CheckCard (segment.cardIDs [0])!=null)
				{
					skit = (SkitCardObject)GamePlaySession.instance.CheckCard (segment.cardIDs [0]);
				}
				else
				{
					skit = (SkitCardObject)GamePlaySession.instance.GSI.skits.cards [segment.cardIDs [0]].Clone ();
				}

			} 
			else 
			{
				skit = (SkitCardObject) segment.cards[0];
			}

			if (skit != null) 
			{
				if (segmentIcon != null) 
				{
					segmentIcon.sprite = skit.headshotImage;


					Image iconBorder = segmentIcon.transform.parent.gameObject.GetComponent<Image>();
					if (iconBorder != null) 
					{
						if (segment.segmentPassed) 
						{
							iconBorder.color = Color.yellow;
						} 
						else 
						{
							iconBorder.color = Color.white;
						}
					}
				}

				string resultMessage = "";
				if (segment.segmentPassed) {
					resultMessage = skit.result.Pass;
				} else {
					resultMessage = skit.result.Fail;
				}

				// Parse the result text and replace (WRESTLER) text with names
				int textPos = -1;
				int lastPos = 0;
				for (int i = 0; i < segment.team1IDs.Length; i++) 
				{
					textPos = resultMessage.IndexOf("(WRESTLER)");
					if (textPos > -1) 
					{
						// We have a (WRESTLER) tag to replace
						string name = GamePlaySession.instance.CheckCard(segment.team1IDs[i]).name.Trim();
						resultMessage = resultMessage.Remove(textPos, 10);
						resultMessage = resultMessage.Insert(textPos, name);
						lastPos = textPos + name.Length;
					} 
					else 
					{
						// We have no more (WRESTLER) tags so append to the end of the last name
						string name = "";
						if (i == (segment.team1IDs.Length - 1)) 
						{
							// This is the last name, so we should add "and" rather than a comma
							name = " and " + GamePlaySession.instance.CheckCard(segment.team1IDs[i]).name.Trim();
						} 
						else 
						{
							// This isn't the last name, so we should use a comma
							name = ", " + GamePlaySession.instance.CheckCard(segment.team1IDs[i]).name.Trim();
						}
						resultMessage = resultMessage.Insert(lastPos, name);
						lastPos += name.Length;
					}
				}

				segmentText.text = resultMessage;
				
				foreach (Image headshot in wrestlerHeadshots) {
					Image border = headshot.transform.parent.gameObject.GetComponent<Image>();
					if (border != null) {
						if (segment.segmentPassed) {
							border.color = Color.yellow;
						} else {
							border.color = Color.white;
						}
					}
				}
			} else {
				segmentText.text = "";
			}
		}
	}
}
