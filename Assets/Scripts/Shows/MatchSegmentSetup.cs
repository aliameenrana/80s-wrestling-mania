﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MatchSegmentSetup : SegmentSetup {

	public override bool CheckReady() {
		bool isReady = true;

		if (cards[0] == null)
			isReady = false;

		if (cards[2] == null)
			isReady = false;

		if (cards[4] == null)
			isReady = false;

		return isReady;
	}

	public override void SetFlavorButtons(List<FlavorCardObject> flavors) {
		int[] flavorOptions = new int[] {1, 3, 5};
		foreach (int option in flavorOptions) {
			Button button = optionButtons[option].GetComponentInChildren<Button>();
			if (button != null) 
			{
				bool hasFlavor = false;

				CardSelector selector = selectors[option];

				foreach (FlavorCardObject flavor in flavors) 
				{
					if (CheckCardPlayable(flavor, selector)) 
					{
						hasFlavor = true;
					}
				}

				if (hasFlavor) {
					button.interactable = true;
				} else {
					button.interactable = false;
				}
			}
		}
	}

	public override void SetTitleCards(SegmentObject segment) {
		if (!segment.isTitleMatch)
			return;

		switch (segment.titleMatchType) {
		case SegmentObject.TitleMatchType.CONTINENTAL:
			segment.cards[2] = GamePlaySession.instance.titleChampion;
			break;
		case SegmentObject.TitleMatchType.WORLD:
			segment.cards[2] = GamePlaySession.instance.worldChampion;
			break;
		}
	}

	public override void CheckCardCompatibility(int index) {
		SegmentObject segment = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID].segments[segmentDisplay.segmentID];

		int wrestlerIndex = 0;
		int flavorIndex = 0;

		if (index == 2 || index == 4) {
			// Wrestler card
			wrestlerIndex = index;
			flavorIndex = index + 1;
		} else if (index == 3 || index == 5) {
			// Flavor card
			wrestlerIndex = index - 1;
			flavorIndex = index;
		}

		if (wrestlerIndex > 0 && flavorIndex > 0) {
			if (cards[wrestlerIndex] != null && cards[flavorIndex] != null) {
				// Check wrestler and their flavor card
				WrestlerCardObject wrestler = (WrestlerCardObject) cards[wrestlerIndex];
				FlavorCardObject flavor = (FlavorCardObject) cards[flavorIndex];

				if (flavor.cardClass == "Manager") {
					// We have a manager card selected, so let's make sure it is compatible in alignment
					if (wrestler.cardAlignment.ToUpper() != flavor.characterAlignment.ToUpper()) {
						// The alignment of the cards is a mismatch, so we should deselect the other card
						if (index == wrestlerIndex) {
							cards[flavorIndex] = null;
						} else {
							cards[wrestlerIndex] = null;
						}
						SetData(segmentDisplay);
					}
				}
			}
		}
	}
}