﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using System.Linq;


[System.Serializable]
public class TagTeamObject
{
    [XmlElement("Wrestler1")]
    public WrestlerCardObject Wrestler2;

    [XmlElement("Wrestler2")]
    public WrestlerCardObject Wrestler1;

    [XmlElement("CrowningDate")]
    public string CrowningDate;

    [XmlElement("Defenses")]
    public int Defenses;

    //public TagTeamObject()
    //{

    //}

    public bool Equals(TagTeamObject Team)
    {
        List<WrestlerCardObject> myWrestlers = new List<WrestlerCardObject>();
        List<WrestlerCardObject> TeamWrestlers = new List<WrestlerCardObject>();
        TeamWrestlers.Add(Team.Wrestler1);TeamWrestlers.Add(Team.Wrestler2);
        myWrestlers.Add(Wrestler1);myWrestlers.Add(Wrestler2);

        myWrestlers = myWrestlers.OrderBy(o=>o.CardNumber).ToList();
        TeamWrestlers = TeamWrestlers.OrderBy(o => o.CardNumber).ToList();
        myWrestlers.Reverse();
        TeamWrestlers.Reverse();

        return (myWrestlers.SequenceEqual(TeamWrestlers));
    }

    public List<string> toChamp()
    {
        string w1 = (Wrestler1.CardNumber.ToString()) + "q" + (Defenses.ToString()) + "q" + CrowningDate;
        string w2 = (Wrestler2.CardNumber.ToString()) + "q" + (Defenses.ToString()) + "q" + CrowningDate;

        List<string> ret = new List<string>();
        ret.Add(w1);ret.Add(w2);

        return ret;
    }

    public bool isSame(List<string> Team)
    {
        List<string> ChampedOut = this.toChamp();
        char[] Delimiters = { 'q' };
        List<int> CardsNumberThis = new List<int>(2);
        List<int> CardsNumberTeam = new List<int>(2);

        for (int i = 0; i < 2; i++)
        {
            CardsNumberTeam.Add(int.Parse(Team[i].Split(Delimiters[0])[0]));
            CardsNumberThis.Add(int.Parse(this.toChamp()[i].Split(Delimiters[0])[0]));
        }

        foreach (var item in CardsNumberTeam)
        {
            Debug.Log("CARDS NUMBER TEAM: " + item);
        }

        foreach (var item in CardsNumberThis)
        {
            Debug.Log("CARDS NUMBER THIS: " + item);
        }
        
        CardsNumberTeam.Sort();
        CardsNumberThis.Sort();

        return (CardsNumberThis == CardsNumberTeam);
    }
}
