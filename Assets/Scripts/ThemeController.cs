﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeController : MonoBehaviour 
{
	GameObject UIManager;
	void Awake()
	{
		
	}


	public void CheckTheme()
	{
		Button[] buttons = gameObject.GetComponentsInChildren<Button> (true);
		Scrollbar[] scrollBars = gameObject.GetComponentsInChildren<Scrollbar> (true);
		Image[] images = gameObject.GetComponentsInChildren<Image> (true);
		if (UIManager==null)
			UIManager = GameObject.Find ("UIManager");
		
		foreach (var item in buttons) 
		{
			if (item.gameObject.GetComponent<Image> () != null && item.gameObject.GetComponent<Image> ().sprite != null) 
			{
				if (item.gameObject.GetComponent<Image> ().sprite.name == "button_Background" || item.gameObject.GetComponent<Image> ().sprite.name == "red button") {
					if (UIManagerScript.instance.Theme90s) {
						item.gameObject.GetComponent<Image> ().sprite = UIManagerScript.instance.RedButton;
					} else {
						item.gameObject.GetComponent<Image> ().sprite = UIManagerScript.instance.PurpleButton;
					}
				} 
				else if (item.gameObject.GetComponent<Image> ().sprite.name.ToUpper() == "BLACKCARDBACKGROUND") 
				{
					if (UIManagerScript.instance.Theme90s) 
					{
						item.gameObject.GetComponent<Image> ().color = Color.red;
					} 
					else 
					{
						item.gameObject.GetComponent<Image> ().color = new Color (0.92f, 0.42f, 0.92f, 1f);
					}
				}
				else 
				{
					if (UIManagerScript.instance.Theme90s) 
					{
						foreach (var item2 in UIManagerScript.instance.redSprites) 
						{
							if ((item2.name.Contains (item.gameObject.GetComponent<Image> ().sprite.name)) && (item2.name.Contains ("_Red"))) 
							{
								item.GetComponent<Image> ().sprite = item2;
							}
						}
					} 
					else if (!UIManagerScript.instance.Theme90s)
					{
						foreach (var item2 in UIManagerScript.instance.pinkSprites) 
						{
							if ((item.gameObject.GetComponent<Image> ().sprite.name.Contains (item2.name))) 
							{
								item.GetComponent<Image> ().sprite = item2;
							}
						}
					}
				}
			}	
		}

		foreach (var item3 in images) 
		{
			if (item3.sprite != null) 
			{
				if (item3.sprite.name.ToUpper () == "BLACKCARDBACKGROUND") 
				{
					if (UIManagerScript.instance.Theme90s) 
					{
						item3.gameObject.GetComponent<Image> ().color = Color.red;
					} 
					else 
					{
						item3.gameObject.GetComponent<Image> ().color = new Color (0.92f, 0.42f, 0.92f, 1f);
					}
				}
			}
		}

		if (UIManager != null) 
		{
			if (UIManagerScript.instance.Theme90s) 
			{
				UIManager.GetComponent<UIManagerScript> ().PlayerHUD.GetComponent<Image> ().color = Color.black;
				UIManager.GetComponent<UIManagerScript> ().PlayerHUD.GetComponent<HUDScript> ().ratingsLabel.color = Color.white;
				UIManager.GetComponent<UIManagerScript> ().PlayerHUD.GetComponent<HUDScript> ().dudText.color = Color.white;
			} 
			else 
			{
				UIManager.GetComponent<UIManagerScript> ().PlayerHUD.GetComponent<Image> ().color = new Color (0.85f, 0.811f, 1f, 1f);
				UIManager.GetComponent<UIManagerScript> ().PlayerHUD.GetComponent<HUDScript> ().ratingsLabel.color = Color.black;
				UIManager.GetComponent<UIManagerScript> ().PlayerHUD.GetComponent<HUDScript> ().dudText.color = Color.black;
			}

			foreach (var item in scrollBars) 
			{
				if (UIManager.GetComponent<UIManagerScript>().Theme90s)
				{
					item.gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = Color.red;
				}
				else
				{
					item.gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = Color.white;
				}
			}
		}
		if (gameObject.name == "MainCalendarScreen") 
		{
			if (UIManager.GetComponent<UIManagerScript> ().Theme90s) 
			{
				gameObject.GetComponent<CalendarScreen> ().nextShowColor = Color.red;
			} 
			else 
			{
				gameObject.GetComponent<CalendarScreen> ().nextShowColor = new Color (0.92f, 0.42f, 0.92f, 1f);
			}
		}
	}

	public void ChangeSprite(GameObject go, bool saver)
	{
		if (saver) 
		{
			if (UIManagerScript.instance.Theme90s) 
			{
				go.GetComponent<Image> ().sprite = UIManagerScript.instance.redSprites.Find (o => o.name.Contains ("SaveExit"));
			} 
			else if (!UIManagerScript.instance.Theme90s) 
			{
				go.GetComponent<Image> ().sprite = UIManagerScript.instance.pinkSprites.Find (o => o.name.Contains ("SaveExit"));
			}
			go.transform.parent.gameObject.GetComponent<LayoutElement> ().minWidth = 200f;
		} 
		else 
		{
			if (go.name.Contains ("Back")) 
			{
				if (UIManagerScript.instance.Theme90s) 
				{
					go.GetComponent<Image> ().sprite = UIManagerScript.instance.redSprites.Find (o => o.name.Contains ("Calendar"));
				} else if (!UIManagerScript.instance.Theme90s) {
					go.GetComponent<Image> ().sprite = UIManagerScript.instance.pinkSprites.Find (o => o.name.Contains ("Calendar"));
				}
			}
			go.transform.parent.gameObject.GetComponent<LayoutElement> ().minWidth = 40f;
		}

		go.transform.parent.parent.transform.Find ("HomeWindow").gameObject.SetActive (!saver);
		go.transform.parent.GetComponent<Image> ().enabled = !saver;
	}

}
