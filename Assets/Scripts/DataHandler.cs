﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System;
using UnityEngine.UI;

public class DataHandler : MonoBehaviour
{
	public string serverURL = "http://bugdevstudios.com/wrestling/server.php";
    public GameObject UIManager;
    public static DataHandler instance = null;
	string data = "";
	Action<bool> callback;
    public bool loaded = false;
	bool showPopUp;
	bool inGame;
	bool saveGame;

    private void Awake()
    {
        instance = this;
    }

	IEnumerator Start()
	{
		while(PlayerPrefs.GetString("UserID", "") == "")
			yield return new WaitForSeconds(0.5f);

		//InvokeRepeating("ScheduleUpload", 60f, 60f);

//		yield return new WaitForSeconds(15f);
//		yield return StartCoroutine("UploadData");
//		yield return new WaitForSeconds(15f);
//		yield return StartCoroutine("InvokeServer");
//		GamePlaySession.instance = GamePlaySession.Load(data);
//		Debug.Log("Process Done");

	}

	public void ScheduleUpload(bool popUp = false)
	{
		StartCoroutine("UploadData");
		showPopUp = popUp;
	}

	IEnumerator UploadData()
	{
		string userId = PlayerPrefs.GetString("UserID");
		string time = System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

		if (showPopUp)
			GamePlaySession.instance.SaveAll ();
		
		string data = UIManagerScript.instance.myStr;

		WWWForm form = new WWWForm();
		form.AddField("REQUEST", "UPDATE_GPS");
		form.AddField("USER_ID", userId);
		form.AddField("TIME", time);
		form.AddField("GPS", data);

		Debug.Log("UserID : " + userId + " Time : " + time);
//		Debug.Log(data);

		WWW www = new WWW(serverURL, form);

		yield return www;
		PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript> ().popUpWindow;
		PopUpMessage uploadMessage = new PopUpMessage ();

		if (www.error != null) 
		{
			uploadMessage.message = "Upload failed, try again later!";
			if (showPopUp)
				popUpWindow.OpenPopUpWindow (new PopUpMessage[]{ uploadMessage }, UIManager.GetComponent<UIManagerScript> ().MyFedScreen);
			Debug.Log (www.error);
			yield break;
		} 
		else 
		{
			uploadMessage.message = "Your Data has been successfully uploaded to the server!";
		}

		//Debug.Log("Umar:: game Session sent to server");
		Debug.Log(www.text);

		www.Dispose();

		if (showPopUp)
			popUpWindow.OpenPopUpWindow (new PopUpMessage[]{ uploadMessage }, UIManager.GetComponent<UIManagerScript> ().MyFedScreen);

	}

	public void GetSessionDataFromServer(Action<bool> OnDataRecieved)
	{
		callback = OnDataRecieved;
		StartCoroutine(InvokeServer());
	}

	IEnumerator InvokeServer(bool inGameDownload = false, bool saveGameInvoke = false)
	{
		inGame = inGameDownload;
		saveGame = saveGameInvoke;
		Debug.Log (PlayerPrefs.GetString ("UserID"));
		string url = serverURL + "?REQUEST=GET_GPS&USER_ID=" + PlayerPrefs.GetString("UserID");
		Debug.Log(url);
		WWW www = new WWW(url);

		yield return www;


		if (saveGameInvoke) 
		{
			ShowSaving ();
			UIManager.GetComponent<UIManagerScript> ().loadingCanvas.GetComponent<LoadGame> ().messageToShow = "";
		}



		if (www.error != null)
		{
			if (!saveGameInvoke && inGameDownload) 
			{
				PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript> ().popUpWindow;
				PopUpMessage downloadErrorMessage = new PopUpMessage ();
				downloadErrorMessage.message = "Error Retrieving Data from server, try again later";
				popUpWindow.OpenPopUpWindow (new PopUpMessage[]{ downloadErrorMessage }, UIManager.GetComponent<UIManagerScript> ().MyFedScreen);
			}

			if (saveGameInvoke) 
			{
				try
				{
					UIManager.GetComponent<UIManagerScript>().loadingCanvas.GetComponent<LoadGame>().messageToShow = "Saving failed, try again later";
				}
				catch 
				{

				}
			}

			Debug.Log(www.error);
			yield break;
		}

		ProcessResponse (www.text);
		www.Dispose();
		yield return null;

	}

	void ProcessResponse(string response)
	{
        var N = JSON.Parse(response);
		var responseStatus = N["CODE"].AsInt;
		data = N["GPS"].Value;
		var lastUpdate = N["GPS_UPDATED"].Value;
		var status = N["STATUS"].Value;
        UIManager = GameObject.Find("UIManager");
        loaded = true;
		PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript> ().popUpWindow;
		PopUpMessage downloadMessage = new PopUpMessage ();

        if (responseStatus == 6)
		{
			GamePlaySession.instance = GamePlaySession.Load (data);
			if (GamePlaySession.instance.selectedFederation == 3)
				GamePlaySession.instance.selectedFederation = 2;

			if (GamePlaySession.instance.selectedComissioner == 3) 
			{
				GamePlaySession.instance.selectedComissioner = 2;
				GamePlaySession.instance.myBonuses.Clear ();
				BonusValue bonus = new BonusValue ();
				bonus.bType = BonusValue.BonusType.PLUS_CASH;
				bonus.value = 500;
				GamePlaySession.instance.myBonuses.Add (bonus);
			}


			GamePlaySession.instance.GSI.GetComponent<UIManagerScript> ().PlayerHUD.GetComponent<HUDScript> ().SetCommishImage ();
			GamePlaySession.instance.GSI.GetComponent<UIManagerScript> ().PlayerHUD.GetComponent<HUDScript> ().SetFedImage ();

			//Debug.Log("Data recieved successfully...");
			OldGamePlaySession oldGPS = OldGamePlaySession.Load(data);
			if (oldGPS != null && oldGPS.transferData != false) 
			{
				UIManager.GetComponent<UIManagerScript> ().transferCash = oldGPS.myCash;
				UIManager.GetComponent<UIManagerScript> ().transferTokens = oldGPS.myTokens;

				if (oldGPS.myCards.Count > 0) 
				{
					foreach (var item in oldGPS.myCards) 
					{
						if (item.CardNumber == 333) 
						{
							UIManager.GetComponent<UIManagerScript> ().transferCardNumbers.Add (332);
						} 
						else 
						{
							UIManager.GetComponent<UIManagerScript> ().transferCardNumbers.Add (item.CardNumber);	
						}

						if (item.myCardType == CardType.WRESTLER) 
						{
							WrestlerCardObject w = (WrestlerCardObject)item;
							if (w.cardVersion == "FOIL") 
							{
								UIManager.GetComponent<UIManagerScript> ().transferCardNumbers.Add (item.CardNumber - 1);
							}
							if (w.FirstActiveYear == 0 || w.LastActiveYear == 0) 
							{
								w = (WrestlerCardObject)UIManagerScript.instance.gsi.wrestlers.cards [w.CardNumber].Clone ();
							}
						}
					}
				}
			} 
			else 
			{
	            GamePlaySession.instance = GamePlaySession.Load(data);
	
	            GamePlaySession.instance.GSI.GetComponent<UIManagerScript>().PlayerHUD.GetComponent<HUDScript>().SetCommishImage();
	            GamePlaySession.instance.GSI.GetComponent<UIManagerScript>().PlayerHUD.GetComponent<HUDScript>().SetFedImage();

			}

			foreach (ShowObject so in GamePlaySession.instance.myShows)
			{
				so.PostDeserialize();
			}

			downloadMessage.message = "Your Game Session has been successfully downloaded from the server!";

			if(callback != null && !inGame && !saveGame)
				callback(true);
		}
		else
		{
			if(callback != null && !inGame && !saveGame)
				callback(false);

			downloadMessage.message = "Error Retrieving Data from server, try again later";

			Debug.Log("Error Retrieving Data from server.");
		}

		if (MyFed.instance != null) 
		{
			MyFed.instance.walletPanel.BroadcastMessage("ShowMyCash", SendMessageOptions.DontRequireReceiver);
			MyFed.instance.walletPanel.BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
		}

		if (inGame)
			popUpWindow.OpenPopUpWindow (new PopUpMessage[]{ downloadMessage }, UIManager.GetComponent<UIManagerScript> ().MyFedScreen);

		if (saveGame) 
		{
			try
			{
				UIManager.GetComponent<UIManagerScript>().loadingCanvas.GetComponent<LoadGame>().messageToShow = "Game Successfully saved";
			}
			catch 
			{
				
			}
		}

	}


	public void DownloadGameDataFromServer()
	{
		StartCoroutine (InvokeServer (true));
	}

	public void LoadGame()
	{
		StartCoroutine (InvokeServer (false,true));
	}


	void ShowSaving()
	{
		UIManager.GetComponent<UIManagerScript> ().loadingCanvas.SetActive (true);
		UIManager.GetComponent<UIManagerScript> ().loadingCanvas.GetComponent<LoadGame> ().RunSaving ();
	}
}