﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardBuyConfirm : MonoBehaviour {
	public Graphic cardLogo;
	public Text cashAmount;
	public Text tokenAmount;

	public void DisplayConfirmCard(StoreCardViewer cardViewer) 
	{
		CardObject card = cardViewer.card.myCard;

		if (GamePlaySession.instance.GetInTutorialMode() && !GamePlaySession.instance.GetStoreRefreshTutorialClear() && card.CardNumber!=5 && card.CardNumber!=83)
		{
			PopUpWindow popUpWindow = UIManagerScript.instance.popUpWindow;
			PopUpMessage BuyTutorialCardsMessage = new PopUpMessage ();
			BuyTutorialCardsMessage.message = "Go ahead and buy <color=#00ffff>ARIZONA CHANCE</color> and <color=#ff0000>KEITH BLOOD</color> cards!";
			if (popUpWindow != null) 
			{
				popUpWindow.OpenPopUpWindow (new PopUpMessage[]{ BuyTutorialCardsMessage }, this.gameObject.transform.parent.GetComponent<Animator> ());
			}
			return;
		}

		if (card != null) 
		{
			GameObject logoImageObject = cardLogo.gameObject;
			if(card.logoImage == null)
			{
				if(cardLogo.gameObject.GetComponent<Image>() != null)
				{
					DestroyImmediate(cardLogo.gameObject.GetComponent<Image>());
					logoImageObject.AddComponent<Text>();
				}
				
				logoImageObject.GetComponent<Text>().font = GameObject.Find("UIManager").GetComponent<UIHelper>().gameFont;
				logoImageObject.GetComponent<Text>().fontSize = 60;
				logoImageObject.GetComponent<Text>().resizeTextForBestFit = true;
				logoImageObject.GetComponent<Text>().text = card.name.Trim();
				logoImageObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
				cardLogo = logoImageObject.GetComponent<Text>();
				
				if (card.myCardType == CardType.SPONSOR) {
					cardLogo.color = Color.green;
				} else {
					cardLogo.color = Color.white;
				}
			}
			else
			{
				if(cardLogo.gameObject.GetComponent<Text>() != null)
				{
					DestroyImmediate(cardLogo.gameObject.GetComponent<Text>());
					logoImageObject.gameObject.AddComponent<Image>();
				}
				
				logoImageObject.GetComponent<Image>().sprite = card.logoImage;
				cardLogo = logoImageObject.GetComponent<Image>();
			}

			cashAmount.text = card.cashCost.ToString();
			tokenAmount.text = card.tokenCost.ToString();

			this.gameObject.SetActive(true);
			cardViewer.storeCardScreen.swipeNode.enabled = false;
		}
	}

	public void CloseConfirmCard(StoreCardViewer cardViewer) {
		this.gameObject.SetActive(false);
		cardViewer.storeCardScreen.swipeNode.enabled = true;
	}
}
