using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Prime31;

[System.Serializable]
public class StoreCard {
	public string rarity;
	public CardObject card;
}

public class StorePage : MonoBehaviour {
	public StoreScreenScript storeCardScreen;
    public static StorePage instance;

	public GameObject[] storePageObjects;
	public string[] storePageNames;
	public string[] storeTapPrompts;
	
	public GameObject walletObject;

	public GameObject purchasedOverlay;

	public GameObject noCardsMessage;
	public Text noCardsTimer;

	public GameObject boughtAllCards;

	public Text refreshTimer;
	
	public GameObject cardBuyConfirm;
	public GameObject tradeConfirm;
	public GameObject refreshConfirm;
	
	public Text pageNameText;
	public Text pageTapText;
	public GameObject notEnoughString;
	
	public Button[] nonTutorialButtons;

	public Button refreshButton;
	
	public PopUpMessage tutorialPopUp;
	
	public int[] tutorialCardIDs;
	
	[HideInInspector]
	public int currentPage = 0;

	private int lastCash;
	private int lastTokens;
	
	private Animator thisPanel;

	public List<StoreCard> storeLibrary = new List<StoreCard>();

	private bool hasRare;
	private bool hasUncommon;
	private bool hasCommon;

	private int commonCardCount;
	private int uncommonCardCount;
	private int rareCardCount;
	
	private int commonNonFoilCardCount;
	private int uncommonNonFoilCardCount;
	private int rareNonFoilCardCount;

	private bool firstEntryDone;
	
	void Awake() { // Entire function moved
		thisPanel = this.GetComponentInParent<Animator>();
        instance = this;
		foreach (GameObject page in storePageObjects) {
			page.SetActive(false);
		}

        if (currentPage < storePageObjects.Length)
        {
            storePageObjects[currentPage].SetActive(true);
        }

        //Debug.Log("Initializing the store!");
		
		//#if UNITY_IPHONE || UNITY_ANDROID
		// TODO: Populate this with actual values for testing
		
		// Initiate our IAP using the keys needed
		//string appKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjuVxxDcCtPyZLhGNB9qNK02pVRfAH43OVo5sr3thKoS4FQKAeZjujrPNFQQO5eeXtDaJ/DDlMOKEyf+qcq24V2MuT+N6riPRIO2ivWUsOFoWV90G1DNh9o4vzkH2cmPujFtGvGRyXAiBKbptz+5HYjnDhtZPRDWFkZ5ctcD+dQZR6Z6HVl0BFA0g2FGEjpzzEAcUkDw2YrSk5OBNcwjYj9wTcWeht1EaE/jPsqETWzWwKTIRVZCI1HrAnHWE1Mwhf/HR50AOpVaLB4YzFwZ2CBt7dCWAENnjfMqd0BaN18rHgtSiPHmifPEjAHNEw0YA9yjkL36mG+eUClphB1jitwIDAQAB";
		//IAP.init(appKey);
		
		//// Request our IAP products list
		//string[] androidSKUs = new string[] {"com.cc.mania.token1", "com.cc.mania.token5", "com.cc.mania.token11"};
		//string[] iOSProductIDs = new string[] {"com.CheckmateCreative.mania.1token", "com.CheckmateCreative.mania.5token", "com.CheckmateCreative.mania.11token"};
		
		//IAP.requestProductData( iOSProductIDs, androidSKUs, productList => {
		//	Debug.Log( "Product list received" );
		//	Utils.logObject( productList );
		//});
        //#endif
	}

    void OnEnable()
    {
        if (notEnoughString.activeSelf == true)
        {
            notEnoughString.SetActive(false);
        }
    }

    public void RefreshCardDisplay() 
	{
		if (GamePlaySession.instance.storeCards.Count == 0) {
			SelectNewCards ();
			foreach (var item in GamePlaySession.instance.storeCards) 
			{
				item.PostSerialize ();
			}
		}
			

		GamePlaySession.instance.storeCards.Sort((x, y) => string.Compare(x.name, y.name));
		storeCardScreen.ShowCurrentCard();
	}

	public void GenerateStoreLibrary() {
		//Debug.Log("Generating store library!");
		GameSetupItems setupItems = GameObject.Find("UIManager").GetComponent<GameSetupItems>();

		foreach (WrestlerCardObject wrestler in setupItems.wrestlers.cardArray) {
			StoreCard card = new StoreCard();
			card.rarity = wrestler.rarity.Trim();
			card.card = wrestler;
			
			storeLibrary.Add(card);
		}
		foreach (MatchCardObject match in setupItems.matches.cardArray) {
			// Skip any freebie cards, as these are given during tutorials
			if (match.cashCost > 0 || match.tokenCost > 0) {
				StoreCard card = new StoreCard();
				card.rarity = "Common";
				card.card = match;
				storeLibrary.Add(card);
			}
		}
		foreach (MicSpotCardObject micSpot in setupItems.micSpots.cardArray) {
			// Skip any freebie cards, as these are given during tutorials
			if (micSpot.cashCost > 0 || micSpot.tokenCost > 0) {
				StoreCard card = new StoreCard();
				card.rarity = "Common";
				card.card = micSpot;
				storeLibrary.Add(card);
			}
		}
		foreach (SkitCardObject skit in setupItems.skits.cardArray) {
			// Skip any freebie cards, as these are given during tutorials
			if (skit.cashCost > 0 || skit.tokenCost > 0) {
				StoreCard card = new StoreCard();
				card.rarity = "Common";
				card.card = skit;
				storeLibrary.Add(card);
			}
		}
		foreach (FlavorCardObject flavor in setupItems.flavors.cardArray) {
			if (flavor.requirements.Trim() == "Sold in Shop") {
				StoreCard card = new StoreCard();
				card.rarity = "Common";
				card.card = flavor;
				storeLibrary.Add(card);
			}
		}
	}

	public void CheckCardLibrary() 
	{
		if (storeLibrary.Count == 0) 
		{
			GenerateStoreLibrary();
			CountCards();
		}
	}

	void Update()
	{
		CheckCardLibrary();

		if (GamePlaySession.instance.GetMyCash() != lastCash || GamePlaySession.instance.GetMyTokens() != lastTokens)
        {
            //Debug.Log("Updating Wallet...");
            UpdateWallet();
        }
			
		
		if (ScreenHandler.current != null && ScreenHandler.current.GetCurrentPanel() == thisPanel) {
			CheckTutorials();
			UpdateTimer();
		}
		
		if (currentPage == 0) {
			// Check if we've bought every card
			bool[] purchased = GamePlaySession.instance.storePurchases;
			bool allCardsPurchased = true;

			for (int i = 0; i < purchased.Length; i++) {
				bool purchase = purchased[i];
				if (!purchase) {
					// If we've not purchased a card and it's not a placeholder, then we haven't purchased all cards available
					CardObject card = GamePlaySession.instance.storeCards[i];
					if (card.name != "Placeholder")
						allCardsPurchased = false;
				}
			}
			
			// Check if we should show purchased overlay
			if (GamePlaySession.instance.storePurchases.Length > storeCardScreen.currentCardIndex) {
				if (GamePlaySession.instance.storePurchases[storeCardScreen.currentCardIndex]) {
					purchasedOverlay.SetActive(true);
				} else {
					purchasedOverlay.SetActive(false);
				}
			} else {
				purchasedOverlay.SetActive(false);
			}

			// Display no cards dialog accordingly
			if (allCardsPurchased) {
				noCardsMessage.SetActive(true);
				purchasedOverlay.SetActive(false);
			} else {
				noCardsMessage.SetActive(false);
			}

			// Have we cleared out the store?
			if (commonCardCount == 0 && uncommonCardCount == 0 && rareCardCount == 0) {
				boughtAllCards.SetActive(true);
			}
		} else {
			// Hide the no cards/purchased dialog since we're on the tokens page
			noCardsMessage.SetActive(false);
			purchasedOverlay.SetActive(false);
			boughtAllCards.SetActive(false);

			// Disable the refresh button if we have no more store cards to give
			if (commonCardCount == 0 && uncommonCardCount == 0 && rareCardCount == 0) {
				refreshButton.interactable = false;
			} else {
				refreshButton.interactable = true;
			}
		}
	}

	public void UpdateTimer() {
		// Check if we should update the store cards
		System.DateTime currentDate = System.DateTime.Now;
		System.DateTime lastRefresh = GamePlaySession.instance.GetLastShopRefresh();
		System.TimeSpan timeToRefresh = System.TimeSpan.FromDays(0.5) - (currentDate - lastRefresh);

		// Refresh the cards if a day has passed
		if (GamePlaySession.instance.GetLastShopRefresh () <= currentDate.AddDays (-0.5f)) 
		{
			SelectNewCards ();
		} 
		else if (!GamePlaySession.instance.GetFirstShopCards ()) 
		{
			SelectNewCards ();
		} 
		else if (GamePlaySession.instance.storeCards == null || GamePlaySession.instance.storeCards.Count == 0) 
		{
			SelectNewCards ();
		}

		// Update our main timer output
		if (refreshTimer != null) 
		{
			string timerString = "";
			if (timeToRefresh.Hours > 0)
				timerString += timeToRefresh.Hours.ToString("00") + ":";
			if (timeToRefresh.Minutes > 0)
				timerString += timeToRefresh.Minutes.ToString("00") + ":";

			timerString += timeToRefresh.Seconds.ToString("00");
			refreshTimer.text = timerString;
		}

		// Update our no card window timer output
		if (noCardsTimer != null) {
			string timerString = "";
			if (timeToRefresh.Hours > 0)
				timerString += timeToRefresh.Hours.ToString() + ((timeToRefresh.Hours > 1) ? " hours and " : " hour and ");
			if (timeToRefresh.Minutes > 0)
				timerString += timeToRefresh.Minutes.ToString() + ((timeToRefresh.Minutes > 1) ? " minutes" : " minute");
			noCardsTimer.text = timerString;
		}
	}
	
	public void CheckTutorials() {
		foreach (Button button in nonTutorialButtons) {
			if (GamePlaySession.instance.GetInTutorialMode() && !GamePlaySession.instance.GetStoreTutorialClear() && !GamePlaySession.instance.GetNewGamePlus()) {
				button.interactable = false;
			} else {
				button.interactable = true;
			}
		}
		
		if (GamePlaySession.instance.GetInTutorialMode() && !GamePlaySession.instance.GetStoreTutorialClear() && !GamePlaySession.instance.GetNewGamePlus()) 
		{
			if (!GamePlaySession.instance.GetStoreRefreshTutorialClear() && GamePlaySession.instance.GetStoreCardsBought() >= 2) 
			{
				OpenShopTutorial();
				GamePlaySession.instance.SetStoreRefreshTutorialClear(true);
			} 
			else if (GamePlaySession.instance.GetStoreRefreshTutorialClear()) 
			{
				SwitchStorePage(1);
			} 
			else 
			{
				if (!firstEntryDone) 
				{
					RefreshCardDisplay();
					firstEntryDone = true;
				}
			}
		}
	}
	
	public void UpdateWallet() {
        //Debug.Log("Shouting from inside UpdateWallet...");
        if (walletObject != null) {
            walletObject.BroadcastMessage("ShowMyCash", SendMessageOptions.DontRequireReceiver);
            walletObject.BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
        }
		
		lastCash = GamePlaySession.instance.GetMyCash();
		lastTokens = GamePlaySession.instance.GetMyTokens();
	}

	public void SwitchStorePage(int newPage) {
		if (storePageObjects.Length == 0)
			return;
		
		// Get the next store page if available
		int nextPage = currentPage + 1;
		if (newPage >= 0)
			nextPage = newPage;
		
		if (nextPage >= storePageObjects.Length)
			nextPage = 0;
		
		storePageObjects[currentPage].SetActive(false);
		storePageObjects[nextPage].SetActive (true);
		
		currentPage = nextPage;
		
		SetPageTitle();
	}
	
	public void SetPageTitle() {
		if (currentPage >= storePageNames.Length)
			return;
		
		if (pageNameText != null) {
			pageNameText.text = storePageNames[currentPage];
		}

		if (pageTapText != null) {
			pageTapText.text = storeTapPrompts[currentPage];
		}
	}
	
	public void SelectNewCards()
    {
		Debug.Log("Selecting new shop cards!");
		GamePlaySession.instance.SetLastShopRefresh(System.DateTime.Now);
		GameSetupItems setupItems = GameObject.Find("UIManager").GetComponent<GameSetupItems>();
		
		if (GamePlaySession.instance.GetInTutorialMode() && !GamePlaySession.instance.GetStoreTutorialClear() && !GamePlaySession.instance.GetNewGamePlus())
        {
			if (!GamePlaySession.instance.GetFirstShopCards())
            {
				GamePlaySession.instance.storeCards.Clear();
				foreach (int cardID in tutorialCardIDs)
                {
					GamePlaySession.instance.storeCards.Add(setupItems.GetCard(cardID).Clone());
				}
				GamePlaySession.instance.SetFirstShopCards(true);
			}
		}
        else
        {
			hasRare = false;
			hasUncommon = false;
			hasCommon = false;
			GamePlaySession.instance.storeCards.Clear();
			GamePlaySession.instance.storePurchases = new bool[5];
			GamePlaySession.instance.storeCards.Add(RollForCard());
			GamePlaySession.instance.storeCards.Add(RollForCard());
			GamePlaySession.instance.storeCards.Add(RollForCard());
			GamePlaySession.instance.storeCards.Add(RollForCard());
			GamePlaySession.instance.storeCards.Add(RollForCard());
			//GamePlaySession.instance.storeCards.Add (GamePlaySession.instance.GSI.wrestlers.cards [35].Clone ());
			//GamePlaySession.instance.storeCards.Add (GamePlaySession.instance.GSI.wrestlers.cards [36].Clone ());

			if (!GamePlaySession.instance.GetFirstShopCards())
            {
				GamePlaySession.instance.SetFirstShopCards(true);
			}
		}
		GamePlaySession.instance.storeCards.Sort((x, y) => string.Compare(x.name, y.name));
		foreach (var item in GamePlaySession.instance.storeCards) 
		{
			item.PostSerialize ();
		}
		storeCardScreen.currentCardIndex = 0;
		storeCardScreen.ShowCurrentCard();
	}
	
	public void BuyCard() 
	{
		storeCardScreen.swipeNode.enabled = true;
		CardObject card = storeCardScreen.GetCurrentCard();
		if (GamePlaySession.instance.GetMyCash() < card.cashCost || GamePlaySession.instance.GetMyTokens() < card.tokenCost) 
		{
			StartCoroutine(FlashMessage(notEnoughString));
		} 
		else 
		{
			GamePlaySession.instance.SetStoreCardsBought(GamePlaySession.instance.GetStoreCardsBought() + 1);
			GamePlaySession.instance.SetMyCash(GamePlaySession.instance.GetMyCash() - card.cashCost);
			List<CardObject> cards = new List<CardObject> ();
			cards.Add (card);

			GamePlaySession.instance.AddSeveralCardObjectsToList(cards,GamePlaySession.instance.myCards);
			GamePlaySession.instance.myCards.Add (card);
			GamePlaySession.instance.storePurchases[storeCardScreen.currentCardIndex] = true;
			storeCardScreen.ShowCurrentCard();

			UIManagerScript.instance.OnSaveAll ();

			// Play buy sound
			if (GameObject.Find("UIManager").GetComponent<UIManagerScript>().sfxEnabled)
				GetComponent<AudioSource>().Play();
		}
	}

	public void CountCards()
    {
		commonCardCount = 0;
		uncommonCardCount = 0;
		rareCardCount = 0;
		
		commonNonFoilCardCount = 0;
		uncommonNonFoilCardCount = 0;
		rareNonFoilCardCount = 0;
		
		foreach (StoreCard storeCard in storeLibrary)
        {
			bool addCard = true;
			
			// Check if we own this card or if it's already in our store list
			if (GamePlaySession.instance.CheckCard(storeCard.card.CardNumber) != null)
				addCard = false;
			
			foreach (CardObject checkCard in GamePlaySession.instance.storeCards)
            {
				if (checkCard.CardNumber == storeCard.card.CardNumber)
					addCard = false;
			}
			
			// Add card to our count if we should
			if (addCard)
            {
				switch (storeCard.rarity)
                {
				case "Common":
					commonCardCount += 1;
					
					if (storeCard.card.myCardType == CardType.WRESTLER) {
						WrestlerCardObject wrestler = (WrestlerCardObject) storeCard.card;
						if (wrestler.cardVersion.ToUpper() != "FOIL")
							commonNonFoilCardCount += 1;
					}
					break;
				case "Uncommon":
					uncommonCardCount += 1;
					
					if (storeCard.card.myCardType == CardType.WRESTLER) {
						WrestlerCardObject wrestler = (WrestlerCardObject) storeCard.card;
						if (wrestler.cardVersion.ToUpper() != "FOIL")
							uncommonNonFoilCardCount += 1;
					}
					break;
				case "Rare":
					rareCardCount += 1;
					
					if (storeCard.card.myCardType == CardType.WRESTLER) {
						WrestlerCardObject wrestler = (WrestlerCardObject) storeCard.card;
						if (wrestler.cardVersion.ToUpper() != "FOIL")
							rareNonFoilCardCount += 1;
					}
					break;
				}
			}
		}
	}

	public CardObject RollForCard()
    {
		CountCards();

		string rarity = "";
		bool hasCard = true;
		do {
			hasCard = true;
			int roll = Random.Range(1, 20);
			if (commonCardCount == 0 && uncommonCardCount == 0 && rareCardCount == 0) {
				// Fallback for when there are zero cards, so we know which placeholder to show
				if (hasCommon && hasUncommon)
                {
					rarity = "Rare";
				}
                else if (hasCommon && hasRare)
                {
					rarity = "Uncommon";
				}
                else if (hasUncommon && hasRare)
                {
					rarity = "Common";
				}
                else if (hasRare)
                {
					if (roll <= 10)
                    {
						rarity = "Common";
					}
                    else
                    {
						rarity = "Uncommon";
					}
				}
                else if (hasUncommon)
                {
					if (roll <= 10)
                    {
						rarity = "Common";
					}
                    else
                    {
						rarity = "Rare";
					}
				}
                else
                {
					if (roll <= 10)
                    {
						rarity = "Uncommon";
					}
                    else
                    {
						rarity = "Rare";
					}
				}
			}
            else
            {
				// Perform standard rarity rolls
				if (roll <= 1)
                {
					// Rare
					rarity = "Rare";
					
					if (rareCardCount == 0 && hasRare)
                    {
						if (GamePlaySession.instance.storeCards.Count < 2)
                        {
							hasCard = false;
						}
                        else
                        {
							if (uncommonCardCount > 0)
								hasCard = false;
						}
					}
				}
                else if (roll <= 4)
                {
					// Uncommon
					rarity = "Uncommon";
					
					if (uncommonCardCount == 0 && hasUncommon)
                    {
						if (GamePlaySession.instance.storeCards.Count < 2)
                        {
							hasCard = false;
						}
                        else
                        {
							if (rareCardCount > 0)
								hasCard = false;
						}
					}
				}
                else
                {
					// Common
					rarity = "Common";
					
					if (GamePlaySession.instance.storeCards.Count == 2 && !(hasUncommon || hasRare))
						hasCard = false;
					
					if (commonCardCount == 0 && hasCommon)
						hasCard = false;
				}
			}
		} while (!hasCard);

		switch (rarity)
        {
		case "Common":
			hasCommon = true;
			break;
		case "Uncommon":
			hasUncommon = true;
			break;
		case "Rare":
			hasRare = true;
			break;
		}

		// Determine if we should allow foil cards or not
		bool allowFoil = false;

		switch (rarity)
        {
		case "Common":
			allowFoil = (Random.Range(0, 30) <= 15);

			if (commonNonFoilCardCount == 0)
            {
				allowFoil = true;
			}
			break;
		case "Uncommon":
			allowFoil = (Random.Range(0, 30) <= 5);
			
			if (uncommonNonFoilCardCount == 0)
            {
				allowFoil = true;
			}
			break;
		case "Rare":
			allowFoil = (Random.Range(0, 30) <= 1);
			
			if (rareNonFoilCardCount == 0)
            {
				allowFoil = true;
			}
			break;
		}

		List<CardObject> cardList = new List<CardObject>();

		foreach (StoreCard storeCard in storeLibrary)
        {
			bool addCard = true;

			// If this card doesn't match the rarity we're searching for or we own it/have it in our store cards already, don't add it
			if (storeCard.rarity != rarity)
				addCard = false;
			if (GamePlaySession.instance.CheckCard(storeCard.card.CardNumber) != null)
				addCard = false;
			if (UIManagerScript.instance.transferCardNumbers.Contains(storeCard.card.CardNumber))
				addCard = false;
			foreach (CardObject checkCard in GamePlaySession.instance.storeCards) 
			{
				if (checkCard.CardNumber == storeCard.card.CardNumber)
					addCard = false;
			}

			// If this card is a wrestler foil and we're not currently allowing foil cards, don't add it
			if (storeCard.card.myCardType == CardType.WRESTLER)
            {
				WrestlerCardObject wrestler = (WrestlerCardObject) storeCard.card;
                if (wrestler.cardVersion.ToUpper() == "FOIL")
                {
                    if (!allowFoil)
                    {
                        addCard = false;
                    }
                    else if (GamePlaySession.instance.CheckCard(storeCard.card.CardNumber - 1) == null)
                    {
                        addCard = false;
                    }   
                }
			}

			// Add card to our list if we should
			if (addCard)
				cardList.Add(storeCard.card);
		}
		
		CardObject card = new CardObject();

		if (cardList.Count > 0)
        {
            // If we have cards to select from, let's do that!
            int c = 0;
            foreach (var item in cardList)
            {
                if (item.myCardType == CardType.WRESTLER)
                {
                    WrestlerCardObject w = (WrestlerCardObject)item;
                    if (w.cardVersion == "FOIL")
                    {
                        c++;
                    }
                }
            }

            card = cardList[Random.Range(0, cardList.Count)].Clone();
		} else {
			card.name = "Placeholder";
			// If we have no cards to show, player must own them all, so create placeholder card
			switch (rarity) {
			case "Common":
				card.myCardType = CardType.SHOP_COMMON;
				break;
			case "Uncommon":
				card.myCardType = CardType.SHOP_UNCOMMON;
				break;
			case "Rare":
				card.myCardType = CardType.SHOP_RARE;
				break;
			}
			card.cashCost = 0;
			card.tokenCost = 0;
		}
		return card;
	}
	
	public void BuyTokens(int tokenAmount) {
		string productID = "";
		
		switch (tokenAmount) 
		{
		case 1:
			#if UNITY_ANDROID
			productID = "com.cc.mania.token1";
			#elif UNITY_IPHONE
			productID = "com.CheckmateCreative.mania.1token";
			#endif
			break;
		case 5:
			#if UNITY_ANDROID
			productID = "com.cc.mania.token5";
			#elif UNITY_IPHONE
			productID = "com.CheckmateCreative.mania.5token";
			#endif
			break;
		case 11:
			#if UNITY_ANDROID
			productID = "com.cc.mania.token11";
			#elif UNITY_IPHONE
			productID = "com.CheckmateCreative.mania.11token";
			#endif
			break;
        case 22:
			#if UNITY_ANDROID
			productID = "com.cc.mania.token22";
			#elif UNITY_IPHONE
			productID = "com.CheckmateCreative.mania.22token";
			#endif
			break;
		}
		
		if (!string.IsNullOrEmpty(productID)) 
		{
			#if UNITY_IPHONE || UNITY_ANDROID
			IAP.purchaseConsumableProduct( productID, ( didSucceed, error ) => 
				{
				Debug.Log( "purchasing product " + productID + " result: " + didSucceed );
				
				if( !didSucceed ) 
				{
					Debug.Log( "purchase error: " + error );
				} 
					else 
					{
					Debug.Log( "purchased " + tokenAmount.ToString() + " tokens");
					GamePlaySession.instance.SetMyTokens(GamePlaySession.instance.GetMyTokens() + tokenAmount);
				}
			});
			#endif
		}
		
		UpdateWallet();
	}
	
	public void TradeTokens(int cashAmount) 
	{
		if (GamePlaySession.instance.GetMyTokens() >= 1) 
		{
			GamePlaySession.instance.SetMyTokens(GamePlaySession.instance.GetMyTokens() - 1);
			GamePlaySession.instance.SetMyCash(GamePlaySession.instance.GetMyCash() + cashAmount);
			UIManagerScript.instance.OnSaveAll ();
		} 
		else 
		{
			StartCoroutine(FlashMessage(notEnoughString));
		}
	}	
	
	public void RefreshStore()
    {
		// Check if we should clear the store tutorial
		if (!GamePlaySession.instance.GetStoreTutorialClear())
        {
			GamePlaySession.instance.SetStoreTutorialClear(true);
			if (GamePlaySession.instance.GetMyTokens() >= 1)
            {
				GamePlaySession.instance.SetMyTokens(GamePlaySession.instance.GetMyTokens() - 1);
			}
			SelectNewCards();
			SwitchStorePage(0);
		}
        else
        {
			if (GamePlaySession.instance.GetMyTokens() >= 1)
            {
				GamePlaySession.instance.SetMyTokens(GamePlaySession.instance.GetMyTokens() - 1);
				SelectNewCards();
				SwitchStorePage(0);
			}
            else
            {
				StartCoroutine(FlashMessage(notEnoughString));
			}
		}
	}
	
	public void CloseStore() {
		GameObject.Find("UIManager").GetComponent<UIManagerScript>().GoToCalendar(false);
	}
	
	public IEnumerator FlashMessage(GameObject messageString){
		messageString.SetActive(true);
		yield return new WaitForSeconds (3f);		
		messageString.SetActive(false);
	}
	
	public void OpenShopTutorial() {
		if (tutorialPopUp != null && !string.IsNullOrEmpty(tutorialPopUp.message) && !GamePlaySession.instance.GetNewGamePlus()) {
			PopUpWindow popUpWindow = GameObject.Find("UIManager").GetComponent<UIManagerScript>().popUpWindow;
			
			if (popUpWindow != null) {
				popUpWindow.OpenPopUpWindow(new PopUpMessage[] {tutorialPopUp}, thisPanel);
			}
			
			SwitchStorePage(1);
		}
	}
}
