﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Prime31;

public class StoreScreenScript : MonoBehaviour {
	public int currentCardIndex = 0;
	public CardView cardViewer;
	public SwipeInteraction swipeNode;
	public PlaceholderCard placeholderCard;
    public static StoreScreenScript instance;
    //public List<CardObject> cardList = new List<CardObject>();

    private void Awake()
    {
        instance = this;
    }

    public void ShowCurrentCard()
    {
		List<CardObject> cardList = GamePlaySession.instance.storeCards;

		if (cardList.Count > 0) 
		{
			if (cardList[currentCardIndex].name == "Placeholder") 
			{
				// This is a placeholder card, so display the placeholder card object
				cardViewer.gameObject.SetActive(false);
				placeholderCard.gameObject.SetActive(true);

				// Set the message for the placeholder object
				switch (cardList[currentCardIndex].myCardType) {
				case CardType.SHOP_COMMON:
					placeholderCard.messageText.text = placeholderCard.commonMessage;
					break;
				case CardType.SHOP_UNCOMMON:
					placeholderCard.messageText.text = placeholderCard.uncommonMessage;
					break;
				case CardType.SHOP_RARE:
					placeholderCard.messageText.text = placeholderCard.rareMessage;
					break;
				}
			} 
			else 
			{
				// This is a normal card, so enable the card viewer and show it
				placeholderCard.gameObject.SetActive(false);
				cardViewer.gameObject.SetActive(true);
				cardViewer.ShowCard(cardList[currentCardIndex].Clone());
			}
		}
	}

	public CardObject GetCurrentCard()
    {
        //Debug.Log("Umar:: index : " + currentCardIndex.ToString());
        //Debug.Log("Umar:: index : " + GamePlaySession.instance.storeCards.Count.ToString());
        if (currentCardIndex < 0)
            return null;
        else
		    return GamePlaySession.instance.storeCards[currentCardIndex];
	}
	
	public void UserSwipeRight() 
	{
		int modNum = GamePlaySession.instance.storeCards.Count;
		currentCardIndex--;
		
		if (currentCardIndex < 0) 
		{
			currentCardIndex = modNum - 1;
		} 
		else 
		{
			currentCardIndex = currentCardIndex % modNum;
		}
		ShowCurrentCard();
	}
	
	public void UserSwipeLeft() {
		int modNum = GamePlaySession.instance.storeCards.Count;
		currentCardIndex++;
		currentCardIndex = currentCardIndex % modNum;
		
		ShowCurrentCard();
	}
}
