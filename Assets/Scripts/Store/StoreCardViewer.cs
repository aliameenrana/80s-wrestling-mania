﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StoreCardViewer : MonoBehaviour
{
    
	public StoreScreenScript storeCardScreen;
	public Button cardBuyButton;

	public Text cashText;
	public Text tokenText;

	public CardView card;

    

    void Update() {
		if (card != null && card.myCard != null && GamePlaySession.instance.storeCards != null
            && GamePlaySession.instance.storeCards.Count>0) {
			// Make sure the card is the currently selected one
			if (card.myCard.CardNumber != storeCardScreen.GetCurrentCard().CardNumber) {
				card.myCard = storeCardScreen.GetCurrentCard();
			}

			// Output the cost of the card to the info panel
			if (cashText != null)
				cashText.text = card.myCard.cashCost.ToString();
			if (tokenText != null)
				tokenText.text = card.myCard.tokenCost.ToString();

			// If this card is a placeholder, disable the buy button
			if (card.myCard.name == "Placeholder") {
				cardBuyButton.interactable = false;
			} else {
				// Update button based on if we can afford the current card or if we already bought it
				if (GamePlaySession.instance.storePurchases.Length > storeCardScreen.currentCardIndex) {
					if (GamePlaySession.instance.storePurchases[storeCardScreen.currentCardIndex]) {
						cardBuyButton.interactable = false;
					} else {
						if (GamePlaySession.instance.GetMyCash() < card.myCard.cashCost || GamePlaySession.instance.GetMyTokens() < card.myCard.tokenCost) {
							cardBuyButton.interactable = false;
						} else {
							cardBuyButton.interactable = true;
						}
					}
				} else {
					cardBuyButton.interactable = true;
				}
			}
		} else {
			if (cashText != null)
				cashText.text = "0";
			if (tokenText != null)
				tokenText.text = "0";
			cardBuyButton.interactable = false;
		}
	}
}
