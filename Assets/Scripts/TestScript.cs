﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TestScript : MonoBehaviour
{
	public string BundleURL;
	public string AssetName;
	public int version;

	void Start()
	{
//		StartCoroutine (DownloadAndCache());
		Debug.Log("deviceModel : " + SystemInfo.deviceModel);
		Debug.Log("graphicsDeviceName : " + SystemInfo.graphicsDeviceName);
		Debug.Log("graphicsDeviceType : " + SystemInfo.graphicsDeviceType);
		Debug.Log("graphicsDeviceVendor : " + SystemInfo.graphicsDeviceVendor);
		Debug.Log("graphicsDeviceVersion : " + SystemInfo.graphicsDeviceVersion);
		Debug.Log("graphicsMemorySize : " + SystemInfo.graphicsMemorySize);
		Debug.Log("operatingSystem : " + SystemInfo.operatingSystem);
		Debug.Log("processorCount : " + SystemInfo.processorCount);
		Debug.Log("processorFrequency : " + SystemInfo.processorFrequency);
		Debug.Log("processorType : " + SystemInfo.processorType);
		Debug.Log("systemMemorySize : " + SystemInfo.systemMemorySize);
	}

	IEnumerator DownloadAndCache ()
	{
		// Wait for the Caching system to be ready
		while (!Caching.ready)
			yield return null;

		// Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
		using(WWW www = WWW.LoadFromCacheOrDownload ("https://s3-ap-southeast-1.amazonaws.com/90wm/90wmbundle", 1))
		{
			while( !www.isDone )
			{
				Debug.Log("Download : " + www.progress.ToString());
				yield return null;
			}

//			yield return www;
			if (www.error != null)
				throw new Exception("WWW download had an error:" + www.error);
			AssetBundle bundle = www.assetBundle;
			Debug.Log(bundle.name);

			string[] str = bundle.GetAllAssetNames();
//			Sprite[] sp = bundle.LoadAllAssets<Sprite>();
			foreach(string st1 in str)
				Debug.Log(st1);

			Debug.Log("Umar:: " + bundle.LoadAsset<TextAsset>("wrestler.xml"));

//			if (AssetName == "")
//				Instantiate(bundle.mainAsset);
//			else
//				Instantiate(bundle.LoadAsset(AssetName));

			// Unload the AssetBundles compressed contents to conserve memory
			bundle.Unload(false);

		} // memory is freed from the web stream (www.Dispose() gets called implicitly)
	}
}
