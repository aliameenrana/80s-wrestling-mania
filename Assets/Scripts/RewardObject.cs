﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardObject
{
	public int cash;
	public int tokens;
	public List<int> cards;


	public RewardObject()
	{
		cash = 0;
		tokens = 0;
		cards = new List<int> ();
	}

}
