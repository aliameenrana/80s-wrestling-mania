﻿using UnityEngine;
using System.Collections;

public class CalendarGenerator : MonoBehaviour 
{
	public int numberOfYears; /* The number of years to generate */
	public int nowYouKnowShowType;  /* This is the ID of the Now You Know segments */
	public int tutorialWeek1ShowType; /* This is the ID of the show for tutorial week 1 */
	public int tutorialWeek2ShowType; /* This is the ID of the show for tutorial week 2 */
	public int tutorialWeek3ShowType; /* This is the ID of the show for tutorial week 3 */
	public int tutorialWeek4ShowType; /* This is the ID of the show for tutorial week 4 */
	public int[] week1ShowTypes; /* These are general show type IDs and will be selected from at random */
	public int[] week2ShowTypes; /* These are general show type IDs and will be selected from at random */
	public int[] week3ShowTypes; /* These are general show type IDs and will be selected from at random */
	public int[] week4ShowTypes; /* These are P4V show type IDs and will be selected based on month number */

	public CalendarGenerator()
	{
		numberOfYears = 20; /* The number of years to generate */
		nowYouKnowShowType = 4;  /* This is the ID of the Now You Know segments */
		tutorialWeek1ShowType =0; /* This is the ID of the show for tutorial week 1 */
		tutorialWeek2ShowType =1; /* This is the ID of the show for tutorial week 2 */
		tutorialWeek3ShowType =2; /* This is the ID of the show for tutorial week 3 */
		tutorialWeek4ShowType =3; /* This is the ID of the show for tutorial week 4 */
		week1ShowTypes = new int[3]{ 5, 6, 7 }; /* These are general show type IDs and will be selected from at random */
		week2ShowTypes = new int[3]{ 8, 9, 10 }; /* These are general show type IDs and will be selected from at random */
		week3ShowTypes = new int[3]{ 11, 12, 13 }; /* These are general show type IDs and will be selected from at random */
		week4ShowTypes = new int[12]{ 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 }; /* These are P4V show type IDs and will be selected based on month number */
	}

}
