﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeekDisplay : MonoBehaviour {
	public WeekItem week;
	
	public Image showIcon;
	public Text weekName;
	
	public Text showName;
	public Text showLockStatus;	
	
	public Text showEarnings;
	public Image showRating;
	
	public Button weekButton;
	
	private GameObject UIManager;
	private GameObject playerHUD;

    public static WeekDisplay instance;

    void Awake()
    {
        instance = this;
    }

	void OnEnable()
	{
		
	}

	public void OpenShowSetup()
    {
        UIManager = GameObject.Find("UIManager");
		playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;

		ShowObject show = week.GetShow();
        show.CurrentWeek = week.weekNum;

        //Debug.Log("Week name: " + week.weekName + "Week Number: " + week.weekNum);


		Animator newScreen = null;

        if (show.showName == "Now You Know")
        {
            Animator nowYouKnowScreen = UIManager.GetComponent<UIManagerScript>().NowYouKnowScreen;

            if (nowYouKnowScreen == null)
                return;

            NowYouKnow nowYouKnow = nowYouKnowScreen.GetComponent<NowYouKnow>();
            nowYouKnow.OpenNowYouKnowPanel(this.week);
        }
        else
        {
			if (show.showFinished)
            {
                Animator resultsScreen = UIManager.GetComponent<UIManagerScript>().ShowResultsScreen;

				GameObject go = resultsScreen.gameObject.transform.Find ("ButtonTitleRow").Find("BackWindow").GetChild(0).gameObject;
				go.GetComponent<Button> ().onClick.RemoveAllListeners ();
				GameObject.FindObjectOfType<ThemeController> ().ChangeSprite (go, false);

				if (resultsScreen == null)
					return;

				ShowResults showResults = resultsScreen.gameObject.GetComponent<ShowResults>();
				showResults.SetResultsData(this.week);
				showResults.oldResult = true;

				if (playerHUD != null)
					playerHUD.SetActive(false);

				if (ScreenHandler.current != null && resultsScreen != null)
					ScreenHandler.current.OpenPanel(resultsScreen);

				newScreen = resultsScreen;
			} 
			else 
			{
				Animator setupScreen = UIManager.GetComponent<UIManagerScript>().ShowSetupScreen;

				if (setupScreen == null){
					return;
				}
				
				ShowSetup showSetup = setupScreen.GetComponent<ShowSetup>();
				showSetup.OpenShowSetupPanel(this.week);

				newScreen = setupScreen;

                //GameObject ShowImage = GameObject.Find("ShowImage");
                //Debug.Log(ShowImage.GetComponent<Image>().sprite.name);
            }
		}

		if (!show.showFinished) {
            //if (show.showPopUp != null && !string.IsNullOrEmpty(show.showPopUp.message) && !show.popUpClear && !GamePlaySession.instance.newGamePlus) {
			if (show.showPopUp != null && !string.IsNullOrEmpty(show.showPopUp.message) && !show.popUpClear && GamePlaySession.instance.GetInTutorialMode())
            {
                PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
				
				if (popUpWindow != null) {
					popUpWindow.OpenPopUpWindow(new PopUpMessage[] {show.showPopUp}, newScreen);
				}

				show.popUpClear = true;
			}
		}
	}
	
	public void UpdateShowDisplay() {
		UIManager = GameObject.Find("UIManager");
		ShowObject show = week.GetShow();
		
		if (show != null) {
			if (weekName != null) {
				if (show.showName == "Now You Know")
                {
                    if (show.AwardShow)
                    {
                        weekName.text = "Year End Awards!";
                    }
                    else
                    {
                        weekName.text = "Now You Know!";
                    }

                } else
                {
					weekName.text = week.weekName;
				}
			}
			
			if (showIcon != null) {
				ShowIcons showIcons = UIManager.GetComponent<ShowIcons>();
				if (showIcons != null) {
					if(show.isP4V){
						showIcon.sprite = showIcons.p4vIcon;
					}
					else{
						showIcon.sprite = showIcons.defaultIcon;
					}
					if (showIcons.showIconsets.Length >= week.showTypeID) {
						ShowIconset iconset = showIcons.showIconsets[week.showTypeID];
                        
                        if (iconset.showIcon != null)
                        {
                            showIcon.sprite = iconset.showIcon;
                        }
                    }
				}
			}
			
			if (showName != null) {
				if (!string.IsNullOrEmpty(show.displayTitle)) {
					showName.text = show.displayTitle;
					showName.gameObject.SetActive(true);
				} else {
					showName.gameObject.SetActive(false);
				}
			}
			
			if (show.showResults && show.showFinished) {
				// Set the results read out
				if (showEarnings != null) {
					showEarnings.gameObject.SetActive(true);
					showEarnings.text = "Earned $" + show.showEarnings.ToString();
				}
				if (showRating != null) {
					showRating.transform.parent.gameObject.SetActive(true);
					showRating.fillAmount = show.GetStarRating();
				}
			} else {
				if (showEarnings != null)
					showEarnings.gameObject.SetActive(false);
				if (showRating != null)
					showRating.transform.parent.gameObject.SetActive(false);
			}
			
			if (showLockStatus != null) {
				if (show.showFinished) {
					showLockStatus.text = "In The Books!";
				} else {
					if (weekButton.interactable) {
						showLockStatus.text = "Up Next!";
					} else {
						showLockStatus.text = "Locked!";
					}
				}
			}
		}
	}
}
