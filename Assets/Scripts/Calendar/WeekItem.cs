﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WeekItem {
    public static WeekItem instance;
	public int weekNum;
	public string weekName;

	public int showID;  // ID of the entry in myShows array.  Serialized
	public int showTypeID; // ID of the show type template to use

    void Awake()
    {
        instance = this;
    }

	public ShowObject GenerateShow() {
		int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName);
        ShowObject ThisShow = GameObject.Find("UIManager").GetComponent<GameSetupItems>().ShowTypes[showTypeID].Clone();

		if (showTypeID == 4 && (GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].months[GamePlaySession.instance.GetCurrentMonth()].monthNum==11))
        {
            ThisShow.AwardShow = true;
        }

        if (ThisYear >= 1990)
        {
            if (ThisShow.isP4V == false)
            {
                if (ThisShow.displayTitle == "Superstar Saturday")
                {
                    ThisShow.displayTitle = "Monday Night Live";
                }
                else if (ThisShow.displayTitle == "Sunday Night Slam")
                {
                    ThisShow.displayTitle = "Sports Entertainment";
                }
                else if (ThisShow.displayTitle == "Weekend Wrasslin'")
                {
                    ThisShow.displayTitle = "Weekend Attitude";
                }
            }
        }
        
        return ThisShow;
	}

	public ShowObject GetShow() {
		ShowObject show;
        if (GamePlaySession.instance != null && showID < GamePlaySession.instance.myShows.Count)
        {
            show = GamePlaySession.instance.myShows[showID];
		}
        else
        {
            show = GenerateShow();
		}

		return show;
	}

	public WeekItem(){

	}
}