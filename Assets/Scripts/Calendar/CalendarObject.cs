﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CalendarObject
{

	public static CalendarObject instance;
	public YearItem[] years;

	void Awake()
	{
		instance = this;
	}

	public CalendarObject()
	{
		
	}

	public int GetNumberOfYears() 
	{
		return 20;
	}

	public CalendarObject GenerateCalendar() 
	{
		int numberOfYears = 20; /* The number of years to generate */
		int nowYouKnowShowType = 4;  /* This is the ID of the Now You Know segments */
		int tutorialWeek1ShowType =0; /* This is the ID of the show for tutorial week 1 */
		int tutorialWeek2ShowType =1; /* This is the ID of the show for tutorial week 2 */
		int tutorialWeek3ShowType =2; /* This is the ID of the show for tutorial week 3 */
		int tutorialWeek4ShowType =3; /* This is the ID of the show for tutorial week 4 */
		int [] week1ShowTypes = new int[3]{ 5, 6, 7 }; /* These are general show type IDs and will be selected from at random */
		int [] week2ShowTypes = new int[3]{ 8, 9, 10 }; /* These are general show type IDs and will be selected from at random */
		int [] week3ShowTypes = new int[3]{ 11, 12, 13 }; /* These are general show type IDs and will be selected from at random */
		int [] week4ShowTypes = new int[12]{ 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 }; /* These are P4V show type IDs and will be selected based on month number */


			// Create a tracking number to generate unique show IDs from
			int showNumber = 0;

		years = new YearItem[numberOfYears];
		for (int i = 0;i < numberOfYears;i++) 
		{
				// Create a new year entry
				years[i] = new YearItem();
				years[i].yearNum = i;
                years[i].yearName = (1980 + (i % 20)).ToString();

                // Generate months for this year
                years[i].months = new MonthItem[12];
				for (int r = 0;r < 12;r++) {
					years[i].months[r] = new MonthItem();
					years[i].months[r].monthNum = r;

					// Pick what the month name should be
					switch (r) {
					case 0:
						years[i].months[r].monthName = "Jan";
						break;
					case 1:
						years[i].months[r].monthName = "Feb";
						break;
					case 2:
						years[i].months[r].monthName = "Mar";
						break;
					case 3:
						years[i].months[r].monthName = "Apr";
						break;
					case 4:
						years[i].months[r].monthName = "May";
						break;
					case 5:
						years[i].months[r].monthName = "Jun";
						break;
					case 6:
						years[i].months[r].monthName = "Jul";
						break;
					case 7:
						years[i].months[r].monthName = "Aug";
						break;
					case 8:
						years[i].months[r].monthName = "Sep";
						break;
					case 9:
						years[i].months[r].monthName = "Oct";
						break;
					case 10:
						years[i].months[r].monthName = "Nov";
						break;
					case 11:
						years[i].months[r].monthName = "Dec";
						break;
					}

					// Generate weeks for this month
					years[i].months[r].weeks = new WeekItem[5];
					for (int k = 0;k < 5;k++) {
						years[i].months[r].weeks[k] = new WeekItem();
						years[i].months[r].weeks[k].weekNum = k;

						// Set the unique ID for this show
						years[i].months[r].weeks[k].showID = showNumber;
						showNumber += 1;

						if (k == 3) {
							// This should be a Now You Know show
							years[i].months[r].weeks[k].weekName = "Now You Know!";
							years[i].months[r].weeks[k].showTypeID = nowYouKnowShowType;
						} else {
							// Generate our week name based on the week number (accounting for the Now You Know show
							string weekDisplayValue = (k > 3) ? (k).ToString() : (k + 1).ToString();
							years[i].months[r].weeks[k].weekName = "Week " + weekDisplayValue;
							if (i == 0 && r == 0) {
								// This is the tutorial month
								switch (k) {
								case 0:
									years[i].months[r].weeks[k].showTypeID = tutorialWeek1ShowType;
									break;
								case 1:
									years[i].months[r].weeks[k].showTypeID = tutorialWeek2ShowType;
									break;
								case 2:
									years[i].months[r].weeks[k].showTypeID = tutorialWeek3ShowType;
									break;
								case 4:
									years[i].months[r].weeks[k].showTypeID = tutorialWeek4ShowType;
									break;
								}
							} else {
								// This is a standard month so randomly select the shows
								switch (k) {
								case 0:
									int week1Roll = Random.Range(0, week1ShowTypes.Length);
									years[i].months[r].weeks[k].showTypeID = week1ShowTypes[week1Roll];
									break;
								case 1:
									int week2Roll = Random.Range(0, week2ShowTypes.Length);
									years[i].months[r].weeks[k].showTypeID = week2ShowTypes[week2Roll];
									break;
								case 2:
									int week3Roll = Random.Range(0, week3ShowTypes.Length);
									years[i].months[r].weeks[k].showTypeID = week3ShowTypes[week3Roll];
									break;
								case 4:
									int week4Roll = Mathf.Min(r, week4ShowTypes.Length - 1);
									years[i].months[r].weeks[k].showTypeID = week4ShowTypes[week4Roll];
									break;
								}
							}
						}
					}
				}
			}

		return this;
	}
}
