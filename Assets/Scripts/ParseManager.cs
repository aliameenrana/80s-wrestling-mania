﻿using System.Collections.Generic;
using UnityEngine;
using Parse;
using System;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;


public class ParseManager : MonoBehaviour
{

	const string MatchEmailPattern =
		@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

	/// <summary>
	/// Checks whether the given Email-Parameter is a valid E-Mail address.
	/// </summary>
	/// <param name="email">Parameter-string that contains an E-Mail address.</param>
	/// <returns>True, when Parameter-string is not null and 
	/// contains a valid E-Mail address;
	/// otherwise false.</returns>


	public static void ResetPassword(string email,Action<bool,string> callBack = null)
	{
		if (IsEmail (email)) 
		{
			ParseUser.RequestPasswordResetAsync (email).ContinueWith(task =>
					{
						if (task.Exception!=null)
						{
							callBack(false,"Password Reset Failed");
						}
						else
						{
							callBack(true,"");
						}
					});
		} 
		else 
		{
			callBack(false,"Invalid email address");
		}
	}

	static bool IsEmail(string email)
	{
		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
		else return false;
	}


	public static void RegisterUser(string username, string password, Action<bool,string,string,string> callback = null)
	{
		var user = new ParseUser()
		{
			Username = username.ToLower(),
			Password = password
		};

		user.SignUpAsync().ContinueWith(task => {

			if(task.Exception != null)
			{
				Debug.Log("Register Exception : " + task.Exception.InnerExceptions[0].Message);
				if(callback != null)
					callback(false,username,password,task.Exception.InnerExceptions[0].Message.ToString() );
				
            }
			else
			{
				var parseObj = new ParseObject("GameSession");
				parseObj["userID"] = ParseUser.CurrentUser.ObjectId;

				parseObj.SaveAsync().ContinueWith(secondTask => {
					if (secondTask.IsFaulted || secondTask.IsCanceled)
					{
						IEnumerable<Exception> exceptions = secondTask.Exception.InnerExceptions;

						foreach (var exception in exceptions)
						{
							Debug.Log("SignUp User error : " + exception.Message);
						}
						if(callback != null)
							callback(false,username,password,task.Exception.InnerExceptions[0].Message.ToString());
					}
					else
					{
						Debug.Log("User Registeration Successful");
						if(callback != null)
							callback(true,username,password,"");
					}
				});
            }
		});
	}

	public static void LogInUser(string username, string password, Action<bool,string> callback = null)
	{
		if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
			return;

		ParseUser.LogInAsync(username.ToLower(), password).ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;
				string exceptionMessage="";
				foreach (var exception in exceptions)
				{
					Debug.Log("LogIn User error : " + exception.Message);
					exceptionMessage = exception.Message;
				}
				if(callback != null)
					callback(false,exceptionMessage);
			}
			else
			{
				// Login was successful.
				Debug.Log("User LogIn Successful");
				if(callback != null)
					callback(true,"User Login Successful");
			}
		});
	}
	
	public static void SaveFirebaseToken(string token, Action<bool> callback = null)
	{
		if(string.IsNullOrEmpty(ParseUser.CurrentUser.ObjectId))
		{
		   Debug.Log("User Session doesnot exist. Register or LogIn user");
		   if(callback != null)
		    callback(false);

		   return;
		}

	  	var parseObj = ParseUser.CurrentUser;
	  	parseObj["firebaseToken"] = token;
	  	parseObj.SaveAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;

				foreach (var exception in exceptions)
				{
					Debug.Log("Saving firebase token error " + exception.Message);
				}
				if(callback != null)
					callback(false);
			}
			else
			{
				Debug.Log("firebase token saved successfully: ");
				if(callback != null)
					callback(true);
			}
		});
	 }
	


	public static void LogOutUser(Action<bool> callback = null)
	{
		ParseUser.LogOutAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;

				foreach (var exception in exceptions)
				{
					Debug.Log("LogOut User error : " + exception.Message);
				}
				if(callback != null)
					callback(false);
			}
			else
			{
				// Login was successful.
				Debug.Log("User LogOut Successful... Session deleted...");
				if(callback != null)
					callback(true);
			}
		});
	}

	public static void UpdateGameSessionData(string key, object value, Action<bool> callback = null)
	{
		if (String.IsNullOrEmpty(ParseUser.CurrentUser.ObjectId))
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(callback != null)
				callback(false);

			return;
		}


		var query = ParseObject.GetQuery("GameSession").WhereEqualTo("userID", ParseUser.CurrentUser.ObjectId);
		query.FirstAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;

				foreach (var exception in exceptions)
				{
					Debug.Log("Getting GameSessionData error " + exception.Message);
				}
				if(callback != null)
					callback(false);
			}
			else
			{
				ParseObject parseObj = task.Result;
				parseObj[key] = value;

				parseObj.SaveAsync().ContinueWith(secondTask => {
					if (secondTask.IsFaulted || secondTask.IsCanceled)
					{
						IEnumerable<Exception> exceptions = secondTask.Exception.InnerExceptions;

						foreach (var exception in exceptions)
						{
							Debug.Log("Updating GameSessionData error " + exception.Message);
						}
						if(callback != null)
							callback(false);
					}
					else
					{
						Debug.Log("GameSessionData Updated Successfully: " + key);
						if(callback != null)
							callback(true);
					}
				});
			}
		});
	}

	public static void UpdateGameSessionData(Dictionary<string, object> data, Action<bool> callback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(callback != null)
				callback(false);
			
			return;
		}

		var query = ParseObject.GetQuery("GameSession").WhereEqualTo("userID", ParseUser.CurrentUser.ObjectId);
		query.FirstAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;

				foreach (var exception in exceptions)
				{
					Debug.Log("Getting GameSessionData error " + exception.Message);
				}
				if(callback != null)
					callback(false);
			}
			else
			{
				ParseObject parseObj = task.Result;
				foreach(var item in data)
				{
					parseObj[item.Key] = item.Value;
				}

				parseObj.SaveAsync().ContinueWith(secondTask => {
					if (secondTask.IsFaulted || secondTask.IsCanceled)
					{
						IEnumerable<Exception> exceptions = secondTask.Exception.InnerExceptions;

						foreach (var exception in exceptions)
						{
							Debug.Log("Updating GameSessionData error " + exception.Message);
						}
						if(callback != null)
							callback(false);
					}
					else
					{
						Debug.Log("GameSessionData Updated Successfully ");
						if(callback != null)
							callback(true);
					}
				});
			}
		});
	}

	public static void UpdateOrCreateWrestlerCardData(int wrestlerID, List<int> stats, Action<bool> callback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(callback != null)
				callback(false);

			return;
		}

		var query = ParseObject.GetQuery("WrestlerCardData").WhereEqualTo("userID", ParseUser.CurrentUser.ObjectId).WhereEqualTo("wrestlerID", wrestlerID);
		query.FirstAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				var parseObj = new ParseObject("WrestlerCardData");
				parseObj["userID"] = ParseUser.CurrentUser.ObjectId;
				parseObj["wrestlerID"] = wrestlerID;
				parseObj["stats"] = stats;

                parseObj.SaveAsync().ContinueWith(secondTask => {
					if (secondTask.IsFaulted || secondTask.IsCanceled)
					{
						IEnumerable<Exception> exceptions = secondTask.Exception.InnerExceptions;

						foreach (var exception in exceptions)
						{
							Debug.Log("Create WrestlerCardData error : " + exception.Message);
						}
						if(callback != null)
							callback(false);
					}
					else
					{
						Debug.Log("WrestlerCardData Created Successfully");
						if(callback != null)
							callback(true);
					}
				});
			}
			else
			{
				ParseObject parseObj = task.Result;
				parseObj["stats"] = stats;

				parseObj.SaveAsync().ContinueWith(secondTask => {
					if (secondTask.IsFaulted || secondTask.IsCanceled)
					{
						IEnumerable<Exception> exceptions = secondTask.Exception.InnerExceptions;

						foreach (var exception in exceptions)
						{
							Debug.Log("Update WrestlerCardData error : " + exception.Message);
						}
						if(callback != null)
							callback(false);
					}
					else
					{
						Debug.Log("WrestlerCardData Updated Successfully");
						if(callback != null)
							callback(true);
					}
				});
			}
		});
	}

	public static void UpdateOrCreateWrestlerCardData(Dictionary<int, List<int>> data, Action<bool> callback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(callback != null)
				callback(false);

			return;
		}

		var query = ParseObject.GetQuery("WrestlerCardData").
			WhereEqualTo("userID", ParseUser.CurrentUser.ObjectId).WhereContainedIn("wrestlerID", data.Keys).Limit(1000);
		query.FindAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;
				foreach (var exception in exceptions)
				{
					Debug.Log("Create WrestlerCardData error : " + exception.Message);
				}
				if(callback != null)
					callback(false);
			}
			else
			{
				var parseObjs = new List<ParseObject>();
				var existingObjs = new List<int>();
				int wrestlerID = 0;

				foreach(var item in task.Result)
				{
					wrestlerID = item.Get<int>("wrestlerID");
					item["stats"] = data[wrestlerID];
					existingObjs.Add(wrestlerID);
					parseObjs.Add(item);
				}

				foreach(var item in data)
				{
					if(existingObjs.Contains(item.Key))
						continue;
					
					var obj = new ParseObject("WrestlerCardData");
					obj["userID"] = ParseUser.CurrentUser.ObjectId;
					obj["wrestlerID"] = item.Key;
					obj["stats"] = item.Value;
					parseObjs.Add(obj);
				}

				ParseObject.SaveAllAsync(parseObjs).ContinueWith(secondTask =>{
					if (secondTask.IsFaulted || secondTask.IsCanceled)
					{
						IEnumerable<Exception> exceptions = secondTask.Exception.InnerExceptions;
						foreach (var exception in exceptions)
						{
							Debug.Log("UpdateOrCreate WrestlerCards Data error : " + exception.Message);
						}
						if(callback != null)
							callback(false);
					}
					else
					{
						Debug.Log("WrestlerCards Data UpdateOrCreate Successful");
						if(callback != null)
							callback(true);
					}
				});
			}
		});
	}

	public static void CreateWrestlerCardData(int wrestlerID, List<int> stats, Action<bool> callback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(callback != null)
				callback(false);

			return;
		}

		var parseObj = new ParseObject("WrestlerCardData");
		parseObj["userID"] = ParseUser.CurrentUser.ObjectId;
		parseObj["wrestlerID"] = wrestlerID;
		parseObj["stats"] = stats;

        parseObj.SaveAsync().ContinueWith(task =>{
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;

				foreach (var exception in exceptions)
				{
					Debug.Log("Create WrestlerCard Data error : " + exception.Message);
				}
				if(callback != null)
					callback(false);
			}
			else
			{
				Debug.Log("WrestlerCard Data Created Successfully");
				if(callback != null)
					callback(true);
			}
		});
	}

	public static void CreateWrestlerCardData(Dictionary<int, List<int>> data, Action<bool> callback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(callback != null)
				callback(false);

			return;
		}

		var parseObjs = new List<ParseObject>();

		foreach(var item in data)
		{
			var obj = new ParseObject("WrestlerCardData");
			obj["userID"] = ParseUser.CurrentUser.ObjectId;
			obj["wrestlerID"] = item.Key;
			obj["stats"] = item.Value;
            parseObjs.Add(obj);
		}

		ParseObject.SaveAllAsync(parseObjs).ContinueWith(task =>{
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;

				foreach (var exception in exceptions)
				{
					Debug.Log("Create WrestlerCards Data error : " + exception.Message);
				}
				if(callback != null)
					callback(false);
			}
			else
			{
				Debug.Log("WrestlerCards Data Created Successfully");
				if(callback != null)
					callback(true);
			}
		});
	}

	public static void DeleteWrestlerCardData(Action<bool> callback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(callback != null)
				callback(false);

			return;
		}

		var query = ParseObject.GetQuery("WrestlerCardData").WhereEqualTo("userID", ParseUser.CurrentUser.ObjectId).Limit(1000);
		query.FindAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;
				foreach (var exception in exceptions)
				{
					Debug.Log("WrestlerCardData Retrieve error : " + exception.Message);
				}
				if(callback != null)
					callback(false);
			}
			else
			{
				Debug.Log("WrestlerCardData Retrieve successful");
				int objectCount=0;
				foreach(var obj in task.Result)
				{
					objectCount++;
				}

				if(objectCount==0)
				{
					if(callback != null)
						callback(false);
					return;
				}

				ParseObject.DeleteAllAsync(task.Result).ContinueWith(secondTask => {
					if (secondTask.IsFaulted || secondTask.IsCanceled)
					{
						IEnumerable<Exception> exceptions = secondTask.Exception.InnerExceptions;
						foreach (var exception in exceptions)
						{
							Debug.Log("Deleting WrestlerCardData error : " + exception.Message);
						}
						if(callback != null)
							callback(false);
					}
					else
					{
						Debug.Log("WrestlerCardData Deleted Successfully");
						if(callback != null)
							callback(true);
					}
				});
			}
		});
	}

	public static void CreateShowData(Dictionary<string, object> baseData, Action<bool> callback = null, List<Dictionary<string, object>> segmentData = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(callback != null)
				callback(false);

			return;
		}

		var objsList = new List<ParseObject>();

		var showObj = new ParseObject("ShowData");
		showObj["userID"] = ParseUser.CurrentUser.ObjectId;
		foreach(var item in baseData)
		{
			showObj[item.Key] = item.Value;
		}
		objsList.Add(showObj);

		if(segmentData != null)
		{
			if (segmentData.Count > 0) 
			{
				foreach(var segment in segmentData)
				{
					var obj = new ParseObject("SegmentData");
					obj["userID"] = ParseUser.CurrentUser.ObjectId;
					foreach(var property in segment)
					{
						obj[property.Key] = property.Value;
					}
					objsList.Add(obj);
				}
			}
		}

		ParseObject.SaveAllAsync(objsList).ContinueWith(task =>{
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;

				foreach (var exception in exceptions)
				{
					Debug.Log("Create Show Data error : " + exception.Message);
				}
				if(callback != null)
					callback(false);
			}
			else
			{
				Debug.Log("Show Data Created Successfully");
				if(callback != null)
					callback(true);
			}
		});
	}

	public static void PrepareAndSendAllUserData()
	{
		try
		{
		#region GameSessionDictionary
		var gameSessionDictionary = new Dictionary<string, object>();

		gameSessionDictionary.Add("selectedFederation", GamePlaySession.instance.selectedFederation);
		gameSessionDictionary.Add("selectedComissioner", GamePlaySession.instance.selectedComissioner);
		gameSessionDictionary.Add("myCash", GamePlaySession.instance.myCash);
		gameSessionDictionary.Add("myTokens", GamePlaySession.instance.myTokens);
		gameSessionDictionary.Add("myLevel", GamePlaySession.instance.myLevel);
		gameSessionDictionary.Add("shownBlurbCount", GamePlaySession.instance.shownBlurbCount);
		gameSessionDictionary.Add("isNewPlayer", GamePlaySession.instance.isNewPlayer);
		gameSessionDictionary.Add("inTutorialMode", GamePlaySession.instance.inTutorialMode);
		gameSessionDictionary.Add("flavorTutorialClear", GamePlaySession.instance.flavorTutorialClear);
		gameSessionDictionary.Add("calendarTutorialClear", GamePlaySession.instance.calendarTutorialClear);
		gameSessionDictionary.Add("resultTutorialClear", GamePlaySession.instance.resultTutorialClear);
		gameSessionDictionary.Add("storePurchaseTutorialClear", GamePlaySession.instance.storePurchaseTutorialClear);
		gameSessionDictionary.Add("storeRefreshTutorialClear", GamePlaySession.instance.storeRefreshTutorialClear);
		gameSessionDictionary.Add("storeTutorialClear", GamePlaySession.instance.storeTutorialClear);
		gameSessionDictionary.Add("transitionMessageClear", GamePlaySession.instance.transitionMessageClear);
		gameSessionDictionary.Add("storeCardsBought", GamePlaySession.instance.storeCardsBought);
		gameSessionDictionary.Add("currentMonth", GamePlaySession.instance.currentMonth);
		gameSessionDictionary.Add("currentYear", GamePlaySession.instance.currentYear);
		gameSessionDictionary.Add("numberOfYears", GamePlaySession.instance.numberOfYears);
		gameSessionDictionary.Add("worldChampCardNumber", GamePlaySession.instance.worldChampCardNumber);
		gameSessionDictionary.Add("titleChampCardNumber", GamePlaySession.instance.titleChampCardNumber);
		gameSessionDictionary.Add("worldChampLastCardNumber", GamePlaySession.instance.worldChampLastCardNumber);
		gameSessionDictionary.Add("titleChampLastCardNumber", GamePlaySession.instance.titleChampLastCardNumber);
		gameSessionDictionary.Add("newGamePlus", GamePlaySession.instance.newGamePlus);
		gameSessionDictionary.Add("firstShopCards", GamePlaySession.instance.firstShopCards);
		gameSessionDictionary.Add("gameCompleted", GamePlaySession.instance.gameCompleted);
		gameSessionDictionary.Add("fantasyPopUpShown", GamePlaySession.instance.fantasyPopUpShown);
		gameSessionDictionary.Add("transferData", GamePlaySession.instance.transferData);
		gameSessionDictionary.Add("averageRating", GamePlaySession.instance.averageRating);
		gameSessionDictionary.Add("lastShopRefresh", GamePlaySession.instance.lastShopRefresh);
		gameSessionDictionary.Add("tagChampCardNumbers", GamePlaySession.instance.tagChampCardNumbers);


		List<string> awards = new List<string>();

		if (GamePlaySession.instance.Awards!=null)
		{
			if (GamePlaySession.instance.Awards.Length!=0)
			{
				for(int i=0;i<20;i++)
				{
					string thisYearsAwards = "";
					if (GamePlaySession.instance.Awards[i]!=null)
					{
						foreach (var item in GamePlaySession.instance.Awards[i]) 
						{
							if (thisYearsAwards=="")
							{
								thisYearsAwards += item.CardNumber.ToString();
							}
							else
							{
								thisYearsAwards += "," + item.CardNumber.ToString();
							}
						}
					}
					awards.Add(thisYearsAwards);
				}
			}
		}
		gameSessionDictionary.Add("awards",awards);



		int pop=0;
		int mic=0;
		int skill=0;
		int plusCash=0;

		foreach (var item in GamePlaySession.instance.myBonuses) 
		{
			if (item.bType == BonusValue.BonusType.POP) 
			{
				pop = item.value;
			}

			if (item.bType == BonusValue.BonusType.MIC) 
			{
				mic = item.value;
			}

			if (item.bType == BonusValue.BonusType.SKILL) 
			{
				skill = item.value;
			}

			if (item.bType == BonusValue.BonusType.PLUS_CASH) 
			{
				plusCash = item.value;
			}
		}

		BonusValue bv1 = new BonusValue ();
		BonusValue bv2 = new BonusValue ();
		BonusValue bv3 = new BonusValue ();
		BonusValue bv4 = new BonusValue ();

		bv1.bType = BonusValue.BonusType.POP;bv1.value = pop;
		bv2.bType = BonusValue.BonusType.MIC;bv2.value = mic;
		bv3.bType = BonusValue.BonusType.SKILL;bv3.value = skill;
		bv4.bType = BonusValue.BonusType.PLUS_CASH;bv4.value = plusCash;

		GamePlaySession.instance.myBonuses.Clear ();
		GamePlaySession.instance.myBonuses.Add (bv1);
		GamePlaySession.instance.myBonuses.Add (bv2);
		GamePlaySession.instance.myBonuses.Add (bv3);
		GamePlaySession.instance.myBonuses.Add (bv4);
		List<int> bonuses = new List<int> ();
		bonuses.Add (bv1.value);
		bonuses.Add (bv2.value);
		bonuses.Add (bv3.value);
		bonuses.Add (bv4.value);

		gameSessionDictionary.Add("myBonuses",bonuses);
		#endregion
		#region CardsDataIncludingWrestlers

		Dictionary<int,List<int>> wrestlersDictionary = new Dictionary<int, List<int>> ();
		List<int> myCardIDs = new List<int>();

		foreach (var item in GamePlaySession.instance.myCards) 
		{
			myCardIDs.Add(item.CardNumber);
			if (item.myCardType == CardType.WRESTLER) 
			{
				WrestlerCardObject wrestler = (WrestlerCardObject)item;
				List<int> wrestlerStats = new List<int>();
				wrestlerStats.Add (wrestler.popCurrent);
				wrestlerStats.Add (wrestler.micCurrent);
				wrestlerStats.Add (wrestler.skillCurrent);
				wrestlerStats.Add (wrestler.pushCurrent);
				wrestlersDictionary.Add (wrestler.CardNumber, wrestlerStats);
			}
		}
		gameSessionDictionary.Add("myCardIDs",myCardIDs);
		//Also upload to Parse Server...
        #endregion
		#region ShowsAndSegmentsData
		int count = 0;
		List<Dictionary<string,object>> allShowsData = new List<Dictionary<string,object>>();
		List<List<Dictionary<string,object>>> allSegmentsData = new List<List<Dictionary<string,object>>>();

		foreach (var show in GamePlaySession.instance.myShows) 
		{
			if (show.showFinished)
			{
				Dictionary<string,object> showBaseData = show.GetShowBaseData();
				count = UnityEngine.Random.Range(1, 100000);
				showBaseData.Add("uniqueID",count);
				allShowsData.Add(showBaseData);
				List<Dictionary<string,object>> segmentsData = new List<Dictionary<string,object>>();

				if (show.showName.ToUpper()!="NOW YOU KNOW")
				{
					foreach (var seg in show.segments) 
					{
						Dictionary<string,object> segmentData = seg.GetSegmentData();
						segmentData.Add("uniqueID",count);
						segmentsData.Add(segmentData);
					}
				}

				allSegmentsData.Add(segmentsData);
				//				count++;
			}
		}
		
		#endregion
		UploadAllUserData (gameSessionDictionary,wrestlersDictionary,allShowsData,allSegmentsData,DisableTitleScreenOverlay);
		}
		catch 
		{
			DisableTitleScreenOverlay (false);
		}
	}
		
	static void DisableTitleScreenOverlay(bool success)
	{
		UIManagerScript.instance.uploadSuccess = success;
		UIManagerScript.instance.sendingDataToParse = false;
	}

	public static void UploadAllUserData(Dictionary<string, object> gameSessionData, Dictionary<int, List<int>> wrestlerCardsData,
		List<Dictionary<string, object>> showsData, List<List<Dictionary<string, object>>> segmentsData, Action<bool> callback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(callback != null)
				callback(false);
			
			return;
		}

		var query = ParseObject.GetQuery("GameSession").WhereEqualTo("userID", ParseUser.CurrentUser.ObjectId);
		query.FirstAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;

				foreach (var exception in exceptions)
				{
					Debug.Log("Getting GameSessionData error " + exception.Message);
				}
				if(callback != null)
					callback(false);
			}
			else
			{
				var parseObjsList = new List<ParseObject>();

				//making gamesession parse object
				var gameSessionObj = task.Result;
				foreach(var item in gameSessionData)
				{
					gameSessionObj[item.Key] = item.Value;
				}
				parseObjsList.Add(gameSessionObj);

				//making wrestler cards parse objects
				foreach(var item in wrestlerCardsData)
				{
					var wrestlerObj = new ParseObject("WrestlerCardData");
					wrestlerObj["userID"] = ParseUser.CurrentUser.ObjectId;
					wrestlerObj["wrestlerID"] = item.Key;
					wrestlerObj["stats"] = item.Value;
					parseObjsList.Add(wrestlerObj);
				}

				//making shows parse objects
				foreach(var show in showsData)
				{
					var showObj = new ParseObject("ShowData");
					showObj["userID"] = ParseUser.CurrentUser.ObjectId;
					foreach(var item in show)
					{
						showObj[item.Key] = item.Value;
					}
					parseObjsList.Add(showObj);
				}

				foreach(var showSegments in segmentsData)
				{
					foreach(var segment in showSegments)
					{
						var segmentObj = new ParseObject("SegmentData");
						segmentObj["userID"] = ParseUser.CurrentUser.ObjectId;
						foreach(var item in segment)
						{
							segmentObj[item.Key] = item.Value;
						}
						parseObjsList.Add(segmentObj);
					}
				}

				ParseObject.SaveAllAsync(parseObjsList).ContinueWith(secondTask =>{
					if (secondTask.IsFaulted || secondTask.IsCanceled)
					{
						IEnumerable<Exception> exceptions = secondTask.Exception.InnerExceptions;
						foreach (var exception in exceptions)
						{
							Debug.Log("All User Data Upload error : " + exception.Message);
						}
						if(callback != null)
							callback(false);
					}
					else
					{
						Debug.Log("All User Data Uploaded Successfully");
						if(callback != null)
							callback(true);
					}
				});
			}
		});
	}

	public static bool[] loadChecks = new bool[4]{false,false,false,false};

	static ParseObject gameSessionParse;
	static IEnumerable<ParseObject> showDataParse;
	static IEnumerable<ParseObject> segmentDataParse;
	static IEnumerable<ParseObject> wrestlerDataParse;


	public static void RetrieveGameSession(Action<bool, ParseObject> gameSessionCallback = null, 
		Action<bool, IEnumerable<ParseObject>> wrestlerCardDataCallback = null, Action<bool, IEnumerable<ParseObject>> showDataCallback = null,
		Action<bool, IEnumerable<ParseObject>> segmentDataCallback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(gameSessionCallback != null && wrestlerCardDataCallback != null && showDataCallback != null && segmentDataCallback != null)
			{
				gameSessionCallback(false, null);
				wrestlerCardDataCallback(false, null);
				showDataCallback(false, null);
				segmentDataCallback(false, null);
			}
			return;
		}

		var gameSession = ParseObject.GetQuery("GameSession").WhereEqualTo("userID", ParseUser.CurrentUser.ObjectId);
		gameSession.FirstAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;
				foreach (var exception in exceptions)
				{
					Debug.Log("GameSession Retrieve error : " + exception.Message);
				}
				if(gameSessionCallback != null)
					gameSessionCallback(false, null);
			}
			else
			{
				Debug.Log("GameSession Retrieve successful");
				gameSessionParse = task.Result;
				loadChecks[0] = true;
			}
		});
	}

	public static void GetUser(Action<bool, ParseUser> callback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(callback != null)
			{
				callback(false, null);
			}
			return;
		}

		var userTable = ParseUser.Query.WhereEqualTo ("objectId", ParseUser.CurrentUser.ObjectId);
		userTable.FirstAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;
				foreach (var exception in exceptions)
				{
					Debug.Log("User Retrieve error : " + exception.Message);
				}
				if(callback != null)
					callback(false, null);
			}
			else
			{
				if(callback != null)
					callback(true, task.Result);
			}
		});
	}

	//TODO::revise code for 1000+ items
	public static void RetrieveSegmentData()
	{
		var segmentData = ParseObject.GetQuery("SegmentData").WhereEqualTo("userID", ParseUser.CurrentUser.ObjectId).Limit(1000);
		segmentData.FindAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;
				foreach (var exception in exceptions)
				{
					Debug.Log("SegmentData Retrieve error : " + exception.Message);
				}
			}
			else
			{
				Debug.Log("SegmentData Retrieve successful");
				segmentDataParse = task.Result;
				loadChecks[3] = true;
			}
		});
	}

	//TODO::revise code for 1000+ items
	public static void RetrieveShowData()
	{
		var showData = ParseObject.GetQuery("ShowData").WhereEqualTo("userID", ParseUser.CurrentUser.ObjectId).Limit(1000);
		showData.FindAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;
				foreach (var exception in exceptions)
				{
					Debug.Log("ShowData Retrieve error : " + exception.Message);
				}
			}
			else
			{
				Debug.Log("ShowData Retrieve successful");
				showDataParse = task.Result;
				loadChecks[2] = true;
			}
		});
	}

	public static void RetrieveWrestlerData()
	{
		var wrestlerCardData = ParseObject.GetQuery("WrestlerCardData").WhereEqualTo("userID", ParseUser.CurrentUser.ObjectId).Limit(1000);
		wrestlerCardData.FindAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;
				foreach (var exception in exceptions)
				{
					Debug.Log("WrestlerCardData Retrieve error : " + exception.Message);
				}
			}
			else
			{
				Debug.Log("WrestlerCardData Retrieve successful");
				wrestlerDataParse = task.Result;
				loadChecks[1] = true;
			}
		});
	}

	public static void RemoveShowData(Action<bool> showDeleteCallback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(showDeleteCallback != null)
				showDeleteCallback(false);

			return;
		}

		var showData = ParseObject.GetQuery("ShowData").WhereEqualTo("userID", Parse.ParseUser.CurrentUser.ObjectId).Limit(1000);
		showData.FindAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;
				foreach (var exception in exceptions)
				{
					Debug.Log("ShowData Retrieve error : " + exception.Message);
					if(showDeleteCallback != null)
						showDeleteCallback(false);
				}
			}
			else
			{
				int objCount=0;
				foreach(var item in task.Result)
				{
					objCount++;
				}

				if(objCount>0)
				{
					Debug.Log("Shows returned by server : " + objCount);
					ParseObject.DeleteAllAsync(task.Result).ContinueWith(secondTask =>{
						if (secondTask.IsFaulted || secondTask.IsCanceled)
						{
							IEnumerable<Exception> exceptions = secondTask.Exception.InnerExceptions;
							foreach (var exception in exceptions)
							{
								Debug.Log("ShowData Delete error : " + exception.Message);
								if(showDeleteCallback != null)
									showDeleteCallback(false);
							}
						}
						else
						{
//							Debug.Log("ShowData Deleted Successful");
//							if(objCount==1000)
//							{
//								RemoveShowData(showDeleteCallback);
//							}
//							else
							{
								Debug.Log("All ShowData Deleted Successful");
								if(showDeleteCallback != null)
									showDeleteCallback(true);
							}
						}
					});
				}
				else
				{
					Debug.Log("No ShowData found for current user");
					if(showDeleteCallback != null)
						showDeleteCallback(true);
				}
			}
		});
	}

	public static void Callback(bool success = false)
	{
		RemoveSegmentData ();
	}


	public static void RemoveSegmentData(Action<bool> segmentDeleteCallback = null)
	{
		if (ParseUser.CurrentUser == null)
		{
			Debug.Log("User Session doesnot exist. Register or LogIn user");
			if(segmentDeleteCallback != null)
				segmentDeleteCallback(false);

			return;
		}

		Debug.Log ("Segment Data running"+Parse.ParseUser.CurrentUser.ObjectId.ToString());		
		var segmentData = ParseObject.GetQuery("SegmentData").WhereEqualTo("userID", Parse.ParseUser.CurrentUser.ObjectId).Limit(999);
		segmentData.FindAsync().ContinueWith(task => {
			if (task.IsFaulted || task.IsCanceled)
			{
				IEnumerable<Exception> exceptions = task.Exception.InnerExceptions;
				foreach (var exception in exceptions)
				{
					Debug.Log("SegmentData Retrieve error : " + exception.Message);
					if(segmentDeleteCallback != null)
						segmentDeleteCallback(false);
				}
			}
			else
			{
				int objCount=0;
				foreach(var item in task.Result)
				{
					objCount++;
				}

				if(objCount>0)
				{
					Debug.Log("Segments returned by server : " + objCount);
					ParseObject.DeleteAllAsync(task.Result).ContinueWith(secondTask =>{
						if (secondTask.IsFaulted || secondTask.IsCanceled)
						{
							IEnumerable<Exception> exceptions = secondTask.Exception.InnerExceptions;
							foreach (var exception in exceptions)
							{
								Debug.Log("SegmentData Delete error : " + exception.Message);
								if(segmentDeleteCallback != null)
									segmentDeleteCallback(false);
							}
						}
						else
						{
//							Debug.Log("SegmentData Deleted Successful");
//							if(objCount==1000)
//							{
//								RemoveSegmentData(segmentDeleteCallback);
//							}
//							else
							{
								Debug.Log("All SegmentData Deleted Successful");
								if(segmentDeleteCallback != null)
									segmentDeleteCallback(true);
							}
						}
					});
				}
				else
				{
					Debug.Log("No SegmentData found for current user"+Parse.ParseUser.CurrentUser.ObjectId.ToString());
					if(segmentDeleteCallback != null)
						segmentDeleteCallback(true);
				}
			}
		});
	}
		
	static GamePlaySession BuildGamePlaySession()
	{
		GamePlaySession session = new GamePlaySession ();

			//session.InitializeSession ();
			//session.MakeCalendar ();
			//De-Serializing GameSession Parse Object
		try
		{
			session.titleChampCardNumber = gameSessionParse.Get<int>("titleChampCardNumber");
			session.worldChampCardNumber = gameSessionParse.Get<int>("titleChampCardNumber");
			session.myTokens = gameSessionParse.Get<int>("myTokens");
			session.myCash = gameSessionParse.Get<int>("myCash");
			session.inTutorialMode = gameSessionParse.Get<bool>("inTutorialMode");
			session.fantasyPopUpShown = gameSessionParse.Get<bool>("fantasyPopUpShown");
			session.storeCardsBought = gameSessionParse.Get<int>("storeCardsBought");
			session.lastShopRefresh = gameSessionParse.Get<System.DateTime>("lastShopRefresh");
			session.calendarTutorialClear = gameSessionParse.Get<bool>("calendarTutorialClear");
			session.flavorTutorialClear = gameSessionParse.Get<bool>("flavorTutorialClear");
			session.resultTutorialClear = gameSessionParse.Get<bool>("resultTutorialClear");
			session.selectedComissioner = gameSessionParse.Get<int>("selectedComissioner");
			session.isNewPlayer = gameSessionParse.Get<bool>("isNewPlayer");
			session.myLevel = gameSessionParse.Get<int>("myLevel");
			session.transitionMessageClear = gameSessionParse.Get<bool>("transitionMessageClear");
			session.selectedFederation = gameSessionParse.Get<int>("selectedFederation");
			session.shownBlurbCount = gameSessionParse.Get<int>("shownBlurbCount");
			session.currentMonth = gameSessionParse.Get<int>("currentMonth");
			session.newGamePlus = gameSessionParse.Get<bool>("newGamePlus");
			session.storePurchaseTutorialClear = gameSessionParse.Get<bool>("storePurchaseTutorialClear");
			session.storeTutorialClear = gameSessionParse.Get<bool>("storeTutorialClear");
			session.storeRefreshTutorialClear = gameSessionParse.Get<bool>("storeRefreshTutorialClear");
			session.transferData = gameSessionParse.Get<bool>("transferData");
			session.titleChampLastCardNumber = gameSessionParse.Get<int>("titleChampLastCardNumber");
			session.currentYear = gameSessionParse.Get<int>("currentYear");
			session.gameCompleted = gameSessionParse.Get<bool>("gameCompleted");
			session.worldChampLastCardNumber = gameSessionParse.Get<int>("worldChampLastCardNumber");
			session.firstShopCards = gameSessionParse.Get<bool>("firstShopCards");
			session.averageRating = gameSessionParse.Get<float>("averageRating");
			//Making Cards out of the received Card List
			session.myCards = new List<CardObject>();
			List<object> awards = new List<object>();

			try
			{
				awards = gameSessionParse.Get<List<object>> ("awards");
			}
			catch
			{
				
			}
			int count = 0;
			if (awards!=null)
			{
				foreach (var item in awards) 
				{
					if (item != null) 
					{
						List<CardObject> awardCards = new List<CardObject> ();
						if (item != "") 
						{
							//char[] chars = item.ToString ().ToCharArray ();	
							char[] delimiter = new char[]{ ',' };
							string[] nums = {"", "", "", ""};
							try
							{
								nums = item.ToString ().Split (delimiter, 4);	
							}
							catch
							{
								
							}


							foreach (var item2 in nums) 
							{
								if (item2 != null) 
								{
									if (item2 != "") 
									{
										if (int.Parse(item2)>0)
										{
											awardCards.Add (UIManagerScript.instance.gsi.GetCard (int.Parse (item2)).Clone ());	
										}
										else
										{
											awardCards.Add (UIManagerScript.instance.gsi.GetCard (700).Clone ());	
										}
									}
								}
							}
						} 
						else if (item == "") 
						{

						}

						if (count < 20) 
						{
							session.Awards [count] = awardCards;
						}
					}	
					count++;
				}
			}


			int currentYear = gameSessionParse.Get<int>("currentYear");

			int startingYear = -1;
			try
			{
				startingYear = gameSessionParse.Get<int>("startingYear");
			}
			catch
			{
				startingYear = 1980;
			}
			if (startingYear==1980)
			{
				if (currentYear!=null)
				{
					if (currentYear>=10)
					{
						UIManagerScript.instance.Theme90s = true;
						UIManagerScript.instance.UpdateTheme();
						var panels = UIManagerScript.instance.TitleScreen.gameObject.transform.parent.GetComponentsInChildren<ThemeController> (true);
						foreach (var item in panels) 
						{
							item.CheckTheme ();
						}
					}
				}
			}
			else if (startingYear==1985)
			{
				if (currentYear!=null)
				{
					if (currentYear>=5)
					{
						UIManagerScript.instance.Theme90s = true;
						UIManagerScript.instance.UpdateTheme();
						var panels = UIManagerScript.instance.TitleScreen.gameObject.transform.parent.GetComponentsInChildren<ThemeController> (true);
						foreach (var item in panels) 
						{
							item.CheckTheme ();
						}
					}
				}
			}
			else if (startingYear == 1990)
			{
				UIManagerScript.instance.Theme90s = true;
				UIManagerScript.instance.UpdateTheme();
				var panels = UIManagerScript.instance.TitleScreen.gameObject.transform.parent.GetComponentsInChildren<ThemeController> (true);
				foreach (var item in panels) 
				{
					item.CheckTheme ();
				}
			}

			var myBonusesList = new List<object>();

			try
			{
				myBonusesList = gameSessionParse.Get<List<object>> ("myBonuses");
			}
			catch
			{
				
			}
			List<int> bonusValuesList = new List<int> ();

			if (myBonusesList!=null)
			{
				foreach (var item in myBonusesList) 
				{
					bonusValuesList.Add (int.Parse(item.ToString()));
				}
			}
			else
			{
				bonusValuesList.Add(0);bonusValuesList.Add(0);bonusValuesList.Add(100);bonusValuesList.Add(0);
			}

			
			BonusValue bv1 = new BonusValue ();bv1.bType = BonusValue.BonusType.POP;bv1.value = bonusValuesList[0];
			BonusValue bv2 = new BonusValue ();bv2.bType = BonusValue.BonusType.MIC;bv2.value = bonusValuesList[1];
			BonusValue bv3 = new BonusValue ();bv3.bType = BonusValue.BonusType.SKILL;bv3.value = bonusValuesList[2];
			BonusValue bv4 = new BonusValue ();bv4.bType = BonusValue.BonusType.PLUS_CASH;bv4.value = bonusValuesList[3];
			session.myBonuses.Add (bv1);session.myBonuses.Add (bv2);session.myBonuses.Add (bv3);session.myBonuses.Add (bv4);
			

			var tagChampCardNums = new List<object>();
			try
			{
				tagChampCardNums = gameSessionParse.Get<List<object>> ("tagChampCardNumbers");
			}
			catch
			{
				tagChampCardNums.Add(73);
				tagChampCardNums.Add(25);
			}

			if (tagChampCardNums.Count==0)
			{
				tagChampCardNums.Add(73);
				tagChampCardNums.Add(25);
			}

			List<int> tagChampCardNumbers = new List<int> ();

			if (tagChampCardNums!=null)
			{
				foreach (var item in tagChampCardNums) 
				{
					tagChampCardNumbers.Add (int.Parse(item.ToString()));
				}
			}
			else
			{
				tagChampCardNumbers.Add(73);
				tagChampCardNumbers.Add(25);
			}

			
			session.tagChampCardNumbers [0] = tagChampCardNumbers [0];
			session.tagChampCardNumbers [1] = tagChampCardNumbers [1];	

			var myCardIDsList = gameSessionParse.Get<List<object>> ("myCardIDs");
			List<int> listofCardIDs = new List<int> ();
			int i = 0;
			if (myCardIDsList!=null)
			{
				foreach (var item in myCardIDsList) 
				{
					listofCardIDs.Add (int.Parse(item.ToString()));
					if (listofCardIDs[i]!=null)
					{
						try
						{
							session.myCards.Add(UIManagerScript.instance.gsi.GetCard (listofCardIDs[i]).Clone ());			
						}
						catch(Exception e)
						{
							Debug.Log(listofCardIDs[i]);
							Debug.Log(e);
						}

						i++;
					}
				}
			}

			
			Dictionary<int,List<int>> dict = new Dictionary<int,List<int>> ();


			foreach (var obj in wrestlerDataParse)
			{
				int wrestlerID = obj.Get<int> ("wrestlerID");
				
				var y = obj.Get<List<object>> ("stats");
				
				List<int> listStats = new List<int> ();

				if (y != null) 
				{
					foreach (var item3 in y) 
					{
						listStats.Add (int.Parse(item3.ToString()));
					}
				}
			try
			{
				dict.Add (wrestlerID, listStats);
			}
			catch 
			{
				dict.Remove (wrestlerID);
				dict.Add (wrestlerID, listStats);
			}
				
			}

			foreach (var item in dict) 
			{
				WrestlerCardObject w = (WrestlerCardObject)session.myCards.Find (o => o.CardNumber == item.Key);
				if (w == null) 
				{
					w = (WrestlerCardObject)UIManagerScript.instance.gsi.GetCard (item.Key).Clone ();
				}
				w.popCurrent = item.Value [0];
				w.micCurrent = item.Value [1];
				w.skillCurrent = item.Value [2];
				w.pushCurrent = item.Value [3];
			}


			List<ParseObject> showBaseDatas = new List<ParseObject> ();
			List<ParseObject> segmentDatas = new List<ParseObject> ();

			foreach (var item in showDataParse) 
			{
				showBaseDatas.Add (item);
			}


			foreach (var item in segmentDataParse) 
			{
				segmentDatas.Add (item);
			}
			session.myShows = GetShows (showBaseDatas, segmentDatas);
			session.GSI = GameObject.Find ("UIManager").GetComponent<GameSetupItems> ();
			session.myCalendar = new CalendarObject ();
			session.myCalendar = session.myCalendar.GenerateCalendar ();

			if (startingYear!=-1)
			{
				if (startingYear==1990)
				{
					for(int s=0;s<20;s++)
					{
						if (s < 10)
						{
							session.myCalendar.years[s].yearName = (1990 + s).ToString();
						}
						else if (i >= 10)
						{
							session.myCalendar.years[s].yearName = (1980 + s).ToString();
						}
					}
				}
				else if (startingYear==1985)
				{
					for(int s=0;s<20;s++)
					{
						if (s < 15)
						{
							session.myCalendar.years[s].yearName = (1985 + s).ToString();
						}
						else if (s >= 15)
						{
							session.myCalendar.years[s].yearName = (1985 + s - 20).ToString();
						}
					}
				}
			}



		//ALI AMEEN...
		GamePlaySession.instance = session;

		if (GamePlaySession.instance.selectedFederation == 3)
			GamePlaySession.instance.selectedFederation = 2;

		if (GamePlaySession.instance.selectedComissioner == 3) 
		{
			GamePlaySession.instance.selectedComissioner = 2;
			GamePlaySession.instance.myBonuses.Clear ();
			BonusValue bonus = new BonusValue ();
			bonus.bType = BonusValue.BonusType.PLUS_CASH;
			bonus.value = 500;
			GamePlaySession.instance.myBonuses.Add (bonus);
		}

		foreach (var item in GamePlaySession.instance.myCards) 
		{
			item.PostSerialize ();	
		}

		foreach (var item in GamePlaySession.instance.myShows) 
		{
			item.PostDeserialize ();	
		}


		if (GameObject.Find ("ContinueButton") != null) 
		{
			if (session.selectedFederation!=-1)
			{
				GameObject.Find ("ContinueButton").GetComponent<Button> ().interactable = true;
			}
		}

		if (GameObject.Find ("NewGameButton") != null) 
		{
			if (session.inTutorialMode != null) 
			{
				if (!session.inTutorialMode) 
				{
					GameObject.Find ("NewGameButton").GetComponent<Image> ().sprite = UIManagerScript.instance.TitleScreen.gameObject.GetComponent<TitleScreen> ().NGPlus;
				}
				else
				{
					GameObject.Find ("NewGameButton").GetComponent<Image> ().sprite = UIManagerScript.instance.TitleScreen.gameObject.GetComponent<TitleScreen> ().NG;
				}
			}
		}
		
		try
		{
			GamePlaySession.instance.titleChampion = (WrestlerCardObject)GamePlaySession.instance.CheckCard (GamePlaySession.instance.titleChampCardNumber);
			GamePlaySession.instance.worldChampion = (WrestlerCardObject)GamePlaySession.instance.CheckCard (GamePlaySession.instance.worldChampCardNumber);
			GamePlaySession.instance.tagChampions = new WrestlerCardObject[] {(WrestlerCardObject)GamePlaySession.instance.CheckCard (GamePlaySession.instance.tagChampCardNumbers [0]),
				(WrestlerCardObject)GamePlaySession.instance.CheckCard (GamePlaySession.instance.tagChampCardNumbers [1])
			};
		}
		catch 
		{
				GamePlaySession.instance.titleChampion = (WrestlerCardObject)GamePlaySession.instance.CheckCard (151);
				GamePlaySession.instance.worldChampion = (WrestlerCardObject)GamePlaySession.instance.CheckCard (45);
				GamePlaySession.instance.tagChampions = new WrestlerCardObject[] {(WrestlerCardObject)GamePlaySession.instance.CheckCard (73),
					(WrestlerCardObject)GamePlaySession.instance.CheckCard (25)
				};
		}
			UIManagerScript.instance.success = true;
			UIManagerScript.instance.sendingDataToParse = false;
			loadChecks [0] = false;
			loadChecks [1] = false;
			loadChecks [2] = false;
			loadChecks [3] = false;
		}

		catch(Exception e)
		{
			Debug.Log (e.ToString());


			loadChecks [0] = false;
			loadChecks [1] = false;
			loadChecks [2] = false;
			loadChecks [3] = false;
			setData = true;
			UIManagerScript.instance.LoadFromParse ();
			Debug.Log ("Session Data Fetch Fail");
//			Debug.Log("Error in creating session from downloaded data... Making new one");
//			UIManagerScript.instance.sendingDataToParse = false;
//			UIManagerScript.instance.success = false;
//			var newSession = new GamePlaySession ();
//			newSession.WriteToBinaryFile();
//			GamePlaySession.instance = newSession;
//			return newSession;

		}

		Debug.Log("Session created successfully from downloaded data");
		return session;
	}

	void Start()
	{
//		LogOutUser();
//		LogInUser("user411@gmail.com", "aliameen");
		//RemoveShowData();
		//RemoveSegmentData();
		InvokeRepeating ("SetData", 1f, 1f);
		StartCoroutine("UploadGameSession");
		//LogOutUser();
		//LogInUser("hypnocobra@hotmail.com","5MN1VGH8");
		//RemoveSegmentData();


		//RemoveShowData();
	}

	IEnumerator UploadGameSession()
	{
		while(true)
		{
			yield return new WaitForSeconds(60f);
			if(GamePlaySession.instance != null)
				GamePlaySession.instance.UploadGameSession();


		}
	}

	public static bool setData;

	void SetData()
	{
		if (loadChecks [0] && loadChecks [1] && loadChecks [2] && loadChecks [3] && setData) 
		{
			//CancelInvoke ("SetData");
			BuildGamePlaySession ();
			setData = false;
		}
	}

	public static List<ShowObject> GetShows(List<ParseObject> baseData, List<ParseObject> segmentDatas)
	{
		List<ShowObject> shows = new List<ShowObject> ();
		int uniqueID = 0;
		foreach (var item in baseData) 
		{
			ShowObject show = new ShowObject ();
			show.showName = item.Get<string> ("ShowName");
			show.showFinished = item.Get<bool> ("ShowFinished");
			if (show.showName != "Now You Know") 
			{
				show.showAttendance = new Attendance ();
				show.showAttendance.attendanceModifier = item.Get<float> ("AttendanceModifier");
				show.showAttendance.attandanceString = show.showAttendance.GetAttendanceString (show.showAttendance.attendanceModifier);
				show.blurbNumber = item.Get<int> ("BlurbNumber");
				show.showEarnings = item.Get<int> ("ShowEarnings");
				show.isP4V = item.Get<bool> ("IsP4V");
				show.venueCard = item.Get<int> ("VenueID");
				show.CurrentWeek = item.Get<int> ("CurrentWeek");
				show.showFinished = item.Get<bool> ("ShowFinished");
				show.showResults = item.Get<bool> ("ShowResults");
				show.AwardShow = item.Get<bool> ("AwardShow");
				show.showRating = item.Get<float> ("ShowRating");
				show.displayTitle = item.Get<string> ("ShowTitle");
				show.showPopUp = new PopUpMessage ();
				show.showResults = item.Get<bool> ("HasResults");
				uniqueID = item.Get<int> ("uniqueID");
			}

			if (show.showName != "Now You Know") 
			{
				List<SegmentObject> segments = new List<SegmentObject> ();
				foreach (var item2 in segmentDatas) 
				{
					if (item2.Get<int> ("uniqueID") == uniqueID) 
					{
						SegmentObject segment = new SegmentObject ();
						segment.DisQualify = item2.Get<int> ("Disqualification");
						segment.popUpClear = item2.Get<bool> ("PopUpClear");

						var k = item2.Get<List<object>> ("Team2IDs");
						List<int> list = new List<int> ();

						if (k != null) 
						{
							foreach (var item3 in k) 
							{
								list.Add (int.Parse(item3.ToString()));
							}
						}

						segment.team2IDs = list.ToArray ();

						segment.segmentPassed = item2.Get<bool> ("SegmentPassed");

						var l = item2.Get<List<object>> ("Team1IDs");
						List<int> list2 = new List<int> ();
						foreach (var item3 in l) 
						{
							list2.Add (int.Parse(item3.ToString()));
						}
						segment.team1IDs = list2.ToArray ();

						segment.segmentFinished = item2.Get<bool> ("SegmentFinished");
						segment.segmentTitle = item2.Get<string> ("SegmentTitle");

						var m = item2.Get<List<object>> ("Team1SkillChanges");
						List<int> list3 = new List<int> ();
						foreach (var item3 in m) 
						{
							list3.Add (int.Parse(item3.ToString()));
						}
						segment.team1SkillChange = list3.ToArray ();



						var e = item2.Get<List<object>> ("CardIDs");
						List<int> list8 = new List<int> ();
						foreach (var itemw in e) 
						{
							list8.Add (int.Parse(itemw.ToString()));
						}
						segment.cardIDs = list8.ToArray ();


						segment.isPendingSetup = item2.Get<bool> ("PendingSetup");

						var n = item2.Get<List<object>> ("Team2SkillChanges");

						List<int> list4 = new List<int> ();
						if (n != null) 
						{
							foreach (var item3 in n) 
							{
								list4.Add (int.Parse(item3.ToString()));
							}
						}

						segment.team2SkillChange = list4.ToArray ();

						string segmentTitleMatchType = item2.Get<string> ("TitleMatchType");
						if (segmentTitleMatchType != null && segmentTitleMatchType != "") 
						{
							if (segmentTitleMatchType == "TAG") {
								segment.titleMatchType = SegmentObject.TitleMatchType.TAG;
							} else if (segmentTitleMatchType == "WORLD") {
								segment.titleMatchType = SegmentObject.TitleMatchType.WORLD;
							} else if (segmentTitleMatchType == "CONTINENTAL") {
								segment.titleMatchType = SegmentObject.TitleMatchType.CONTINENTAL;
							} 
						}

						string segmentType = item2.Get<string> ("SegmentType");
						if (segmentType != null && segmentType != "") 
						{
							if (segmentType == "MATCH") 
							{
								segment.segmentType = SegmentObject.SegmentType.MATCH;
							} 
							else if (segmentType == "MIC_SPOT") 
							{
								segment.segmentType = SegmentObject.SegmentType.MIC_SPOT;
							} 
							else if (segmentType == "SKIT") 
							{
								segment.segmentType = SegmentObject.SegmentType.SKIT;
							} 
							else if (segmentType == "TAG_MATCH") 
							{
								segment.segmentType = SegmentObject.SegmentType.TAG_MATCH;
							} 
						}

						segment.segmentRating = item2.Get<int> ("SegmentRating");
						segment.lockedOptions = null;
						//segment.segmentWrestlerRatings = item2.Get<int[]> ("SegmentWrestlerRatings");

						var o = item2.Get<List<object>> ("SegmentWrestlerRatings");

						List<int> list5 = new List<int> ();
						foreach (var item3 in o) 
						{
							list5.Add (int.Parse(item3.ToString()));
						}

						segment.segmentWrestlerRatings = list5.ToArray ();

						segment.isTitleMatch = item2.Get<bool> ("IsTitleMatch");


						//segment.winnerIDs = item2.Get<int[]> ("WinnerIDs");

						var p = item2.Get<List<object>> ("WinnerIDs");

						List<int> list6 = new List<int> ();
						if (p != null) 
						{
							foreach (var item3 in p) 
							{
								list6.Add (int.Parse(item3.ToString()));
							}
						}

						segment.winnerIDs = list6.ToArray ();


						segment.popupMessage = new PopUpMessage ();
						segment.availableCardIDs = null;


						segments.Add (segment);
					}
				}
				show.segments = segments.ToArray ();
			}
			shows.Add (show);
		}

		return shows;
	}
}
