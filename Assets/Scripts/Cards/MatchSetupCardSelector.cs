﻿using UnityEngine;
using System.Collections;

public class MatchSetupCardSelector : CardSelector {
	public MatchSegmentSetup matchSetup;

	public override void SelectCard(CardObject card)
    {
        
		matchSetup.UpdateOption(optionIndex, card);
	}
	
	public override CardObject GetCard () {
		return matchSetup.cards[optionIndex];
	}
	
	public override void ResetCard() {
		matchSetup.UpdateOption(optionIndex, null);
	}
}
