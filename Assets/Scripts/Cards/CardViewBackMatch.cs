﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardViewBackMatch : CardViewBack {

	public Image cardLogo;
	public Image headshotSprite;

	public Text skillValue;
	public Text popValue;
	public Text pushValue;
	public Text micValue;

	public Text costToPlayValue;
	public Text wrestlerType;
	public Text flavorText;
	
	public override void UpdateBackInfo(CardObject card) {
		// Clone the card - this avoids the wrestling match logo bug on continue
		card = card.Clone();

		// Set card back
		Image cardBackImage = transform.parent.gameObject.GetComponent<Image>();
		cardBackImage.sprite = regCardBackSprite;

		cardLogo.sprite = card.logoImage;
		headshotSprite.sprite = card.headshotImage;

		MatchCardObject mco = (MatchCardObject) card;
		skillValue.text = "+"+mco.bonuses.Skill;
		popValue.text = "+"+mco.bonuses.Pop;
		pushValue.text ="+"+ mco.bonuses.Push;
		micValue.text = "+"+mco.bonuses.Mic;

		if(mco.costToPlay == 0){
			costToPlayValue.text = "FREE";
		}
		else{
			costToPlayValue.text = "$"+mco.costToPlay;
		}

		wrestlerType.text = ""+mco.wrestlerType;

		flavorText.text = mco.flavorText;
	}
}
