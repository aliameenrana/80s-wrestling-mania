﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CardSelector : MonoBehaviour 
{
	public CardSelectScreenScript.CardFilter filterType;
	public List<CardType> hideTypes;
	public int optionIndex;
    public bool Award;

	[HideInInspector]
	public List<CardObject> cards;

	public virtual void SelectCard(CardObject card) 
	{
		
	}
	public virtual CardObject GetCard() 
	{
		return null;
	}
	public virtual void ResetCard() {}
}