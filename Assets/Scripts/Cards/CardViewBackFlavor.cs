﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardViewBackFlavor : CardViewBack {

	public Text cardTitle;
	public Image headshotSprite;
	public Image logoSprite;

	public Image wrestler1Sprite;
	public Image wrestler2Sprite;

	public Text flavorCardClass;
	public Text flavorType;
	public Text AlignmentValue;

	public Text SkillValue;
	public Text MicValue;
	public Text PopValue;
	public Text PushValue;
	public Text MatchValue;
	public Text CashValue;
	public Text LimitValue;

	public Text cardDescription;
	public Text careerYears;


	public override void UpdateBackInfo(CardObject card)
    {
		// Set card back
		Image cardBackImage = transform.parent.gameObject.GetComponent<Image>();
		cardBackImage.sprite = regCardBackSprite;

		if (cardTitle)
			cardTitle.text = card.name;

		if (headshotSprite)
			headshotSprite.sprite = card.headshotImage;
		if (logoSprite)
			logoSprite.sprite = card.logoImage;

		FlavorCardObject co = (FlavorCardObject) card;
		//flavorCardClass.text = co.cardClass;
		//flavorType.text = co.flavorCardType;

		if (careerYears != null) 
		{
			if (co.cardClass.ToUpper () == "MANAGER") {
				careerYears.text = "Career Years: (" + co.FirstActiveYear + " - " + co.LastActiveYear + ")";
			} 
		}




		if (wrestler1Sprite) {
			GameSetupItems items = GamePlaySession.instance.GSI;
			WrestlerCardObject wco1 = (WrestlerCardObject) items.wrestlers.cards[co.unlockRequirements.CardA].Clone();
			wrestler1Sprite.sprite = wco1.logoImage;
		}
		if (wrestler2Sprite) {
			GameSetupItems items = GamePlaySession.instance.GSI;
			WrestlerCardObject wco2 = (WrestlerCardObject) items.wrestlers.cards[co.unlockRequirements.CardB].Clone();
			wrestler2Sprite.sprite = wco2.logoImage;
		}

		if (SkillValue)
			SkillValue.text = "+"+co.bonuses.Skill;
		if (MicValue)
			MicValue.text = "+"+co.bonuses.Mic;
		if (PopValue)
			PopValue.text = "+"+co.bonuses.Pop;
		if (PushValue)
			PushValue.text = "+"+co.bonuses.Push;
		if (MatchValue)
			MatchValue.text = "+"+co.bonuses.Match;
		if (CashValue)
			CashValue.text = "+$"+co.bonuses.Cash+" Per Match";

		if (AlignmentValue) {
			AlignmentValue.text = co.characterAlignment;
			if (co.characterAlignment == "GOOD") {
				AlignmentValue.color = goodColor;
			} else {
				AlignmentValue.color = badColor;
			}
		}

        if (cardDescription)
        {
            cardDescription.text = co.flavorText;
        }
			
	}
}
