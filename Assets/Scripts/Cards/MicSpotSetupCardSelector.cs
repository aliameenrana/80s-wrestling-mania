﻿using UnityEngine;
using System.Collections;

public class MicSpotSetupCardSelector : CardSelector {
	public MicSpotSegmentSetup micSpotSetup;
	
	public override void SelectCard(CardObject card) {
		micSpotSetup.UpdateOption(optionIndex, card);
	}
	
	public override CardObject GetCard () {
		return micSpotSetup.cards[optionIndex];
	}
	
	public override void ResetCard() {
		micSpotSetup.UpdateOption(optionIndex, null);
	}
}
