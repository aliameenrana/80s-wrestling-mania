﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardViewBackMicSpot : CardViewBack {
	
	public Text cardTitle;
	public Image logoSprite;
	public Image headshotSprite;

	public Text alignmentText;
	public Text minWrestlerText;
	public Text maxWrestlerText;

	public Text flavorText;
	public Text costText;
	public Text careerYears;

	public override void UpdateBackInfo(CardObject card) {
		// Set card back
		Image cardBackImage = transform.parent.gameObject.GetComponent<Image>();
		cardBackImage.sprite = regCardBackSprite;

		cardTitle.text = card.name;

		logoSprite.sprite = card.logoImage;
		headshotSprite.sprite = card.headshotImage;
		
		MicSpotCardObject msco = (MicSpotCardObject) card;
		careerYears.text = "Active Years: "+"("+msco.FirstActiveYear + " - " + msco.LastActiveYear+")"; 
		alignmentText.text = msco.bonuses.TypeBonusA + " & " + msco.bonuses.TypeBonusB;
		minWrestlerText.text = msco.wrestlerMin.ToString();
		maxWrestlerText.text = msco.wrestlerMax.ToString();
		if (msco.costToPlay == 0) 
		{
			costText.text = "FREE";
		} 
		else 
		{
			costText.text = "$" + msco.costToPlay.ToString ();
		}
		flavorText.text = msco.flavorText;
	}
}
