﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CardTextSizeCheck : MonoBehaviour {
	public List<Text> textObjects = new List<Text>(); // List of text objects to be affected
	public int sizeModifier = 6; // Fraction to use when reducing size; default 1/6th

#if UNITY_IOS
	void Awake() {
		if ((UnityEngine.iOS.Device.generation.ToString()).IndexOf("iPad") > -1) {
			foreach (Text textObject in textObjects) {
				textObject.fontSize -= (textObject.fontSize / sizeModifier);
				textObject.resizeTextMaxSize = textObject.fontSize;
			}
		}
	}
#endif

}
