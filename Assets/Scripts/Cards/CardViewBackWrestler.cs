﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardViewBackWrestler : CardViewBack {
    
	public Image logoSprite;
	public Image headshotSprite;

	public Text statSkill;
	public Text statPush;
	public Text statMic;
	public Text statPop;
	
	public Text statusData;
	public Text styleData;

	public Text fanData;
	public Text otherStatsData;
	public Text cardDescription;
    public Text CareerYears;

	public LayoutElement beltImages;
	public Image worldBeltImage;
	public Image titleBeltImage;
	public Image tagBeltImage;

	public Sprite worldChampionSprite;
	public Sprite titleChampionSprite;
	public Sprite tagChampionSprite;

    public GameObject PromoteStatusButton;
    public GameObject DemoteStatusButton;
    public GameObject FoilButton;
	int cardNumber;


	public static CardViewBackWrestler instance;

	GameSetupItems items;
	bool listenersAdded;
    private void Awake()
    {
        instance = this;
		listenersAdded = false;
    }

    public void Warn()
    {
        UIManagerScript.instance.CantDoThisScreen.gameObject.SetActive(true);
        UIManagerScript.instance.CantDoThisScreen.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "You can’t use these features until you own this card!";
        UIManagerScript.instance.CantDoThisScreen.gameObject.transform.SetAsLastSibling();
    }

	void Update()
	{
		if (!listenersAdded) 
		{
//			if (GamePlaySession.instance!=null) 
//			{
//				if (cardNumber != 0) 
//				{
//					if ((GamePlaySession.instance.CheckCard (cardNumber + 1)!=null) || (GamePlaySession.instance.CheckCard (cardNumber - 1)!=null)) 
//					{
//						FoilButton.GetComponent<Button> ().interactable = true;
//					}
//				}
//			}


			if (ScreenHandler.current.FindOpenScreen().name == "MainCalendarScreen" || ScreenHandler.current.FindOpenScreen().name == "ShopScreen")
			{
				FoilButton.GetComponent<Button>().onClick.AddListener(() => Warn());
				PromoteStatusButton.GetComponent<Button>().onClick.AddListener(() => Warn());
				DemoteStatusButton.GetComponent<Button>().onClick.AddListener(() => Warn());
				listenersAdded = true;
			}
			else if (ScreenHandler.current.FindOpenScreen().name == "CardViewScreen")
			{
				FoilButton.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.ShowFoilCard());
				PromoteStatusButton.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.PromoteWrestlerStatus());
				DemoteStatusButton.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.DemoteWrestlerStatus());
				listenersAdded = true;
			}
		}
	}

    public override void UpdateBackInfo(CardObject card)
    {
		FoilButton.GetComponent<Button> ().interactable = false;

		WrestlerCardObject wrestler = (WrestlerCardObject) card;
		cardNumber = wrestler.CardNumber;

		if (GamePlaySession.instance!=null) 
		{
			if (cardNumber != 0) 
			{
				if ((GamePlaySession.instance.CheckCard (cardNumber + 1)!=null) || (GamePlaySession.instance.CheckCard (cardNumber - 1)!=null)) 
				{
					FoilButton.GetComponent<Button> ().interactable = true;
				}
			}
		}

		// Check if card is foil
		Image cardBackImage = transform.parent.gameObject.GetComponent<Image>();
		if (wrestler.cardVersion == "FOIL")
        {
			cardBackImage.sprite = foilCardBackSprite;
        }
        else
        {
			cardBackImage.sprite = regCardBackSprite;
        }

        logoSprite.sprite = wrestler.logoImage;
        headshotSprite.sprite = wrestler.headshotImage;
        statusData.text = wrestler.characterStatus;
        
		styleData.text = wrestler.wrestlerType;

		fanData.text = wrestler.cardAlignment;
        CareerYears.text = "Career Years: "+"("+wrestler.FirstActiveYear + " - " + wrestler.LastActiveYear+")"; 
		if (wrestler.cardAlignment == "Good")
        {
			fanData.color = goodColor;
		}
        else
        {
			fanData.color = badColor;
		}

        if (wrestler.skillCurrent >= wrestler.stats.SkillMax)
        {
			wrestler.skillCurrent = wrestler.stats.SkillMax;
            statSkill.text = (wrestler.skillCurrent.ToString()) + ("<color=#ff0000>(MAX)</color>");
            statSkill.GetComponent<Text>().resizeTextForBestFit = true;
        }
        else if (wrestler.skillCurrent < wrestler.stats.SkillMax)
        {
            statSkill.text = (wrestler.skillCurrent.ToString());
        }

		if (wrestler.micCurrent >= wrestler.stats.MicMax)
        {
			wrestler.micCurrent = wrestler.stats.MicMax;
			statMic.text = (wrestler.micCurrent.ToString()) + ("<color=#ff0000>(MAX)</color>");
            statMic.GetComponent<Text>().resizeTextForBestFit = true;
        }

        else if (wrestler.micCurrent < wrestler.stats.MicMax)
        {
            statMic.text = (wrestler.micCurrent.ToString());
        }

        if (wrestler.popCurrent >= wrestler.stats.PopMax)
        {
			wrestler.popCurrent = wrestler.stats.PopMax;
            statPop.text = (wrestler.popCurrent.ToString()) + ("<color=#ff0000>(MAX)</color>");
            statPop.GetComponent<Text>().resizeTextForBestFit = true;
        }

        else if (wrestler.popCurrent < wrestler.stats.PopMax)
        {
            statPop.text = (wrestler.popCurrent.ToString());
        }

		statPush.text = wrestler.pushCurrent.ToString();

		otherStatsData.text = wrestler.characterHeight + " " + wrestler.characterWeight + " " + wrestler.characterHometown;
		cardDescription.text = wrestler.flavorText;


		if (beltImages != null) {
			bool worldChamp = false;
			bool titleChamp = false;
			bool tagChamp = false;

			if (GamePlaySession.instance != null) {
				if (GamePlaySession.instance.worldChampion != null && GamePlaySession.instance.worldChampion.CardNumber== wrestler.CardNumber) {
					worldChamp = true;
				}
				if (GamePlaySession.instance.titleChampion != null && GamePlaySession.instance.titleChampion.CardNumber == wrestler.CardNumber)
                {
                    titleChamp = true;
				}
				if (GamePlaySession.instance.tagChampions != null)
                {
					if (wrestler.CardNumber == GamePlaySession.instance.tagChampions[0].CardNumber || wrestler.CardNumber == GamePlaySession.instance.tagChampions[1].CardNumber)
                    {
                        tagChamp = true;
                    }	
				}

				if (worldChamp || titleChamp || tagChamp)
                {
					beltImages.preferredHeight = 40;
					statusData.text = "CHAMP";
					if (worldChamp) {
						beltImages.preferredHeight += 20;
						worldBeltImage.gameObject.SetActive(true);
					} else {
						worldBeltImage.gameObject.SetActive(false);
					}
					if (titleChamp) {
                        beltImages.preferredHeight += 20;
						titleBeltImage.gameObject.SetActive(true);
					} else {
						titleBeltImage.gameObject.SetActive(false);
					}
					if (tagChamp) {
						beltImages.preferredHeight += 20;
						tagBeltImage.gameObject.SetActive(true);
					} else {
						tagBeltImage.gameObject.SetActive(false);
					}
				} else {
					beltImages.preferredHeight = 0;
					worldBeltImage.gameObject.SetActive(false);
					titleBeltImage.gameObject.SetActive(false);
					tagBeltImage.gameObject.SetActive(false);
				}
			}
		}
	}

	public void SetAverageImage(Image img, float avg){
		img.fillAmount = avg;
	}
}
