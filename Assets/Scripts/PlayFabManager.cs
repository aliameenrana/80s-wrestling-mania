﻿//using UnityEngine;
//
////Umar::
//using System;
//using PlayFab;
//using PlayFab.ClientModels;
//using System.Collections.Generic;
//using System.Collections;
//using System.Linq;
//
//public delegate void OnUserDataRecieved(Dictionary<string, string> data);
//
//public class PlayFabManager
//{
//	public string titleId = "3171";
//	public event OnUserDataRecieved dataRecievedEvent;	
//
//	private static PlayFabManager instance = null;
//	private string playFabId;
//
//	private PlayFabManager()
//	{
//		PlayFabSettings.TitleId = titleId;
//		playFabId = "";
//	}
//
//	public static PlayFabManager GetInstance()
//	{
//		if(instance == null)
//		{
//			instance = new PlayFabManager();
//		}
//		return instance;
//	}
//
//	void PlayFabErrorCallback(PlayFabError error)
//	{
//		Debug.Log("Error occured while making playfab api call");
//		Debug.Log("Error : " + error.Error.ToString() + " HTTPCode : " + error.HttpCode + "HTTPStatus : " + error.HttpStatus + " Message : " + error.ErrorMessage);
//	}
//
//	void LoginResultCallback(LoginResult result)
//	{
//		playFabId = result.PlayFabId;
//
//		//Debug.Log("Got PlayFabID: " + playFabId);
//
//		if(result.NewlyCreated)
//		{
//			//Debug.Log("(new account)");
//		}
//		else
//		{
//			//Debug.Log("(existing account)");
//		}
//	}
//
//	void UpdateUserDataResultCallback(UpdateUserDataResult result)
//	{
//		Debug.Log("Successfully updated user data");
//	}
//
//	void GetUserDataResultCallback(GetUserDataResult result)
//	{
//		if ((result.Data == null) || (result.Data.Count == 0))
//		{
//			Debug.Log("No user data available");
//			dataRecievedEvent(null);
//		}
//		else
//		{
//			Debug.Log("Got user data:");
//			dataRecievedEvent(result.Data.ToDictionary(item => item.Key, item => item.Value.Value));
//		}
//	}
//
//	public void Login(string customID)
//	{
//		LoginWithCustomIDRequest request = new LoginWithCustomIDRequest()
//		{
//			TitleId = titleId,
//			CreateAccount = true,
//			CustomId = customID
//		};
//
//		PlayFabClientAPI.LoginWithCustomID( request, LoginResultCallback, PlayFabErrorCallback);
//	}
//
//	public bool IsPFLoggedIn()
//	{
//		return PlayFabClientAPI.IsClientLoggedIn();
//	}
//
//	public void SetUserData(Dictionary<string, string> data)
//	{
//		UpdateUserDataRequest request = new UpdateUserDataRequest()
//		{
//			Data = data
//		};
//
//		PlayFabClientAPI.UpdateUserData(request, UpdateUserDataResultCallback, PlayFabErrorCallback);
//	}
//
//	public void GetUserData()
//	{
//		GetUserDataRequest request = new GetUserDataRequest()
//		{
//			PlayFabId = playFabId,
//			Keys = null
//		};
//
//		PlayFabClientAPI.GetUserData(request, GetUserDataResultCallback, PlayFabErrorCallback);
//	}
//
//	//Umar:: Custom function to generate dictionary from args
//	public static Dictionary<string, string> GetDict(params object[] args)
//	{
//		if (args.Length%2 != 0)
//		{
//			Debug.LogError("Umar:: Error: Dictionary requires an even number of arguments!"); 
//			return null;
//		}
//		else
//		{
//			Dictionary<string, string> dict = new Dictionary<string, string>(args.Length/2);
//
//			int i = 0;
//			while(i < args.Length - 1)
//			{
//				dict.Add(args[i].ToString(), args[i+1].ToString());
//				i += 2;
//			}
//			return dict;
//		}
//	}
//}
