﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TransitionScreen : MonoBehaviour {
	public float fadeTime = 5.5f;
	public Animator resultsScreen;
    public GameObject UIManager;
	public TransitionSegmentDisplay segmentDisplay;

	public PopUpMessage transitionPopUpMessage;
	
	[HideInInspector]
	public bool needSave;
	
	[HideInInspector]
	public ShowObject show;

	private bool showingSegment;
	private int segmentNum;


    private void Awake()
    {
        UIManager = GameObject.Find("UIManager");    
    }
    public void OpenScreen()
    {
		if (!GamePlaySession.instance.GetTransitionMessageClear() && GamePlaySession.instance.GetInTutorialMode())
        {
			PopUpWindow popUpWindow = GameObject.Find("UIManager").GetComponent<UIManagerScript>().popUpWindow;

			if (popUpWindow != null) {
				popUpWindow.OpenPopUpWindow(new PopUpMessage[] {transitionPopUpMessage}, this.gameObject.GetComponent<Animator> ());
			}

			GamePlaySession.instance.SetTransitionMessageClear(true);
		} 
		else 
		{
			if (ScreenHandler.current != null)
				ScreenHandler.current.OpenPanel (this.gameObject.GetComponent<Animator> ());
		}

		/*
		if (GameObject.Find("UIManager").GetComponent<UIManagerScript>().sfxEnabled)
			audio.Play();
		*/

		ResetDisplay();
	}
	
	private void ResetDisplay()
    {
		segmentNum = 0;
		showingSegment = false;
		segmentDisplay.ResetSegment();
		segmentDisplay.needSave = needSave;
	}
	
	void Update() {
		if (!showingSegment) {
			segmentDisplay.ProcessSegment(show.segments[segmentNum]);
			segmentDisplay.ShowSegment();
			showingSegment = true;
		}
		
		if (showingSegment) {
			if (segmentDisplay.SegmentDone()) {
				segmentNum += 1;
				if (segmentNum >= show.segments.Length) {
					ChangeScreen();
				} else {
					segmentDisplay.ResetSegment();
					showingSegment = false;
				}
			}
		}

		/*
		// If we need to save and we're mid-transition then do so
		if (timePassed >= (fadeTime / 4f) && needSave) {
			GamePlaySession.instance.SaveAll();
			needSave = false;
		}

		if (timePassed > fadeTime)
			ChangeScreen();
		*/
	}
	
	public void ChangeScreen()
	{
		GameObject go = resultsScreen.gameObject.transform.Find ("ButtonTitleRow").Find("BackWindow").GetChild(0).gameObject;
		go.GetComponent<Button> ().onClick.AddListener (() => UIManagerScript.instance.OnSaveAll ());
		gameObject.GetComponent<ThemeController> ().ChangeSprite (go, true);

		if (ScreenHandler.current != null && resultsScreen != null)
			ScreenHandler.current.OpenPanel(resultsScreen);

        

//        foreach (AudioSource item in UIManager.GetComponents<AudioSource>())
//        {
//            if (item.clip.name == "Cheer")
//            {
//                item.Stop();
//            }
//        }
    }
}
