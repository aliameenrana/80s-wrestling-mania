﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class SplashLogo : MonoBehaviour 
{
	public float fadeTime = 5f;
	public Animator titleScreen, loginScreen;
	
	private float timePassed;

	public GameObject blackBackground;
	public GameObject UIManager;
	public bool isSecondScreen;
	public CardSelectScreenScript cardViewer;
//	public GameSetupItems gsi;
	string myEmail = "";
	string myPassword = "";
	bool waitForRegister;
	bool sendNow;
	bool giveUp;

	void Start()
	{
		UIManager = GameObject.Find ("UIManager");
		if (blackBackground != null)
			blackBackground.SetActive(true);

		waitForRegister = true;

		myEmail = PlayerPrefs.GetString ("myEmail","");
		myPassword = PlayerPrefs.GetString ("myPassword","");
	}

	void Update() 
	{
		timePassed += Time.deltaTime;

		if (timePassed > fadeTime)
			ChangeScreen();
	}

	public void ChangeScreen()
    {
		string path = "";

		#if UNITY_EDITOR
		path = Application.dataPath + "/userdata.dat";
		#elif UNITY_ANDROID || UNITY_IOS
		path = Application.persistentDataPath + "/userdata.dat";
		#endif

		if (ScreenHandler.current != null && titleScreen != null && loginScreen != null)
		{
			if (PlayerPrefs.GetString ("UserID", "") == "" && PlayerPrefs.GetString ("parseUserID", "") == "") 
			{
				//This is a new User - We will register them and take them straight to the Title Screen where they can play
				UIManager.GetComponent<UIManagerScript> ().CreateOrLoadGamePlaySession (true);
				ScreenHandler.current.OpenPanel (UIManagerScript.instance.RegisterScreen);
			} 
			else
			{
				if (File.Exists (path) && PlayerPrefs.GetString("myEmail","")!="") 
				{
					UIManager.GetComponent<UIManagerScript> ().CreateOrLoadGamePlaySession ();
					ScreenHandler.current.OpenPanel (UIManagerScript.instance.TitleScreen);
					sendNow = false;
					giveUp = false;
					StartCoroutine (WaitForSending ());
					ParseManager.LogInUser (myEmail, myPassword, OnLoginParse);
				} 
				else 
				{
					ScreenHandler.current.OpenPanel (UIManagerScript.instance.LoginScreen);
				}
			}		
		}
	}


	IEnumerator WaitForSending()
	{
		yield return new WaitUntil (() => (sendNow == true || giveUp == true));
		if (sendNow == true) 
		{
			PlayerPrefs.SetString ("parseUserID", Parse.ParseUser.CurrentUser.ObjectId.ToString ());
			ParseManager.PrepareAndSendAllUserData ();	
		}
	}


	public void OnDisable(){
		if(!isSecondScreen)
			GameObject.Find("UIManager").GetComponent<GameSetupItems>().PlayTheme();
	}

	void OnLoginParse(bool success, string exceptionString = "")
	{
		if (success) 
		{
			sendNow = true;
		} 
		else 
		{
			ParseManager.RegisterUser (myEmail, myPassword, OnRegisterParse);	
		}
	}


	void OnRegisterParse(bool success, string email,string password, string exceptionString = "")
	{
		if (success) 
		{
			sendNow = true;
		} 
		else
		{
			giveUp = true;
		}
	}

	bool HasSave()
	{
		return (PlayerPrefs.GetString("GAMEPLAYSESSION", "NONE") != "NONE");
	}



}
