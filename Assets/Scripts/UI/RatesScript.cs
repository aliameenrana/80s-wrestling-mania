﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RatesScript : MonoBehaviour
{
    public static RatesScript instance;
    List<int> FedNumbers;
    List<float> FedRatings;

    void Awake()
    {
        GameObject UIManager = GameObject.Find("UIManager");
        FedNumbers = new List<int> { 1, 2, 0 };
        FedRatings = new List<float> { 0, 0, 0 };
        instance = this;
    }

    public void UpdateRatings()
    {
        //if (GamePlaySession.instance.RatingPositionHolder.Count == 0)
        //{
        //    GamePlaySession.instance.RatingPositionHolder.Add(1);
        //    GamePlaySession.instance.RatingPositionHolder.Add(2);
        //    GamePlaySession.instance.RatingPositionHolder.Add(0);
        //}

        //if (GamePlaySession.instance.FedRatings.Count == 0)
        //{
        //    GamePlaySession.instance.FedRatings.Add(0);
        //    GamePlaySession.instance.FedRatings.Add(0);
        //    GamePlaySession.instance.FedRatings.Add(0);
        //}
        

        Dictionary<int, float> RatingDict = new Dictionary<int, float>();
        int i = 0;

        for (int t = 0; t < 3; t++)
        {
			if (FedNumbers[t] == GamePlaySession.instance.GetSelectedFederation())
            {
				if (GamePlaySession.instance.GetAverageRating() >= 20.0f)
                {
					GamePlaySession.instance.SetAverageRating(20.0f);
                }
				FedRatings[t] = GamePlaySession.instance.GetAverageRating();
            }
            else
            {
                float R = UnityEngine.Random.Range(0.0f, 20.0f);
                FedRatings[t] = R;
            }
        }

        foreach (float item in FedRatings)
        {
            RatingDict.Add(FedNumbers[i], item);
            i++;
        }
        FedRatings.Sort(); FedRatings.Reverse();
        List<int> FedsOut = new List<int>();
        foreach (var item in FedRatings)
        {
            FedsOut.Add(RatingDict.FirstOrDefault(x => x.Value == item).Key);
        }

        for (int o = 0; o < 3; o++)
        {
            GamePlaySession.instance.RatingPositionHolder[o] = FedsOut[o];
            GamePlaySession.instance.FedRatings[o] = FedRatings[o];
        }
    }

}
