using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class ScreenHandler : MonoBehaviour {

	public static ScreenHandler current { get; set; }
	
	//Screen to open automatically at the start of the Scene
	public Animator initiallyOpen;
	//Currently Open Screen
	private Animator m_Open;
	
	//Hash of the parameter we use to control the transitions.
	private int m_OpenParameterId;
	
	//The GameObject Selected before we opened the current Screen.
	//Used when closing a Screen, so we can go back to the button that opened it.
	private GameObject m_PreviouslySelected;
	
	//Animator State and Transition names we need to check against.
	const string k_OpenTransitionName = "Open";
	const string k_ClosedStateName = "Closed";

	void Awake() {
		// Make sure there's only one current LobbyHandler and our static reference is set to it
		if (current == null)
			current = this;
		#if UNITY_EDITOR
		else {
			Debug.LogWarning("Multiple ScreenHandlers in scene.");
		}
		#endif
	}

	public Animator GetCurrentPanel() {
		return m_Open;
	}

	public void OnEnable()
	{
		//We cache the Hash to the "Open" Parameter, so we can feed to Animator.SetBool.
		m_OpenParameterId = Animator.StringToHash (k_OpenTransitionName);
		
		//If set, open the initial Screen now.
		if (initiallyOpen == null)
			return;
		OpenPanel(initiallyOpen);
		StartCoroutine(SetupScreenHack());
	}
	
	//Closes the currently open panel and opens the provided one.
	//It also takes care of handling the navigation, setting the new Selected element.
	public void OpenPanel (Animator anim)
	{
		if (m_Open == anim){
			return;
		}
		//Activate the new Screen hierarchy so we can animate it.
		anim.gameObject.SetActive(true);
        //Save the currently selected button that was used to open this Screen. (CloseCurrent will modify it)
        var newPreviouslySelected = EventSystem.current.currentSelectedGameObject;
        //Move the Screen to front.
        anim.transform.SetAsLastSibling();
//		anim.GetComponent<ThemeController> ().CheckTheme ();

		CloseCurrent(anim);
		
		m_PreviouslySelected = newPreviouslySelected;
		if(m_PreviouslySelected != null){
			//Debug.Log ("new previously selected: "+m_PreviouslySelected.name);
			//Debug.Log ("new current selected: "+anim.name);
		}
		//Set the new Screen as then open one.
		m_Open = anim;

		//Debug.Log ("current selected state:" +m_Open.GetBool(m_OpenParameterId));
		//Debug.Log("transitioning?  "+m_Open.GetAnimatorTransitionInfo(0).nameHash);
		//Start the open animation
		//m_Open.gameObject.SetActive(true);
		m_Open.SetBool(m_OpenParameterId, true);

        //StartCoroutine(EnablePanelDeleyed(m_Open));
        //Debug.Log ("current selected state:" +m_Open.GetBool(m_OpenParameterId));
        //Debug.Log("transitioning?  "+m_Open.GetAnimatorTransitionInfo(0).n);

        //Set an element in the new screen as the new Selected one.
        GameObject go = FindFirstEnabledSelectable(anim.gameObject);
        SetSelected(go);
    }

	public void RefreshPanel() {
		if (m_Open == null)
			return;

		//Start the open animation
		m_Open.SetBool(m_OpenParameterId, true);

        //Set an element in the new screen as the new Selected one.

        GameObject go = FindFirstEnabledSelectable(m_Open.gameObject);
        SetSelected(go);
    }
	
	//Finds the first Selectable element in the providade hierarchy.
	static GameObject FindFirstEnabledSelectable (GameObject gameObject)
	{
		GameObject go = null;
		var selectables = gameObject.GetComponentsInChildren<Selectable> (true);
		foreach (var selectable in selectables) {
            //Debug.Log("This Possible-Selectable is: " + selectable.name);
			if (selectable.IsActive () && selectable.IsInteractable ())
            {
                //Debug.Log("Found a Selectable. Selectable name: "+selectable);
				go = selectable.gameObject;
				break;
			}
		}
		return go;
	}
	
	//Closes the currently open Screen
	//It also takes care of navigation.
	//Reverting selection to the Selectable used before opening the current screen.
	public void CloseCurrent(Animator Saver)
	{
		if (m_Open == null)
			return;
		
		//Start the close animation.
		m_Open.SetBool(m_OpenParameterId, false);
		
		//Reverting selection to the Selectable used before opening the current screen.
		SetSelected(m_PreviouslySelected);
		//Start Coroutine to disable the hierarchy when closing animation finishes.
		StartCoroutine(DisablePanelDeleyed(m_Open,Saver));
		//No screen open.
		m_Open = null;
	}
	
	//Coroutine that will detect when the Closing animation is finished and it will deactivate the
	//hierarchy.
	IEnumerator DisablePanelDeleyed(Animator anim, Animator Saver)
	{
		bool closedStateReached = false;
		bool wantToClose = true;
		while (!closedStateReached && wantToClose)
		{
			if (!anim.IsInTransition(0))
				closedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(k_ClosedStateName);
			
			wantToClose = !anim.GetBool(m_OpenParameterId);
			
			yield return new WaitForEndOfFrame();
		}

        if (wantToClose)
        {
            anim.gameObject.SetActive(false);
//            if (Saver.gameObject.CompareTag("Saver"))
//            {
//				//StartCoroutine (SaveDelayed ());
//            }
        }
	}

	public IEnumerator SetupScreenHack(){
		while(true){
			yield return new WaitForSeconds (0.5f);
			if(!m_Open.transform.parent.GetChild(m_Open.transform.parent.childCount - 1).gameObject.activeSelf && m_Open.transform.parent.GetChild(m_Open.transform.parent.childCount - 1).name.Contains("SetupScreen")){
				m_Open.gameObject.SetActive(true);
				m_Open.SetBool(m_OpenParameterId, true);
				//Debug.Log("HACKKKS");
			}
		}
	}

	//Coroutine that will detect when the Closing animation is finished and it will deactivate the
	//hierarchy.
	IEnumerator EnablePanelDeleyed(Animator anim)
	{
		bool openStateReached = false;
		bool wantToOpen = true;
		while (!openStateReached && wantToOpen)
		{
			if (!anim.IsInTransition(0))
				openStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName("Open");
			
			wantToOpen = anim.GetBool(m_OpenParameterId);
			
			yield return new WaitForEndOfFrame();
		}

        if (wantToOpen)
        {
            anim.gameObject.SetActive(true);
            
        }
	}
	
	//Make the provided GameObject selected
	//When using the mouse/touch we actually want to set it as the previously selected and 
	//set nothing as selected for now.
	private void SetSelected(GameObject go)
	{
		//Select the GameObject.
		EventSystem.current.SetSelectedGameObject(go);
		
		//If we are using the keyboard right now, that's all we need to do.
		var standaloneInputModule = EventSystem.current.currentInputModule as StandaloneInputModule;
		if (standaloneInputModule != null && standaloneInputModule.inputMode == StandaloneInputModule.InputMode.Buttons)
			return;
		
		//Since we are using a pointer device, we don't want anything selected. 
		//But if the user switches to the keyboard, we want to start the navigation from the provided game object.
		//So here we set the current Selected to null, so the provided gameObject becomes the Last Selected in the EventSystem.
		EventSystem.current.SetSelectedGameObject(null);
	}

    public GameObject FindOpenScreen()
    {
        return m_Open.gameObject;
    }


//	IEnumerator SaveDelayed()
//	{
//		yield return new WaitForSeconds (2f);
//		GamePlaySession.instance.SaveAll(true,true);
//	}

}