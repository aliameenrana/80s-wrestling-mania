﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeBackground : MonoBehaviour
{

	public float fadeTime = 1f;
	public bool fadeOut;

	private Image image;
	private float timePassed;

	void Awake()
    {
		image = this.gameObject.GetComponent<Image>();
	}

	void Update()
    {
		if (!fadeOut)
			return;

		timePassed += Time.deltaTime;

		Color newColor = new Color(1f, 1f, 1f, Mathf.Lerp(1f, 0f, timePassed / fadeTime));

		if (image != null)
			image.color = Color.Lerp(image.color, newColor, Time.deltaTime * 5f);

		if (timePassed > fadeTime + 0.5f)
			this.gameObject.SetActive(false);
	}

	public void SetFade(bool fade)
    {
		fadeOut = fade;
	}
}
