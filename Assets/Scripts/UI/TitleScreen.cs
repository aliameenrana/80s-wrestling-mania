﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class TitleScreen : MonoBehaviour {
	public GameObject newGameConfirmWindow;
	public GameObject newGamePlusConfirmWindow;
    public GameObject ContinueButton,NewGameButton;
    public GameObject blackBG;
    public Sprite NG;
    public Sprite NGPlus;
	public string newGamePlusText = "New Game+!";
    string SaveData;
	public UIManagerScript UIManager;

    private void Awake()
    {
        SaveData = PlayerPrefs.GetString("GAMEPLAYSESSION", "NONE");
    }

    public void OnEnable()
    {
		string path = "";
		#if UNITY_EDITOR
		path = Application.dataPath + "/userdata.dat";
		#elif UNITY_ANDROID || UNITY_IOS
		path = Application.persistentDataPath + "/userdata.dat";
		#endif
        //Debug.Log(HasSave ());
        blackBG.SetActive(false);

		if (File.Exists(path) || (PlayerPrefs.GetString("GAMEPLAYSESSION","NONE")!="NONE"))
        {
			ContinueButton.GetComponent<Button>().interactable = true;
			//NewGameButton.GetComponent<Button> ().interactable = false;
		}
        else
        {
			ContinueButton.GetComponent<Button>().interactable = false;
			NewGameButton.GetComponent<Button> ().interactable = true;
		}

		if (NewGamePlus())
        {
			NewGameButton.GetComponent<Image>().sprite = NGPlus;
		}

		if (UIManagerScript.instance.transferCash > -1) 
		{
			ContinueButton.GetComponent<Button> ().interactable = false;
			NewGameButton.GetComponent<Button> ().interactable = true;
			NewGameButton.GetComponent<Image> ().sprite = NGPlus;
		}
		UIManagerScript.instance.CheckGifts ();
	}
    private void OnDisable()
    {
        newGameConfirmWindow.SetActive(false);
		newGamePlusConfirmWindow.SetActive(false);
    }

    public void OpenNewGame()
    {
        if (NewGamePlus())
        {
			newGamePlusConfirmWindow.SetActive(true);
		}
//        else if (HasSave())
//        {
//			newGameConfirmWindow.SetActive(true);
//		}
        else
        {
			UIManager.NewGame();
		}
	}

	bool HasSave()
	{
		return (PlayerPrefs.GetString("GAMEPLAYSESSION", "NONE") != "NONE");
	}

	bool NewGamePlus()
    {
		if ((UIManagerScript.instance.transferCash > -1) || (GamePlaySession.instance != null && (!GamePlaySession.instance.GetInTutorialMode () || GamePlaySession.instance.GetNewGamePlus ()))) 
		{
//			Debug.Log ("TransferCash: " + UIManagerScript.instance.transferCash);
//
//			Debug.Log ("GamePlaySession.instance.GetInTutorialMode (): " + GamePlaySession.instance.GetInTutorialMode ());
//			Debug.Log ("GamePlaySession.instance.GetNewGamePlus (): " + GamePlaySession.instance.GetNewGamePlus ());
			return true;
		} 
		else
		{
			return false;
		}
	}
}
