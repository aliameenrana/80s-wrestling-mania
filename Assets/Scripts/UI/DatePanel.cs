﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DatePanel : MonoBehaviour {
	public Text yearText;
	public Text monthText;

	public void UpdateDateDisplay() {
		int year = GamePlaySession.instance.GetCurrentYear();
		int month = GamePlaySession.instance.GetCurrentMonth();

		if(year >= GamePlaySession.instance.myCalendar.years.Length)
        {
			yearText.text = GamePlaySession.instance.myCalendar.years[0].yearName;
			monthText.text = GamePlaySession.instance.myCalendar.years[0].months[month].monthName;
		}
		else
        {
			yearText.text = GamePlaySession.instance.myCalendar.years[year].yearName;
			monthText.text = GamePlaySession.instance.myCalendar.years[year].months[month].monthName;
		}
	}

	public void NextMonth() {
		GamePlaySession.instance.SetCurrentMonth(GamePlaySession.instance.GetCurrentMonth() + 1);
		if (GamePlaySession.instance.GetCurrentMonth() >= 12) {
			GamePlaySession.instance.SetCurrentMonth(0);
			GamePlaySession.instance.SetCurrentYear(GamePlaySession.instance.GetCurrentYear() + 1);

			if (GamePlaySession.instance.GetCurrentYear() >= GamePlaySession.instance.GetNumberOfYears()) {
				GamePlaySession.instance.SetCurrentYear(0);
			}
		}

		UpdateDateDisplay();
	}

	public void PrevMonth() {
		GamePlaySession.instance.SetCurrentMonth(GamePlaySession.instance.GetCurrentMonth() - 1);

		if (GamePlaySession.instance.GetCurrentMonth() < 0) {
			GamePlaySession.instance.SetCurrentMonth(11);
			GamePlaySession.instance.SetCurrentYear(GamePlaySession.instance.GetCurrentYear() - 1);

			if (GamePlaySession.instance.GetCurrentYear() < 0) {
				GamePlaySession.instance.SetCurrentYear(GamePlaySession.instance.GetNumberOfYears() - 1);
			}
		}

		UpdateDateDisplay();
	}
}
