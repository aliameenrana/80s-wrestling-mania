﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreditsScreen : MonoBehaviour {
	private GameObject UIManager;
	private GameObject playerHUD;

    void Awake()
    {
        UIManager = GameObject.Find("UIManager");
    }

    public void CloseBack() 
	{		
		//UIManager = GameObject.Find("UIManager");
		UIManager.GetComponent<UIManagerScript>().GoToTitle();
	}
	
	public void CloseHome() 
	{
		//UIManager = GameObject.Find("UIManager");
		UIManager.GetComponent<UIManagerScript>().GoToTitle();
	}

	public void OpenGameWebsite() {
		Application.OpenURL("http://80sManiaWrestling.com/");
	}
	
	public void OpenGameTwitter() {
		Application.OpenURL("https://twitter.com/80sManiaWrestle/");
	}
	
	public void OpenGameFacebook() {
		Application.OpenURL("https://www.facebook.com/80sManiaWrestling");
	}
	
	public void OpenCMTwitter() {
		Application.OpenURL("https://twitter.com/CheckmateCreate/");
	}
	
	public void OpenOskTwitter() {
		Application.OpenURL("https://twitter.com/WizardofOsk/");
	}
}
