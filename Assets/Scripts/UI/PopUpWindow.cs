﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PopUpMessage
{
	public string dummy; /* dummy property to avoid the weird overlap in Inspector */
	[Multiline]
	public string message;
	public GiftObject gift;
	public bool isNewYear;
    [HideInInspector]
    public bool FantasyPopup;
}

public class PopUpWindow : MonoBehaviour {
	public Text messageText;
	public GiftInfo giftInfo;
	public Animator previousScreen;
    [HideInInspector]
    public Image FantasyPopUpBanner;


	public GameObject newYearWindow;
	public GameObject confirmButton;
	public CardSelectScreenScript cardViewer;

	private Queue<PopUpMessage> messageQueue = new Queue<PopUpMessage>();
	public GameObject playerHUD;
	GameObject UIManager;
	private bool hadHUD;

	private bool hasCards;
	private string cardMessage;
	private List<CardObject> cards = new List<CardObject>();

	public void OpenPopUpWindow(PopUpMessage[] messages, Animator screen, bool hasHUD = false, bool fromNotification = false)
    {
		if (UIManager == null)
			UIManager = GameObject.Find ("UIManager");

		playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;
		
		if (playerHUD != null)
        {
			if (playerHUD.activeSelf || hasHUD)
            {
				hadHUD = true;
				playerHUD.SetActive(false);
			}
            else
            {
				hadHUD = false;
			}
		}
		if (fromNotification) 
		{
			hadHUD = false;
		}

		previousScreen = screen;

		// Set up our message queue based on the messages provided
		messageQueue.Clear();
		foreach (PopUpMessage message in messages)
        {
			messageQueue.Enqueue(message);
		}

		// Set our first message
		DisplayMessage();

		if (ScreenHandler.current != null)
        {
			Animator popUpScreen = this.GetComponent<Animator>();
			ScreenHandler.current.OpenPanel(popUpScreen);
		}
	}

	public void ConfirmButton()
    {
		UIManagerScript.instance.OnSaveAll ();
		FantasyPopUpBanner.gameObject.SetActive(false);
        if (messageQueue.Count == 0)
        {
            if (!hasCards)
            {
                if (hadHUD)
                {
                    if (playerHUD != null) 
					{
						playerHUD.SetActive(true);
					}
				}

				if (ScreenHandler.current != null && previousScreen != null)
					ScreenHandler.current.OpenPanel(previousScreen);
			}
            else
            {
                NewCardsObject newCards = new NewCardsObject();
				newCards.message = new PopUpMessage();
				newCards.message.message = cardMessage;
				
				newCards.cards = cards;
				
				if (cardViewer != null)
					cardViewer.OpenCardView(newCards, previousScreen, hadHUD);
			}
		}
        else
        {
            DisplayMessage();
		}
	}

	public void NewYearExchange() 
	{
		if (newYearWindow != null)
			newYearWindow.SetActive(false);

		GamePlaySession.instance.SetMyCash(GamePlaySession.instance.GetMyCash() - 100000);
		GamePlaySession.instance.SetMyTokens(GamePlaySession.instance.GetMyTokens() + 1);
	}

	public void DisplayMessage()
    {
		
		confirmButton.transform.GetChild (0).gameObject.GetComponent<Text> ().text = "OKAY!";

		if (messageText != null)
        {
			PopUpMessage message = messageQueue.Dequeue();
			messageText.text = message.message;
            if (message.FantasyPopup)
            {
                FantasyPopUpBanner.gameObject.SetActive(true);
            }
                


			if (newYearWindow != null)
            {
				if (message.isNewYear && GamePlaySession.instance.GetMyCash() >= 100000)
                {
					newYearWindow.SetActive(true);
				}
                else
                {
					newYearWindow.SetActive(false);
				}
			}

			hasCards = false;

			if (message.gift != null)
            {
				if (message.gift.cash > 0 || message.gift.tokens > 0)
                {
					if (message.gift.cash > 0)
						GamePlaySession.instance.SetMyCash(GamePlaySession.instance.GetMyCash() + message.gift.cash);

					if (message.gift.tokens > 0)
						GamePlaySession.instance.SetMyTokens(GamePlaySession.instance.GetMyTokens() + message.gift.tokens);

					if (giftInfo != null)
                    {
						giftInfo.gameObject.SetActive(true);
						giftInfo.ShowGift(message.gift);
					}
				}
                else
                {
					if (giftInfo != null)
                    {
						giftInfo.gameObject.SetActive(false);
					}
				}

				if (message.gift.cardIDs != null && message.gift.cardIDs.Length > 0)
                {
					if (UIManager == null)
						UIManager = GameObject.Find ("UIManager");

					if (message.gift.cardIDs[0] > 0) 
					{
						confirmButton.transform.GetChild (0).gameObject.GetComponent<Text> ().text = "Click to Accept";

						GameSetupItems items = UIManager.GetComponent<GameSetupItems>();

						hasCards = true;
						cardMessage = message.gift.cardMessage;

						cards.Clear();
						foreach (int cardID in message.gift.cardIDs)
						{
							if (cardID > 0) 
							{
								cards.Add(items.GetCard(cardID).Clone());
							}
						}
					}
				}
			}
            else
            {
				if (giftInfo != null)
                {
					giftInfo.gameObject.SetActive(false);
				}
			}
		}
	}


}
