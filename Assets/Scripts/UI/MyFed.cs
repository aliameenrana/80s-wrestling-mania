﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MyFed : MonoBehaviour {
	
	public Text FedNameText;
	public Text FedBonusText;
	public Image FedImage;
	
	public Text CommishNameText;
	public Text CommishBonusText;
	public Image CommishImage;

	public Text Tag1ChampText;
	public Text Tag2ChampText;
	public Text ContinentalChampText;
	public Text WorldChampText;
	public Text Tag1LastChampText;
	public Text Tag2LastChampText;
	public Text ContinentalLastChampText;
	public Text WorldLastChampText;

	public Text cashAmount;
	public Text tokensAmount;

	public GameObject walletPanel;

	public Image ratingsImage;

	public CardSelectScreenScript cardViewer;

	public PopUpMessage newWrestlersMessage;

	private GameObject UIManager;
	private GameObject playerHUD;

	private GameSetupItems setupItems;
	public static MyFed instance;

	void Awake()
	{
		instance = this;
	}

	public void OpenMyFedPanel()
    {
		UIManager = GameObject.Find("UIManager");
		playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;
		setupItems = UIManager.GetComponent<GameSetupItems>();

        if (playerHUD != null)
			playerHUD.SetActive(false);

		CheckMyFedData();

		if (ScreenHandler.current != null)
			ScreenHandler.current.OpenPanel(this.gameObject.GetComponent<Animator>());

		walletPanel.BroadcastMessage("ShowMyCash", SendMessageOptions.DontRequireReceiver);
		walletPanel.BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
	}

    private void OnEnable()
    {
		int year = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName);
        if (year >= 1990)
        {
			FedImage.sprite = GameObject.Find("UIManager").GetComponent<GameSetupItems>().Federations[GamePlaySession.instance.GetSelectedFederation()].littleSprite90s;
        }

		cashAmount.text = GamePlaySession.instance.myCash.ToString();
		tokensAmount.text = GamePlaySession.instance.myTokens.ToString();
    }

    public void CloseFedPanel()
    {
		Debug.Log ("Time Then: " + Time.realtimeSinceStartup);

		UIManager = GameObject.Find("UIManager");

		playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;

		bool hasCards = false;
		NewCardsObject newCards = new NewCardsObject();

		if (GamePlaySession.instance.GetInTutorialMode() && GamePlaySession.instance.GiveInitialCards () && !GamePlaySession.instance.GetNewGamePlus()) {
			if (playerHUD != null)
				playerHUD.SetActive (false);

			newCards.message = newWrestlersMessage;
			if (GamePlaySession.instance.CheckCard (153) == null) 
			{
				newCards.cards.Add (setupItems.wrestlers.cards [153].Clone ());
				newCards.cards.Add (setupItems.wrestlers.cards [129].Clone ());
				newCards.cards.Add (setupItems.wrestlers.cards [21].Clone ());
				newCards.cards.Add (setupItems.wrestlers.cards [119].Clone ());
				newCards.cards.Add (setupItems.wrestlers.cards [79].Clone ());
				newCards.cards.Add (setupItems.wrestlers.cards [73].Clone ());  //tag champ
				newCards.cards.Add (setupItems.wrestlers.cards [25].Clone ());  //tag champ
				newCards.cards.Add (setupItems.wrestlers.cards [123].Clone ());
				newCards.cards.Add (setupItems.wrestlers.cards [131].Clone ());
				newCards.cards.Add (setupItems.wrestlers.cards [157].Clone ());
				newCards.cards.Add (setupItems.wrestlers.cards [700].Clone ());  //Jobber
				newCards.cards.Add (setupItems.wrestlers.cards [151].Clone ());  //continental champ
				newCards.cards.Add (setupItems.wrestlers.cards [45].Clone ());  //world champ
				newCards.cards.Add (setupItems.venues.cards [178].Clone ());
				newCards.cards.Add (setupItems.venues.cards [177].Clone ());
				newCards.cards.Add (setupItems.venues.cards [179].Clone ());
				newCards.cards.Add (setupItems.matches.cards [189].Clone ());  // wrestling match
			}
			hasCards = true;
		} 

		else if (GamePlaySession.instance.GetInTutorialMode() && GamePlaySession.instance.GetNewGamePlus())
        {
			hasCards = true;
            newCards.message = newWrestlersMessage;
            newCards.cards = GamePlaySession.instance.myCards;
        }
		else if (!GamePlaySession.instance.GetInTutorialMode() && GamePlaySession.instance.GetNewGamePlus())
		{
			if (UIManager.GetComponent<UIManagerScript> ().transferCardNumbers.Count > 0) 
			{
				newCards.cards.Add (setupItems.wrestlers.cards [700].Clone ());
				foreach (var item in UIManager.GetComponent<UIManagerScript>().transferCardNumbers) 
				{
					if (setupItems.flavors.cards.ContainsKey (item)) 
					{
						if (!newCards.cards.Exists (x => x.CardNumber == item)) 
						{
							newCards.cards.Add (setupItems.flavors.cards [item].Clone());
						}
					}

					if (setupItems.matches.cards.ContainsKey (item)) 
					{
						if (!newCards.cards.Exists (x => x.CardNumber == item)) 
						{
							newCards.cards.Add (setupItems.matches.cards [item].Clone());
						}
					}

					if (setupItems.micSpots.cards.ContainsKey (item)) 
					{
						if (!newCards.cards.Exists (x => x.CardNumber == item)) 
						{
							newCards.cards.Add (setupItems.micSpots.cards [item].Clone());
						}
					}

					if (setupItems.skits.cards.ContainsKey (item)) 
					{
						if (!newCards.cards.Exists (x => x.CardNumber == item)) 
						{
							newCards.cards.Add (setupItems.skits.cards [item].Clone());
						}
					}

					if (setupItems.venues.cards.ContainsKey (item)) 
					{
						if (!newCards.cards.Exists (x => x.CardNumber == item)) 
						{
							newCards.cards.Add (setupItems.venues.cards [item].Clone());
						}
					}

					if (setupItems.wrestlers.cards.ContainsKey (item)) 
					{
						if (!newCards.cards.Exists (x => x.CardNumber == item)) 
						{
							var go = (WrestlerCardObject)setupItems.wrestlers.cards [item].Clone();
							Debug.Log (go.cardAlignment);
							newCards.cards.Add (setupItems.wrestlers.cards [item].Clone());
						}
					}
				}
				hasCards = true;
			}
		}
		if (hasCards) 
		{
			if (cardViewer != null)
				cardViewer.OpenCardView(newCards);
		} 
		else 
		{
			UIManager.GetComponent<UIManagerScript>().GoToCalendar(false);
		}
		// Set current championship belt holders if they've not yet been set
		if (GamePlaySession.instance.titleChampion == null && UIManager.GetComponent<UIManagerScript> ().transferCardNumbers.Count == 0) {
			GamePlaySession.instance.titleChampion = (WrestlerCardObject)GamePlaySession.instance.CheckCard (151);


			GamePlaySession.instance.worldChampion = (WrestlerCardObject)GamePlaySession.instance.CheckCard (45);


			GamePlaySession.instance.tagChampions = new WrestlerCardObject[] { (WrestlerCardObject)GamePlaySession.instance.CheckCard (73),
				(WrestlerCardObject)GamePlaySession.instance.CheckCard (25)
			};

			TagTeamObject DefaultTagTeamChampions = new TagTeamObject ();
			DefaultTagTeamChampions.Wrestler1 = GamePlaySession.instance.tagChampions [0];
			DefaultTagTeamChampions.Wrestler2 = GamePlaySession.instance.tagChampions [1];

		} 
		else if (GamePlaySession.instance.titleChampion == null && UIManager.GetComponent<UIManagerScript> ().transferCardNumbers.Count != 0) 
		{
			GamePlaySession.instance.titleChampion = (WrestlerCardObject)newCards.cards.Find (o => o.CardNumber == 151);

			GamePlaySession.instance.worldChampion = (WrestlerCardObject)newCards.cards.Find (o => o.CardNumber == 45);

			GamePlaySession.instance.tagChampions = new WrestlerCardObject[] { (WrestlerCardObject)newCards.cards.Find (o => o.CardNumber == 73),
				(WrestlerCardObject)newCards.cards.Find (o => o.CardNumber == 25)
			};

			TagTeamObject DefaultTagTeamChampions = new TagTeamObject ();
			DefaultTagTeamChampions.Wrestler1 = GamePlaySession.instance.tagChampions [0];
			DefaultTagTeamChampions.Wrestler2 = GamePlaySession.instance.tagChampions [1];
		}
		Debug.Log ("Time now: " + Time.realtimeSinceStartup);
	}

	public void CheckMyFedData()
    {
		if(walletPanel.activeSelf){
            try
            {
                walletPanel.BroadcastMessage("ShowMyCash", SendMessageOptions.DontRequireReceiver);
                walletPanel.BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
            }
            catch
            {
                Debug.Log("ShowMyCash & ShowMyTokens has no receiver...");
            }
		}
		ratingsImage.fillAmount = GamePlaySession.instance.GetAverageRating();

		//Debug.Log("Federation num: "+GamePlaySession.instance.selectedFederation);
		FedNameText.text = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.GetSelectedFederation()].NameString;
		FedBonusText.text = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.GetSelectedFederation()].BonusString;

		int year = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName);

        if (year < 1990)
        {
			FedImage.sprite = GameObject.Find("UIManager").GetComponent<GameSetupItems>().Federations[GamePlaySession.instance.GetSelectedFederation()].littleSprite;
        }
        else
        {
			FedImage.sprite = GameObject.Find("UIManager").GetComponent<GameSetupItems>().Federations[GamePlaySession.instance.GetSelectedFederation()].littleSprite90s;
        }

   //     FedImage.sprite = UIManager.GetComponent<GameSetupItems>().
			//Federations[GamePlaySession.instance.selectedFederation].littleSprite;
		
		CommishNameText.text = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.GetSelectedComissioner()].NameString;
		CommishBonusText.text = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.GetSelectedComissioner()].BonusString;
		CommishImage.sprite = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.GetSelectedComissioner()].littleSprite;

		if (ratingsImage != null)
			ratingsImage.fillAmount = GamePlaySession.instance.GetAverageStarRating();

		string wcn;
		string ccn;
		string[] ttns = new string[2];

		if(GamePlaySession.instance.tagChampions == null){
			ttns[0] = "Jesse Wild";
			ttns[1] = "Chet Skye";
		}
		else{
			try{
				ttns[0] = GamePlaySession.instance.tagChampions[0].name;
				ttns[1] = GamePlaySession.instance.tagChampions[1].name;
			}
			catch(System.Exception e){
				ttns[0] = "Jesse Wild";
				ttns[1] = "Chet Skye";
			}
		}

		if(GamePlaySession.instance.titleChampion == null){
			ccn = "Star Boy";
		}
		else{
			ccn = GamePlaySession.instance.titleChampion.name;
		}

		if(GamePlaySession.instance.worldChampion == null){
			wcn = "Dr. B";
		}
		else{
			wcn = GamePlaySession.instance.worldChampion.name;
		}

		Tag1ChampText.text = ttns[0];
		Tag2ChampText.text = ttns[1];
		ContinentalChampText.text = ccn;
		WorldChampText.text = wcn;

		string wcnl;
		string ccnl;
		string[] ttnsl = new string[2];

		ttnsl[0] = "";
		ttnsl[1] = "";
	
		 if(GamePlaySession.instance.tagLastChampions != null){
			if(GamePlaySession.instance.tagLastChampions[0] != null){
				ttnsl[0] = GamePlaySession.instance.tagLastChampions[0].name;
				ttnsl[1] = GamePlaySession.instance.tagLastChampions[1].name;
			}
		}
		
		if(GamePlaySession.instance.titleLastChampion == null){
			ccnl = "";
		}
		else{
			ccnl = GamePlaySession.instance.titleLastChampion.name;
		}
		
		if(GamePlaySession.instance.worldLastChampion == null){
			wcnl = "";
		}
		else{
			wcnl = GamePlaySession.instance.worldLastChampion.name;
		}
		
		Tag1LastChampText.text = ttnsl[0];
		Tag2LastChampText.text = ttnsl[1];
		ContinentalLastChampText.text = ccnl;
		WorldLastChampText.text = wcnl;
	}
}
