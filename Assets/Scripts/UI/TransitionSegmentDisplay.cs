﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TransitionSegmentDisplay : MonoBehaviour {
	public Animator segmentAnim;

	public Image segmentCardImage;

	public CanvasGroup cardGroup;
	public List<Animator> cardAnims = new List<Animator>();
	public List<Image> cardImages = new List<Image>();
	public Text cardValues;
	public List<Image> cardHighlights = new List<Image>();
	public List<Image> winnerImages = new List<Image>();

	public StarRatings starRating;
	public Text resultTag;
	public Image resultBackground;

	public AudioClip sfxDing;
	public AudioClip sfxDud;
	public AudioClip sfxWinner;
	public AudioClip sfxStamp;

	public GameObject resultOutput;
	public string resultTagText = "Overall Segment Rating:";

	public Sprite versusIcon;

	public float counterTime = 0.9f;

	[HideInInspector]
	public bool needSave;

	private float timePassed;

	private SegmentObject segment;

	private int[] targetValues;
	private int[] currentValues;
	private int targetOverallValue;
	private int currentOverallValue;

	private bool valuesSet;
	private float lastValueCountTime;
	private float valueCompleteTime;
	private bool waitForClose;

	private bool needBonuses;
	private bool needWait;
	private bool bonusTime;
	private int bonusNum;
	private float overallModifier;
	private bool lossModifierSet;

	private bool showOverall;
	private UIManagerScript UIManager;
	private bool sfxEnabled;

	void Update() {
		if (SegmentOpen() || SegmentDone()) {
			cardGroup.alpha = 1;
		}
		
//		if (needSave) 
//		{
//			StartCoroutine (SaveGameData());
//			needSave = false;
//		}
	}

	IEnumerator SaveGameData() {
		yield return new WaitForSeconds(.1f);
		GamePlaySession.instance.SaveAll(true,true);
	}

	IEnumerator PlaySegmentAnimations(){
		while(segment == null){
			yield return new WaitForSeconds(.025f);
		}
		ShowSegment();

		while(!SegmentOpen() && !SegmentDone()){
			yield return null;
		}

		while(segment != null){
			winnerImages[0].enabled = false;
			winnerImages[1].enabled = false;
			winnerImages[2].enabled = false;
			winnerImages[3].enabled = false;

			switch (segment.segmentType) {
			case SegmentObject.SegmentType.MATCH:
				ShowMatch();
				break;
			case SegmentObject.SegmentType.TAG_MATCH:
				ShowTagMatch();
				break;
			case SegmentObject.SegmentType.MIC_SPOT:
				ShowMicSpot();
				break;
			case SegmentObject.SegmentType.SKIT:
				ShowSkit();
				break;
			default:
				break;
			}
			// Wait a short time to let cards animate on to the screen
			yield return new WaitForSeconds(.25f);

		resultOutput.SetActive(false);

		while (!valuesSet){	yield return null;		}

		for(int i = 0; i < currentValues.Length; i++){
			yield return StartCoroutine(UpdateValues(i));
			//yield return new WaitForSeconds(.9f);
		}
		
			// Slight pause between values and bonuses
		yield return new WaitForSeconds(.9f);

		for(int i = 0; i < segment.segmentBonuses.Count; i++) {
			yield return StartCoroutine(UpdateBonuses(i));
		}

			
		if (segment.segmentType != SegmentObject.SegmentType.MATCH && segment.segmentType != SegmentObject.SegmentType.TAG_MATCH) {
			if (!segment.segmentPassed) {
				overallModifier -= 4f;
					
				if (sfxEnabled){
					GetComponent<AudioSource>().clip = sfxDud;
					GetComponent<AudioSource>().Play();
				}
					
				UpdateDisplay("<size=20>Fail Penalty:</size>\n-4");
				yield return new WaitForSeconds(1f);
			}
		}

		//show Winners;
		if(segment.winnerIDs != null) {
			/*
			if(segment.segmentType == SegmentObject.SegmentType.MATCH && segment.winnerIDs[0] == segment.cards[2].CardNumber){
					winnerImages[0].enabled = true;
			}
			else if(segment.segmentType == SegmentObject.SegmentType.TAG_MATCH && (segment.winnerIDs[0] == segment.cards[3].CardNumber || segment.winnerIDs[1] == segment.cards[3].CardNumber)){
					winnerImages[0].enabled = true;
					winnerImages[1].enabled = true;
			}
			else if(segment.segmentType == SegmentObject.SegmentType.TAG_MATCH){
					winnerImages[2].enabled = true;
					winnerImages[3].enabled = true;
			}
			else if(segment.segmentType == SegmentObject.SegmentType.MATCH){
					winnerImages[3].enabled = true;
			}
			*/
				if(segment.segmentType == SegmentObject.SegmentType.MATCH) 
				{
					if (segment.winners[0].CardNumber == segment.cards[2].CardNumber) 
					{
						if (segment.DisQualify != 1 && segment.DisQualify != 3 && segment.DisQualify != 4) 
						{
							winnerImages [0].enabled = true;
							winnerImages [1].enabled = false;
							winnerImages [2].enabled = false;
							winnerImages [3].enabled = false;
						} 
						else 
						{
							winnerImages [0].enabled = false;
							winnerImages [1].enabled = false;
							winnerImages [2].enabled = false;
							winnerImages [3].enabled = false;
						}
					} 
					else 
					{
						if (segment.DisQualify != 1 && segment.DisQualify != 3 && segment.DisQualify != 4) 
						{
							winnerImages [0].enabled = false;
							winnerImages [1].enabled = false;
							winnerImages [2].enabled = false;
							winnerImages [3].enabled = true;
						} 
						else
						{
							winnerImages [0].enabled = false;
							winnerImages [1].enabled = false;
							winnerImages [2].enabled = false;
							winnerImages [3].enabled = false;
						}
					}
				} 
				else if (segment.segmentType == SegmentObject.SegmentType.TAG_MATCH) 
				{
					if (segment.winners[0].CardNumber == segment.cards[2].CardNumber || segment.winners[1].CardNumber == segment.cards[2].CardNumber) 
					{
						if (segment.DisQualify != 1 && segment.DisQualify != 3 && segment.DisQualify != 4) 
						{
							winnerImages [0].enabled = true;
							winnerImages [1].enabled = true;
							winnerImages [2].enabled = false;
							winnerImages [3].enabled = false;
						} 
						else 
						{
							winnerImages [0].enabled = false;
							winnerImages [1].enabled = false;
							winnerImages [2].enabled = false;
							winnerImages [3].enabled = false;
						}
					} 
					else 
					{
						if (segment.DisQualify != 1 && segment.DisQualify != 3 && segment.DisQualify != 4) 
						{
							winnerImages [0].enabled = false;
							winnerImages [1].enabled = false;
							winnerImages [2].enabled = true;
							winnerImages [3].enabled = true;
						} 
						else 
						{
							winnerImages [0].enabled = false;
							winnerImages [1].enabled = false;
							winnerImages [2].enabled = false;
							winnerImages [3].enabled = false;
						}
					}
				}

			if(sfxEnabled)
				{
				GetComponent<AudioSource>().clip = sfxWinner;
				GetComponent<AudioSource>().Play();
			}
		}
		yield return new WaitForSeconds(.95f);

		starRating.ratingsImage.fillAmount = segment.GetStarRating();
		resultOutput.SetActive(true);
		if(sfxEnabled){
				GetComponent<AudioSource>().clip = sfxStamp;
				GetComponent<AudioSource>().Play();
		}
		
		// This wait time should be a little longer to let the player view the final results
		yield return new WaitForSeconds(1.5f); //yield return new WaitForSeconds(.45f);

		CloseSegment();
		yield return new WaitForSeconds(.5f);
		}
	}
	public void OnEnable(){
		if(UIManager == null){
			UIManager = GamePlaySession.instance.GSI.GetComponent<UIManagerScript>();
			sfxEnabled = UIManager.sfxEnabled;
		}
		if (SegmentOpen() || SegmentDone()) {
			cardGroup.alpha = 1;
		}
		StartCoroutine(PlaySegmentAnimations());
	}
	public bool SegmentDone() {
		return segmentAnim.GetCurrentAnimatorStateInfo(0).IsName("OffscreenUp");
	}

	public bool SegmentOpen() {
		return segmentAnim.GetCurrentAnimatorStateInfo(0).IsName("Shown");
	}

	public void ShowSegment() {
		segmentAnim.SetBool("Open", true);
	}

	public void CloseSegment() {
		segmentAnim.SetBool("Closing", true);
	}

	public void ResetSegment() {
		valuesSet = false;
		waitForClose = false;
		needBonuses = false;
		bonusTime = false;
		bonusNum = 0;
		overallModifier = 0f;
		lossModifierSet = false;
		showOverall = false;

		cardGroup.alpha = 0;
		foreach (Animator anim in cardAnims) {
			anim.ResetTrigger("Highlight");
			anim.SetBool("Visible", false);
			anim.Play("Hide");
		}

		//foreach (Text value in cardValues) {
			cardValues.text = string.Empty;
		//}
		resultTag.text = string.Empty;
		starRating.ratingsImage.enabled =false;
		resultBackground.enabled = false;

		segmentAnim.SetBool("Open", false);
		segmentAnim.SetBool("Closing", false);
		segmentAnim.Play("OffscreenDown");

		timePassed = 0f;
		valueCompleteTime = 0f;
		lastValueCountTime = 0f;
	}
	
	void PulseCardValue() {
		//if (currentValues[valueID] != targetValues[valueID]) {
			Animator anim = cardValues.GetComponent<Animator>();
			if (anim != null) {
				if (cardValues.text == "+1" || cardValues.text == "+1 Cheer!" || cardValues.text == "+1 Boo!") {
					//anim.animation["Pulse"].speed = .5f;
					anim.SetTrigger("PulseFast");
				} else {
					//anim.animation["Pulse"].speed = 2.0f;
					anim.SetTrigger("PulseSlow");
				}
			}
		//}
	}
	
	void PulseResultValue() {
		/*if (currentOverallValue != targetOverallValue) {
			Animator anim = resultValue.GetComponent<Animator>();
			if (anim != null) {
				anim.SetTrigger("Pulse");
			}
		}*/
	}

	public bool ValuesDone() {
		bool isDone = true;

		for (int i = 0; i < segment.segmentWrestlerRatings.Length; i++) {
			if (currentValues[i] != targetValues[i]) {
				isDone = false;
			}
		}
		
		if (isDone) {
			needBonuses = true;
		}
		
		if (currentOverallValue != targetOverallValue) {
			isDone = false;
		}

		return isDone;
	}

	public IEnumerator UpdateValues(int i) {
		//bool playedAudio = false;

		//for (int i = 0; i < segment.segmentWrestlerRatings.Length; i++) {
		while(currentValues[i] < targetValues[i]){
			currentValues[i] = Mathf.Min(currentValues[i] + 1, targetValues[i]);

			int cn = 0;

			if(segment.segmentType == SegmentObject.SegmentType.MATCH){
				if(i == 1){
						cn = 8;
				}
			}
			else if(segment.segmentType == SegmentObject.SegmentType.TAG_MATCH){
				switch(i){
				case 1:
					cn = 2;
					break;
				case 2:
					cn = 6;
					break;
				case 3:
					cn = 8;
					break;
				default:
					break;
				}
			}
			else if(segment.segmentType == SegmentObject.SegmentType.SKIT || segment.segmentType == SegmentObject.SegmentType.MIC_SPOT){
					switch(i){
					case 0:
						cn = 4;
						break;
					case 1:
						cn = 0;
						break;
					case 2:
						cn = 8;
						break;
					case 3:
						cn = 2;
						break;
					case 4:
						cn = 6;
						break;
					default:
						break;
					}
			}
			cardHighlights[cn].color = Color.yellow;
			cardAnims[cn].SetTrigger("Highlight");
			//if(!playedAudio && currentValues[i] != targetValues[i]){
			//	playedAudio = true;
				if (sfxEnabled){
					GetComponent<AudioSource>().clip = sfxDing;
					GetComponent<AudioSource>().Play();
			}
			//}
		//}
			WrestlerCardObject card = GetWrestlerCard(i);
			if (card != null) {
				if (CheckGoodAlignment (card) == "GOOD") 
				{
					UpdateDisplay ("+1 Cheer!");
				} 
				else if (CheckGoodAlignment (card) == "TWEENER") 
				{
					if (Random.Range (0, 2) == 1) 
					{
						UpdateDisplay ("+1 Cheer!");
					} 
					else 
					{
						UpdateDisplay ("+1 Boo!");
					}
				}
				else 
				{
					UpdateDisplay("+1 Boo!");
				}
			} else {
				UpdateDisplay();
			}
			yield return new WaitForSeconds(.25f);
			cardAnims[cn].ResetTrigger("Highlight");

			yield return new WaitForSeconds(.20f);
		}
	}

	WrestlerCardObject GetWrestlerCard(int i) {
		CardObject card = null;
		switch (segment.segmentType) {
		case SegmentObject.SegmentType.MATCH:
			switch(i){
			case 0:
				card = segment.cards[2];
				break;
			case 1:
				card = segment.cards[4];
				break;
			}
			break;
		case SegmentObject.SegmentType.TAG_MATCH:
			switch(i){
			case 0:
				card = segment.cards[2];
				break;
			case 1:
				card = segment.cards[3];
				break;
			case 2:
				card = segment.cards[5];
				break;
			case 3:
				card = segment.cards[6];
				break;
			}
			break;
		case SegmentObject.SegmentType.MIC_SPOT:
			switch(i){
			case 0:
				card = segment.cards[1];
				break;
			case 1:
				card = segment.cards[2];
				break;
			case 2:
				card = segment.cards[3];
				break;
			case 3:
				card = segment.cards[4];
				break;
			case 4:
				card = segment.cards[5];
				break;
			}
			break;
		case SegmentObject.SegmentType.SKIT:
			switch(i){
			case 0:
				card = segment.cards[1];
				break;
			case 1:
				card = segment.cards[2];
				break;
			case 2:
				card = segment.cards[3];
				break;
			case 3:
				card = segment.cards[4];
				break;
			case 4:
				card = segment.cards[5];
				break;
			}
			break;
		}
		return (WrestlerCardObject)card;
	}

	string CheckGoodAlignment(WrestlerCardObject card) 
	{
		// Check whether it's a "boo" or "cheer" value!
		return (card.cardAlignment.ToUpper());
	}

	public IEnumerator UpdateBonuses(int i) {

		ResultBonus bonus = segment.segmentBonuses[i];
		string bonusText = string.Empty;

		switch (bonus.type) {
		case BonusType.GoodBadMatch:
			bool team1Good = false;
			
			if (segment.favorites[0].CardNumber == segment.cards[2].CardNumber) {
				if (bonus.favGood) {
					team1Good = true;
				} else {
					team1Good = false;
				}
			} else {
				if (bonus.favGood) {
					team1Good = false;
				} else {
					team1Good = true;
				}
			}
			
			if (team1Good) {
				cardHighlights[0].color = Color.blue;
				cardHighlights[2].color = Color.blue;
				cardHighlights[6].color = Color.red;
				cardHighlights[8].color = Color.red;
			} else {
				cardHighlights[0].color = Color.red;
				cardHighlights[2].color = Color.red;
				cardHighlights[6].color = Color.blue;
				cardHighlights[8].color = Color.blue;
			}
			
			cardAnims[0].SetTrigger("Highlight");
			cardAnims[2].SetTrigger("Highlight");
			cardAnims[6].SetTrigger("Highlight");
			cardAnims[8].SetTrigger("Highlight");
			
			overallModifier += bonus.value;
			bonusText = "<size=20>Good vs. Bad:</size>\n+" + bonus.value.ToString();

			break;
		case BonusType.Flavor:
			if (segment.segmentType == SegmentObject.SegmentType.TAG_MATCH) {
				switch (bonus.wrestlerNum) {
				case 0:
					if (segment.favorites[0].CardNumber == segment.team1[0].CardNumber) {
						// Team 1
						cardHighlights[3].color = Color.yellow;
						cardAnims[3].SetTrigger("Highlight");
					} else {
						// Team 2
						cardHighlights[9].color = Color.yellow;
						cardAnims[9].SetTrigger("Highlight");
					}
					break;
				case 1:
					if (segment.underdogs[0].CardNumber == segment.team1[0].CardNumber) {
						// Team 1
						cardHighlights[3].color = Color.yellow;
						cardAnims[3].SetTrigger("Highlight");
					} else {
						// Team 2
						cardHighlights[9].color = Color.yellow;
						cardAnims[9].SetTrigger("Highlight");
					}
					break;
				}
			} else {
				switch (bonus.wrestlerNum) {
				case 0:
					cardHighlights[5].color = Color.yellow;
					cardAnims[5].SetTrigger("Highlight");
					break;
				case 1:
					cardHighlights[1].color = Color.yellow;
					cardAnims[1].SetTrigger("Highlight");
					break;
				case 2:
					cardHighlights[9].color = Color.yellow;
					cardAnims[9].SetTrigger("Highlight");
					break;
				case 3:
					cardHighlights[3].color = Color.yellow;
					cardAnims[3].SetTrigger("Highlight");
					break;
				case 4:
					cardHighlights[7].color = Color.yellow;
					cardAnims[7].SetTrigger("Highlight");
					break;
				}
			}
			
			currentValues[bonus.wrestlerNum] += (int) bonus.value;
			targetValues[bonus.wrestlerNum] += (int) bonus.value;

			bonusText = "<size=20>Flavor Bonus:</size>\n+" + bonus.value.ToString();
			break;
		case BonusType.Wrestler:
			switch (bonus.wrestlerNum) {
			case 0:
				cardHighlights[4].color = Color.yellow;
				cardAnims[4].SetTrigger("Highlight");
				break;
			case 1:
				cardHighlights[0].color = Color.yellow;
				cardAnims[0].SetTrigger("Highlight");
				break;
			case 2:
				cardHighlights[8].color = Color.yellow;
				cardAnims[8].SetTrigger("Highlight");
				break;
			case 3:
				cardHighlights[2].color = Color.yellow;
				cardAnims[2].SetTrigger("Highlight");
				break;
			case 4:
				cardHighlights[6].color = Color.yellow;
				cardAnims[6].SetTrigger("Highlight");
				break;
			}
			
			currentValues[bonus.wrestlerNum] += (int) bonus.value;
			targetValues[bonus.wrestlerNum] += (int) bonus.value;
				
			bonusText = "<size=20>Type Bonus:</size>\n+" + bonus.value.ToString();
			break;
		case BonusType.MatchFlavor:
			cardHighlights[5].color = Color.yellow;
			cardAnims[5].SetTrigger("Highlight");
			overallModifier += bonus.value;
			bonusText = "<size=20>Flavor Bonus:</size>\n+" + bonus.value.ToString();

			break;
		}
		
		if (sfxEnabled){
			GetComponent<AudioSource>().clip = sfxDing;
			GetComponent<AudioSource>().Play();
		}
		
		UpdateDisplay(bonusText);
		yield return new WaitForSeconds(.35f);
		foreach (Animator anim in cardAnims) {
			anim.ResetTrigger("Highlight");
		}
		yield return new WaitForSeconds(.80f);
	}

	void UpdateDisplay(string pulseText = "+1") {
		// Since all the screen shows now is a single pulse per value
		// and cycles through wrestlers, we only need to use the following:
		cardValues.text = pulseText;
		PulseCardValue();
		
		currentOverallValue = GetCurrentCombinedRating();

		resultTag.text = resultTagText;
		resultBackground.enabled = true;
		starRating.ratingsImage.enabled =true;
	}

	public int GetCurrentCombinedRating() {
		float combined = 0f;
		
		foreach (int value in currentValues) {
			combined += (float) value;
		}
		
		combined /= currentValues.Length;
		
		combined += overallModifier;

		if(combined <= 0){
			combined = 0;
		}

		return (int) combined;
	}

	public int GetTargetCombinedRating() {
		float combined = 0f;
		
		foreach (int value in targetValues) {
			combined += (float) value;
		}
		
		combined /= targetValues.Length;

		combined += overallModifier;
		
		if(combined <= 0){
			combined = 0;
		}
		
		return (int) combined;
	}

	public void ProcessSegment(SegmentObject data) {
		segment = data;

		segmentCardImage.sprite = segment.cards[0].frontImage;

		switch (segment.segmentType) {
		case SegmentObject.SegmentType.MATCH:
			ProcessMatch();
			break;
		case SegmentObject.SegmentType.TAG_MATCH:
			ProcessTagMatch();
			break;
		case SegmentObject.SegmentType.MIC_SPOT:
			ProcessMicSpot();
			break;
		case SegmentObject.SegmentType.SKIT:
			ProcessSkit();
			break;
		}

		targetOverallValue = (int) segment.segmentRating;

		valuesSet = true;
	}

	void ProcessMatch() {
		cardImages[4].sprite = versusIcon;

		targetValues = new int[segment.segmentWrestlerRatings.Length];
		currentValues = new int[segment.segmentWrestlerRatings.Length];

		if (segment.cards[1] != null) {
			cardImages[5].sprite = GamePlaySession.instance.CheckCard(segment.cards[1].CardNumber).frontImage;
		}
		
		if (segment.cards[2] != null) {
			cardImages[0].sprite = GamePlaySession.instance.CheckCard(segment.cards[2].CardNumber).frontImage;
			targetValues[0] = segment.segmentWrestlerRatings[0];
		}
		
		if (segment.cards[3] != null) {
			cardImages[1].sprite = GamePlaySession.instance.CheckCard(segment.cards[3].CardNumber).frontImage;
		}
		
		if (segment.cards[4] != null) {
			cardImages[8].sprite = GamePlaySession.instance.CheckCard(segment.cards[4].CardNumber).frontImage;
			targetValues[1] = segment.segmentWrestlerRatings[1];
		}
		
		if (segment.cards[5] != null) {
			cardImages[9].sprite = GamePlaySession.instance.CheckCard(segment.cards[5].CardNumber).frontImage;
		}
	}
	
	void ProcessTagMatch() {
		cardImages[4].sprite = versusIcon;
		
		targetValues = new int[segment.segmentWrestlerRatings.Length];
		currentValues = new int[segment.segmentWrestlerRatings.Length];
		
		if (segment.cards[1] != null) {
			cardImages[5].sprite = GamePlaySession.instance.CheckCard(segment.cards[1].CardNumber).frontImage;
		}
		
		if (segment.cards[2] != null) {
			cardImages[0].sprite = GamePlaySession.instance.CheckCard(segment.cards[2].CardNumber).frontImage;
			targetValues[0] = segment.segmentWrestlerRatings[0];
		}
		
		if (segment.cards[3] != null) {
			cardImages[2].sprite = GamePlaySession.instance.CheckCard(segment.cards[3].CardNumber).frontImage;
			targetValues[1] = segment.segmentWrestlerRatings[1];
		}
		
		if (segment.cards[4] != null) {
			cardImages[3].sprite = GamePlaySession.instance.CheckCard(segment.cards[4].CardNumber).frontImage;
		}
		
		if (segment.cards[5] != null) {
			cardImages[6].sprite = GamePlaySession.instance.CheckCard(segment.cards[5].CardNumber).frontImage;
			targetValues[2] = segment.segmentWrestlerRatings[2];
		}
		
		if (segment.cards[6] != null) {
			cardImages[8].sprite = GamePlaySession.instance.CheckCard(segment.cards[6].CardNumber).frontImage;
			targetValues[3] = segment.segmentWrestlerRatings[3];
		}
		
		if (segment.cards[7] != null) {
			cardImages[9].sprite = GamePlaySession.instance.CheckCard(segment.cards[7].CardNumber).frontImage;
		}
	}
	
	void ProcessMicSpot() {
		targetValues = new int[segment.segmentWrestlerRatings.Length];
		currentValues = new int[segment.segmentWrestlerRatings.Length];

		if (segment.cards[1] != null) {
			cardImages[4].sprite = segment.cards[1].frontImage;
			targetValues[0] = segment.segmentWrestlerRatings[0];
		}
		
		if (segment.cards[2] != null) {
			cardImages[0].sprite = segment.cards[2].frontImage;
			targetValues[1] = segment.segmentWrestlerRatings[1];
		}
		
		if (segment.cards[3] != null) {
			cardImages[8].sprite = segment.cards[3].frontImage;
			targetValues[2] = segment.segmentWrestlerRatings[2];
		}
		
		if (segment.cards[4] != null) {
			cardImages[2].sprite = segment.cards[4].frontImage;
			targetValues[3] = segment.segmentWrestlerRatings[3];
		}
		
		if (segment.cards[5] != null) {
			cardImages[6].sprite = segment.cards[5].frontImage;
			targetValues[4] = segment.segmentWrestlerRatings[4];
		}

		if (segment.cards[6] != null) {
			cardImages[5].sprite = segment.cards[6].frontImage;
		}
	}
	
	void ProcessSkit() {
		targetValues = new int[segment.segmentWrestlerRatings.Length];
		currentValues = new int[segment.segmentWrestlerRatings.Length];

		if (segment.cards[1] != null) {
			cardImages[4].sprite = segment.cards[1].frontImage;
			targetValues[0] = segment.segmentWrestlerRatings[0];
		}
		
		if (segment.cards[2] != null) {
			cardImages[0].sprite = segment.cards[2].frontImage;
			targetValues[1] = segment.segmentWrestlerRatings[1];
		}
		
		if (segment.cards[3] != null) {
			cardImages[8].sprite = segment.cards[3].frontImage;
			targetValues[2] = segment.segmentWrestlerRatings[2];
		}
		
		if (segment.cards[4] != null) {
			cardImages[2].sprite = segment.cards[4].frontImage;
			targetValues[3] = segment.segmentWrestlerRatings[3];
		}
		
		if (segment.cards[5] != null) {
			cardImages[6].sprite = segment.cards[5].frontImage;
			targetValues[4] = segment.segmentWrestlerRatings[4];
		}
		
		if (segment.cards[6] != null) {
			cardImages[5].sprite = segment.cards[6].frontImage;
		}
	}
	
	void ShowMatch() {
		cardAnims[4].SetBool("Visible", true);
		
		if (segment.cards[1] != null)
			cardAnims[5].SetBool("Visible", true);
		
		if (segment.cards[2] != null)
			cardAnims[0].SetBool("Visible", true);
		
		if (segment.cards[3] != null)
			cardAnims[1].SetBool("Visible", true);
		
		if (segment.cards[4] != null)
			cardAnims[8].SetBool("Visible", true);
		
		if (segment.cards[5] != null)
			cardAnims[9].SetBool("Visible", true);
	}
	
	void ShowTagMatch() {
		cardAnims[4].SetBool("Visible", true);
		
		if (segment.cards[1] != null)
			cardAnims[5].SetBool("Visible", true);
		
		//if (segment.cards[2] != null)
			cardAnims[0].SetBool("Visible", true);
		
		//if (segment.cards[3] != null)
			cardAnims[2].SetBool("Visible", true);
		
		if (segment.cards[4] != null)
			cardAnims[3].SetBool("Visible", true);
		
		//if (segment.cards[5] != null)
			cardAnims[6].SetBool("Visible", true);
		
		//if (segment.cards[6] != null)
			cardAnims[8].SetBool("Visible", true);
		
		if (segment.cards[7] != null)
			cardAnims[9].SetBool("Visible", true);
	}
	
	void ShowMicSpot() {
		//if (segment.cards[1] != null)
			cardAnims[4].SetBool("Visible", true);
		
		if (segment.cards[2] != null)
			cardAnims[0].SetBool("Visible", true);
		
		if (segment.cards[3] != null)
			cardAnims[8].SetBool("Visible", true);
		
		if (segment.cards[4] != null)
			cardAnims[2].SetBool("Visible", true);
		
		if (segment.cards[5] != null)
			cardAnims[6].SetBool("Visible", true);
		
		if (segment.cards[6] != null)
			cardAnims[5].SetBool("Visible", true);
	}
	
	void ShowSkit() {
		//if (segment.cards[1] != null)
			cardAnims[4].SetBool("Visible", true);
		
		if (segment.cards[2] != null)
			cardAnims[0].SetBool("Visible", true);
		
		if (segment.cards[3] != null)
			cardAnims[8].SetBool("Visible", true);
		
		if (segment.cards[4] != null)
			cardAnims[2].SetBool("Visible", true);
		
		if (segment.cards[5] != null)
			cardAnims[6].SetBool("Visible", true);
		
		if (segment.cards[6] != null)
			cardAnims[5].SetBool("Visible", true);
	}

	void ShowMatchValues() {
		//cardValues[0].text = currentValues[0].ToString();
		//cardValues[4].text = currentValues[1].ToString();
		cardValues.text = "+1";
		PulseCardValue();
		//PulseCardValue(4, 1);
	}
	
	void ShowTagMatchValues() {
		cardValues.text = "+1";
		//cardValues[1].text = currentValues[1].ToString();
		//cardValues[3].text = currentValues[2].ToString();
		//cardValues[4].text = currentValues[3].ToString();
		PulseCardValue();
		//PulseCardValue(1, 1);
		//PulseCardValue(3, 2);
		//PulseCardValue(4, 3);
	}
	
	void ShowMicSpotValues() {
		if (segment.cards[1] != null) {
			cardValues.text = "+1"; //currentValues[0].ToString();
			PulseCardValue();
		}
		
		if (segment.cards[2] != null) {
			cardValues.text = "+1"; //currentValues[1].ToString();
			PulseCardValue();
		}
		
		if (segment.cards[3] != null) {
			cardValues.text = "+1"; //currentValues[2].ToString();
			PulseCardValue();
		}
		
		if (segment.cards[4] != null) {
			cardValues.text = "+1"; //currentValues[3].ToString();
			PulseCardValue();
		}
		
		if (segment.cards[5] != null) {
			cardValues.text = "+1"; //currentValues[4].ToString();
			PulseCardValue();
		}
	}
	
	void ShowSkitValues() {
		if (segment.cards[1] != null) {
			cardValues.text = "+1"; //currentValues[0].ToString();
			PulseCardValue();
		}
		
		if (segment.cards[2] != null) {
			cardValues.text = "+1"; //currentValues[1].ToString();
			PulseCardValue();
		}
		
		if (segment.cards[3] != null) {
			cardValues.text = "+1"; //currentValues[2].ToString();
			PulseCardValue();
		}
		
		if (segment.cards[4] != null) {
			cardValues.text = "+1"; //currentValues[3].ToString();
			PulseCardValue();
		}
		
		if (segment.cards[5] != null) {
			cardValues.text = "+1"; //currentValues[4].ToString();
			PulseCardValue();
		}
	}
}
