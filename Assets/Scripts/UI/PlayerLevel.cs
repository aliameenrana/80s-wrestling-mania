﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerLevel : MonoBehaviour {
	public Text levelText;

	void Update() {
		if (GamePlaySession.instance != null) {
			levelText.text = "Level " + GamePlaySession.instance.GetMyLevel();
		}
	}
}
