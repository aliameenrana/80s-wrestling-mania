﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System.Text.RegularExpressions;

public class LoginScreenController : MonoBehaviour
{
	public InputField emailTF, passwordTF;
	public Text message;
    public GameObject blackBG, bg;
	public GameObject socialManager;
    public GameObject UIManager;
    public GameObject ServerManager;
	public GameObject loadingOverlay;

	public PopUpMessage dataTransferPopup;
	public Text gotoMyFedScreen;
	public Text animText;
	public GameObject loginButton;
	bool parseLoginSuccess;
	bool bugdevLoginSuccess;
	bool parseLoginOver;
	bool bugdevLoginOver;
	string parseMessage;
	bool serverWait = false;
	FaceBookManager FBManager;
    bool newUser = false;
	bool cancelInvoke = false;
	string path = "";
	bool sendNow;
	bool giveUp;
	/// <summary>
	/// Regular expression, which is used to validate an E-Mail address.
	/// </summary>
	const string MatchEmailPattern = 
		@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

	/// <summary>
	/// Checks whether the given Email-Parameter is a valid E-Mail address.
	/// </summary>
	/// <param name="email">Parameter-string that contains an E-Mail address.</param>
	/// <returns>True, when Parameter-string is not null and 
	/// contains a valid E-Mail address;
	/// otherwise false.</returns>
	bool IsEmail(string email)
	{
		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
		else return false;
	}

	void Awake()
	{
		FBManager = socialManager.GetComponent<FaceBookManager>();
        UIManager = GameObject.Find("UIManager");
        ServerManager = GameObject.Find("ServerManager");


		#if UNITY_EDITOR
		path = Application.dataPath + "/userdata.dat";
		#elif UNITY_ANDROID || UNITY_IOS
		path = Application.persistentDataPath + "/userdata.dat";
		#endif
	}

	void OnEnable()
	{
        blackBG.SetActive(true);
		//FBManager.loginCallbackEvent += FBLoginCallback;

		if (GamePlaySession.instance != null) 
		{
			if (GamePlaySession.instance.GetSelectedFederation () == -1) 
			{
				gotoMyFedScreen.gameObject.SetActive (false);
			} 
			else 
			{
				if (GamePlaySession.instance.GetSelectedFederation () == -1) 
				{
					gotoMyFedScreen.gameObject.SetActive (false);
				} 
				else 
				{
					gotoMyFedScreen.gameObject.SetActive (true);
				}
			}
		}
	}

	void OnDisable()
	{
		//FBManager.loginCallbackEvent -= FBLoginCallback;
	}

	void ShowToast(string text)
	{
		StartCoroutine(ShowToastCR(text));
	}

	IEnumerator ShowToastCR(string text)
	{
		message.gameObject.SetActive(true);
		message.text = text;
		yield return new WaitForSeconds(3f);
		message.gameObject.SetActive(false);
	}

    public void GameLogin()
    {
		if(!string.IsNullOrEmpty(emailTF.text) && !string.IsNullOrEmpty(passwordTF.text))
		{
			if(!IsEmail(emailTF.text))
			{
				ShowToast("Please enter valid email address");
			}
			else if(passwordTF.text.Length < 8)
			{
				ShowToast("Password should be atleast 8 characters long.");
			}
			else
			{
				if ((System.IO.File.Exists (path)) && (PlayerPrefs.GetString ("myEmail", "") == "")) 
				{
					PlayerPrefs.SetString ("myEmail", emailTF.text.ToString ());
					PlayerPrefs.SetString ("myPassword", passwordTF.text.ToString ());
					UIManagerScript.instance.CreateOrLoadGamePlaySession ();
					sendNow = false;
					giveUp = false;
					StartCoroutine (WaitForSending ());
					ParseManager.LogInUser (emailTF.text.ToString (), passwordTF.text.ToString (), SendDataToParseAfterLogin);
				} 
				else 
				{
					StartCoroutine (DoubleLogin ());
					StartCoroutine (LoginOnBugDevServer ());
					StartCoroutine (LoginOnParse (emailTF.text.ToString(),passwordTF.text.ToString()));
				}
			}
		}
		else
		{
			ShowToast("Please Enter Email & Password");
		}
    }

	IEnumerator WaitForSending()
	{
		yield return new WaitUntil (() => (sendNow == true || giveUp == true));
		if (sendNow) 
		{
			PlayerPrefs.SetString ("parseUserID", Parse.ParseUser.CurrentUser.ObjectId.ToString ());
			ParseManager.PrepareAndSendAllUserData ();
		}
		UIManagerScript.instance.GoToTitle ();
	}

	void SendDataToParseAfterLogin(bool success, string exceptionString="")
	{
		if (success) 
		{
			sendNow = true;
		} 
		else 
		{
			ParseManager.RegisterUser (emailTF.text.ToString (), passwordTF.text.ToString (), SendDataToParseAfterRegister);
		}
	}

	void SendDataToParseAfterRegister(bool success, string email="", string password="", string exceptionString="")
	{
		if (success) 
		{
			sendNow = true;
		} 
		else 
		{
			giveUp = true;
		}
	}

	IEnumerator DoubleLogin()
	{
		loadingOverlay.SetActive (true);
		StartCoroutine ("WaitingAnim");
		loginButton.GetComponent<Button> ().interactable = false;
		yield return new WaitUntil (() => (parseLoginOver == true && bugdevLoginOver == true));
		yield return new WaitForSeconds (0.25f);
		StartCoroutine (DecisionCoroutine ());
	}
	IEnumerator LoginOnParse(string email, string password)
	{
		ParseManager.LogInUser (email, password,OnLoginParse);
		yield return null;
	}
	IEnumerator LoginOnBugDevServer()
	{
		string url = "http://bugdevstudios.com/wrestling/server.php?REQUEST=LOGIN&EMAIL=" +emailTF.text+"&PASSWORD="+passwordTF.text;
		WWW www = new WWW (url);
		yield return www;
		Debug.Log(www.text);
		var N = JSON.Parse(www.text);
		if (N == null) 
		{
			bugdevLoginSuccess = false;
			bugdevLoginOver = true;
			yield break;
		}

		int responseCode = N["CODE"].AsInt;
		var userID = N ["USER_ID"].Value;

		if (responseCode > 0) 
		{
			bugdevLoginSuccess = true;
			PlayerPrefs.SetString ("UserID", userID);
		} 
		else 
		{
			bugdevLoginSuccess = false;
		}

		bugdevLoginOver = true;

		yield return null;

	}

	bool setPrefNow = false;

	IEnumerator DecisionCoroutine()
	{
		loadingOverlay.SetActive (false);
		loginButton.GetComponent<Button> ().interactable = true;
		StopCoroutine("WaitingAnim");
		if (parseLoginSuccess) 
		{
			PlayerPrefs.SetString ("myEmail", emailTF.text.ToString ());
			PlayerPrefs.SetString ("myPassword", passwordTF.text.ToString ());
			ParseManager.setData = true;
			UIManager.GetComponent<UIManagerScript> ().GoToTitle (false, true);
			ShowToast (parseMessage);	
		}
		else if (!parseLoginSuccess && !bugdevLoginSuccess) 
		{
			ShowToast ("Login failed.");	
		} 
		else if (!parseLoginSuccess && bugdevLoginSuccess) 
		{
			PlayerPrefs.SetString ("myEmail", emailTF.text.ToString ());
			PlayerPrefs.SetString ("myPassword", passwordTF.text.ToString ());
			cancelInvoke = false;
			StartCoroutine (InvokeCanceller ());
			ParseManager.RegisterUser (emailTF.text.ToString (), passwordTF.text.ToString (),EnablePref);
			if (System.IO.File.Exists (UIManagerScript.instance.path)) 
			{
				UIManagerScript.instance.CreateOrLoadGamePlaySession (true);
				UIManagerScript.instance.GoToTitle (true);
			} 
			else if (PlayerPrefs.GetString ("GAMEPLAYSESSION", "NONE") != "NONE") 
			{
				GamePlaySession.instance = GamePlaySession.Load (PlayerPrefs.GetString ("GAMEPLAYSESSION"));
				UIManagerScript.instance.GoToTitle (true);
			} 
			else 
			{
				Debug.Log ("Loading from server");
				loadingOverlay.SetActive (true);
				ServerManager.GetComponent<DataHandler> ().GetSessionDataFromServer (GPSLoadCallback);
				StartCoroutine ("WaitingAnim");
			}
		}


		yield return new WaitUntil (() => setPrefNow == true);

		PlayerPrefs.SetString ("parseUserID", Parse.ParseUser.CurrentUser.ObjectId);
		yield return null;
	} 

	void RegisterOnParse()
	{
		cancelInvoke = false;
		try
		{
			StartCoroutine (InvokeCanceller ());	
		}
		catch 
		{
			CancelInvoke ("RegisterOnParse");
		}

		ParseManager.RegisterUser (PlayerPrefs.GetString ("myEmail"), PlayerPrefs.GetString ("myPassword"), EnablePref);
	}



	IEnumerator InvokeCanceller()
	{
		yield return new WaitUntil (() => cancelInvoke == true);
		CancelInvoke ("RegisterOnParse");
	}

	void EnablePref(bool success, string email, string password, string exceptionString = "")
	{
		if (success) 
		{
			setPrefNow = true;
			try 
			{
				cancelInvoke = true;
			} 
			catch 
			{
				
			}

		} 
		else if (exceptionString != "Account already exists for this username.") 
		{
			InvokeRepeating ("RegisterOnParse", 2, 2);
		} 
		else 
		{
			LoginOnParse (emailTF.text.ToString (), passwordTF.text.ToString ());
			cancelInvoke = true;
		}
	}

	void GPSLoadCallback(bool success)
	{
		loadingOverlay.SetActive (false);
		if (success) 
		{
			UIManagerScript.instance.GoToTitle (true);
		} 
		else 
		{
			UIManagerScript.instance.CreateOrLoadGamePlaySession ();
			UIManagerScript.instance.GoToTitle (true);
		}

	}



	void OnLoginParse(bool success = false, string error = "")
	{
		parseLoginSuccess = success;
		parseLoginOver = true;
		parseMessage = error;
	}
		
	private IEnumerator SendRequest(string reqType)
    {
		string url = "http://bugdevstudios.com/wrestling/server.php?REQUEST="+reqType+"&EMAIL=" +emailTF.text+"&PASSWORD="+passwordTF.text;
		WWW www = new WWW (url);
		loginButton.GetComponent<Button> ().interactable = false;
		animText.gameObject.SetActive (true);

		while (!www.isDone) 
		{
			yield return StartCoroutine ("WaitingAnim");
		}

		if (!string.IsNullOrEmpty(www.error))
		{
			ShowToast("Internet/Server error.");

			yield break;
		}

		loginButton.GetComponent<Button> ().interactable = true;
		animText.gameObject.SetActive (true);

		Debug.Log (www.text);

		ProcessResponse(www.text);

		www.Dispose ();
//		string url = "http://bugdevstudios.com/wrestling/server.php?REQUEST="+reqType+"&ACCESS_TOKEN="+"0"+"&EMAIL="+emailTF.text+"&PASSWORD="+passwordTF.text;
//		GameSaveController.instance.myEmail = emailTF.text.ToString();
//		WWW www = new WWW(url);
//		loginButton.GetComponent<Button> ().interactable = false;
//		animText.gameObject.SetActive (true);
//
//		while (!www.isDone) 
//		{
//			yield return StartCoroutine ("WaitingAnim");
//		}
//
//
//        //yield return www;
//
//		if (!string.IsNullOrEmpty(www.error))
//		{
//			ShowToast("Internet/Server error.");
//			yield break;
//		}
//
//		//Debug.Log(www.text);
//		ProcessResponse(www.text);
//
//        www.Dispose();
    }


	void ProcessResponse(string response)
	{
		ParseManager.setData = true;
		var N = JSON.Parse(response);
		if (N == null) 
		{
			ShowToast("Login failed");
			loginButton.GetComponent<Button> ().interactable = true;
			return;
		}

		int responseCode = 0;
		int dataOnParse = 0;
		int transferData = 0;
		try
		{
			responseCode = N["CODE"].AsInt;
			dataOnParse = N["DATA_ON_PARSE"].AsInt;
			transferData = N["TRANSFER_DATA"].AsInt;
		}
		catch
		{
			ShowToast("Invalid Email or Password");
		}
		if (responseCode > 0) 
		{
			PlayerPrefs.SetString ("myEmail", emailTF.text.ToString ());
			PlayerPrefs.SetString ("myPassword", passwordTF.text.ToString ());
			StartCoroutine (WaitingOnPrefs ());
			if (dataOnParse == 0) 
			{
				bool transfer = false;
				if (PlayerPrefs.GetString ("GAMEPLAYSESSION", "NONE") != "NONE") 
				{
					GamePlaySession.instance = GamePlaySession.Load (PlayerPrefs.GetString ("GAMEPLAYSESSION", "NONE"));
					GamePlaySession.instance.UploadGameSession ();
					transfer = true;
				} 
				else 
				{
					GamePlaySession.instance = new GamePlaySession ();
					GamePlaySession.instance.InitializeSession (); 
				}

				foreach (var item in GamePlaySession.instance.myCards) 
				{
					item.PostSerialize ();
				}

				UIManager.GetComponent<UIManagerScript> ().GoToTitle (transfer);
			} 
			else if (dataOnParse == 1) 
			{
				bool load = false;
				if (PlayerPrefs.GetString ("GAMEPLAYSESSION", "NONE") != "NONE") 
				{
					GamePlaySession.instance = GamePlaySession.Load (PlayerPrefs.GetString ("GAMEPLAYSESSION", "NONE"));
					GamePlaySession.instance.UploadGameSession ();
				} 
				else 
				{
					GamePlaySession.instance = new GamePlaySession ();
					GamePlaySession.instance.GSI = UIManager.GetComponent<GameSetupItems> ();
					//GamePlaySession.instance.InitializeSession ();
					load = true;
				}
				UIManager.GetComponent<UIManagerScript> ().GoToTitle (false, load);
			}
		} 
		else 
		{
			ShowToast("Invalid Email or Password");
		}







//		if(responseCode>0)
//		{
//			if(responseCode == 1)//1=new user
//			{
//                newUser = true;
//				//Debug.Log("Server code : " + responseCode.ToString() + "Logging on PF with ID : " + userID);
//				//PlayerPrefs.SetString("UserID", userID);
//				//PlayFabManager.GetInstance().Login(userID);
//				StartCoroutine("PFLoginWait");
//            }
//            else if(responseCode == 2)//2=existing user
//			{
//				newUser = false;
//				serverWait = true;
//				StartCoroutine("ServerWait");
//
//
//				//ServerManager.GetComponent<DataHandler>().GetSessionDataFromServer(UpdateExistingUserData);
//
////				PlayerPrefs.SetString ("myEmail", emailTF.text.ToString ());
////				PlayerPrefs.SetString ("myPassword", passwordTF.text.ToString ());
////                newUser = false;
////				serverWait = true;
////				StartCoroutine("ServerWait");
////				//Debug.Log("Server code : " + responseCode.ToString() + " Logging on PF with ID : " + userID);
////				PlayerPrefs.SetString("UserID", userID);
////				ParseManager.RegisterUser (emailTF.text.ToString (), passwordTF.text.ToString (),OnParseRegister);
////				ServerManager.GetComponent<DataHandler>().GetSessionDataFromServer(UpdateExistingUserData);
////				//PlayFabManager.GetInstance().Login(userID);
////                StartCoroutine("PFLoginWait");
////				Debug.Log ("About to Register");
//            }
//            else if(responseCode == 3)
//			{
//				ShowToast("New password sent to email address.");
//			}
//		}
//		else
//		{
//			if(responseCode == -2)
//			{
//				ShowToast("An error occured while sending email for password reset. Please try again.");
//			}
//			else if(responseCode == -3)
//			{
//				ShowToast("No record found of given email address.");
//			}
//		}
	}

	IEnumerator PFLoginWait()
	{
		
        if (!newUser)
        {
            //while (!PlayFabManager.GetInstance().IsPFLoggedIn())
            //yield return new WaitUntil(()=>ServerManager.GetComponent<DataHandler>().loaded);

			while (!ServerManager.GetComponent<DataHandler> ().loaded) 
			{
				yield return StartCoroutine ("WaitingAnim");
			}

			animText.gameObject.SetActive (false);
			loginButton.GetComponent<Button> ().interactable = true;
            //Debug.Log("Logged in on PF, Loaded");

			if (GamePlaySession.instance == null) 
			{
				Debug.Log ("Creating GPS01");
				UIManager.GetComponent<UIManagerScript> ().CreateOrLoadGamePlaySession ();
			}
				


            UIManager.GetComponent<UIManagerScript>().GoToTitle();
			if (UIManagerScript.instance.transferCash > -1) 
			{
				PopUpWindow popUpWindow = GameObject.Find("UIManager").GetComponent<UIManagerScript>().popUpWindow;

				if (popUpWindow != null) 
				{
					popUpWindow.OpenPopUpWindow(new PopUpMessage[] {dataTransferPopup}, UIManager.GetComponent<UIManagerScript>().TitleScreen);
				}
			}


        }
        else if (newUser)
        {
            //while (!PlayFabManager.GetInstance().IsPFLoggedIn())
            yield return new WaitForSeconds(0.5f);
			if (GamePlaySession.instance == null) 
			{
				Debug.Log ("Creating GPS02");
				UIManager.GetComponent<UIManagerScript> ().CreateOrLoadGamePlaySession ();
			}
				
            //Debug.Log("Logged in on PF");

            UIManager.GetComponent<UIManagerScript>().GoToTitle();
        }
		
	}

	IEnumerator ServerWait()
	{
		//while(!PlayFabManager.GetInstance().IsPFLoggedIn())
		//	yield return new WaitForSeconds(0.5f);

		while(serverWait)
			yield return new WaitForSeconds(0.5f);

		if (GamePlaySession.instance == null) 
		{
			UIManager.GetComponent<UIManagerScript> ().CreateOrLoadGamePlaySession ();
		}
			
		//Debug.Log("Logged in on PF and got data from server");
		UIManager.GetComponent<UIManagerScript>().GoToTitle();
	}

	public void UpdateExistingUserData(bool result)
	{
		serverWait = !result;
	}

    public void OpenRegistrationScreen()
    {
        if (ScreenHandler.current != null && UIManager.GetComponent<UIManagerScript>().RegisterScreen != null)
            ScreenHandler.current.OpenPanel(UIManager.GetComponent<UIManagerScript>().RegisterScreen);
    }
	bool waitingOnPrefs = false;

	IEnumerator WaitingOnPrefs()
	{
		yield return new WaitUntil (() => waitingOnPrefs == true);
		PlayerPrefs.SetString ("parseUserID", Parse.ParseUser.CurrentUser.ObjectId);
	}

	IEnumerator WaitingAnim()
	{
		string loadingText;
		for (int i = 1; i <= 10; i++)
		{
			loadingText = "";
			for (int j = 0; j < i; j++)
			{
				loadingText += ".";
			}
			animText.text = loadingText;
			yield return new WaitForSeconds (0.1f);
		}
	}

	public void FacebookLogin()
	{
		//if(!FBManager.IsFBLoggedIn())
		//{
		//	FBManager.FBLogIn();
		//	//Debug.Log("Sending login request to FB");
		//}
	}

	bool forgotDone = false;
	string toastToShow = "";
	public void ForgotPassword()
	{
		forgotDone = false;
		StartCoroutine (ForgotWait ());

		if(!string.IsNullOrEmpty(emailTF.text) && IsEmail(emailTF.text))
		{
			ParseManager.ResetPassword (emailTF.text,ForgotCallback);
		}
		else if(!IsEmail(emailTF.text))
		{
			toastToShow = "Please enter a valid email address";
			forgotDone = true;
		}
	}

	IEnumerator ForgotWait()
	{
		yield return new WaitUntil (() => forgotDone == true);
		ShowToast (toastToShow);
	}

	public void ForgotCallback(bool success,string message = "")
	{
		if (success) 
		{
			toastToShow = ("Reset instructions sent to email");
		} 
		else 
		{
			toastToShow = (message.ToString ());
		}
		forgotDone = true;
	}

	void FBLoginCallback(bool result)
	{
		if(result)
		{
			StartCoroutine(SendRequest("GET_DATA_ON_PARSE"));
			Debug.Log("Sending login request via FB token");
		}
	}
}
