﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonSound : MonoBehaviour 
{
	private UIManagerScript UIManager;

	void Start()
    {
		UIManager = GameObject.Find("UIManager").GetComponent<UIManagerScript>();

		Button button = this.gameObject.GetComponent<Button>();
		button.onClick.AddListener(() => ClickSound());

		if (gameObject.GetComponent<Image>().sprite != null)
		{
			if (gameObject.GetComponent<Image>().sprite.name == "button_Background" || gameObject.GetComponent<Image>().sprite.name=="red button")
			{
				if (UIManagerScript.instance.Theme90s) 
				{
					gameObject.GetComponent<Image> ().sprite = UIManagerScript.instance.RedButton;
				} 
				else 
				{
					gameObject.GetComponent<Image> ().sprite = UIManagerScript.instance.PurpleButton;

				}
			} 
		}
    }

	void ClickSound() 
	{
		if (UIManager.sfxEnabled && UIManager.buttonSoundSource != null) 
		{
			UIManager.buttonSoundSource.Play();
		}
	}
	void OnEnable()
	{
		if (gameObject.GetComponent<Image>().sprite != null)
		{
			if (gameObject.GetComponent<Image>().sprite.name == "button_Background" || gameObject.GetComponent<Image>().sprite.name=="red button")
			{
				
				if (UIManagerScript.instance.Theme90s) 
				{
					gameObject.GetComponent<Image> ().sprite = UIManagerScript.instance.RedButton;
				} 
				else 
				{
					gameObject.GetComponent<Image> ().sprite = UIManagerScript.instance.PurpleButton;
				}
			}
		}
	}
}
