﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GiftObject {
	public int cash;
	public int tokens;
	public string cardMessage;
	public int[] cardIDs;
}

public class GiftInfo : MonoBehaviour {
	public Text cashAmount;
	public Text tokenAmount;

	public void ShowGift(GiftObject gift) {
		if (gift.cash > 0 && cashAmount != null) {
			cashAmount.transform.parent.gameObject.SetActive(true);
			cashAmount.text = gift.cash.ToString();
		} else {
			cashAmount.transform.parent.gameObject.SetActive(false);
		}
		
		if (gift.tokens > 0 && tokenAmount != null) {
			tokenAmount.transform.parent.gameObject.SetActive(true);
			tokenAmount.text = gift.tokens.ToString();
		} else {
			tokenAmount.transform.parent.gameObject.SetActive(false);
		}
	}
}
