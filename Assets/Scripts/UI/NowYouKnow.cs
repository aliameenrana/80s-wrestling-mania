﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class NowYouKnow : MonoBehaviour {
	public int cardChance;
	public int[] cardIDsAvailable;

	public Text nowYouKnowText;

	public CardSelectScreenScript cardViewer;

	private WeekItem weekData;

	private GameObject UIManager;
	private GameObject playerHUD;

	public void OpenNowYouKnowPanel(WeekItem week)
    {
		SetCurrentShow(week);

		UIManager = GameObject.Find("UIManager");
		playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;
		
		if (playerHUD != null)
			playerHUD.SetActive(false);

        if (GamePlaySession.instance.myShows[weekData.showID].AwardShow == false)
        {
            if (ScreenHandler.current != null)
                ScreenHandler.current.OpenPanel(this.gameObject.GetComponent<Animator>());

            if (UIManager.GetComponent<UIManagerScript>().sfxEnabled)
                GetComponent<AudioSource>().Play();
        }
        if (GamePlaySession.instance.myShows[weekData.showID].AwardShow == true && GamePlaySession.instance.myShows[weekData.showID].showFinished==false)
        {
            if (ScreenHandler.current != null)
            {
                ScreenHandler.current.OpenPanel(UIManager.GetComponent<UIManagerScript>().AwardsGivingScreen);
                UIManager.GetComponent<UIManagerScript>().GetComponent<AwardsController>().CurrentWeek = week;
            }
            PopUpWindow popUpWindow1 = UIManager.GetComponent<UIManagerScript>().popUpWindow;
            if (popUpWindow1 != null)
            {
                popUpWindow1.OpenPopUpWindow(new PopUpMessage[] { UIManager.GetComponent<AwardsController>().AwardsPopupMessage }, UIManager.GetComponent<UIManagerScript>().AwardsGivingScreen, false);
            }

            AwardScreenController.instance.GiveAwardsButton.GetComponent<Button>().interactable = true;
        }
        else if (GamePlaySession.instance.myShows[weekData.showID].AwardShow == true && GamePlaySession.instance.myShows[weekData.showID].showFinished == true)
        {
            Debug.Log("Show is: "+GamePlaySession.instance.myShows[weekData.showID].showName);
            if (ScreenHandler.current != null)
            {
                ScreenHandler.current.OpenPanel(UIManager.GetComponent<UIManagerScript>().AwardsGivingScreen);
                if (AwardScreenController.instance != null)
                {
					int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName);
                    List<CardObject> AwardCards = GamePlaySession.instance.Awards[ThisYear - 1980];

					AwardCards[0].logoImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(AwardCards[0].CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[AwardCards[0].CardNumber];
					AwardCards[1].logoImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(AwardCards[1].CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[AwardCards[1].CardNumber];
					AwardCards[2].logoImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(AwardCards[2].CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[AwardCards[2].CardNumber];
					AwardCards[3].logoImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(AwardCards[3].CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[AwardCards[3].CardNumber];
                         
                    AwardScreenController.instance.WrestlerOfTheYear.GetComponent<Image>().sprite = AwardCards[0].logoImage;
                    AwardScreenController.instance.TagTeamOfTheYear.GetComponent<Image>().sprite = AwardCards[1].logoImage;
                    AwardScreenController.instance.MostPopularWrestler.GetComponent<Image>().sprite = AwardCards[2].logoImage;
                    AwardScreenController.instance.MostHatedWrestler.GetComponent<Image>().sprite = AwardCards[3].logoImage;

                    ColorBlock white = AwardScreenController.instance.WrestlerOfTheYear.GetComponent<Button>().colors;
                    white.disabledColor = new Color(1, 1, 1, 1);

                    AwardScreenController.instance.WrestlerOfTheYear.GetComponent<Button>().colors = white;
                    AwardScreenController.instance.TagTeamOfTheYear.GetComponent<Button>().colors = white;
                    AwardScreenController.instance.MostPopularWrestler.GetComponent<Button>().colors = white;
                    AwardScreenController.instance.MostHatedWrestler.GetComponent<Button>().colors = white;

                    AwardScreenController.instance.GiveAwardsButton.GetComponent<Button>().interactable = false;
                    AwardScreenController.instance.WrestlerOfTheYear.GetComponent<Button>().interactable = false;
                    AwardScreenController.instance.TagTeamOfTheYear.GetComponent<Button>().interactable = false;
                    AwardScreenController.instance.MostPopularWrestler.GetComponent<Button>().interactable = false;
                    AwardScreenController.instance.MostHatedWrestler.GetComponent<Button>().interactable = false;

                    foreach (Text item in AwardScreenController.instance.Texts)
                    {
                        item.GetComponent<Text>().text = "";
                    }
                }
                UIManager.GetComponent<UIManagerScript>().GetComponent<AwardsController>().CurrentWeek = week;
            }
        }
    }
	
	public void SetCurrentShow(WeekItem week) {
		weekData = week;
		
		if (GamePlaySession.instance.myShows.Count <= weekData.showID)
        {
			GamePlaySession.instance.myShows.Insert(weekData.showID, weekData.GenerateShow());
		}
        else if (GamePlaySession.instance.myShows[weekData.showID] == null)
        {
			GamePlaySession.instance.myShows[weekData.showID] = weekData.GenerateShow();
		}

		UpdateDisplay();
	}

	public void UpdateDisplay() {
		if (nowYouKnowText != null) {
			if (GamePlaySession.instance.GetCurrentYear() == 0 && GamePlaySession.instance.GetCurrentMonth() == 0) {
				// We're in the tutorial week, so show the pre-defined Now You Know
				nowYouKnowText.text = "Every month “Now You Know!” segments will provide helpful hints to improve your 8MW experience!\n\nFor example: At the end of every single game year you’ll earn a bigger venue card, a better sponsor card, plus a free Token! Be sure to keep on playing to get awesome free stuff!";
			} else {
				GameSetupItems items = GameObject.Find("UIManager").GetComponent<GameSetupItems>();
				if (weekData.GetShow().showFinished) {
					nowYouKnowText.text = items.nowyouknows.blurbs[weekData.GetShow().blurbNumber];

				} else {
					int blurbNum = RollForBlurb(items.nowyouknows.blurbs);
					nowYouKnowText.text = items.nowyouknows.blurbs[blurbNum];
					weekData.GetShow().blurbNumber = blurbNum;
					GamePlaySession.instance.shownBlurbs.Add(blurbNum);
				}
			}
		}
	}

	int RollForBlurb(string[] blurbs) {
		int blurbNum = 0;
		
		List<int> blurbNums = new List<int>();
		
		for (int i = 0; i < blurbs.Length; i++) {
			if (!GamePlaySession.instance.shownBlurbs.Contains(i))
				blurbNums.Add(i);
		}
		
		if (blurbNums.Count > 0) {
			blurbNum = blurbNums[Random.Range(0, blurbNums.Count - 1)];
		} else {
			blurbNum = Random.Range(0, blurbs.Length - 1);
		}
		
		return blurbNum;
	}

	public void CloseNowYouKnowPanel() 
	{
        //GameObject ThisButton = EventSystem.current.currentSelectedGameObject;
		GameSetupItems items = GameObject.Find("UIManager").GetComponent<GameSetupItems>();

		if (weekData.GetShow().showFinished) 
		{
			GameObject.Find("UIManager").GetComponent<UIManagerScript>().GoToCalendar(false);
		} 
		else 
		{
			weekData.GetShow().showFinished = true;
			weekData.GetShow ().showRating = 20f;
			SaveNowYouKnowToServer ();

			// Check for unlocking a new card
			if (Random.Range(1, 100) <= cardChance && cardIDsAvailable.Length > 0) 
			{
				int cardID = 0;
				int cardAttempt = 0;
				bool foundCardToGive = false;
				while (!foundCardToGive) 
				{
					// Select a card from the pool of IDs and see if the player doesn't already own it
					cardID = cardIDsAvailable[Random.Range(0, cardIDsAvailable.Length - 1)];
					if (GamePlaySession.instance.CheckCard(cardID) == null) 
					{
						foundCardToGive = true;
					}

					// Make sure we don't fall in to an infinite loop by giving a finite number of attempts
					cardAttempt += 1;
					if (cardAttempt >= 20)
						break;
				}

				// If we got a card to give to the player, let's process it
				if (foundCardToGive) 
				{
					NewCardsObject newCards = new NewCardsObject();
					newCards.message = new PopUpMessage();
					newCards.message.message = "Congrats! You unlocked a new card!";

					newCards.cards.Add(items.GetCard(cardID).Clone());

					if (cardViewer != null)
						cardViewer.OpenCardView(newCards);
				} 
				else 
				{
					// Else just go back to the calendar
					GameObject.Find("UIManager").GetComponent<UIManagerScript>().GoToCalendar(false);
				}
			} 
			else 
			{
				GameObject.Find("UIManager").GetComponent<UIManagerScript>().GoToCalendar(false);
			}
		}
	}


	public void SaveNowYouKnowToServer()
	{
		Dictionary<string,object> baseData = weekData.GetShow().GetShowBaseData ();
		ParseManager.CreateShowData (baseData,null, null);
	}
}
