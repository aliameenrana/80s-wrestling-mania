﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ShowIconset {
	public Sprite showIcon;
	public Sprite showHeader;
    public Sprite showHeader90s;
}

public class ShowIcons : MonoBehaviour {
	public Sprite defaultIcon;
	public Sprite p4vIcon;
	public ShowIconset[] showIconsets;
}
