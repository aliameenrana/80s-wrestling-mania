﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Championship
{
	public int cardNumber;
	public int weekNumber;
	public int defenses;


	public Championship(int cardNum, int startingWeek)
	{
		cardNumber = cardNum;
		weekNumber = startingWeek;
	}
}
public class TagChampionship
{
	public int cardA;
	public int cardB;
	public int weekNumber;
	public int defenses;


	public TagChampionship(int cardnumA, int cardnumB, int startingWeek)
	{
		cardA = cardnumA;
		cardB = cardnumB;
		weekNumber = startingWeek;
	}
}