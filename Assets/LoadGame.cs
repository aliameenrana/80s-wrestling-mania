﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class LoadGame : MonoBehaviour {

	public Text noticeText;
	public FlashText flash;
	public GameObject MainCanvas;
	public GameObject UIManager;
	public GameObject SocialManager;
    public GameObject LoadingBar;
    public List<GameObject> LoadingDots;
	public string messageToShow;

    int versionNumber = 5, index = 0;
	int loadCheck1 = 0, loadCheck2 = 0, loadCheck3 = 0;
	string url;
	bool serverWait = false;
	Image loadingImage;
	bool saveTimeOver;

	GameSetupItems gameSetupItems;
	UIManagerScript UIManagerScript;
	FaceBookManager FBManager;

	// Use this for initialization
	IEnumerator Start()
	{
//		ParseManager.RegisterUser("umar", "test");
//		yield return new WaitForSeconds(2f);
//		ParseManager.LogInUser("umar", "test");
//		yield return new WaitForSeconds(2f);
//
//		yield break;

        serverWait = false;
        
        gameSetupItems = UIManager.GetComponent<GameSetupItems>();
		UIManagerScript = UIManager.GetComponent<UIManagerScript>();
		FBManager = SocialManager.GetComponent<FaceBookManager>();

		loadingImage = LoadingBar.transform.Find("fill").GetComponent<Image>();



#if UNITY_EDITOR
        url = "https://s3-ap-southeast-1.amazonaws.com/80wm/";
		//url = Application.streamingAssetsPath + "/";
#else
        url = Application.streamingAssetsPath + "/";
#endif
        bool loaded = false;
        
        
        yield return StartCoroutine(LoadAssets());
        if (loadCheck1 + loadCheck2 + loadCheck3 == 3 && PlayerPrefs.GetInt("FirstLoad", 1) == 0)
        {
            loaded = true;
            //StartCoroutine(RunLoading());
			ActivateObjects();
        }

        if (PlayerPrefs.GetInt("FirstLoad", 1) == 1)
        {
            PlayerPrefs.SetInt("FirstLoad", 0);
        }

		while(serverWait)
		{
			Debug.Log("Waiting for server");
			yield return new WaitForSeconds(0.5f);
		}
        
        if (!loaded)
        {
            ActivateObjects();
        }
	}

    IEnumerator RunLoading()
    {
        //StopCoroutine(Start());
        float time = Time.time;

        while (Time.time - time < 4f)
        {
            loadingImage.fillAmount = ((Time.time - time) / 4f);
            yield return null;
        }
        ActivateObjects();
    }

	void ActivateObjects()
	{
		UIManager.SetActive(true);
		MainCanvas.SetActive(true);
		flash.waiting = false;
		gameObject.SetActive(false);
	}

	IEnumerator CheckPFLogin()
	{
		//while(!PlayFabManager.GetInstance().IsPFLoggedIn())
		yield return null;
		
		serverWait = false;
	}

	IEnumerator LoadAssets()
	{
		yield return StartCoroutine(DownloadAndCache("xmlresources", versionNumber));
		yield return StartCoroutine(DownloadAndCache("audio", versionNumber));
		yield return StartCoroutine(DownloadAndCache("cards", versionNumber));
	}

    IEnumerator DownloadAndCache(string bundleName, int version)
    {
        // Wait for the Caching system to be ready
        while (!Caching.ready)
            yield return null;

        string urlCurr = url + bundleName;
        // Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache

		WWW www = WWW.LoadFromCacheOrDownload(urlCurr, version);
        {
            // if (bundleName == "cards")
            //
            while (!www.isDone)
            {
                if (bundleName == "audio" && PlayerPrefs.GetInt("FirstLoad", 1) == 1)
                {
                    loadingImage.fillAmount = www.progress / 2f;
                }
                else if(bundleName == "cards" && PlayerPrefs.GetInt("FirstLoad", 1) == 1)
                {
                    loadingImage.fillAmount = 0.5f + www.progress / 2f;
                }
                yield return new WaitForSeconds(0.1f);
            }
               

            if (www.error != null)
            {
                throw new Exception("WWW download had an error:" + www.error);
            }

            AssetBundle bundle = www.assetBundle;
            //Debug.Log("Umar:: Loaded Asset Bundle : " + bundle.name);

            if (bundleName == "audio")
            {
                gameSetupItems.audio = bundle;
                loadCheck1++;
            }
			else if (bundleName == "cards")
            {
                gameSetupItems.cards = bundle;
                loadCheck2++;

            }
            else if (bundleName == "xmlresources")
            {
               gameSetupItems.nowyouknows = (NYKCardCollection)NYKCardCollection.Load(bundle.LoadAsset<TextAsset>("nowyouknow.xml").text);
			   gameSetupItems.micSpots = (MicSpotCardCollection)MicSpotCardCollection.Load(bundle.LoadAsset<TextAsset>("micspots.xml").text);
			   gameSetupItems.venues = (VenueCardCollection)VenueCardCollection.Load(bundle.LoadAsset<TextAsset>("venues.xml").text);
			   gameSetupItems.flavors = (FlavorCardCollection)FlavorCardCollection.Load(bundle.LoadAsset<TextAsset>("flavors.xml").text);
			   gameSetupItems.matches = (MatchCardCollection)MatchCardCollection.Load(bundle.LoadAsset<TextAsset>("matches.xml").text);
			   gameSetupItems.skits = (SkitCardCollection)SkitCardCollection.Load(bundle.LoadAsset<TextAsset>("skits.xml").text);
			   gameSetupItems.wrestlers = (WrestlerCardCollection)WrestlerCardCollection.Load(bundle.LoadAsset<TextAsset>("wrestler.xml").text);
				bundle.Unload(true);
               loadCheck3++;
            }
        }
        www.Dispose();
	}

	public void RunSaving()
	{
		noticeText.gameObject.GetComponent<Text> ().enabled = true;
		noticeText.text = "Saving";
		loadingImage.fillAmount = 0;

		StartCoroutine (SavingDisplayCoroutine ());
	}

	public IEnumerator SavingDisplayCoroutine()
	{
		float t = Time.time;

		while (Time.time - t < 4) 
		{
			loadingImage.fillAmount = ((Time.time - t) / 4f);
			yield return null;
		}

		StartCoroutine (SaveTimer ());
		yield return new WaitUntil (() => ((messageToShow != "") || (saveTimeOver == true)));
		noticeText.text = messageToShow;	

		yield return new WaitForSeconds (3f);
		gameObject.SetActive (false);
	}

	IEnumerator SaveTimer()
	{
		saveTimeOver = false;
		yield return new WaitForSeconds (8f);
		saveTimeOver = true;
	}
}
