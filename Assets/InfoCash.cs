﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum InfoType{
	CASH_COST,
	TOKENS_COST,
	MY_CASH,
	MY_TOKENS,
	BONUS
}
public class InfoCash: MonoBehaviour {

	public InfoType myType;

	public Color positiveCashColor = Color.green;
	public Color negativeCashColor = Color.red;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ItemCost(int cost){
		if(myType == InfoType.CASH_COST)
			this.GetComponent<Text>().text = cost.ToString();
	}

	public void ItemTokenCost(int cost){
		if(myType == InfoType.TOKENS_COST)
			this.GetComponent<Text>().text = cost.ToString();
	}

	public void ShowMyCash()
	{
		if(myType == InfoType.MY_CASH) {
			Text amountText = this.GetComponent<Text>();
			amountText.text = GamePlaySession.instance.GetMyCash().ToString();
			
			// Get both the amount text object and the currency sign
			Text[] textObjects = amountText.gameObject.transform.parent.gameObject.GetComponentsInChildren<Text>();
			foreach (Text textObject in textObjects) {
				if (GamePlaySession.instance.GetMyCash() < 0) {
					textObject.color = negativeCashColor;
				} else {
					textObject.color = positiveCashColor;
				}
			}
		}
	}

	public void ShowMyTokens(){
		if(myType == InfoType.MY_TOKENS)
			this.GetComponent<Text>().text = GamePlaySession.instance.GetMyTokens().ToString();
	}
}
