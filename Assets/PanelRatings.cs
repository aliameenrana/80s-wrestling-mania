﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelRatings : MonoBehaviour{

    public Text[] FedTitles;
    public Text[] FedRatings;
    public GameObject UIManager;
    public static PanelRatings instance;

    void Awake()
    {
        UIManager = GameObject.Find("UIManager");
        instance = this;
    }

    public void UpdateRatingsDisplay(bool UpdateRatings)
    {
        if (GamePlaySession.instance.RatingPositionHolder.Count == 0)
        {
            List<int> DefaultRatedFeds = new List<int> { 2, 1, 0 };
            GamePlaySession.instance.RatingPositionHolder.AddRange(DefaultRatedFeds);
        }

        if (GamePlaySession.instance.FedRatings.Count == 0)
        {
            List<float> DefaultRatings = new List<float> { 5, 3, 0 };
            GamePlaySession.instance.FedRatings.AddRange(DefaultRatings);
        }
        
        int i = 0;
        foreach (Text item in FedTitles)
        {
            item.text = UIManager.GetComponent<GameSetupItems>().Federations[GamePlaySession.instance.RatingPositionHolder[i]].Acronym;
            i++;
        }

        int j = 0;
        foreach (Text item in FedRatings)
        {
            item.text = GamePlaySession.instance.FedRatings[j].ToString();
            j++;
        }
    }
}
