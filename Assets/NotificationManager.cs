﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Linq;

public class NotificationManager : MonoBehaviour
{
    public Text txt;
	public 	List<RewardObject> allRewards;
	public GameObject UIManager;
	public CardSelectScreenScript cardViewer;

	public void Start()
    {
		Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
        Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
		Firebase.Messaging.FirebaseMessaging.Subscribe("/topics/global");
		allRewards = new List<RewardObject> ();
    }

    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
		PlayerPrefs.SetString ("FirebaseToken", token.Token);
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
		UIManagerScript.instance.CheckGifts ();
//		string[] rewardsReceived = new string[3]{"","",""};
//		Debug.Log ("Message Received");
//		if (e.Message.Data.Count > 0)
//		{
//			foreach (var item in e.Message.Data)
//			{
//				if (item.Key == "rewards")
//				{
//					rewardsReceived = item.Value.Split (new char[]{ '%' });
//				}
//			}
//		}
//		RewardObject reward = new RewardObject ();
//
//		reward = ExtractRewards (rewardsReceived);
//
//		allRewards.Add (reward);
////
////		Debug.Log ("REWARD CASH: " + reward.cash);
////		Debug.Log ("REWARD TOKENS: " + reward.tokens);
////
////		if (UIManager == null)
////			UIManager = GameObject.Find ("UIManager");
//
//		StartCoroutine (WaitForCalendar ());
    }

	RewardObject ExtractRewards(string[] rewardsReceived)
	{
		RewardObject reward = new RewardObject ();

		reward.cash = int.Parse (rewardsReceived[1]);

		reward.tokens = int.Parse (rewardsReceived[2]);

		reward.cards = new List<int> ();

		string[] cardsReceived = rewardsReceived [3].Split (new char[]{ ',' });

		foreach (var item in cardsReceived) 
		{
			reward.cards.Add (int.Parse (item));
		}

		return reward;
	}

	public void ShowRewards()
	{
		RewardObject totalReward = CalculateRewards ();
		PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript> ().popUpWindow;
		PopUpMessage rewardMessage = new PopUpMessage ();
		rewardMessage.gift = new GiftObject ();
		rewardMessage.message = "You have new Gifts!";
		rewardMessage.gift.cash = totalReward.cash;
		rewardMessage.gift.tokens = totalReward.tokens;
		rewardMessage.gift.cardMessage = "Also, you have been gifted the following card(s)";
		rewardMessage.gift.cardIDs = totalReward.cards.ToArray ();

		popUpWindow.OpenPopUpWindow (new PopUpMessage[]{ rewardMessage }, UIManagerScript.instance.MyFedScreen,false,true);
		allRewards.Clear ();
		StartCoroutine (UpdateCashTokens ());
	}

	public RewardObject CalculateRewards()
	{
		RewardObject totalReward = new RewardObject ();

		foreach (var reward in allRewards) 
		{
			totalReward.cash += reward.cash;	
			totalReward.tokens += reward.tokens;
			totalReward.cards = new List<int> ();

			foreach (var item in reward.cards) 
			{
				if (!totalReward.cards.Contains (item)) 
				{
					totalReward.cards.Add (item);	
				}
			}
		}
		return totalReward;
	}

	public IEnumerator WaitForCalendar()
	{
		Debug.Log ("WAITING FOR CALENDAR");
		yield return new WaitUntil (() => UIManager.gameObject.activeSelf == true);
		yield return new WaitUntil (() => (UIManager.GetComponent<UIManagerScript> ().MainCalendarScreen.gameObject.activeSelf==true || UIManager.GetComponent<UIManagerScript> ().MyFedScreen.gameObject.activeSelf==true));
		yield return new WaitForSeconds (0.5f);
		ShowRewards ();
	}

	IEnumerator UpdateCashTokens()
	{
		yield return new WaitForSeconds (1f);
		if (MyFed.instance != null) 
		{
			MyFed.instance.walletPanel.BroadcastMessage("ShowMyCash", SendMessageOptions.DontRequireReceiver);
			MyFed.instance.walletPanel.BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
		}
	}

}