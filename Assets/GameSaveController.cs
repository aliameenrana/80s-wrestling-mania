﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSaveController : MonoBehaviour 
{
	string FirebaseToken = "";
	[HideInInspector]
	public string myEmail = "";
	[HideInInspector]
	public bool firstSave = true;

	public static GameSaveController instance;
	public void Awake()
	{
		instance = this;
	}
	public void UploadUserData()
	{
		if (FirebaseToken == "") 
		{
			FirebaseToken = PlayerPrefs.GetString ("FirebaseToken", "");
		}
		//StartCoroutine (SendRequest ("UPDATE_USER_BY_ID"));
		//ParseManager.UpdateGameSessionData ("firebaseToken", FirebaseToken);
		ParseManager.SaveFirebaseToken(FirebaseToken);
	}

	IEnumerator SendRequest(string reqType)
	{
		string url = "http://bugdevstudios.com/wrestling/server.php?REQUEST=" + reqType + "&FIREBASE_TOKEN=" + FirebaseToken +
			"&CASH=" + GamePlaySession.instance.GetMyCash().ToString () +
		             "&CARDS=" + GamePlaySession.instance.myCards.Count.ToString () +
			"&TOKENS=" + GamePlaySession.instance.GetMyTokens().ToString () +
		             "&EMAIL=" + PlayerPrefs.GetString ("myEmail", myEmail);
		//Debug.Log(url);

		WWW www = new WWW(url);

		yield return www;

		if (www.error != null)
		{
			yield break;
		}
		www.Dispose();
		firstSave = false;
	}



}
