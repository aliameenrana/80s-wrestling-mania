﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;

[RequireComponent(typeof(AudioSource))]

public class GameSetupItems : MonoBehaviour
{
    public FedComItem[] Federations;
    public FedComItem[] Commisioners;
    public ShowObject[] ShowTypes;

	[HideInInspector]
    public WrestlerCardCollection wrestlers;
	[HideInInspector]
    public VenueCardCollection venues;
	[HideInInspector]
    public FlavorCardCollection flavors;
	[HideInInspector]
    public MatchCardCollection matches;
	[HideInInspector]
    public MicSpotCardCollection micSpots;
	[HideInInspector]
    public SkitCardCollection skits;
	[HideInInspector]
    public NYKCardCollection nowyouknows;


	[HideInInspector]
//	public string wrestlerDoc, venuesDoc, flavorsDoc, matchesDoc, micSpotsDoc, skitsDoc, nykDoc;

    public StorePage storePage;
	public AssetBundle cards, audio, xmlResources;
//    public List<Sprite> cardFronts;
//	public List<Sprite> cardLogos;
//	public List<Sprite> cardHeadshots;
//	public List<AudioClip> cardAudio;
//
//    public Dictionary<int, Sprite> cardFrontDict;
//    public Dictionary<int, Sprite> cardHeadshotDict;
//    public Dictionary<int, Sprite> cardLogoDict;
//    public Dictionary<int, AudioClip> cardAudioDict;

    public static GameSetupItems instance;
    public List<StoreCard> ShopCards;
	//Umar:: temp function
//	void PrintOut()
//	{
//		string assetBundleManagerResourcesDirectory = "Backup";
//		string assetBundleUrlPath = Path.Combine(assetBundleManagerResourcesDirectory, "cardAudio.txt");
//		Directory.CreateDirectory(assetBundleManagerResourcesDirectory);
//
//		string data = "";
//
//		foreach(AudioClip sprite in cardAudio)
//		{
//			data += "," + sprite.name;
//		}
//
//		File.WriteAllText(assetBundleUrlPath, data);
//	}

    void Awake()
    {
        System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
        instance = this;
    }

//    private IEnumerator ReadEditorType(string xml, Type type)
//    {
//		WWW www = new WWW("http://red-clove.com.ua/90wsEditor/" + xml);
//
//        yield return www;
//
//        if (www.error != null)
//        {
//            Debug.Log(www.error);
//            yield break;
//        }
//
//        if (type == typeof(WrestlerCardCollection))
//            wrestlerDoc = www.text;
//        else if (type == typeof(VenueCardCollection))
//            venuesDoc = www.text;
//        else if (type == typeof(FlavorCardCollection))
//            flavorsDoc = www.text;
//        else if (type == typeof(MatchCardCollection))
//            matchesDoc = www.text;
//        else if (type == typeof(MicSpotCardCollection))
//            micSpotsDoc = www.text;
//        else if (type == typeof(SkitCardCollection))
//            skitsDoc = www.text;
//        else if (type == typeof(NYKCardCollection))
//            nykDoc = www.text;
//
//		Debug.Log(xml + " Loaded.");
//
//        www.Dispose();
//    }

//    IEnumerator Start()
//    {
//		yield return new WaitForSeconds(1f);
//		nowyouknows = (NYKCardCollection)NYKCardCollection.Load(xmlResources.LoadAsset<TextAsset>("nowyouknow.xml").text);
//		micSpots = (MicSpotCardCollection)MicSpotCardCollection.Load(xmlResources.LoadAsset<TextAsset>("micspots.xml").text);
//		venues = (VenueCardCollection)VenueCardCollection.Load(xmlResources.LoadAsset<TextAsset>("venues.xml").text);
//		flavors = (FlavorCardCollection)FlavorCardCollection.Load(xmlResources.LoadAsset<TextAsset>("flavors.xml").text);
//		matches = (MatchCardCollection)MatchCardCollection.Load(xmlResources.LoadAsset<TextAsset>("matches.xml").text);
//		skits = (SkitCardCollection)SkitCardCollection.Load(xmlResources.LoadAsset<TextAsset>("skits.xml").text);
//		wrestlers = (WrestlerCardCollection)WrestlerCardCollection.Load(xmlResources.LoadAsset<TextAsset>("wrestler.xml").text);
//
//		xmlResources.Unload(true);        
//    }

    public CardObject GetCard(int id)
    {
        //Debug.Log(id);

        for (int i = 0; i < wrestlers.cardArray.Length; i++)
        {
            if (wrestlers.cardArray[i].CardNumber == id)
            {
                return wrestlers.cards[id];
            }
        }

        for (int i = 0; i < venues.cardArray.Length; i++)
        {
            if (venues.cardArray[i].CardNumber == id)
            {
                return venues.cards[id];
            }
        }

        for (int i = 0; i < flavors.cardArray.Length; i++)
        {
            if (flavors.cardArray[i].CardNumber == id)
            {
                return flavors.cards[id];
            }
        }

        for (int i = 0; i < matches.cardArray.Length; i++)
        {
            if (matches.cardArray[i].CardNumber == id)
            {
                return matches.cards[id];
            }
        }

        for (int i = 0; i < micSpots.cardArray.Length; i++)
        {
            if (micSpots.cardArray[i].CardNumber == id)
            {
                return micSpots.cards[id];
            }
        }

        for (int i = 0; i < skits.cardArray.Length; i++)
        {
            if (skits.cardArray[i].CardNumber == id)
            {
                return skits.cards[id];
            }
        }

        return null;
    }

    public void PlayTheme()
    {
        GetComponent<AudioSource>().Play();
    }

	public void GetResourcesReady()
	{
		for (int i = 0; i < GamePlaySession.instance.myCards.Count; i++)
		{
			if (GamePlaySession.instance.myCards[i].myCardType == CardType.WRESTLER)
			{
				GamePlaySession.instance.myCards[i] = GetCard(GamePlaySession.instance.myCards[i].CardNumber).CloneWithProgress(GamePlaySession.instance.myCards[i]);
			}
			else
			{
				GamePlaySession.instance.myCards[i] = GetCard(GamePlaySession.instance.myCards[i].CardNumber).Clone();
			}
		}

		foreach (ShowObject so in GamePlaySession.instance.myShows)
		{
			so.PostDeserialize();
		}

		if (storePage != null)
		{
			storePage.CheckCardLibrary();
			ShopCards = storePage.storeLibrary;
		}
	}
}

[System.Serializable]
public class CardSpriteEntry
{
    public int CardNumber;
    public Sprite sprite;
}