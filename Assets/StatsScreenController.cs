﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class StatsScreenController : MonoBehaviour
{

	public GameObject UIManager, WLRecordButton, TitlesButton, AwardsButton, WLHeads, WLRecords, TitlesHeads, TitlesRecords, AwardHeads, AwardRecords, WLRecordLine, TitleRecordLine, World;
	public Button StatsBackButton;
	public Text[] PlaceHolders;
	public GameObject[] AwardHolderLogos;
	int currentStatMenu, TitleRecordSelected = 1;
	int[] SelectableYears = new int[20] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
	int SelectedYear = 0;

	void Awake ()
	{
		UIManager = GameObject.Find ("UIManager");
		WLHeads.transform.GetChild (1).gameObject.GetComponent<Button> ().onClick.AddListener (() => PopulateWLRecordsPanel (SelectedYear, true, false));
		WLHeads.transform.GetChild (2).gameObject.GetComponent<Button> ().onClick.AddListener (() => PopulateWLRecordsPanel (SelectedYear, false, false));
	}

	public void OpenWLRecords ()
	{
		WLRecordButton.SetActive (false);
		TitlesButton.SetActive (false);
		AwardsButton.SetActive (false);
		WLHeads.SetActive (true);
		WLRecords.SetActive (true);
		PopulateWLRecordsPanel (int.Parse (GamePlaySession.instance.myCalendar.years [GamePlaySession.instance.GetCurrentYear()].yearName) - 1980, true, true);
		//PopulateWLRecordsPanel(0 , true, true);
		SelectedYear = int.Parse (GamePlaySession.instance.myCalendar.years [GamePlaySession.instance.GetCurrentYear()].yearName) - 1980;
		currentStatMenu = 0;
		StatsBackButton.onClick.SetPersistentListenerState (0, UnityEngine.Events.UnityEventCallState.Off);
		StatsBackButton.onClick.AddListener (() => CloseCurrentStatsMenu ());
	}

	public void OpenTitlesRecords ()
	{
		WLRecordButton.SetActive (false);
		TitlesButton.SetActive (false);
		AwardsButton.SetActive (false);
		TitlesHeads.SetActive (true);
		TitlesRecords.SetActive (true);
		PopulateTitlesRecordsPanel (World);
		currentStatMenu = 1;
		StatsBackButton.onClick.SetPersistentListenerState (0, UnityEngine.Events.UnityEventCallState.Off);
		StatsBackButton.onClick.AddListener (() => CloseCurrentStatsMenu ());
	}

	public void OpenAwards ()
	{
		WLRecordButton.SetActive (false);
		TitlesButton.SetActive (false);
		AwardsButton.SetActive (false);
		AwardHeads.SetActive (true);
		AwardRecords.SetActive (true);
		currentStatMenu = 2;
		StatsBackButton.onClick.SetPersistentListenerState (0, UnityEngine.Events.UnityEventCallState.Off);
		StatsBackButton.onClick.AddListener (() => CloseCurrentStatsMenu ());
        
		int ThisYear = int.Parse (GamePlaySession.instance.myCalendar.years [GamePlaySession.instance.GetCurrentYear()].yearName);
		SelectedYear = ThisYear - 1980;
		PopulateAwardsPanel (SelectedYear);
		//AwardYearLabel.text = "Year: "+SelectedYear + 1980.ToString();
        
	}

	public void ChangeYearAward (bool add)
	{
		if (add) 
		{
			SelectedYear++;
			if (SelectedYear >= 20) {
				SelectedYear = 0;
			}
		} 
		else 
		{
			if (SelectedYear <= 0) {
				SelectedYear = 20;
			}
			SelectedYear--;
		}

		PopulateAwardsPanel (SelectedYear);
	}

	public void ChangeYearWL (bool add)
	{
		if (add) 
		{
			SelectedYear++;
			if (SelectedYear >= 20) {
				SelectedYear = 0;
			}
		} 
		else 
		{
			if (SelectedYear <= 0) {
				SelectedYear = 20;
			}
			SelectedYear--;
		}
		if (SelectedYear >= 20) {
			SelectedYear = 0;
		}

		PopulateWLRecordsPanel (SelectedYear, true, true);
	}

	public void PopulateWLRecordsPanel (int Year, bool WithWins, bool Name)
	{
		//Debug.Log("Populating...");
		WLHeads.transform.GetChild (0).GetChild (0).gameObject.GetComponent<Text> ().text = (Year + 1980).ToString ();


		foreach (Transform item in WLRecords.transform.GetChild(0).transform.GetChild(0).transform) {
			Destroy (item.gameObject);
		}



		List<WrestlerCardObject> wrestlers = new List<WrestlerCardObject> ();

		foreach (var item in GamePlaySession.instance.myCards) {
			if (item.myCardType == CardType.WRESTLER) {
				WrestlerCardObject w = (WrestlerCardObject)item;
				wrestlers.Add (w);
			}
		}
		List<WrestlerCardObject> SortedList = wrestlers;
		if (Name) {
			SortedList = wrestlers.OrderBy (o => o.name).ToList ();
			SortedList.Reverse ();
		} else {
			if (WithWins) 
			{
				SortedList = wrestlers.OrderBy (o => o.GetWinLossDataB (Year, true)).ToList ();
			} 
			else 
			{
				SortedList = wrestlers.OrderBy (o => o.GetWinLossDataB (Year, false)).ToList ();
			}
		}
        
		SortedList.Reverse ();
		foreach (WrestlerCardObject item in SortedList) {
			if (item.myCardType == CardType.WRESTLER) {
				WrestlerCardObject wrestler = (WrestlerCardObject)item;
				GameObject WLFigures = WLRecords.transform.GetChild (0).transform.GetChild (0).transform.gameObject;
				GameObject WLRLine = Instantiate (WLRecordLine, WLFigures.transform);
				WLRLine.transform.localScale = new Vector3 (1, 1, 1);
				foreach (Transform item2 in WLRLine.transform) {
					if (item2.name == "WrestlerName") {
						item2.gameObject.GetComponent<Text> ().text = wrestler.name;
						if (wrestler.cardVersion.ToUpper () == "FOIL") {
							item2.gameObject.GetComponent<Text> ().color = new Color (255, 199, 0, 1);
						}
					} else if (item2.name == "Wins") {
						item2.gameObject.GetComponent<Text> ().text = wrestler.GetWinLossDataB (Year, true).ToString ();
					} else if (item2.name == "Losses") {
						item2.gameObject.GetComponent<Text> ().text = wrestler.GetWinLossDataB (Year, false).ToString ();
					}
				}
			}
		}

		WLRecords.transform.GetChild (0).transform.GetChild (0).GetComponent<RectTransform> ().sizeDelta = new Vector2 (WLRecords.transform.GetChild (0).transform.GetChild (0).GetComponent<RectTransform> ().rect.width, SortedList.Count * 31);
	}

	public void PopulateTitlesRecordsPanel (GameObject selector)
	{
		foreach (Transform item in TitlesRecords.transform.GetChild(0).transform.GetChild(0).transform) 
		{
			if (item.name.ToUpper () != "HEADERS") 
			{
				Destroy (item.gameObject);
			}
		}
		if (selector.name.ToUpper () == "TAG") 
		{
			List<TagChampionship> tagChampionships = GamePlaySession.instance.GetTagChampionships ();
			if (tagChampionships.Count > 0) 
			{
				tagChampionships.OrderBy (o => o.weekNumber);
				tagChampionships.Reverse ();
				foreach (var item2 in tagChampionships) 
				{
					GameObject TitleFigures = TitlesRecords.transform.GetChild (0).transform.GetChild (0).transform.gameObject;
					GameObject TitlesLine = Instantiate (TitleRecordLine, TitleFigures.transform);
					TitlesLine.transform.localScale = new Vector3 (1, 1, 1);

					string Wrestler1 = ((WrestlerCardObject)(GamePlaySession.instance.CheckCard (item2.cardA))).name.ToString();
					string Wrestler2 = ((WrestlerCardObject)(GamePlaySession.instance.CheckCard (item2.cardB))).name.ToString ();

					foreach (Transform item3 in TitlesLine.transform) 
					{
						if (item3.name == "WrestlerName") 
						{
							if (Wrestler1 != null && Wrestler2 != null)
								item3.gameObject.GetComponent<Text> ().text = Wrestler1 + " & " + Wrestler2;
						} 
						else if (item3.name == "Won on") 
						{
							item3.gameObject.GetComponent<Text> ().text = "Week " + WeekToDate (item2.weekNumber)[2] + ", Month: " + WeekToDate (item2.weekNumber)[1] + ", " + WeekToDate (item2.weekNumber)[0];//   item2.weekNumber.ToString();//  item2.CrowningDate.ToString();		
						} 
						else if (item3.name == "Defenses") 
						{
							item3.gameObject.GetComponent<Text> ().text = item2.defenses.ToString();
						}
					}
				}
			}
		}
		else if (selector.name.ToUpper () == "WORLD") 
		{
			List<Championship> allWorldChampionships = GamePlaySession.instance.GetWorldChampionships ();
			if (allWorldChampionships.Count > 0) 
			{
				allWorldChampionships.OrderBy (o => o.weekNumber);
				allWorldChampionships.Reverse ();
				foreach (var item2 in allWorldChampionships) 
				{	
					WrestlerCardObject w = (WrestlerCardObject)GamePlaySession.instance.CheckCard (item2.cardNumber);
					GameObject TitleFigures = TitlesRecords.transform.GetChild (0).transform.GetChild (0).transform.gameObject;
					GameObject TitlesLine = Instantiate (TitleRecordLine, TitleFigures.transform);
					TitlesLine.transform.localScale = new Vector3 (1, 1, 1);
					foreach (Transform item3 in TitlesLine.transform) 
					{
						if (item3.name == "WrestlerName") 
						{
							item3.gameObject.GetComponent<Text> ().text = w.name;
						} 
						else if (item3.name == "Won on") 
						{
							item3.gameObject.GetComponent<Text> ().text = "Week " + WeekToDate (item2.weekNumber)[2] + ", Month: " + WeekToDate (item2.weekNumber)[1] + ", " + WeekToDate (item2.weekNumber)[0];//   item2.weekNumber.ToString();//  item2.CrowningDate.ToString();
							//Debug.Log (WeekToDate (item2.weekNumber));
						} 
						else if (item3.name == "Defenses") 
						{
							item3.gameObject.GetComponent<Text> ().text = item2.defenses.ToString ();
						}
					}
				}
			}
		} 
		else if (selector.name.ToUpper () == "TV") 
		{
			List<Championship> allTitleChampionShips = GamePlaySession.instance.GetTitleChampionships ();
			if (allTitleChampionShips.Count > 0) 
			{
				allTitleChampionShips.OrderBy (o => o.weekNumber);
				allTitleChampionShips.Reverse ();
				//GamePlaySession.instance.AllTitleChampions.OrderBy (o => GamePlaySession.instance.GetDate (o));
				foreach (var item2 in allTitleChampionShips) 
				{	
					WrestlerCardObject w = (WrestlerCardObject)GamePlaySession.instance.CheckCard (item2.cardNumber);
					GameObject TitleFigures = TitlesRecords.transform.GetChild (0).transform.GetChild (0).transform.gameObject;
					GameObject TitlesLine = Instantiate (TitleRecordLine, TitleFigures.transform);
					TitlesLine.transform.localScale = new Vector3 (1, 1, 1);
					foreach (Transform item3 in TitlesLine.transform) 
					{
						if (item3.name == "WrestlerName") 
						{
							item3.gameObject.GetComponent<Text> ().text = w.name;
						} 
						else if (item3.name == "Won on") 
						{
							item3.gameObject.GetComponent<Text> ().text = "Week " + WeekToDate (item2.weekNumber)[2] + ", Month: " + WeekToDate (item2.weekNumber)[1] + ", " + WeekToDate (item2.weekNumber)[0];//   item2.weekNumber.ToString();//  item2.CrowningDate.ToString();
							//Debug.Log (WeekToDate (item2.weekNumber));
						} 
						else if (item3.name == "Defenses") 
						{
							item3.gameObject.GetComponent<Text> ().text = item2.defenses.ToString ();
						}
					}
				}
			}
		}
		TitlesRecords.transform.GetChild (0).transform.GetChild (0).GetComponent<RectTransform> ().sizeDelta = new Vector2 (TitlesRecords.transform.GetChild (0).transform.GetChild (0).GetComponent<RectTransform> ().rect.width, TitlesRecords.transform.GetChild (0).transform.GetChild (0).childCount * 31);
	}

	public void PopulateAwardsPanel (int Year)
	{
		AwardHeads.transform.GetChild (0).GetChild (0).gameObject.GetComponent<Text> ().text = (Year + 1980).ToString ();
		if (GamePlaySession.instance.Awards != null) {
			if (GamePlaySession.instance.Awards [SelectedYear] == null || GamePlaySession.instance.Awards [SelectedYear].Count == 0) {
				foreach (Text item in PlaceHolders) {
					item.gameObject.SetActive (true);
				}
				foreach (var item in AwardHolderLogos) {
					item.GetComponent<Image> ().sprite = null;
					item.GetComponent<Image> ().color = Color.black;
				}
			} else if (GamePlaySession.instance.Awards [SelectedYear] != null && GamePlaySession.instance.Awards [SelectedYear].Count > 0) {
				foreach (var item in AwardHolderLogos) {
					item.GetComponent<Image> ().color = new Color (1, 1, 1, 1);
				}
				foreach (Text item in PlaceHolders) {
					item.gameObject.SetActive (false);
				}
				CardObject[] C = new CardObject[4] {
					GamePlaySession.instance.Awards [SelectedYear] [0],
					GamePlaySession.instance.Awards [SelectedYear] [1],
					GamePlaySession.instance.Awards [SelectedYear] [2],
					GamePlaySession.instance.Awards [SelectedYear] [3]
				};

				int[] cardNumbers = new int[]{ C [0].CardNumber, C [1].CardNumber, C [2].CardNumber, C [3].CardNumber };


				WrestlerCardObject wrestlerOfTheYear = (WrestlerCardObject)C [0];
				if (wrestlerOfTheYear.cardVersion == "FOIL")
					cardNumbers [0] -= 1;

				WrestlerCardObject mostPopularWrestler = (WrestlerCardObject)C [2];
				if (mostPopularWrestler.cardVersion == "FOIL")
					cardNumbers [2] -= 1;


				WrestlerCardObject mostHatedWrestler = (WrestlerCardObject)C [3];
				if (mostHatedWrestler.cardVersion == "FOIL")
					cardNumbers [3] -= 1;


				AwardHolderLogos [0].GetComponent<Image> ().sprite = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite> (cardNumbers [0].ToString ("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[C[0].CardNumber];
				AwardHolderLogos [1].GetComponent<Image> ().sprite = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite> (cardNumbers [1].ToString ("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[C[1].CardNumber];
				AwardHolderLogos [2].GetComponent<Image> ().sprite = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite> (cardNumbers [2].ToString ("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[C[2].CardNumber];
				AwardHolderLogos [3].GetComponent<Image> ().sprite = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite> (cardNumbers [3].ToString ("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[C[3].CardNumber];
			}
		} else if (GamePlaySession.instance.Awards == null) {

		}
	}

	public void CloseCurrentStatsMenu ()
	{
		switch (currentStatMenu) {
		case 0:
			WLHeads.SetActive (false);
			WLRecords.SetActive (false);
			break;
		case 1:
			TitlesHeads.SetActive (false);
			TitlesRecords.SetActive (false);
			break;
		case 2:
			AwardRecords.SetActive (false);
			AwardHeads.SetActive (false);
			break;
		}
		StatsBackButton.onClick.RemoveAllListeners ();
		StatsBackButton.onClick.SetPersistentListenerState (0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
		WLRecordButton.SetActive (true);
		TitlesButton.SetActive (true);
		AwardsButton.SetActive (true);
	}

	public void SwitchTitlesRecords (GameObject selector)
	{
		PopulateTitlesRecordsPanel (selector);
	}

	int[] WeekToDate(int weekNum)
	{
		int[] date = new int[3];
		int year = ((int)Mathf.Floor(weekNum / 60) + int.Parse(GamePlaySession.instance.myCalendar.years[0].yearName));
		double month = (weekNum / 60.0f)*12;
		double week = (month - (int)month)*4;
		date = new int[3]{ year, (int)(month + 1), (int)(week + 1) };
		return date;
	}

}
