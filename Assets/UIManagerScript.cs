﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.IO;
using Parse;
using System;
using System.Linq;


public class UIManagerScript : MonoBehaviour
{
    public Animator TitleScreen;
    public Animator LoginScreen;
    public Animator DisclaimerScreen;
    public Animator WelcomeScreen;
    public Animator FedScreen;
    public Animator FedScreenVerify;

    public Animator MyFedScreen;
    public Animator CommishSelectScreen;
    public Animator CommishScreenVerify;

    public Animator MainCalendarScreen;
    public Animator NowYouKnowScreen;
    public Animator ShowSetupScreen;
    public Animator MatchSetupScreen;
    public Animator TagMatchSetupScreen;
    public Animator MicSpotSetupScreen;
    public Animator SkitSetupScreen;
    public Animator ShowTransitionScreen;
    public Animator ShowResultsScreen;
    public Animator CantDoThisScreen;
    public Animator DemoteVerifyScreen;
    public Animator StatsSelectScreen;
    public Animator CheckListScreen;
    public Animator PromoteVerifyScreen;
    public Animator RegisterScreen;

    public Animator ShopScreen;

    public Animator HelpScreen;
    public Animator CreditsScreen;
    public Animator StartYearSelect;
    public Animator AwardsGivingScreen;

    public EndGameScreen endGameScreen;

    public PopUpWindow popUpWindow;

    public CardSelectScreenScript cardViewer;

    public GameObject PlayerHUD;
	public GameObject NotificationManager;
    public bool sfxEnabled;

    public AudioSource buttonSoundSource;

    public GameObject screenNode;
    public GameObject SelectedYear;
    public GameObject CheckListLinePrefab, CheckListActiveType;
    public Sprite CheckedCheckBox;
    public Sprite UnCheckedCheckBox;
    public Sprite RedButton;
	public Sprite PurpleButton;
    public Image Background90s;
	public AudioClip Track90s;
	public AudioClip Track80s;


    public static UIManagerScript instance;
    public string myStr = "";
    public float T;
    public List<GameObject> CheckListLines;
    public bool Theme90s;

    bool Order = false;
	public List<Sprite> redSprites;
	public List<Sprite> pinkSprites;
	public int transferCash;
	public int transferTokens;
	public List<int> transferCardNumbers;

	public Text collectionText;

    CardType ActiveCheckListType = new CardType();

	public GamePlaySession copyGamePlaySession;
	public GameObject loadingCanvas;

	private string myEmail;
	private string myPassword;
	public CalendarObject calendar;

	public Dictionary<int,List<int>> wrestlersDictionary;
	public GameSetupItems gsi;

	public int onParse = 0;
	public string path = "";
	RewardObject totalReward = new RewardObject();

    public UIManagerScript GetInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
        return instance;
    }

    void Awake()
    {
		#if UNITY_EDITOR
		path = Application.dataPath + "/userdata.dat";
		#elif UNITY_ANDROID
		path = Application.persistentDataPath + "/userdata.dat";
		#endif

		onParse = PlayerPrefs.GetInt ("onParse", 0);
        instance = this;
		gsi = GetComponent<GameSetupItems> ();
        T = Time.time;
		if (PlayerPrefs.GetInt ("theme", 0) != 0) 
		{
			Theme90s = true;
			Background90s.gameObject.SetActive(true);
		}
		var panels = TitleScreen.gameObject.transform.parent.GetComponentsInChildren<ThemeController> (true);
		foreach (var item in panels) 
		{
			item.CheckTheme ();
		}
		//ALI AMEEN
		transferCash = -1;
		transferTokens = -1;
		transferCardNumbers.Clear ();
    }

    void Update()
    {
        if (screenNode != null && !screenNode.activeInHierarchy)
        {
            screenNode.SetActive(true);
            screenNode.transform.parent.gameObject.SetActive(true);

            if (ScreenHandler.current != null)
                ScreenHandler.current.RefreshPanel();
        }
        if (Time.time - T > 15)
        {
            Resources.UnloadUnusedAssets();
            T = Time.time;
        }
		if (Input.GetKeyDown (KeyCode.B)) 
		{
			Debug.Log (transferCash);
		}
    }

    public void NewGame()
    {
        GamePlaySession.instance = new GamePlaySession();
		GamePlaySession.instance.InitializeSession();
        GamePlaySession.instance.GSI = GetComponent<GameSetupItems>();
		GamePlaySession.instance.MakeCalendar ();
//         Add any code needed to flag the player as beginning a new game!
        if (ScreenHandler.current != null && DisclaimerScreen != null)
            ScreenHandler.current.OpenPanel(DisclaimerScreen);
    }

    public void NewGamePlus()
    {
        // Add any code needed to flag the player as beginning a new game+!
        GamePlaySession newGamePlus = new GamePlaySession();
		newGamePlus.InitializeSession();
		newGamePlus.MakeCalendar ();
    
		newGamePlus.SetIsNewPlayer(false);

		//newGamePlus.SetMyCash (10000);
        // Copy over cards, cash, and tokens
        newGamePlus.myCards = GamePlaySession.instance.myCards;
		newGamePlus.SetMyCash(Mathf.Max(GamePlaySession.instance.GetMyCash(), 0));
		newGamePlus.SetMyTokens(GamePlaySession.instance.GetMyTokens());

        // Ensure the store doesn't reset itself
		newGamePlus.SetFirstShopCards(true);
		if (transferCash > -1)
			newGamePlus.SetFirstShopCards(false);
		
        newGamePlus.storeCards = GamePlaySession.instance.storeCards;
		newGamePlus.SetStoreCardsBought(GamePlaySession.instance.GetStoreCardsBought());
        newGamePlus.storePurchases = GamePlaySession.instance.storePurchases;
		newGamePlus.SetLastShopRefresh(GamePlaySession.instance.GetLastShopRefresh());
		newGamePlus.tagChampCardNumbers = GamePlaySession.instance.tagChampCardNumbers;
		newGamePlus.SetTitleChampCardNumber(GamePlaySession.instance.GetTitleChampCardNumber());
		newGamePlus.SetWorldChampCardNumber(GamePlaySession.instance.GetWorldChampCardNumber());
		newGamePlus.tagChampLastCardNumbers = GamePlaySession.instance.tagChampLastCardNumbers;
		newGamePlus.SetTitleChampLastCardNumber(GamePlaySession.instance.GetTitleChampLastCardNumber());
		newGamePlus.SetWorldChampLastCardNumber(GamePlaySession.instance.GetWorldChampLastCardNumber());
        // Set as a new game plus playthrough
		newGamePlus.SetNewGamePlus(true);

		//StartCoroutine (DeleteMyShowDataOnParse ());
		//DeleteShowDataParse();
        GamePlaySession.instance = newGamePlus;

		//StartCoroutine (DeleteShowData ());

        GamePlaySession.instance.GSI = GetComponent<GameSetupItems>();
        if (ScreenHandler.current != null && DisclaimerScreen != null)
            ScreenHandler.current.OpenPanel(DisclaimerScreen);
    }
	bool removeSegmentData = false;
	IEnumerator DeleteShowData()
	{
		yield return new WaitForSeconds (7.5f);
		ParseManager.RemoveShowData (ShowDataRemoved);
		yield return new WaitUntil (() => removeSegmentData == true);
		ParseManager.RemoveSegmentData ();
	}

	public void ShowDataRemoved(bool success)
	{
		Debug.Log ("HERE");
		removeSegmentData = true;
	}

	public void DeleteShowDataParse()
	{
		ParseManager.RemoveShowData ();
	}

	public void DeleteSegmentDataParse()
	{
		ParseManager.RemoveSegmentData ();
	}

	IEnumerator SegmentDataRemover()
	{
		yield return new WaitUntil (() => MainCalendarScreen.gameObject.activeSelf == true);
		ParseManager.RemoveSegmentData ();
	}


    public void ContinueGame()
    {
        // Add any code needed to flag the player as continuing an existing game!
        //Update the HUD and turn it on where appropriate
		if (GamePlaySession.instance == null)
			Debug.Log ("NULL SESSION");
		
		if (GamePlaySession.instance.GetSelectedFederation() == -1)
        {
            PlayerHUD.SetActive(true);
            if (ScreenHandler.current != null && FedScreen != null)
                ScreenHandler.current.OpenPanel(FedScreen);
            return;
        }

		if (GamePlaySession.instance.GetSelectedComissioner() == -1)
        {
            PlayerHUD.SetActive(true);
            if (ScreenHandler.current != null && CommishSelectScreen != null)
                ScreenHandler.current.OpenPanel(CommishSelectScreen);
            return;
        }

        //StartMyFed();

        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
    }

    public void StartGame()
    {
        //Debug.Log(GamePlaySession.instance.isNewPlayer);
        //Debug.Log(GamePlaySession.instance.newGamePlus);
		if (GamePlaySession.instance.GetIsNewPlayer() && !GamePlaySession.instance.GetNewGamePlus())
        {

            // If new player, head to the welcome screen and begin the tutorial!
            if (ScreenHandler.current != null && WelcomeScreen != null)
                ScreenHandler.current.OpenPanel(WelcomeScreen);

			GamePlaySession.instance.SetMyCash(10000);
			GamePlaySession.instance.SetMyTokens(1);



			GamePlaySession.instance.SetIsNewPlayer(false);
        }

        

		else if (!GamePlaySession.instance.GetIsNewPlayer() && GamePlaySession.instance.GetNewGamePlus())
		{
			Debug.Log ("Here");
			UIManagerScript.instance.UpdateTheme (false);
			GamePlaySession.instance.SetInTutorialMode(true);
			GamePlaySession.instance.SetFlavorTutorialClear(false);
			GamePlaySession.instance.SetResultTutorialClear(false);
			GamePlaySession.instance.SetCalendarTutorialClear(false);
            //GamePlaySession.instance.storePurchaseTutorialClear = false;
            //GamePlaySession.instance.storeRefreshTutorialClear = false;
            //GamePlaySession.instance.storeTutorialClear = false;
			GamePlaySession.instance.SetTransitionMessageClear(false);
			GamePlaySession.instance.SetSelectedFederation(-1);
			GamePlaySession.instance.SetSelectedComissioner(-1);

            PlayerHUD.SetActive(true);
            if (ScreenHandler.current != null && FedScreen != null)
            {
                ScreenHandler.current.OpenPanel(FedScreen);
                FedScreen.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            }

			GamePlaySession.instance.SetIsNewPlayer(false);
        }

        else
        {
			PlayerHUD.SetActive(true);
            if (ScreenHandler.current != null && FedScreen != null)
            {
                ScreenHandler.current.OpenPanel(FedScreen);
                FedScreen.transform.GetChild(1).gameObject.SetActive(true);
                //Debug.Log(FedScreen.transform.GetChild(1).gameObject.name);
            }
        }

		if (transferCash > -1) 
		{
			GamePlaySession.instance.SetMyCash(transferCash);
		}
		if (transferTokens > -1) 
		{
			GamePlaySession.instance.SetMyTokens(transferTokens);
		}

        PlayerHUD.GetComponent<HUDScript>().SetFedImage();
        PlayerHUD.GetComponent<HUDScript>().SetCommishImage();
        PlayerHUD.GetComponent<HUDScript>().UpdateHUD();
		var panels = TitleScreen.gameObject.transform.parent.GetComponentsInChildren<ThemeController> (true);
		foreach (var item in panels) 
		{
			item.CheckTheme ();
		}
    }

    public void DisclaimerOKCallback()
    {

		if (GamePlaySession.instance.GetNewGamePlus() && transferCash == -1) 
		{
			DisclaimerScreen.gameObject.transform.GetChild (1).gameObject.SetActive (true);
		} 
		else if (GamePlaySession.instance.GetNewGamePlus() && transferCash > -1) 
		{
			OpenYearSelect();
		} 
		else 
		{
			StartGame(); 
		}
    }

    public void NewGamePlusNoTut()
    {
		GamePlaySession.instance.SetIsNewPlayer(true);
		GamePlaySession.instance.SetInTutorialMode(false);
        StartGame();
    }

    public void ShowHUD()
    {
        PlayerHUD.SetActive(true);
        UpdateHUD();
    }

    public void UpdateHUD()
    {
        PlayerHUD.BroadcastMessage("ShowMyCash", SendMessageOptions.DontRequireReceiver);
        PlayerHUD.BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
    }

    public void VerifyFed()
    {
        if (ScreenHandler.current != null && FedScreenVerify != null)
            ScreenHandler.current.OpenPanel(FedScreenVerify);
    }

    public void VerifyCommish()
    {
        if (ScreenHandler.current != null && CommishScreenVerify != null)
            ScreenHandler.current.OpenPanel(CommishScreenVerify);
    }

    public void StartMyFed()
    {
        if (MyFedScreen == null)
            return;

        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
    }

    public void GoToMyFed()
    {
        // Should do a check to see if we can go to MyFed screen from current place in game...
        Animator currentPanel = null;
        if (ScreenHandler.current != null)
            currentPanel = ScreenHandler.current.GetCurrentPanel();

        if (currentPanel != MainCalendarScreen)
            return;

        if (MyFedScreen == null)
            return;
        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
        //StartCoroutine(SaveGamePlaySession());
        //OnSaveAll();
    }

    public void HomeButton()
    {
        if (MyFedScreen == null)
            return;
        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
        //StartCoroutine(SaveGamePlaySession());
        //OnSaveAll();
    }

    public void GoToCalendar(bool UpdateRatings)
    {
        if (MainCalendarScreen == null)
            return;
        CalendarScreen calScreen = MainCalendarScreen.GetComponent<CalendarScreen>();

        calScreen.OpenCalendar(UpdateRatings);

		if (!GamePlaySession.instance.GetCalendarTutorialClear() && GamePlaySession.instance.GetInTutorialMode())
        {
			GamePlaySession.instance.SetCalendarTutorialClear(true);

            PopUpWindow popUpWindow = GameObject.Find("UIManager").GetComponent<UIManagerScript>().popUpWindow;

            if (popUpWindow != null)
            {
                popUpWindow.OpenPopUpWindow(new PopUpMessage[] { calScreen.initialPopUp }, MainCalendarScreen);
            }
        }
		//OnSaveAll();
		GameSaveController.instance.UploadUserData ();

        //StartCoroutine(SaveGamePlaySession());
        
    }

    public void GoToShop()
    {
        if (GamePlaySession.instance == null)
            return;

        if (ShopScreen != null)
        {

            StorePage storePage = ShopScreen.gameObject.GetComponent<StorePage>();

            if (storePage != null)
            {
                //Debug.Log("Refreshing Card Display..");
                storePage.RefreshCardDisplay();
            }


            if (ScreenHandler.current != null)
            {
                PlayerHUD.SetActive(false);
                ScreenHandler.current.OpenPanel(ShopScreen);
            }
            //StartCoroutine(SaveGamePlaySession());
            //OnSaveAll();
        }
    }

	public bool sendingDataToParse;
	public bool success;
	public bool uploadSuccess;
	IEnumerator SendingDataToParse()
	{
		yield return new WaitForSeconds (3f);
		PlayerPrefs.SetString ("parseUserID", Parse.ParseUser.CurrentUser.ObjectId);
		uploadSuccess = false;
		ParseManager.PrepareAndSendAllUserData ();
		yield return new WaitUntil (()=>sendingDataToParse == false);
		string message = "";
		if (uploadSuccess) 
		{
			message = "Data Uploaded Successfully";
			PlayerPrefs.SetInt ("allOnParse", 1); 
		} 
		else 
		{
			message = "Data backup failed.";
		}
		TitleScreen.gameObject.transform.Find ("PleaseWaitOverlay").gameObject.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = message.ToString();
		yield return new WaitForSeconds (5f);
		TitleScreen.gameObject.transform.Find ("PleaseWaitOverlay").gameObject.SetActive (false);
		if (GamePlaySession.instance.selectedFederation != -1 && transferCash<=-1) 
		{
			TitleScreen.gameObject.GetComponent<TitleScreen> ().ContinueButton.GetComponent<Button> ().interactable = true;
		}
		OnSaveAll ();
		PlayerPrefs.SetInt ("onParse", 1);
		onParse = 1;
	}
		
	IEnumerator FetchingDataFromParse()
	{
		success = false;
		yield return new WaitForSeconds (5f);
		LoadFromParse ();
		PlayerPrefs.SetString ("parseUserID", Parse.ParseUser.CurrentUser.ObjectId);
		yield return new WaitUntil (()=>sendingDataToParse == false);
		if (success) 
		{
			TitleScreen.gameObject.transform.Find ("PleaseWaitOverlay").gameObject.transform.GetChild (0).GetChild (0).gameObject.GetComponent<Text> ().text = "Data Fetched Successfully";
		} 
		else 
		{
			TitleScreen.gameObject.transform.Find ("PleaseWaitOverlay").gameObject.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "Data Fetching Failed.";
		}

		yield return new WaitForSeconds (3f);
		TitleScreen.gameObject.transform.Find ("PleaseWaitOverlay").gameObject.SetActive (false);
		OnSaveAll ();
		PlayerPrefs.SetString ("parseUserID", Parse.ParseUser.CurrentUser.ObjectId);
	}

	public void DataOnParse()
	{
		StartCoroutine (DataUploadedToParse ());
	}

	IEnumerator DataUploadedToParse()
	{
		string url = "http://bugdevstudios.com/wrestling/server.php?REQUEST=REGISTER&ACCESS_TOKEN=NA&EMAIL=" +PlayerPrefs.GetString("myEmail")+"&PASSWORD="+PlayerPrefs.GetString("myPassword")+"&ROLL="+UnityEngine.Random.Range(1,1000);
		WWW www = new WWW (url);
		yield return www;
		url = "http://bugdevstudios.com/wrestling/server.php?REQUEST=UPDATE_DATA_ON_PARSE&EMAIL=" +PlayerPrefs.GetString("myEmail")+"&PASSWORD="+PlayerPrefs.GetString("myPassword")+"&DATA_ON_PARSE=1";
		WWW www2 = new WWW (url);
		yield return www2;
		PlayerPrefs.SetInt ("onParse", 1);
		onParse = 1;
	}

	public void GoToTitle(bool transferData = false, bool loadData = false)
    {
        if (ScreenHandler.current != null)
        {
            PlayerHUD.SetActive(false);
            ScreenHandler.current.OpenPanel(TitleScreen);
        }
		if (transferData) 
		{
			TitleScreen.gameObject.transform.Find ("PleaseWaitOverlay").gameObject.SetActive (true);
			sendingDataToParse = true;
			StartCoroutine (Blink ());
			StartCoroutine (SendingDataToParse ());
		} 
		else if (loadData && transferCash==-1) 
		{
			TitleScreen.gameObject.transform.Find ("PleaseWaitOverlay").gameObject.SetActive (true);
			GameObject go = TitleScreen.gameObject.transform.Find ("PleaseWaitOverlay").gameObject.transform.GetChild (0).GetChild (0).gameObject;
			go.GetComponent<Text>().text = "Fetching Game Data, Please wait";
			sendingDataToParse = true;
			StartCoroutine (Blink ());
			StartCoroutine (FetchingDataFromParse ());
		}
    }

	IEnumerator Blink()
	{
		GameObject go = TitleScreen.gameObject.transform.Find ("PleaseWaitOverlay").gameObject.transform.GetChild (0).GetChild (0).gameObject;
		while (sendingDataToParse) 
		{
			yield return new WaitForSeconds (0.75f);
			go.SetActive (!go.activeSelf);
		}
		go.SetActive (true);
	}


	public void SendToParse()
	{
		ParseManager.PrepareAndSendAllUserData ();
	}

    public void GoToCardView(bool filter = false)
    {
        if (cardViewer == null)
            return;
        cardViewer.OpenCardView();
        
        //StartCoroutine(SaveGamePlaySession());
    }

	public IEnumerator Start()
    {
		//DISABLING DEBUG LOGGING
		//Debug.logger.logEnabled = false;
		yield return new WaitForSeconds (0.5f);
		myEmail = PlayerPrefs.GetString ("myEmail", "");
		myPassword = PlayerPrefs.GetString ("myPassword", "");

        CloseAllPanels();
    }

    public void OnSaveAll()
    {
		if (String.IsNullOrEmpty(Parse.ParseUser.CurrentUser.ObjectId)) 
		{
			myEmail = PlayerPrefs.GetString ("myEmail", "");
			myPassword = PlayerPrefs.GetString ("myPassword", "");
			ParseManager.LogInUser (myEmail, myPassword, LoginCallback);
		}
		else if (!String.IsNullOrEmpty(Parse.ParseUser.CurrentUser.ObjectId))
		{
			if (PlayerPrefs.GetInt ("allOnParse", 0) == 0) 
			{
				sendingDataToParse = true;
				StartCoroutine (WaitForParse ());
				ParseManager.PrepareAndSendAllUserData ();
			}
		}

       GamePlaySession.instance.SaveAll(true,true);
    }

	IEnumerator WaitForParse()
	{
		yield return new WaitUntil (() => sendingDataToParse == false);
		if (uploadSuccess == true) 
		{
			PlayerPrefs.SetInt ("allOnParse", 1);
		}
	}

	void LoginCallback(bool success, string exceptionMessage)
	{
		if (success) 
		{
			StartCoroutine (SaveGameDelayed());
		} 
		else 
		{
			ParseManager.RegisterUser (myEmail, myPassword,RegisterCallback);
		}
	}

	void RegisterCallback(bool success, string email,string password, string exceptionMessage)
	{
		if (success) 
		{
			StartCoroutine (SaveGameDelayed());
		} 
		else 
		{
			
		}
	}
		
	IEnumerator SaveGameDelayed()
	{
		yield return new WaitForSeconds (1f);
		OnSaveAll ();
	}

    public void OpenYearSelect()
    {
		//if (GamePlaySession.instance.newGamePlus && transferCash > -1) 
		//{
		//	SelectYear (StartYearSelect.gameObject.transform.GetChild (0).GetChild (2).gameObject);
		//	return;
		//} 
		//PlayerHUD.SetActive(true);
        if (ScreenHandler.current != null && StartYearSelect != null)
            ScreenHandler.current.OpenPanel(StartYearSelect);
    }

    public void SelectYear(GameObject SelectedYear)
    {
        //SelectedYear = EventSystem.current.currentSelectedGameObject;
		for (int i = 0; i < GamePlaySession.instance.GetNumberOfYears(); i++)
        {

            if (SelectedYear.name == "Year1980")
            {
                GamePlaySession.instance.myCalendar.years[i].yearName = (1980 + i).ToString();
				UIManagerScript.instance.UpdateTheme (false);
            }
            else if (SelectedYear.name == "Year1990")
            {
                if (i < 10)
                {
                    GamePlaySession.instance.myCalendar.years[i].yearName = (1990 + i).ToString();
                }
                else if (i >= 10)
                {
                    GamePlaySession.instance.myCalendar.years[i].yearName = (1980 + i).ToString();
                }
				UIManagerScript.instance.UpdateTheme (true);
            }
            else if (SelectedYear.name == "Year1985")
            {
                if (i < 15)
                {
                    GamePlaySession.instance.myCalendar.years[i].yearName = (1985 + i).ToString();
                }
                else if (i >= 15)
                {
                    GamePlaySession.instance.myCalendar.years[i].yearName = (1985 + i - 20).ToString();
                }
				UIManagerScript.instance.UpdateTheme (false);
            }


			var panels = TitleScreen.gameObject.transform.parent.GetComponentsInChildren<ThemeController> (true);
			foreach (var item in panels) 
			{
				item.CheckTheme ();
			}
        }

		if (GamePlaySession.instance.GetNewGamePlus())
        {
			NewGamePlusNoTut();
        }
        else
        {
			StartGame();
        }
    }

    public void Close(GameObject go)
    {
        go.SetActive(false);
    }

    void CloseAllPanels()
    {
        if (TitleScreen != null)
            TitleScreen.gameObject.SetActive(false);
        if (LoginScreen != null)
            LoginScreen.gameObject.SetActive(false);
        if (DisclaimerScreen != null)
            DisclaimerScreen.gameObject.SetActive(false);
        if (WelcomeScreen != null)
            WelcomeScreen.gameObject.SetActive(false);
        if (FedScreen != null)
            FedScreen.gameObject.SetActive(false);
        if (FedScreenVerify != null)
            FedScreenVerify.gameObject.SetActive(false);
        if (MyFedScreen != null)
            MyFedScreen.gameObject.SetActive(false);
        if (CommishSelectScreen != null)
            CommishSelectScreen.gameObject.SetActive(false);
        if (CommishScreenVerify != null)
            CommishScreenVerify.gameObject.SetActive(false);
        if (MainCalendarScreen != null)
            MainCalendarScreen.gameObject.SetActive(false);
        if (NowYouKnowScreen != null)
            NowYouKnowScreen.gameObject.SetActive(false);
        if (ShowSetupScreen != null)
            ShowSetupScreen.gameObject.SetActive(false);
        if (MatchSetupScreen != null)
            MatchSetupScreen.gameObject.SetActive(false);
        if (TagMatchSetupScreen != null)
            TagMatchSetupScreen.gameObject.SetActive(false);
        if (MicSpotSetupScreen != null)
            MicSpotSetupScreen.gameObject.SetActive(false);
        if (SkitSetupScreen != null)
            SkitSetupScreen.gameObject.SetActive(false);
        if (ShowTransitionScreen != null)
            ShowTransitionScreen.gameObject.SetActive(false);
        if (ShowResultsScreen != null)
            ShowResultsScreen.gameObject.SetActive(false);
        if (ShopScreen != null)
            ShopScreen.gameObject.SetActive(false);
        if (HelpScreen != null)
            HelpScreen.gameObject.SetActive(false);
        if (CreditsScreen != null)
            CreditsScreen.gameObject.SetActive(false);
    }



    public void OpenStatsSelect()
    {
        if (ScreenHandler.current != null && StatsSelectScreen != null)
            ScreenHandler.current.OpenPanel(StatsSelectScreen);

        if (PlayerHUD != null && PlayerHUD.activeSelf == true)
        {
            PlayerHUD.SetActive(false);
        }
    }

    public void OpenCheckListScreen()
    {
        if (ScreenHandler.current != null && CheckListScreen != null)
            ScreenHandler.current.OpenPanel(CheckListScreen);

        if (PlayerHUD != null && PlayerHUD.activeSelf == true)
        {
            PlayerHUD.SetActive(false);
        }
        ActiveCheckListType = CardType.WRESTLER;
        PopulateCheckListScreen(CardType.WRESTLER, true);
		UpdateCollectionData ();
    }

    public void PopulateCheckListScreen(CardType CardType, bool Ascending)
    {
        GameObject CheckListFigures = GameObject.Find("CheckListFigures");
        CheckListFigures.transform.localPosition = new Vector3(CheckListFigures.transform.position.x, 0, CheckListFigures.transform.position.z);
        string activeChecklist = ActiveCheckListType.ToString();

        activeChecklist = activeChecklist.Replace('_', ' ');

        CheckListActiveType.transform.GetChild(0).gameObject.GetComponent<Text>().text = activeChecklist;

        if (CheckListFigures.transform.childCount == 0)
        {
            CheckListLines = new List<GameObject>();
            for (int i = 0; i < 170; i++)
            {
                GameObject CheckListLine = Instantiate(CheckListLinePrefab, CheckListFigures.transform);
                CheckListLine.transform.localScale = new Vector3(1, 1, 1);
                CheckListLines.Add(CheckListLine);
            }
        }

        foreach (GameObject item in CheckListLines)
        {
            foreach (Transform item2 in item.transform)
            {
                if (item2.gameObject.GetComponent<Text>() != null)
                {
                    item2.gameObject.GetComponent<Text>().text = "";
                }
                else if (item2.gameObject.GetComponent<Text>() == null && item2.gameObject.name == "CheckBox")
                {
                    item2.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0);
                }
            }
        }

        int CardNumber = 1;

        List<CardObject> CardsList = new List<CardObject>();
        bool Wrestler = false;


        if ((CardType == CardType.ALL) || (CardType == CardType.SHOP_COMMON) || (CardType == CardType.SHOP_RARE) || (CardType == CardType.SHOP_UNCOMMON))
        {
            List<CardObject> Values = new List<CardObject>();

            for (int i = 0; i < GameSetupItems.instance.wrestlers.cardArray.Length; i++)
            {
                WrestlerCardObject w = (WrestlerCardObject)GameSetupItems.instance.wrestlers.cardArray[i];
                if (w.cardVersion != "FOIL")
                {
                    Values.Add(GameSetupItems.instance.wrestlers.cardArray[i]);
                }
            }
            for (int i = 0; i < GameSetupItems.instance.venues.cardArray.Length; i++)
            {
                Values.Add(GameSetupItems.instance.venues.cardArray[i]);
            }
            for (int i = 0; i < GameSetupItems.instance.flavors.cardArray.Length; i++)
            {
                Values.Add(GameSetupItems.instance.flavors.cardArray[i]);
            }
            for (int i = 0; i < GameSetupItems.instance.matches.cardArray.Length; i++)
            {
                Values.Add(GameSetupItems.instance.matches.cardArray[i]);
            }
            for (int i = 0; i < GameSetupItems.instance.micSpots.cardArray.Length; i++)
            {
                Values.Add(GameSetupItems.instance.micSpots.cardArray[i]);
            }
            for (int i = 0; i < GameSetupItems.instance.skits.cardArray.Length; i++)
            {
                Values.Add(GameSetupItems.instance.skits.cardArray[i]);
            }
            if (!Ascending)
            {
                Values.Reverse();
            }

            foreach (CardObject item in Values)
            {
                {
                    bool found = false;
                    foreach (CardObject item4 in GamePlaySession.instance.myCards)
                    {
                        if (item4.CardNumber == item.CardNumber)
                        {
                            found = true;
                        }
                    }
                    foreach (Transform item3 in CheckListLines[CardNumber - 1].transform)
                    {
                        if (item3.name == "CardNumber")
                        {
                            if (!Ascending)
                            {
                                item3.gameObject.GetComponent<Text>().text = ((Values.Count + 1 - CardNumber)).ToString();
                            }
                            else
                            {
                                item3.gameObject.GetComponent<Text>().text = CardNumber.ToString();
                            }

                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.black;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.white;
                            }
                        }
                        else if (item3.name == "CardName")
                        {
                            if (item.myCardType == CardType.FLAVOR || item.myCardType == CardType.FEUD || item.myCardType == CardType.MANAGER || item.myCardType == CardType.SPONSOR || item.myCardType == CardType.TAG_TEAM || item.myCardType == CardType.MERCH)
                            {
                                FlavorCardObject flavor = (FlavorCardObject)item;
                                if (flavor.cardClass == "Merchandise")
                                {
                                    item3.gameObject.GetComponent<Text>().text = flavor.name;
                                    //string temp = Regex.Replace(flavor.name.ToString(), "[^0-9]", string.Empty, RegexOptions.IgnoreCase).ToString();
                                    //int k = int.Parse(temp);
                                    //WrestlerCardObject w = (WrestlerCardObject)GameSetupItems.instance.wrestlers.cards[k];
                                    //item3.gameObject.GetComponent<Text>().text = (GameSetupItems.instance.wrestlers.cards[k].name.ToString() + " 's Merch").ToString();
                                }
                                else
                                {
                                    item3.gameObject.GetComponent<Text>().text = item.name.ToString();
                                }

                            }
                            else
                            {
                                item3.gameObject.GetComponent<Text>().text = item.name.ToString();
                            }

                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.black;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.white;
                            }
                        }
                        else if (item3.name == "Rarity")
                        {
                            if (item.myCardType == CardType.WRESTLER)
                            {
                                WrestlerCardObject w = (WrestlerCardObject)item;
                                item3.gameObject.GetComponent<Text>().text = w.rarity.ToString();
                            }
                            else
                            {
                                item3.gameObject.GetComponent<Text>().text = "";
                            }

                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.black;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.white;
                            }
                        }
                        else if (item3.name == "Type")
                        {
                            if (item.myCardType != CardType.FLAVOR)
                            {
                                item3.gameObject.GetComponent<Text>().text = item.myCardType.ToString();
                            }
                            else if (item.myCardType == CardType.FLAVOR)
                            {
                                FlavorCardObject flav = (FlavorCardObject)item;
                                item3.gameObject.GetComponent<Text>().text = flav.cardClass.ToUpper().ToString();
                            }
                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.black;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.white;
                            }
                        }
                        else if (item3.name == "CheckBox")
                        {
                            item3.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Image>().sprite = CheckedCheckBox;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Image>().sprite = UnCheckedCheckBox;
                            }
                        }
                    }
                }
                CardNumber++;
            }
        }
        else if (CardType == CardType.WRESTLER)
        {
            List<CardObject> Values = new List<CardObject>();

            for (int i = 0; i < GameSetupItems.instance.wrestlers.cardArray.Length; i++)
            {
                Values.Add(GameSetupItems.instance.wrestlers.cardArray[i]);
            }
            if (!Ascending)
            {
                Values.Reverse();
            }

            for (int i = 0; i < 170; i++)
            {
                CheckListFigures.transform.GetChild(i).gameObject.SetActive(true);
            }

            foreach (CardObject item in Values)
            {
                if (item.myCardType == CardType.WRESTLER)
                {
                    Wrestler = true;
                    WrestlerCardObject wrestler = (WrestlerCardObject)item;
                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                    {
                        bool found = false;
                        foreach (CardObject item4 in GamePlaySession.instance.myCards)
                        {
                            if (item4.CardNumber == item.CardNumber)
                            {
                                found = true;
                            }
                        }
                        foreach (Transform item3 in CheckListLines[CardNumber - 1].transform)
                        {
                            if (item3.name == "CardNumber")
                            {
                                if (!Ascending)
                                {
                                    item3.gameObject.GetComponent<Text>().text = (((Values.Count / 2) + 1) - CardNumber + 1).ToString();
                                }
                                else
                                {
                                    item3.gameObject.GetComponent<Text>().text = CardNumber.ToString();
                                }
                                if (found == true)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.black;
                                }
                                else if (found == false)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.white;
                                }
                            }
                            else if (item3.name == "CardName")
                            {
                                item3.gameObject.GetComponent<Text>().text = wrestler.name.ToString();
                                if (found == true)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.black;
                                }
                                else if (found == false)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.white;
                                }
                            }
                            else if (item3.name == "Rarity")
                            {
                                item3.gameObject.GetComponent<Text>().text = wrestler.rarity.ToString();
                                if (found == true)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.black;
                                }
                                else if (found == false)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.white;
                                }
                            }
                            else if (item3.name == "Type")
                            {
                                item3.gameObject.GetComponent<Text>().text = "WRESTLER";
                                if (found == true)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.black;
                                }
                                else if (found == false)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.white;
                                }
                            }
                            else if (item3.name == "CheckBox")
                            {
                                item3.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                                if (found == true)
                                {
                                    item3.gameObject.GetComponent<Image>().sprite = CheckedCheckBox;
                                }
                                else if (found == false)
                                {
                                    item3.gameObject.GetComponent<Image>().sprite = UnCheckedCheckBox;
                                }
                            }
                        }
                        CardNumber++;
                    }
                }
            }
        }

        else if ((CardType == CardType.FEUD) || (CardType == CardType.SPONSOR) || (CardType == CardType.TAG_TEAM) || (CardType == CardType.MANAGER) || (CardType == CardType.MERCH))
        {
            foreach (CardObject item in GameSetupItems.instance.flavors.cards.Values)
            {
                FlavorCardObject flavor = (FlavorCardObject)item;
				if ((CardType == CardType.FEUD && flavor.cardClass == "Feud") || (CardType == CardType.SPONSOR && flavor.cardClass == "Sponsor") || (CardType == CardType.TAG_TEAM && flavor.cardClass == "Tag Team") || (CardType == CardType.MANAGER && flavor.cardClass == "Manager") || (CardType == CardType.MERCH && flavor.cardClass == "Merchandise"))
                {
					CardsList.Add(item);
                }
            }
        }
        else if ((CardType == CardType.FLAVOR))
        {
            foreach (CardObject item in GameSetupItems.instance.flavors.cards.Values)
            {
                FlavorCardObject flavor = (FlavorCardObject)item;
                if ((flavor.cardClass != "Merchandise") && (flavor.cardClass != "Manager") && (flavor.cardClass != "Sponsor") && (flavor.cardClass != "Feud") && (flavor.cardClass != "Tag Team"))
                {
                    CardsList.Add(item);
                }
            }
        }
        else if (CardType == CardType.MATCH)
        {
            foreach (CardObject item in GameSetupItems.instance.matches.cards.Values)
            {
                CardsList.Add(item);
            }
        }
        else if (CardType == CardType.MIC_SPOT)
        {
            foreach (CardObject item in GameSetupItems.instance.micSpots.cards.Values)
            {
                CardsList.Add(item);
            }
        }
        else if (CardType == CardType.SKIT)
        {
            foreach (CardObject item in GameSetupItems.instance.skits.cards.Values)
            {
                CardsList.Add(item);
            }
        }
        else if (CardType == CardType.VENUE)
        {
            foreach (CardObject item in GameSetupItems.instance.venues.cards.Values)
            {
                CardsList.Add(item);
            }
        }
        else
        {
            Debug.Log("Invalid Card type selected");
        }

        if (Wrestler == false && CardType != CardType.ALL && CardType != CardType.WRESTLER)
        {
            List<CardObject> Values = new List<CardObject>();

            for (int i = 0; i < CardsList.Count; i++)
            {
                Values.Add(CardsList[i]);
            }
            if (!Ascending)
            {
                Values.Reverse();
            }

			int o = 0;
			foreach (Transform item in CheckListFigures.transform) 
			{
				if (o < Values.Count) {
					CheckListFigures.transform.GetChild (o).gameObject.SetActive (true);
				} 
				else 
				{
					CheckListFigures.transform.GetChild(o).gameObject.SetActive(false);
				}
				o++;	
			}

            foreach (CardObject item in Values)
            {
                bool found = false;
                foreach (CardObject itemx in GamePlaySession.instance.myCards)
                {
                    if (itemx.CardNumber == item.CardNumber)
                    {
                        found = true;
                    }
                    foreach (Transform item3 in CheckListLines[CardNumber - 1].transform)
                    {
                        if (item3.name == "CardNumber")
                        {
                            if (!Ascending)
                            {
                                item3.gameObject.GetComponent<Text>().text = ((Values.Count) - CardNumber + 1).ToString();
                            }
                            else
                            {
                                item3.gameObject.GetComponent<Text>().text = CardNumber.ToString();
                            }
                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.black;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.white;
                            }
                        }
                        else if (item3.name == "CardName")
                        {
                            if (item.myCardType == CardType.FLAVOR || item.myCardType == CardType.FEUD || item.myCardType == CardType.MANAGER || item.myCardType == CardType.SPONSOR || item.myCardType == CardType.TAG_TEAM || item.myCardType == CardType.MERCH)
                            {
                                FlavorCardObject flavor = (FlavorCardObject)item;
                                if (flavor.cardClass == "Merchandise")
                                {
                                    item3.gameObject.GetComponent<Text>().text = flavor.name;
                                }
                                else
                                {
                                    item3.gameObject.GetComponent<Text>().text = item.name.ToString();
                                }

                            }
                            else
                            {
                                item3.gameObject.GetComponent<Text>().text = item.name.ToString();
                            }

                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.black;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.white;
                            }
                        }
                        else if (item3.name == "Type")
                        {
                            if (item.myCardType != CardType.FLAVOR)
                            {
                                string type = item.myCardType.ToString();
                                type = type.Replace("_", " ");
                                item3.gameObject.GetComponent<Text>().text = type;
                            }
                            else if (item.myCardType == CardType.FLAVOR)
                            {
                                FlavorCardObject flav = (FlavorCardObject)item;
                                string cardClass = flav.cardClass.ToUpper().ToString();
                                cardClass = cardClass.Replace("_", " ");
                                item3.gameObject.GetComponent<Text>().text = cardClass;
                            }
                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.black;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.white;
                            }
                        }
                        else if (item3.name == "CheckBox")
                        {
                            item3.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Image>().sprite = CheckedCheckBox;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Image>().sprite = UnCheckedCheckBox;
                            }
                        }
                    }
                }
                CardNumber++;
            }
        }
    }

	public void UpdateCollectionData() 
	{
		if (collectionText != null) 
		{
			if (GamePlaySession.instance != null) 
			{
				collectionText.text = "Collection: " + GamePlaySession.instance.myCards.Count.ToString() + "/" + 687.ToString();
				collectionText.transform.parent.gameObject.SetActive(true);
			} 
			else 
			{
				collectionText.transform.parent.gameObject.SetActive(false);
			}
		}
	}

    public void CheckListSwitchType()
    {
        
        ActiveCheckListType += 1;
        if (ActiveCheckListType == CardType.SHOP_COMMON)
        {
            ActiveCheckListType = CardType.WRESTLER;
        }
        else if (ActiveCheckListType == CardType.FLAVOR)
        {
            ActiveCheckListType = CardType.MIC_SPOT;
        }

        PopulateCheckListScreen(ActiveCheckListType, true);
    }

    public void CardNumberFilter()
    {
        PopulateCheckListScreen(ActiveCheckListType, Order);
        Order = !Order;
    }

    List<Button> allButtons = new List<Button>();

    public void SearchButton(Transform searchIn)
    {
        foreach (Transform go in searchIn.transform)
        {
            if (go.childCount > 0)
            {
                SearchButton(go);
            }
            Button btn = go.GetComponent<Button>();
            if (btn != null)
                allButtons.Add(btn);
        }
    }

	public void UpdateTheme(bool ninetys = true)
    {
		if (!ninetys) 
		{
			Background90s.gameObject.SetActive (false);
			Theme90s = false;
			PlayerPrefs.SetInt ("theme", 0);
			gameObject.GetComponent<AudioSource> ().clip = Track80s;
			gameObject.GetComponent<AudioSource> ().Play ();
		} 
		else
		{
			Background90s.gameObject.SetActive(true);
			Theme90s = true;
			PlayerPrefs.SetInt ("theme", 1);
			gameObject.GetComponent<AudioSource> ().clip = Track90s;
			gameObject.GetComponent<AudioSource> ().Play ();
		}
    }
    
	public void UpdateHUDDelayed()
	{
		StartCoroutine (WaitForHUD ());
	}

	public IEnumerator WaitForHUD()
	{
		yield return new WaitUntil (() => PlayerHUD.gameObject.activeSelf == true);
		PlayerHUD.GetComponent<HUDScript> ().SetFedImage ();
		PlayerHUD.GetComponent<HUDScript> ().SetCommishImage ();
		PlayerHUD.GetComponent<HUDScript> ().UpdateHUD ();
	}

	public void Enable(GameObject go)
	{
		go.SetActive (true);
	}
	public void Disable(GameObject go)
	{
		go.SetActive (false);
	}

	public void UploadGameData()
	{
		DataHandler.instance.ScheduleUpload (true);
	}

	public void DownloadGameData()
	{
		DataHandler.instance.DownloadGameDataFromServer ();
	}

//	public void OnApplicationQuit()
//	{
//		GamePlaySession.instance.SaveAll(true, true);
//		string data = myStr;
//		NotificationManager.GetComponent<NotificationManager> ().fromNotification = false;
//		GameSaveController.instance.UploadUserData ();
//	}
//
//	public void OnApplicationPause(bool pause)
//	{
//		GamePlaySession.instance.SaveAll(true, true);
//		string data = myStr;
//		NotificationManager.GetComponent<NotificationManager> ().fromNotification = false;
//		GameSaveController.instance.UploadUserData ();
//	}

	public void SaveGame()
	{
		GamePlaySession.instance.SaveAll(true,true);
	}

	public string GetMyEmail()
	{
		return myEmail;
	}

	public string GetMyPassword()
	{
		return myPassword;
	}

	public GamePlaySession ReadFromBinaryFile()
	{
		string path = "";
		#if UNITY_EDITOR
		path = Application.dataPath + "/userdata.dat";
		#elif UNITY_ANDROID
		path = Application.persistentDataPath + "/userdata.dat";
		#endif

		using (Stream stream = File.Open (path, FileMode.Open)) 
		{
			var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
			GamePlaySession g = (GamePlaySession)binaryFormatter.Deserialize (stream);;
			return g;
		}


	}

	public void CreateOrLoadGamePlaySession(bool onlyLoad = false)
	{
		string path = "";

		#if UNITY_EDITOR
		path = Application.dataPath + "/userdata.dat";
		#elif UNITY_ANDROID || UNITY_IOS
		path = Application.persistentDataPath + "/userdata.dat";
		#endif

		if (File.Exists(path))
		{
			//GamePlaySession.instance = GamePlaySession.Load(PlayerPrefs.GetString("GAMEPLAYSESSION", "NONE"));
			GamePlaySession.instance = ReadFromBinaryFile();
			GamePlaySession.instance.GSI = GetComponent<GameSetupItems>();
			PlayerHUD.GetComponent<HUDScript>().SetFedImage();
			PlayerHUD.GetComponent<HUDScript>().SetCommishImage();
			PlayerHUD.GetComponent<HUDScript>().UpdateHUD();
			GamePlaySession.instance.GSI.GetResourcesReady ();

			foreach (var item in GamePlaySession.instance.myCards) 
			{
				item.PostSerialize ();
			}

			foreach (var item in GamePlaySession.instance.myShows) 
			{
				item.PostDeserialize ();
			}

			try
			{
				GamePlaySession.instance.titleChampion = (WrestlerCardObject)GamePlaySession.instance.CheckCard (GamePlaySession.instance.titleChampCardNumber);
				GamePlaySession.instance.worldChampion = (WrestlerCardObject)GamePlaySession.instance.CheckCard (GamePlaySession.instance.worldChampCardNumber);
				GamePlaySession.instance.tagChampions = new WrestlerCardObject[] {(WrestlerCardObject)GamePlaySession.instance.CheckCard (GamePlaySession.instance.tagChampCardNumbers [0]),
					(WrestlerCardObject)GamePlaySession.instance.CheckCard (GamePlaySession.instance.tagChampCardNumbers [1])
				};
			}
			catch 
			{
				GamePlaySession.instance.titleChampion = (WrestlerCardObject)GamePlaySession.instance.CheckCard (151);
				GamePlaySession.instance.worldChampion = (WrestlerCardObject)GamePlaySession.instance.CheckCard (45);
				GamePlaySession.instance.tagChampions = new WrestlerCardObject[] {(WrestlerCardObject)GamePlaySession.instance.CheckCard (73),
					(WrestlerCardObject)GamePlaySession.instance.CheckCard (25)
				};
			}
		}
		else if (!onlyLoad)
		{
			calendar = new CalendarObject ();
			calendar = calendar.GenerateCalendar ();
			//Debug.Log("Building new Gameplay Session!");
			GamePlaySession.instance = new GamePlaySession();
			GamePlaySession.instance.InitializeSession();
			GamePlaySession.instance.GSI = GetComponent<GameSetupItems>();
			GamePlaySession.instance.GSI.GetResourcesReady ();
		}

	}
	public void UploadDictionary(bool action)
	{
		Debug.Log ("Uploading Dictionary");
		if (wrestlersDictionary.Count != 0) 
		{
			ParseManager.CreateWrestlerCardData (wrestlersDictionary);
		}
	}

	public void LoadFromParse()
	{
		ParseManager.RetrieveGameSession ();
		ParseManager.RetrieveSegmentData ();
		ParseManager.RetrieveShowData ();
		ParseManager.RetrieveWrestlerData ();
	}
	bool rewards = false;
	public void CheckGifts()
	{
		ParseManager.GetUser (CheckGiftsCallback);
		rewards = false;
		StartCoroutine (WaitForRewards ());
	}


	public void CheckGiftsCallback(bool success, ParseUser userTable)
	{
		string giftString = "";

		try
		{
			giftString = userTable.Get<string> ("gifts");
		}

		catch
		{
			return;
		}

		if (String.IsNullOrEmpty (giftString))
			return;

		string[] giftsArray = giftString.Split (new char[]{ '!' });

		totalReward = new RewardObject ();

		foreach (var item in giftsArray) 
		{
			RewardObject reward = ProcessSingleReward (item);
			totalReward.cash += reward.cash;
			totalReward.tokens += reward.tokens;

			foreach (var cardID in reward.cards) 
			{
				if (!totalReward.cards.Contains (cardID)) 
				{
					totalReward.cards.Add (cardID);
				}
			}
		}
		if (totalReward.cash > 0 || totalReward.tokens > 0 || totalReward.cards.Count > 0) 
		{
			rewards = true;
		}
	}

	RewardObject ProcessSingleReward(string rewardString)
	{
		RewardObject rewards = new RewardObject ();

		if (String.IsNullOrEmpty (rewardString))
			return rewards;


		string[] rewardValues = rewardString.Split (new char[]{ '%' });
		int cash = int.Parse (rewardValues [1]);
		int tokens = int.Parse (rewardValues [2]);

		string[] rewardCards = rewardValues [0].Split (new char[]{ '#' });
		foreach (var cardID in rewardCards) 
		{
			rewards.cards.Add (int.Parse (cardID));
		}


		rewards.cash = cash;
		rewards.tokens = tokens;

		return rewards;
	}

	IEnumerator WaitForRewards()
	{
		yield return new WaitUntil (()=> rewards==true);
		StartCoroutine (GiveRewards (totalReward));
	}


	IEnumerator GiveRewards(RewardObject totalReward)
	{
		yield return new WaitUntil (() => ((MyFedScreen.gameObject.activeSelf == true) || (MainCalendarScreen.gameObject.activeSelf == true)));

		PopUpMessage rewardMessage = new PopUpMessage ();
		rewardMessage.gift = new GiftObject ();
		rewardMessage.message = "You have new Gifts!";
		rewardMessage.gift.cash = totalReward.cash;
		rewardMessage.gift.tokens = totalReward.tokens;
		rewardMessage.gift.cardMessage = "Also, you have been gifted the following card(s)";
		rewardMessage.gift.cardIDs = totalReward.cards.ToArray ();
		popUpWindow.OpenPopUpWindow (new PopUpMessage[]{ rewardMessage }, UIManagerScript.instance.MyFedScreen,false,true);
		Parse.ParseUser.CurrentUser.Remove ("gifts");
		rewards = false;
		totalReward = new RewardObject ();
	}


}