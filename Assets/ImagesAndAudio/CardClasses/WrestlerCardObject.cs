﻿using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Linq;

[System.Serializable]
public class WrestlerCardObject : CardObject {

	[XmlElement("CharacterNumber")]
	public int characterNumber;

	[XmlElement("CharacterAllignment")]
	public string cardAlignment;

	[XmlElement("CardVersion")]
	public string cardVersion;

	[XmlElement("CharacterStatus")]
	public string characterStatus;

	[XmlElement("Rarity")]
	public string rarity;

	[XmlElement("WrestlerType")]
	public string wrestlerType;

	[XmlElement("Stats")]
	public Stats stats;

	[XmlElement("SkillCurrent")]
	public int skillCurrent;

	[XmlElement("MicCurrent")]
	public int micCurrent;

	[XmlElement("PopCurrent")]
	public int popCurrent;

	[XmlElement("PushCurrent")]
	public int pushCurrent;

	[XmlElement("CharacterHeight")]
	public string characterHeight;

	[XmlElement("CharacterWeight")]
	public string characterWeight;

//	[XmlElement("CharacterHome")]
	[XmlElement("CharacterHome")]
	public string characterHometown;

	[XmlElement("CharacterFinisher")]
	public string characterFinisher;

	[XmlElement("VictoryLine")]
	public string victoryLine;

    [XmlElement("FirstActiveYear")]
    public int FirstActiveYear;

    [XmlElement("FinalActiveYear")]
    public int LastActiveYear;

    // Use this for initialization
    public WrestlerCardObject() : base()
	{
		myCardType = CardType.WRESTLER;
    }

	public override CardObject Clone()
	{
		myCardType = CardType.WRESTLER;
		byte[] wcoBA = this.ObjectToByteArray(this);
		WrestlerCardObject wco = (WrestlerCardObject)this.ByteArrayToObject(wcoBA);

		wco.skillCurrent = stats.SkillBase;
		wco.micCurrent = stats.MicBase;
		wco.popCurrent = stats.PopBase;
		wco.pushCurrent = stats.Push;

		wco.cardAlignment = cardAlignment;
		if (GamePlaySession.instance != null) 
		{
			foreach(BonusValue bonus in GamePlaySession.instance.myBonuses)
			{
				if(bonus.bType == BonusValue.BonusType.MIC){
					wco.micCurrent += bonus.value;
				}
				else if(bonus.bType == BonusValue.BonusType.POP){
					wco.popCurrent += bonus.value;
				}
				else if(bonus.bType == BonusValue.BonusType.SKILL){
					wco.skillCurrent += bonus.value;
				}
			}
		}

		int cardNum = (CardNumber % 2 == 1)?CardNumber:(CardNumber-1);
        if (CardNumber == 0)
        {
            cardNum = 0;
        }
		//wco.frontImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_wrestler");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
        if (CardNumber == 700)
        {
            cardNum = 700;
        }
        if (wco.cardVersion == "FOIL")
        {
			wco.logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>((CardNumber-1).ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
			wco.headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>((CardNumber-1).ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
			wco.frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>((CardNumber-1).ToString("D3"));
        }
        else if (wco.cardVersion != "FOIL")
        {
			wco.logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
			wco.headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
			wco.frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));
        }

        //wco.WinsCurrent = new int[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        //wco.LossesCurrent = new int[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        // Debug output for tracking down filenames, etc.
        /*Debug.Log("Card: " + wco.name);
		Debug.Log("Front Image = " + "Cards_Front/"+CardNumber.ToString("D3")+"_wrestler("+Regex.Replace(this.name,  @"[^\w\@-]", string.Empty)+")");
		Debug.Log("Logo Image = " + "Cards_Back/"+cardNum.ToString("D3")+"_logo("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
		Debug.Log("Headshot Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_headshot("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
*/
        return wco;
	}


	public override bool IsActiveNow()
	{
		int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName);
		if (FirstActiveYear <= ThisYear && LastActiveYear >= ThisYear) 
		{
			return true;	
		}
		return false;
	}

    public override CardObject CloneWithProgress(CardObject obj)
    {
        myCardType = CardType.WRESTLER;
        byte[] wcoBA = this.ObjectToByteArray(this);
        WrestlerCardObject wco = (WrestlerCardObject)this.ByteArrayToObject(wcoBA);
        WrestlerCardObject current = (WrestlerCardObject)obj;

        wco.skillCurrent = current.skillCurrent;
        wco.micCurrent = current.micCurrent;
        wco.popCurrent = current.popCurrent;
        wco.pushCurrent = current.pushCurrent;
        wco.characterStatus = current.characterStatus;
       
        wco.stats = current.stats;

        int cardNum = (CardNumber % 2 == 1) ? CardNumber : (CardNumber - 1);
        if (CardNumber == 700)
        {
            cardNum = 700;
        }
        //wco.frontImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_wrestler");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
        //wco.logoImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
        //wco.headshotImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
        if (wco.cardVersion == "FOIL")
        {
			wco.logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>((CardNumber - 1).ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
			wco.headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>((CardNumber - 1).ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
			wco.frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>((CardNumber - 1).ToString("D3"));
        }
        else if (wco.cardVersion != "FOIL")
        {
			wco.logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
			wco.headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
			wco.frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));
        }

        //wco.WinsCurrent = current.WinsCurrent;
        //wco.LossesCurrent = current.LossesCurrent;
		return wco;
	}


	public virtual CardObject GenerateWithProgress(int cardNumber, int mic, int pop, int push, int skill)
	{
		WrestlerCardObject wco = (WrestlerCardObject)GamePlaySession.instance.GSI.wrestlers.cards [cardNumber].Clone ();
		wco.micCurrent = mic;
		wco.popCurrent = pop;
		wco.pushCurrent = push;
		wco.skillCurrent = skill;
		wco.characterStatus = GetCharacterStatus (wco);

		int cardNum = (CardNumber % 2 == 1) ? CardNumber : (CardNumber - 1);
		if (CardNumber == 700)
		{
			cardNum = 700;
		}
		//wco.frontImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_wrestler");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
		//wco.logoImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
		//wco.headshotImage = GamePlaySession.instance.GSI.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
		if (wco.cardVersion == "FOIL")
		{
			wco.logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>((CardNumber - 1).ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
			wco.headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>((CardNumber - 1).ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
			wco.frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>((CardNumber - 1).ToString("D3"));
		}
		else if (wco.cardVersion != "FOIL")
		{
			wco.logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
			wco.headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
			wco.frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));
		}

		return wco;

	}

	public override List<int> SerializeReady()
	{
		List<int> theList = new List<int> ();
		theList.Add (CardNumber);
		theList.Add (micCurrent);
		theList.Add (popCurrent);
		theList.Add (pushCurrent);
		theList.Add (skillCurrent);

		return theList;
	}

	public int GetWinLossDataB(int Year,bool win)
	{
		int offset = int.Parse (GamePlaySession.instance.myCalendar.years [0].yearName) - 1980;
		int k = Year - offset;

		if (Year - offset < 0) 
		{
			k = 20 + Year - offset;	
		}
		//Debug.Log (GamePlaySession.instance.myShows.Count);
		if (win) 
		{
			int wins = 0;
			for (int i = 0; i < GamePlaySession.instance.myShows.Count; i++) 
			{
				if (GamePlaySession.instance.myShows [i].showFinished) 
				{
					if ((60 * (k)) <= i && i < (60 * (k + 1))) 
					{
						if (GamePlaySession.instance.myShows[i].isWinner (this)) 
						{
							wins++;
						}
					}
				}
			}
			return wins;
		} 
		else 
		{
			int losses = 0;
			for (int i = 0; i < GamePlaySession.instance.myShows.Count; i++) 
			{
				if (GamePlaySession.instance.myShows [i].showFinished) 
				{
					if ((60 * (k)) <= i && i < (60 * (k + 1))) 
					{
						if (GamePlaySession.instance.myShows[i].isLoser (this)) 
						{
							losses++;
						}
					}
				}
			}
			return losses;
		}
	}

    public bool IncreaseMicRating(int segmentRating){
		int xp = Random.Range(1, 20);

		xp -= Mathf.Max(0 , this.micCurrent - segmentRating);

		if(xp > this.micCurrent){
			this.micCurrent += 1;
			this.micCurrent = Mathf.Min(this.micCurrent, this.stats.MicMax);
			return true;
		}
		return false;

	}

	public bool IncreasePopRating(int segmentRating){
		int xp = Random.Range(1, 20);
		
		xp -= Mathf.Max(0 , this.popCurrent - segmentRating);
		
		if(xp > this.popCurrent){
			this.popCurrent += 1;
			this.popCurrent = Mathf.Min(this.popCurrent, this.stats.PopMax);
			return true;
		}
		return false;
		
	}

	public bool IncreaseSkillRating(int segmentRating){
		int xp = Random.Range(1, 20);
		
		xp -= Mathf.Max(0 , this.skillCurrent - segmentRating);
		
		if(xp > this.skillCurrent){
			this.skillCurrent += 1;
			this.skillCurrent = Mathf.Min(this.skillCurrent, this.stats.SkillMax);
			return true;
		}
		return false;
	}

    public override void PostSerialize()
    {
		if (FirstActiveYear == 0) 
		{
			WrestlerCardObject w = (WrestlerCardObject)UIManagerScript.instance.gsi.wrestlers.cards[CardNumber].Clone();
			FirstActiveYear = w.FirstActiveYear;
			LastActiveYear = w.LastActiveYear;
		}
		int cardNum;
		if (cardVersion == "FOIL") 
		{
			cardNum = CardNumber - 1;
		} 
		else 
		{
			cardNum = CardNumber;
		}

        //int cardNum = (CardNumber % 2 == 1) ? CardNumber : (CardNumber - 1);
        if (CardNumber == 700)
        {
            cardNum = 700;
        }
        //this.frontImage = Resources.Load("Cards_Front/"+CardNumber.ToString("D3")+"_wrestler", typeof(Sprite)) as Sprite;
        //this.logoImage =  Resources.Load("Cards_Back/"+cardNum.ToString("D3")+"_logo", typeof(Sprite)) as Sprite;
        //this.headshotImage = Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_headshot", typeof(Sprite)) as Sprite;
		if (cardVersion == "FOIL") 
		{
			frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite> ((cardNum + 1).ToString ("D3"));	
		} 
		else 
		{
			frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(cardNum.ToString("D3"));	
		}
		// + "_wrestler");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
		logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(cardNum.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
		headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(cardNum.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];

	}
	public string GetCharacterStatus(WrestlerCardObject w)
	{
		string status = "";
		if (w.pushCurrent < 1)
		{
			w.pushCurrent = 1;
		}

		if (w.pushCurrent == 1)
		{
			status = "Jobber";
		}
		else if (w.pushCurrent > 1 && w.pushCurrent < 5)
		{
			status = "Opener";
		}
		else if (w.pushCurrent >= 5 && w.pushCurrent < 10)
		{
			status = "Midcard";
		}
		else if (w.pushCurrent >= 10 && w.pushCurrent < 15)
		{
			status = "Upper Midcard";
		}
		else if (w.pushCurrent >= 15 && w.pushCurrent < 20)
		{
			status = "Main Event";
		}
		else if (w.pushCurrent >= 20)
		{
			status = "Legendary";
		}
		return status;
	}

}

[System.Serializable]
public class Stats{
	[XmlElement("SkillBase")]
	public int SkillBase;

	[XmlElement("SkillMax")]
	public int SkillMax;

	[XmlElement( "MicBase")]
	public int MicBase;

	[XmlElement( "MicMax")]
	public int MicMax;

	[XmlElement("PopBase")]
	public int PopBase;

	[XmlElement("PopMax")]
	public int PopMax;

	[XmlElement("Push")]
	public int Push;

	public Stats(){

	}


}
