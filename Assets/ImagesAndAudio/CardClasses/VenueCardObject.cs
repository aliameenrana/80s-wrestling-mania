﻿using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

[System.Serializable]
public class VenueCardObject : CardObject {
	
	[XmlElement("Year")]
	public int year;
	
	[XmlElement("CostToPlay")]
	public int costToPlay;
	
	[XmlElement("Capacity")]
	public int capacity;
	
	[XmlElement("VenueLimit")]
	public string venueLimit;

	// Use this for initialization
	public VenueCardObject() : base(){
		myCardType = CardType.VENUE;
	}

	public void testfunction(){
		Debug.Log("bull crap");
	}
	public override CardObject Clone(){
		myCardType = CardType.VENUE;
		byte[] wcoBA = this.ObjectToByteArray(this);
		VenueCardObject wco = (VenueCardObject)this.ByteArrayToObject(wcoBA);

		wco.frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_venue");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
		wco.headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];

		// Debug output for tracking down filenames, etc.
		//Debug.Log("Card: " + wco.name);
		//Debug.Log("Front Image = " + "Cards_Front/"+CardNumber.ToString("D3")+"_venue("+Regex.Replace(this.name,  @"[^\w\@-]", string.Empty)+")");
		//Debug.Log("Headshot Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_headshot("+Regex.Replace(this.name,  @"[^\w\@-]", string.Empty)+")");

		//Debug.Log("Cards_Back/"+CardNumber.ToString("D3")+"_headshot("+this.name.Trim( new char[] { ' ', '*', '.' } )+")");
		return (CardObject)wco;
	}
	
	public override int GetPlayCost() {
		return costToPlay;
	}

	public Attendance GetAttendanceResults(){
		Attendance attendance = new Attendance();
		return attendance.GetAttendance(this.costToPlay, this.capacity);
	}
	
	/*	public WrestlerCardObject Clone(){

	}*/

	public override void PostSerialize(){
		frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_venue");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
		headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
	}
	
}

[System.Serializable]
public class Attendance
{

	public float attendanceModifier;
	public string attandanceString;
	public int cashEarnedGross;
	public int venueCost;

	public Attendance GetAttendance(int cost, int maxCap) 
	{
		int diceroll = Random.Range(1, 10);

		if(diceroll == 1){
			this.attendanceModifier = .25f;
			this.attandanceString = "Poor Attendance";
		}
		else if(diceroll == 2 || diceroll == 3){
			this.attendanceModifier = .5f;
			this.attandanceString = "Decent Crowd";
		}
		else if(diceroll >= 4 && diceroll <= 7){
			this.attendanceModifier = .75f;
			this.attandanceString = "Great Crowd";
		}
		else{
			this.attendanceModifier = 1f;
			this.attandanceString = "Sold Out!";
		}

		this.cashEarnedGross = (int)((maxCap / 10) * this.attendanceModifier);
		this.venueCost = cost;

		return this;
	}

	public string GetAttendanceString(float attendanceModifier)
	{
		if (attendanceModifier >= 0f && attendanceModifier <= 0.25f)
			return "Poor Attendance";
		else if (attendanceModifier > 0.25f && attendanceModifier <= 0.50f)
			return "Decent Crowd";
		else if (attendanceModifier > 0.50f && attendanceModifier <= 0.75f)
			return "Great Crowd";
		else if (attendanceModifier > 0.75f && attendanceModifier <= 1.00f)
			return "Sold Out!";
		else
			return "";
	}
}
 