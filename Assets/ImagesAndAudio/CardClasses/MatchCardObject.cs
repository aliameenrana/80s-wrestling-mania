﻿using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

[System.Serializable]
public class MatchCardObject: CardObject{

	[XmlElement("Year")]
	public int yearAcquired;

	[XmlElement("CostToPlay")]
	public int costToPlay;

	[XmlElement("WrestlerType")]
	public string wrestlerType;

	[XmlElement("Bonuses")]
	public Bonuses bonuses;

	[XmlElement("Era")]
	public string Era;

	[XmlElement("ResultsLine")]
	public string ResultsLine;

	// Use this for initialization
	public MatchCardObject() : base(){
		myCardType = CardType.MATCH;
		
	}

	public override CardObject Clone(){
		myCardType = CardType.MATCH;
		byte[] wcoBA = this.ObjectToByteArray(this);
		MatchCardObject wco = (MatchCardObject)this.ByteArrayToObject(wcoBA);

		wco.name = this.name.Trim();
		wco.frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_match");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
		wco.logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
		wco.headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
		
		// Debug output for tracking down filenames, etc.
		//Debug.Log("Card: " + wco.name);
		//Debug.Log("Front Image = " + "Cards_Front/"+CardNumber.ToString("D3")+"_match("+Regex.Replace(this.name,  @"[^\w\@-]", string.Empty)+")");
		//Debug.Log("Logo Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_logo("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
		//Debug.Log("Headshot Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_headshot("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");

		return wco;
	}

	public override int GetPlayCost() {
		return costToPlay;
	}

	public WrestlerCardObject[] GetMatchFavorite(WrestlerCardObject[] team1, FlavorCardObject flavor1, 
	                                                    WrestlerCardObject[] team2, FlavorCardObject flavor2){
		int combinedPush1 = 0;
		int combinedPush2 = 0;

		for(int i = 0; i < team1.Length; i++){
			combinedPush1 += team1[i].pushCurrent;

			if(this.wrestlerType == team1[i].wrestlerType){
				combinedPush1 += 1;
			}

			if (GamePlaySession.instance.worldChampion.CardNumber == team1[i].CardNumber)
				combinedPush1 += 2;
			if (GamePlaySession.instance.titleChampion.CardNumber == team1[i].CardNumber)
				combinedPush1 += 1;
			foreach (WrestlerCardObject wrestler in GamePlaySession.instance.tagChampions) {
				if (wrestler.CardNumber == team1[i].CardNumber)
					combinedPush1 += 1;
			}
		}
		if(flavor1 != null){
			combinedPush1 += flavor1.bonuses.Push;
		}


		for(int i = 0; i < team2.Length; i++){
			combinedPush2 += team2[i].pushCurrent;

			if(this.wrestlerType == team2[i].wrestlerType){
				combinedPush2 += 1;
			}
			
			if (GamePlaySession.instance.worldChampion.CardNumber == team2[i].CardNumber)
				combinedPush2 += 2;
			if (GamePlaySession.instance.titleChampion.CardNumber == team2[i].CardNumber)
				combinedPush2 += 1;
			foreach (WrestlerCardObject wrestler in GamePlaySession.instance.tagChampions) {
				if (wrestler.CardNumber == team2[i].CardNumber)
					combinedPush2 += 1;
			}
		}
		if(flavor2 != null){
			combinedPush2 += flavor2.bonuses.Push;
		}

		if(combinedPush1 > combinedPush2){
			return team1;
		}
		else if(combinedPush1 < combinedPush2){
			return team2;
		}
		else{
			if(Random.value < .5f){
				return team1;
			}
			else{
				return team2;
			}
		}
		return team1;
	}

	public int GetMatchTarget(WrestlerCardObject[] favorite, FlavorCardObject flavor1,
	                                 WrestlerCardObject[] underdog, FlavorCardObject flavor2){
		int combinedPush1 = 0;
		int combinedPush2 = 0;
		
		for(int i = 0; i < favorite.Length; i++){
			combinedPush1 += favorite[i].pushCurrent;

			if(this.wrestlerType == favorite[i].wrestlerType){
				combinedPush1 += 1;
			}
			
			if (GamePlaySession.instance.worldChampion.CardNumber == favorite[i].CardNumber)
				combinedPush1 += 2;
			if (GamePlaySession.instance.titleChampion.CardNumber == favorite[i].CardNumber)
				combinedPush1 += 1;
			foreach (WrestlerCardObject wrestler in GamePlaySession.instance.tagChampions) {
				if (wrestler.CardNumber == favorite[i].CardNumber)
					combinedPush1 += 1;
			}
		}
		if(flavor1 != null){
			combinedPush1 += flavor1.bonuses.Push;
		}

		
		for(int i = 0; i < underdog.Length; i++){
			combinedPush2 += underdog[i].pushCurrent;

			if(this.wrestlerType == underdog[i].wrestlerType){
				combinedPush2 += 1;
			}
			
			if (GamePlaySession.instance.worldChampion.CardNumber == underdog[i].CardNumber)
				combinedPush2 += 2;
			if (GamePlaySession.instance.titleChampion.CardNumber == underdog[i].CardNumber)
				combinedPush2 += 1;
			foreach (WrestlerCardObject wrestler in GamePlaySession.instance.tagChampions) {
				if (wrestler.CardNumber == underdog[i].CardNumber)
					combinedPush2 += 1;
			}
		}
		if(flavor2 != null){
			combinedPush2 += flavor2.bonuses.Push;
		}


		int diff = combinedPush1 - combinedPush2;
		diff = 10 - diff;

		return diff;
	}

    public WrestlerCardObject[] GetWinner(WrestlerCardObject[] favorite, WrestlerCardObject[] underdog, int target, SegmentObject segment) 
	{
        int random1 = Random.Range(1, 10);
        int random2 = Random.Range(1, 10);
        int D = 0;
        int DisQualifier = Random.Range(1, 100);
        
        if (DisQualifier > 0 && DisQualifier <= 5)
        {
            D = Random.Range(1, 6);
        }
        segment.DisQualify = D;
		//segment.DisQualify = 1;
		int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName);
        if (random1 + random2 > target)
        {
            return favorite;
        }
        else
        {
            return underdog;
        }
	}

	public int RateMatch(WrestlerCardObject[] favorite, WrestlerCardObject[] underdog, FlavorCardObject flavorCard, FlavorCardObject fFlavor, FlavorCardObject uFlavor, out int[] wrestlerRatings, out List<ResultBonus> bonuses){
		int[] fRatings = new int[favorite.Length];
		int[] uRatings = new int[underdog.Length];

		bonuses = new List<ResultBonus>();

		float combinedMatch = 0f;

		bool isGood1 = false;
		string alignment1 = favorite[0].cardAlignment.ToUpper();
		if(alignment1 == "GOOD"){
			isGood1 = true;
		}

		int fSkillBonus = 0;
		if (fFlavor != null) {
			if (fFlavor.myCardType == CardType.TAG_TEAM) {
				fSkillBonus = fFlavor.bonuses.Skill;

				ResultBonus bonus = new ResultBonus ();
				bonus.type = BonusType.Flavor;
				bonus.value = fSkillBonus;
				bonus.cardNumber = fFlavor.CardNumber;
				bonus.wrestlerNum = 0;

				if (fSkillBonus > 0)
					bonuses.Add(bonus);
			}
		}

		for(int i = 0; i < favorite.Length; i++){
			fRatings[i] = Random.Range(1, 20);
			int fSkill = favorite[i].skillCurrent;
			fSkill += fSkillBonus;

			if(fRatings[i] > fSkill){
				fRatings[i] = fSkill - 1;
				int penalty = (fRatings[i] - fSkill)/4;
				fRatings[i] -= penalty;

				//this function increase the skill rating based on a probability
				//inside the if statement you should handle displaying text if skill rating increased.
				if(favorite[i].IncreaseSkillRating(fRatings[i])){
					//On Show Results, it would display as “Crash +1 SKILL!”  
					//And then on the Crash card going forward, his SKILL is displayed as 6. 

				}
			}

			combinedMatch += (float)fRatings[i];
		}

		bool isGood2 = false;
		string alignment2 = underdog[0].cardAlignment.ToUpper();
		if(alignment2 == "GOOD"){
			isGood2 = true;
		}
		
		int uSkillBonus = 0;
		if (uFlavor != null) {
			if (uFlavor.myCardType == CardType.TAG_TEAM) {
				uSkillBonus = uFlavor.bonuses.Skill;
			
				ResultBonus bonus = new ResultBonus ();
				bonus.type = BonusType.Flavor;
				bonus.value = uSkillBonus;
				bonus.cardNumber = uFlavor.CardNumber;
				bonus.wrestlerNum = 1;
				
				if (uSkillBonus > 0)
					bonuses.Add(bonus);
			}
		}

		for(int i = 0; i < underdog.Length; i++){
			uRatings[i] = Random.Range(1, 20);
			int uSkill = underdog[i].skillCurrent;
			uSkill += uSkillBonus;

			if(uRatings[i] > uSkill){
				uRatings[i] = uSkill - 1;
				int penalty = (uRatings[i] - uSkill)/4;
				uRatings[i] -= penalty;
			}
			
			combinedMatch += (float)uRatings[i];
		}

		wrestlerRatings = new int[fRatings.Length + uRatings.Length];
		fRatings.CopyTo(wrestlerRatings, 0);
		uRatings.CopyTo(wrestlerRatings, fRatings.Length);

		combinedMatch /= (fRatings.Length + uRatings.Length);

		if((alignment1=="GOOD" && alignment2=="BAD")||(alignment1=="BAD" && alignment2=="GOOD"))
		{
			ResultBonus bonus = new ResultBonus();
			bonus.type = BonusType.GoodBadMatch;

			if (isGood1) {
				bonus.favGood = true;
			} else {
				bonus.favGood = false;
			}

			combinedMatch += 1f;
			bonus.value += 1f;

			bonuses.Add(bonus);
		}

		// Check match flavor card bonus
		ResultBonus flavorBonus = new ResultBonus();
		flavorBonus.type = BonusType.MatchFlavor;
		
		if (flavorCard != null) {
			flavorBonus.cardNumber = flavorCard.CardNumber;
			if(flavorCard.bonuses.Match > 0) {
				combinedMatch += flavorCard.bonuses.Match;
				flavorBonus.value += flavorCard.bonuses.Match;
			}
		}

		if (flavorBonus.value > 0)
			bonuses.Add(flavorBonus);
        bool JobberFound = false;

        foreach (WrestlerCardObject item in favorite)
        {
            if (item.characterNumber == 0)
            {
                JobberFound = true;
            }
        }
        foreach (WrestlerCardObject item in underdog)
        {
            if (item.characterNumber == 0)
            {
                JobberFound = true;
            }
        }

        if (JobberFound == true)
        {
            combinedMatch = 0;
        }
        

        return (int) combinedMatch;
	}

	public void LevelUpWrestler(WrestlerCardObject wrestler, int matchRating){
		int roll = Random.Range(1, 20);
		if(roll - (wrestler.skillCurrent - matchRating) > wrestler.skillCurrent){
			wrestler.skillCurrent += 1;
		}
		wrestler.skillCurrent = Mathf.Clamp(wrestler.skillCurrent + 1,1, wrestler.stats.SkillMax); 
		Debug.Log("wrestler "+wrestler.name+ "has leveled up his SKILL!");
	}

	public override void PostSerialize()
	{
		frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_match");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
		logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
		headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
	}

}

[System.Serializable]
public class Bonuses{
	public int Skill;
	public int Mic;
	public int Pop;
	public int Push;

	public int Match;
	public int Cash;

	public Bonuses(){

	}
}