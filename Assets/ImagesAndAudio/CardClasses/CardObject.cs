﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public enum CardType
{
    WRESTLER,
    VENUE,
    MATCH,
    FLAVOR,
    MIC_SPOT,
    SKIT,
    MANAGER,
    SPONSOR,
    MERCH,
    TAG_TEAM,
    FEUD,
    SHOP_COMMON,
    SHOP_UNCOMMON,
    SHOP_RARE,
    ALL
}


[System.Serializable]
public enum Rarity
{
    COMMON,
    UNCOMMON,
    RARE
}


[Serializable]
[XmlInclude(typeof(WrestlerCardObject))]
[XmlInclude(typeof(VenueCardObject))]
[XmlInclude(typeof(MatchCardObject))]
[XmlInclude(typeof(MicSpotCardObject))]
[XmlInclude(typeof(SkitCardObject))]
[XmlInclude(typeof(FlavorCardObject))]
public class CardObject
{

    [NonSerialized]
    [XmlIgnore]
    public Sprite frontImage;
    [NonSerialized]
    [XmlIgnore]
    public Sprite logoImage;
    [NonSerialized]
    [XmlIgnore]
    public Sprite headshotImage;

    [XmlElement("CardNumber")]
    public int CardNumber;

    [XmlElement("CardName")]
    public string name;

    [XmlElement("CashCost")]
    public int cashCost;

    [XmlElement("TokenCost")]
    public int tokenCost;


    [XmlElement("FlavorText")]
    public string flavorText;

    [XmlElement("SFX")]
    public string sfx;

    public CardType myCardType;

    public CardObject()
    {

    }

    // Convert an object to a byte array
    protected byte[] ObjectToByteArray(System.Object obj)
    {
        if (obj == null)
            return null;
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, obj);
        return ms.ToArray();
    }

    // Convert a byte array to an Object
    protected System.Object ByteArrayToObject(byte[] arrBytes)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(arrBytes, 0, arrBytes.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        System.Object obj = (System.Object)binForm.Deserialize(memStream);
        return obj;
    }

    public virtual CardObject Clone()
    {
        return new CardObject();
    }

    public virtual CardObject CloneWithProgress(CardObject obj)
    {
        return obj;
    }

	public virtual CardObject GenerateWithProgress(int cardNumber)
	{
		return new CardObject ();
	}

    public virtual void PostSerialize()
    {
        Debug.Log("doing stuff");
    }

    public virtual int GetPlayCost()
    {
		return 0;
    }

	public virtual void IncrementWinLoss(int year, bool win)
	{
		
	}

	public virtual List<int> SerializeReady()
	{
		List<int> theList = new List<int> ();
		theList.Add (CardNumber);
		for (int i = 0; i < 4; i++) 
		{
			theList.Add (0);
		}
		return theList;
	}


	public virtual bool IsActiveNow()
	{
		return true;
	}

	public virtual Card toCard()
	{
		return new Card ();
	}

}

[System.Serializable]
[XmlRoot("data-set")]
public class VenueCardCollection
{
    [XmlElement("card", typeof(VenueCardObject))]
    public CardObject[] cardArray { get; set; }

    [XmlIgnore]
    public Dictionary<int, CardObject> cards;

    // Use this for initialization
    public VenueCardCollection() : base()
    {

    }

    public static VenueCardCollection Load(string tf)
    {
        var serializer = new XmlSerializer(typeof(VenueCardCollection));
        var reader = new StringReader(tf);
        VenueCardCollection cc = serializer.Deserialize(reader) as VenueCardCollection;
        cc.cards = new Dictionary<int, CardObject>();
        foreach (CardObject vo in cc.cardArray)
        {
            cc.cards.Add(vo.CardNumber, vo);
        }
        return cc;
    }

}

[System.Serializable]
[XmlRoot("data-set")]
public class MatchCardCollection
{
    [XmlElement("card", typeof(MatchCardObject))]
    public CardObject[] cardArray { get; set; }

    [XmlIgnore]
    public Dictionary<int, CardObject> cards;

    // Use this for initialization
    public MatchCardCollection() : base()
    {

    }

    public static MatchCardCollection Load(string tf)
    {
        var serializer = new XmlSerializer(typeof(MatchCardCollection));
        var reader = new StringReader(tf);
        MatchCardCollection cc = serializer.Deserialize(reader) as MatchCardCollection;
        cc.cards = new Dictionary<int, CardObject>();
        foreach (CardObject vo in cc.cardArray)
        {
            cc.cards.Add(vo.CardNumber, vo);
        }
        return cc;
    }

}

public static class AOTDummy
{
    public static void Dummy()
    {
        Dictionary<int, CardObject> dummy01;
    }
}

[System.Serializable]
[XmlRoot("data-set")]
public class WrestlerCardCollection
{
    [XmlElement("card", typeof(WrestlerCardObject))]
    public WrestlerCardObject[] cardArray { get; set; }

    [XmlIgnore]
    public Dictionary<int, CardObject> cards;

    // Use this for initialization
    public WrestlerCardCollection() : base()
    {

    }

    public static WrestlerCardCollection Load(string tf)
    {
        var serializer = new XmlSerializer(typeof(WrestlerCardCollection));
        var reader = new StringReader(tf);
        WrestlerCardCollection cc = serializer.Deserialize(reader) as WrestlerCardCollection;
        cc.cards = new Dictionary<int, CardObject>();
        foreach (CardObject vo in cc.cardArray)
        {
            cc.cards.Add(vo.CardNumber, vo);
            //Debug.Log(vo.CardNumber);
        }
        return cc;
    }

}
[System.Serializable]
[XmlRoot("data-set")]
public class FlavorCardCollection
{
    [XmlElement("card", typeof(FlavorCardObject))]
    public CardObject[] cardArray { get; set; }

    [XmlIgnore]
    public Dictionary<int, CardObject> cards;

    // Use this for initialization
    public FlavorCardCollection() : base()
    {

    }

    public static FlavorCardCollection Load(string tf)
    {
        var serializer = new XmlSerializer(typeof(FlavorCardCollection));
        var reader = new StringReader(tf);
        FlavorCardCollection cc = serializer.Deserialize(reader) as FlavorCardCollection;
        cc.cards = new Dictionary<int, CardObject>();
        foreach (CardObject vo in cc.cardArray)
        {
            cc.cards[vo.CardNumber] = vo;
        }
        return cc;
    }

}

[System.Serializable]
[XmlRoot("data-set")]
public class MicSpotCardCollection
{
    [XmlElement("card", typeof(MicSpotCardObject))]
    public CardObject[] cardArray { get; set; }

    [XmlIgnore]
    public Dictionary<int, CardObject> cards;

    // Use this for initialization
    public MicSpotCardCollection() : base()
    {

    }

    public static MicSpotCardCollection Load(string tf)
    {
        var serializer = new XmlSerializer(typeof(MicSpotCardCollection));
        var reader = new StringReader(tf);
        MicSpotCardCollection cc = serializer.Deserialize(reader) as MicSpotCardCollection;
        cc.cards = new Dictionary<int, CardObject>();
        foreach (CardObject vo in cc.cardArray)
        {
            cc.cards.Add(vo.CardNumber, vo);
        }
        return cc;
    }

}

[System.Serializable]
[XmlRoot("data-set")]
public class SkitCardCollection
{
    [XmlElement("card", typeof(SkitCardObject))]
    public CardObject[] cardArray { get; set; }

    [XmlIgnore]
    public Dictionary<int, CardObject> cards;

    // Use this for initialization
    public SkitCardCollection() : base()
    {

    }

    public static SkitCardCollection Load(string tf)
    {
        var serializer = new XmlSerializer(typeof(SkitCardCollection));
        var reader = new StringReader(tf);
        SkitCardCollection cc = serializer.Deserialize(reader) as SkitCardCollection;
        cc.cards = new Dictionary<int, CardObject>();
        foreach (CardObject vo in cc.cardArray)
        {
            cc.cards.Add(vo.CardNumber, vo);
        }
        return cc;
    }

}

[System.Serializable]
[XmlRoot("data-set")]
public class NYKCardCollection
{
    [XmlElement("SegmentText", typeof(string))]
    public string[] blurbs { get; set; }

    // Use this for initialization
    public NYKCardCollection() : base()
    {

    }

    public static NYKCardCollection Load(string tf)
    {
        var serializer = new XmlSerializer(typeof(NYKCardCollection));
        var reader = new StringReader(tf);
        NYKCardCollection cc = serializer.Deserialize(reader) as NYKCardCollection;
        return cc;
    }
}