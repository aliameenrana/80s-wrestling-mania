﻿using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

[System.Serializable]
public class SkitCardObject : CardObject {

	[XmlElement("Limit")]
	public int limit;
	
	[XmlElement("WrestlerMin")]
	public int wrestlerMin;
	
	[XmlElement("WrestlerMax")]
	public int wrestlerMax;
	
	[XmlElement("PassCheck")]
	public string passCheck;

	[XmlElement("WrestlerType")]
	public string wrestlerType;

	[XmlElement("Results")]
	public Results result;

	[XmlElement("Era")]
	public string Era;

	[XmlElement("FirstActiveYear")]
	public int FirstActiveYear;

	[XmlElement("FinalActiveYear")]
	public int LastActiveYear;

	[XmlElement("SyncEffect")]
	public string SyncEffect;

	[XmlElement("CostToPlay")]
	public int costToPlay;

    [System.NonSerialized]
	public bool lastCheckPassed;

	public SkitCardObject() : base() {
		myCardType = CardType.SKIT;
	}

	public override CardObject Clone(){
		myCardType = CardType.SKIT;
		byte[] wcoBA = this.ObjectToByteArray(this);
		SkitCardObject fco = (SkitCardObject)this.ByteArrayToObject(wcoBA);
		
		//fco.frontImage = Resources.Load("Cards_Front/"+CardNumber.ToString("D3")+"_skit", typeof(Sprite)) as Sprite;
		////fco.logoImage = Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_logo", typeof(Sprite)) as Sprite;
		//fco.headshotImage = Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_headshot", typeof(Sprite)) as Sprite;
		fco.frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_skit");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
		fco.headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
		try{
			fco.logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
		}
		catch(System.Exception w)
		{
			//Debug.LogWarning("RECOVERABLE ERROR: " + w.Message + " CardNumber is: " + CardNumber);
		}
		// Debug output for tracking down filenames, etc.
		/*Debug.Log("Card: " + fco.name);
		Debug.Log("Front Image = " + "Cards_Front/"+CardNumber.ToString("D3")+"_skit("+Regex.Replace(this.name,  @"[^\w\@]", string.Empty)+")");
		Debug.Log("Logo Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_logo("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
		Debug.Log("Headshot Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_headshot("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
		*/
		return fco;
	}

	public override bool IsActiveNow()
	{
		int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName);
		if (FirstActiveYear <= ThisYear && LastActiveYear >= ThisYear) 
		{
			return true;	
		}
		return false;
	}

	public int RateSkit(WrestlerCardObject[] wrestlers, FlavorCardObject flavorCard, out int[] wrestlerRatings, out List<ResultBonus> bonuses){
		int[] wRatings = new int[wrestlers.Length];
		bool skipCheck = false;
		
		bonuses = new List<ResultBonus>();

		float combined = 0f;

		wrestlerRatings = wRatings;

		for(int i = 0; i < wrestlers.Length; i++){
			
			wRatings[i] = Random.Range(1, 20);
			if(wRatings[i] <= wrestlers[i].popCurrent){
				wRatings[i] = wrestlers[i].popCurrent;
			}
			else{
				int penalty = (wrestlers[i].popCurrent - 1) - ((wRatings[i] - wrestlers[i].popCurrent)/3);
				wRatings[i] = penalty;
			}

			//TODO: At this point This is the base value for animated trasitions:  wRatings[i]
			wrestlerRatings[i] = wRatings[i];

			ResultBonus wrestlerBonus = new ResultBonus();
			wrestlerBonus.type = BonusType.Wrestler;
			wrestlerBonus.wrestlerNum = i;
			wrestlerBonus.cardNumber = wrestlers[i].CardNumber;

			if(wrestlers[i].wrestlerType == this.wrestlerType){
				skipCheck = true;
				wRatings[i] += 1;
				wrestlerBonus.value += 1;
			}
			
			ResultBonus flavorBonus = new ResultBonus();
			flavorBonus.type = BonusType.Flavor;
			flavorBonus.wrestlerNum = i;

			if(flavorCard != null){
				flavorBonus.cardNumber = flavorCard.CardNumber;
				if(flavorCard.bonuses.Pop > 0) {
					wRatings[i] += flavorCard.bonuses.Pop;
					flavorBonus.value += flavorCard.bonuses.Pop;
				}
			}
			
			if (wrestlerBonus.value > 0)
				bonuses.Add(wrestlerBonus);
			if (flavorBonus.value > 0)
				bonuses.Add(flavorBonus);

			//this function increase the skill rating based on a probability
			//inside the if statement you should handle displaying text if skill rating increased.
			if(wrestlers[i].IncreasePopRating(wRatings[i])){

			}

			//TODO: At this point This is the bonus value for animated trasitions:  wRatings[i]
			combined += (float)wRatings[i];

		}
		
		combined /= wRatings.Length;
		
		if(!(Random.value <= .5f) && !skipCheck){
			lastCheckPassed = false;
			combined -= 4;
			if(combined <= 0){
				combined = 0;
			}
		} else {
			lastCheckPassed = true;
		}

		return (int) combined;
	}

	public override void PostSerialize()
	{
		if (FirstActiveYear == 0) 
		{
			SkitCardObject w = (SkitCardObject)UIManagerScript.instance.gsi.skits.cards[CardNumber].Clone();
			FirstActiveYear = w.FirstActiveYear;
			LastActiveYear = w.LastActiveYear;
		}
		frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_skit");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
		headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
		costToPlay = UIManagerScript.instance.gsi.skits.cards[CardNumber].GetPlayCost();
		try{
			logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
		}
		catch(System.Exception w){
			//Debug.LogWarning("RECOVERABLE ERROR: "+w.Message);
		}
//		Debug.Log ("created SkitSpot from player prefs");
	}

	public override int GetPlayCost()
	{
		return costToPlay;
	}
	

}
