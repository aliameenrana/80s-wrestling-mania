﻿using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

[System.Serializable]
public class FlavorCardObject : CardObject {
	[XmlElement("CardClass")]
	public string cardClass;
	
	[XmlElement("Requirements")]
	public string requirements;

	[XmlElement("Bonuses")]
	public Bonuses bonuses;

	[XmlElement("UnlockRequirements")]
	public UnlockRequirements unlockRequirements;

	[XmlElement("FlavorCardType")]
	public string flavorCardType;

	[XmlElement("Limit")]
	public int limit;

	[XmlElement("Alignment")]
	public string characterAlignment;

	[XmlElement("Era")]
	public string Era;

	[XmlElement("FirstActiveYear")]
	public int FirstActiveYear;

	[XmlElement("FinalActiveYear")]
	public int LastActiveYear;

	// Use this for initialization
	public FlavorCardObject() : base(){
		myCardType = CardType.FLAVOR;

	}
	
	public override CardObject Clone(){
		// Add check for which card type this FLAVOR card is
		switch (cardClass) {
		case "Manager":
			myCardType = CardType.MANAGER;
			break;
		case "Sponsor":
			myCardType = CardType.SPONSOR;
			break;
		case "Feud":
			myCardType = CardType.FEUD;
			break;
		case "Tag Team":
			myCardType = CardType.TAG_TEAM;
			break;
		case "Merchandise":
			myCardType = CardType.MERCH;
			break;
		default:
			myCardType = CardType.FLAVOR;
			break;
		}
		byte[] wcoBA = this.ObjectToByteArray(this);
		FlavorCardObject fco = (FlavorCardObject)this.ByteArrayToObject(wcoBA);
		
		fco.frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_flavor_" + Regex.Replace(cardClass,  @"[^\w\@-]", string.Empty).ToLower());//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];

		try
		{
			fco.logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
		}
		catch(System.Exception w)
		{
			//Debug.LogWarning("RECOVERABLE ERROR: " + w.Message + " CardNumber: " + CardNumber);
		}

		fco.headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
		
		// Debug output for tracking down filenames, etc.
		/*Debug.Log("Card: " + fco.name);
		Debug.Log("Front Image = " + "Cards_Front/"+CardNumber.ToString("D3")+"_flavor_"+Regex.Replace(this.cardClass,  @"[^\w\@-]", string.Empty).ToLower()+"("+Regex.Replace(this.name,  @"[^\w\@-]", string.Empty)+")");
		Debug.Log("Logo Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_logo("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
		Debug.Log("Headshot Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_headshot("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
*/
		return fco;
	}

	public int GetSponsorBonus(){
		if(this.cardClass == "Sponsor"){
			Debug.LogError("ERROR: This isn't a SPONSOR card");
		}

		return this.bonuses.Cash;
	}

	public override bool IsActiveNow()
	{
		if (this.cardClass.ToUpper() != "MANAGER")
			return true;

		int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName);
		if (FirstActiveYear <= ThisYear && LastActiveYear >= ThisYear) 
		{
			return true;	
		}
		return false;
	}

	public override void PostSerialize(){
		/*frontImage = Resources.Load("Cards_Front/"+CardNumber.ToString("D3")+"_flavor_"+Regex.Replace(this.cardClass,  @"[^\w\@-]", string.Empty).ToLower(), typeof(Sprite)) as Sprite;
		logoImage = Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_logo", typeof(Sprite)) as Sprite;
		headshotImage = Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_headshot", typeof(Sprite)) as Sprite;*/
		frontImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3"));// + "_match");//GamePlaySession.instance.GSI.cardFrontDict[CardNumber];


		if (FirstActiveYear == 0) 
		{
			FlavorCardObject w = (FlavorCardObject)UIManagerScript.instance.gsi.flavors.cards[CardNumber].Clone();
			FirstActiveYear = w.FirstActiveYear;
			LastActiveYear = w.LastActiveYear;
		}

		headshotImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_head");//GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];

		try{
			logoImage = UIManagerScript.instance.gsi.cards.LoadAsset<Sprite>(CardNumber.ToString("D3") + "_banner");//GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
		}
		catch(System.Exception w){
			Debug.LogError("RECOVER ERROR: "+w.Message);
		}
		Debug.Log ("creating flavor card from player prefs");
	}

}

[System.Serializable]
public class UnlockRequirements{
	public int CardA;
	public int CardB;
	public int MaxOut;

	public UnlockRequirements(){

	}
}

