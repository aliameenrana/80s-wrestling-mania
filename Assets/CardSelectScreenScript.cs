using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;
using UnityEngine.EventSystems;

[System.Serializable]
public class NewCardsObject {
	public PopUpMessage message;
	public List<CardObject> cards;

	public NewCardsObject(){
		cards = new List<CardObject>();
	}
}

public class CardSelectScreenScript : MonoBehaviour 
{
    public enum CardFilter 
	{
		WRESTLERS_ALL,
        VENUES,
        MATCHES,
        FLAVORS,
        MIC_SPOTS,
        SKITS,
        MANAGERS,
        SPONSORS,
        MERCH,
        TAG_TEAMS,
        FEUDS,
        ALL,
        WRESTLERS_GOOD,
        WRESTLERS_TWEENER,
        WRESTLERS_BAD,
        WRESTLERS_MAIN_EVENT,
        WRESTLERS_MIDCARDER, 
        WRESTLERS_UPPER_MIDCARD,
        WRESTLERS_OPENER,
        WRESTLERS_JOBBER, 
        WRESTLERS_CHAMP,
        WRESTLERS_LEGENDARY
    }

    private bool isSelectingCard;
    public bool isCardSelect;
	bool showingActive;
    public int maximumCards;
    public static CardSelectScreenScript instance;

    public CardFilter currentFilter;
    public Text titleText;
    public Text filterText;
    public Text collectionText;

    public int currentCardIndex;

    public CardView cardViewer;
    public Animator cardViewerScreen;

    public GameObject collectionPanel;
    public GameObject selectButton;
    public GameObject notEnoughString;
	public GameObject notActiveString;
	public List<CardObject> FoilCards;
    public bool IsAwardShow;
	public GameObject goBackButton;
	public GameObject activeInactiveFilterButton;
	public GameObject noCardsFound;

	//------------------------------------------------------------//
	public CardDeckInterface cardDeck;
	public GameObject cardPrefab;



    [HideInInspector]
    public Animator previousScreen;

    [HideInInspector]
    public bool hadHUD;

    private CardSelector selector;

    private GameObject UIManager;
    private GameObject playerHUD;

    GameObject AwardButton;

    private Queue<CardFilter> filterQueue = new Queue<CardFilter>();
    private List<CardObject> cardList = new List<CardObject>();
    private List<CardObject> cardLibrary = new List<CardObject>();

    private string[] alphabet = new string[26]
    {
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
        "o",
        "p",
        "q",
        "r",
        "s",
        "t",
        "u",
        "v",
        "w",
        "x",
        "y",
        "z"
    };

    void Awake()
    {
        instance = this;
        //UIManager = GameObject.Find("UIManager");
		StartCoroutine(WaitForActive());
    }

    public void GenerateFilters(CardFilter newFilter, List<CardType> exclude = null, bool Tweener=false)
    {
        bool isFound;
        currentFilter = newFilter;

        // Wipe queue if it already exists
        if (filterQueue.Count > 0)
            filterQueue.Clear();

        // Set up the queue of filters available
        filterQueue.Enqueue(currentFilter);
        foreach (CardFilter filter in Enum.GetValues(typeof(CardFilter)))
        {
            if (filter == currentFilter)
            {
                continue;
            }

            isFound = false;
            if (exclude != null)
                foreach (CardType f in exclude)
                {
                    if (filter.ToString().Contains(f.ToString()))
                    {
                        isFound = true;
                        break;
                    }
                }

            if (isFound)
                continue;

            filterQueue.Enqueue(filter);
        }
        if (Tweener)
            filterQueue.Enqueue(CardFilter.WRESTLERS_TWEENER);

        currentCardIndex = 0;        
    }

	public void OpenCardView(NewCardsObject newCards = null, Animator nextScreen = null, bool showHUD = false, bool isFoil = false)
    {
		notActiveString.SetActive (false);
		GameObject.FindObjectOfType<ThemeController> ().ChangeSprite (goBackButton, false);
		goBackButton.GetComponent<Button> ().onClick.RemoveAllListeners ();

        UIManager = GameObject.Find("UIManager");
        playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;
        previousScreen = nextScreen;
        hadHUD = showHUD;
        if (playerHUD != null)
            playerHUD.SetActive(false);

        if (newCards != null) 
		{
			GameObject.FindObjectOfType<ThemeController> ().ChangeSprite (goBackButton, true);
            cardLibrary = newCards.cards;



			GamePlaySession.instance.AddSeveralCardObjectsToList (newCards.cards, GamePlaySession.instance.myCards);

			if (UIManagerScript.instance.transferCash > -1) 
			{
				GamePlaySession.instance.myCards.Clear ();
			}
				

            foreach (var card in newCards.cards) 
			{
				if (GamePlaySession.instance.CheckCard (card.CardNumber) == null) 
				{
					GamePlaySession.instance.myCards.Add(card);
				}    
            }
            // Now reprocess segment based on new card list
            if (GamePlaySession.instance.currentSegment != null) 
			{
                GamePlaySession.instance.currentSegment.ProcessCardIDs();
                if (nextScreen != null) 
				{
                    SegmentSetup setup = nextScreen.GetComponent<SegmentSetup>();
                    if (setup != null) 
					{
                        setup.SetData(setup.segmentDisplay);
                        setup.UpdateDisplay();
                    }
                }
            }
			goBackButton.GetComponent<Button> ().onClick.AddListener (()=> UIManagerScript.instance.OnSaveAll ());
        } 

		else 
		{
            cardLibrary = GamePlaySession.instance.myCards;
        }

        GenerateFilters(CardFilter.ALL);
		if (!isFoil) 
		{
			FilterCards ();
		} 
		else 
		{
			FilterCards(isFoil);
		}
        
        UpdateCollectionData();

        SetViewSelect(false);
        

        if (ScreenHandler.current != null && cardViewerScreen != null) 
		{
            ScreenHandler.current.OpenPanel(cardViewerScreen);
        }
        if (newCards != null && newCards.message != null) 
		{
            if (newCards.message.message != "") 
			{
                PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
                if (popUpWindow != null) 
				{
                    popUpWindow.OpenPopUpWindow(new PopUpMessage[] { newCards.message }, this.cardViewerScreen);
                }
            }
        }
    }

    public void OpenCardSelect(CardSelector caller)
    {
		showingActive = true;
		activeInactiveFilterButton.transform.GetChild (0).GetComponent<Text> ().text = "Active";
		notActiveString.SetActive (false);
		GameObject.FindObjectOfType<ThemeController> ().ChangeSprite (goBackButton, false); 
		goBackButton.GetComponent<Button> ().onClick.RemoveAllListeners ();

        // Set the selector and the previous screen
        IsAwardShow = false;
        selector = caller;
        previousScreen = selector.gameObject.GetComponentInParent<Animator>();

        // Refund any previously selected card's costs
        CardObject card = selector.GetCard();

        if (card != null) {
			GamePlaySession.instance.SetMyCash(GamePlaySession.instance.GetMyCash() + card.GetPlayCost());
        }
        
        // Establish and filter the card library
        cardLibrary = caller.cards;

        GenerateFilters(selector.filterType, selector.hideTypes);
		FilterCards(false,showingActive);
		SetViewSelect(true);
        if (cardList.Count == 0) {
            return;
        }

        

        if (ScreenHandler.current != null && cardViewerScreen != null)
        {
            ScreenHandler.current.OpenPanel(cardViewerScreen);
            //GameObject FilterPanel = cardViewerScreen.transform.GetChild(1).GetChild(0).GetChild(0).gameObject;
            //FilterPanel.SetActive(true);
            //if (selector.filterType != CardFilter.WRESTLERS_ALL || selector.filterType != CardFilter.WRESTLERS_BAD || selector.filterType != CardFilter.WRESTLERS_CURTAIN_JERKER || selector.filterType != CardFilter.WRESTLERS_GOOD || selector.filterType != CardFilter.WRESTLERS_MAIN_EVENT || selector.filterType != CardFilter.WRESTLERS_MIDCARDER)
            //{
            //    FilterPanel.GetComponent<Button>().interactable = false;
            //}
            //else
            //{
            //    FilterPanel.GetComponent<Button>().interactable = true;
            //}
        }
        else {
            Debug.Log("FAILURE!!!!");
        }
    }

    public void OpenCardSelectAward(GameObject caller)
    {
		notActiveString.SetActive (false);
        //if (ShowObject.instance.showFinished==true)
        //{
        //    Debug.Log("Show Finished...");
        //}
        IsAwardShow = true;
        previousScreen = caller.gameObject.GetComponentInParent<Animator>();
        AwardButton = EventSystem.current.currentSelectedGameObject;
        //if (caller.name=="WrestlerOfTheYear")
        //CardSelector ThisSelector = new CardSelector();
        CardSelector ThisSelector = gameObject.AddComponent<CardSelector>();
        ThisSelector.hideTypes = new List<CardType>();

        switch (caller.name)
        {
            case "WrestlerOfTheYear":
                ThisSelector.filterType = CardFilter.WRESTLERS_ALL;
                foreach (CardType type in Enum.GetValues(typeof(CardType)))
                {
                    if (type != CardType.WRESTLER)
                    {
                        ThisSelector.hideTypes.Add(type);
                    }
                }
                break;
            case "TagTeamOfTheYear":
                ThisSelector.filterType = CardFilter.TAG_TEAMS;
                foreach (CardType type in Enum.GetValues(typeof(CardType)))
                {
                    if (type != CardType.TAG_TEAM)
                    {
                        ThisSelector.hideTypes.Add(type);
                    }
                }
                break;
            case "MostPopularWrestler":
				ThisSelector.filterType = CardFilter.WRESTLERS_GOOD;
                foreach (CardType type in Enum.GetValues(typeof(CardType)))
                {
					if (type != CardType.WRESTLER) 
					{
						ThisSelector.hideTypes.Add (type);
					} 
                }
                break;
            case "MostHatedWrestler":
                ThisSelector.filterType = CardFilter.WRESTLERS_BAD;
                foreach (CardType type in Enum.GetValues(typeof(CardType)))
                {
                    if (type != CardType.WRESTLER)
                    {
                        ThisSelector.hideTypes.Add(type);
                    }
                }
                break;
        }
        selector = ThisSelector;

        cardLibrary = GamePlaySession.instance.myCards;
		if (caller.name == "MostPopularWrestler" || caller.name == "MostHatedWrestler" || caller.name == "WrestlerOfTheYear")
        {
            GenerateFilters(ThisSelector.filterType, ThisSelector.hideTypes,true);
        }
        else
        {
            GenerateFilters(ThisSelector.filterType, ThisSelector.hideTypes);
        }
        
        FilterCards();

        if (cardList.Count == 0)
        {
            return;
        }

        SetViewSelect(true);

        if (ScreenHandler.current != null && cardViewerScreen != null)
        {
            ScreenHandler.current.OpenPanel(cardViewerScreen);
        }
        else
        {
            Debug.Log("FAILURE!!!!");
        }
    }


    public void SetViewSelect(bool select) {
        isCardSelect = select;

        if (isCardSelect) 
		{
            if (collectionPanel)
                collectionPanel.SetActive(false);
            if (selectButton)
                selectButton.SetActive(true);
			if (filterText && (selector.filterType != CardFilter.WRESTLERS_ALL && selector.filterType != CardFilter.FLAVORS && selector.filterType != CardFilter.MIC_SPOTS && selector.filterType != CardFilter.SKITS)) 
			{
				activeInactiveFilterButton.gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 0);
				activeInactiveFilterButton.gameObject.GetComponent<Button> ().interactable = false;
				activeInactiveFilterButton.transform.GetChild (0).GetComponent<Text> ().color = new Color (1, 1, 1, 0);
				filterText.transform.parent.gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 0);
				filterText.transform.parent.gameObject.GetComponent<Button> ().interactable = false;
				filterText.GetComponent<Text> ().color = new Color (1, 1, 1, 0);
				filterText.text = "";
				//filterText.transform.parent.gameObject.SetActive(false);
			} 
			else if (selector.filterType == CardFilter.FLAVORS || selector.filterType == CardFilter.MIC_SPOTS || selector.filterType == CardFilter.SKITS) 
			{
				activeInactiveFilterButton.gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 1);
				activeInactiveFilterButton.gameObject.GetComponent<Button> ().interactable = true;
				activeInactiveFilterButton.transform.GetChild (0).GetComponent<Text> ().color = new Color (1, 1, 1, 1);
				filterText.transform.parent.gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 0);
				filterText.transform.parent.gameObject.GetComponent<Button> ().interactable = false;
				filterText.GetComponent<Text> ().color = new Color (1, 1, 1, 0);
				filterText.text = "";
			}
            else
            {
				filterText.transform.parent.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
				filterText.transform.parent.gameObject.GetComponent<Button>().interactable = true;
				filterText.GetComponent<Text>().color = new Color(1, 1, 1, 1);
				//filterText.transform.parent.gameObject.SetActive(true);
				activeInactiveFilterButton.gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 1);
				activeInactiveFilterButton.gameObject.GetComponent<Button> ().interactable = true;
				activeInactiveFilterButton.transform.GetChild (0).GetComponent<Text> ().color = new Color (1, 1, 1, 1);
            }

        } 
		else 
		{
            if (collectionPanel)
                collectionPanel.SetActive(true);
            if (selectButton) 
			{
                selectButton.SetActive(false);
            }
           
			if (filterText)
                filterText.transform.parent.gameObject.SetActive(true);

            filterText.transform.parent.gameObject.GetComponent<Button>().interactable = true;
            filterText.transform.parent.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            filterText.GetComponent<Text>().color = new Color(1, 1, 1, 1);

			activeInactiveFilterButton.gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 1);
			activeInactiveFilterButton.gameObject.GetComponent<Button> ().interactable = true;
			activeInactiveFilterButton.transform.GetChild (0).GetComponent<Text> ().color = new Color (1, 1, 1, 1);
                //filterText.transform.parent.gameObject.SetActive(true);
        }

        SetScreenTitle();
    }

    public void SetScreenTitle() {
        if (titleText) {
            if (isCardSelect) {
                titleText.text = GetCurrentFilterNameHeading();
            } else {
                titleText.text = "Cards";
            }
        }
    }

    public void ReturnToLastScreen()
    {
		if (notEnoughString!=null && notEnoughString.activeSelf==true)
			notEnoughString.SetActive (false);
		
        if (hadHUD)
        {
			playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;

            if (playerHUD != null)
                playerHUD.SetActive(true);
        }

        if (!isCardSelect && previousScreen == null)
        {
            //UIManager = GameObject.Find("UIManager");

            UIManager.GetComponent<UIManagerScript>().GoToCalendar(false);
            return;
        }

        if (isCardSelect)
        {
            if (selector != null)
                selector.ResetCard();
            
        }

        if (ScreenHandler.current != null && previousScreen != null)
        {
            ScreenHandler.current.OpenPanel(previousScreen);
        }
    }
    public void SelectCard(CardView viewer) {
        StartCoroutine(SelectCardC(viewer));
    }
    public IEnumerator SelectCardC(CardView viewer) 
	{
        isSelectingCard = true;
        //Debug.Log(viewer.myCard.CardNumber);
        // Determine card cost
        int cost = viewer.myCard.GetPlayCost();
        // Check we can afford it or if it's free
		bool canSelect = true;

		if (GamePlaySession.instance.GetMyCash() >= cost || cost <= 0) 
		{
			if (viewer.myCard.myCardType == CardType.WRESTLER || viewer.myCard.myCardType == CardType.SKIT || viewer.myCard.myCardType == CardType.MIC_SPOT || viewer.myCard.myCardType == CardType.MANAGER) 
			{
				if (!viewer.myCard.IsActiveNow()) 
				{
					canSelect = false;
					yield return StartCoroutine (FlashMessage (notActiveString));
				}
			}

			if (canSelect) 
			{
				// Subtract card cost
				if (cost>0)
					GamePlaySession.instance.SetMyCash(GamePlaySession.instance.GetMyCash() - cost);
				
				// Switch screens
				if (ScreenHandler.current != null && previousScreen != null) 
				{
					ScreenHandler.current.OpenPanel(previousScreen);
				}
				else
				{
					//Debug.Log("FAILURE");
					//Debug.Log(ScreenHandler.current.name);
					//Debug.Log(previousScreen.name);
				}

				// Set the selector to select the card we chose
				yield return StartCoroutine(WaitForSelector(viewer));
			}
        } 
		else 
		{
            yield return StartCoroutine(FlashMessage(notEnoughString));
        }
        isSelectingCard = false;
        yield return null;
    }

    public IEnumerator WaitForSelector(CardView viewer)
    {
        while (selector == null)
        {
            yield return null;
        }
        if (IsAwardShow == false)
        {
            selector.SelectCard(viewer.myCard);
        }
        else if (IsAwardShow == true)
        {
            if (AwardButton != null && AwardButton.GetComponent<Image>()!=null)
            {
                AwardButton.GetComponent<Image>().sprite = viewer.myCard.logoImage;
                AwardButton.transform.GetChild(0).gameObject.GetComponent<Text>().text = "";
                switch (AwardButton.name)
                {
                    case "WrestlerOfTheYearButton":
                        AwardScreenController.instance.WYSelected = true;
                        AwardScreenController.instance.AwardHolders[0] = viewer.myCard;
                        break;
                    case "TagTeamOfTheYearButton":
                        AwardScreenController.instance.TTSelected = true;
                        AwardScreenController.instance.AwardHolders[1] = viewer.myCard;
                        break;
                    case "MostPopularWrestlerButton":
                        AwardScreenController.instance.MPWSelected = true;
                        AwardScreenController.instance.AwardHolders[2] = viewer.myCard;
                        break;
                    case "MostHatedWrestlerButton":
                        AwardScreenController.instance.MHWSelected = true;
                        AwardScreenController.instance.AwardHolders[3] = viewer.myCard;
                        break;
                }
                if (AwardScreenController.instance.MHWSelected && AwardScreenController.instance.MPWSelected && AwardScreenController.instance.WYSelected && AwardScreenController.instance.TTSelected)
                {
                    AwardScreenController.instance.GiveAwardsButton.GetComponent<Button>().interactable = true;
                }
            }
        }
    }
    public void UpdateCollectionData() {
        if (collectionText != null) {
            if (GamePlaySession.instance != null) {
                collectionText.text = "Collection: " + GamePlaySession.instance.myCards.Count.ToString() + "/" + maximumCards.ToString();
                collectionText.transform.parent.gameObject.SetActive(true);
            } else {
                collectionText.transform.parent.gameObject.SetActive(false);
            }
        }
    }


	public void SwitchActive(GameObject caller)
	{
		if (caller.transform.GetChild (0).GetComponent<Text> ().text == "Active") 
		{
			showingActive = false;
			caller.transform.GetChild (0).GetComponent<Text> ().text = "InActive";
		} 
		else 
		{
			showingActive = true;
			caller.transform.GetChild (0).GetComponent<Text> ().text = "Active";
		}
		FilterCards (false, showingActive);
	}

    public void SwitchCardFilter()
    {
        //if (isCardSelect)
        //	return;

        if (filterQueue.Count > 0)
        {
            CardFilter lastFilter = filterQueue.Dequeue();
            filterQueue.Enqueue(lastFilter);
            currentFilter = filterQueue.Peek();

            currentCardIndex = 0;
			FilterCards(false,showingActive);
        }
    }
    public void OnDisable() 
	{
        if (isSelectingCard)
        {
            if (previousScreen.name != "AwardsGivingScreen")
            {
                //Debug.Log("I AM DISABLING While selecting card");
                UIManager = GameObject.Find("UIManager");
                UIManager.GetComponent<UIManagerScript>().GoToCalendar(false);
            }
        }
		noCardsFound.SetActive (false);
    }
	public void FilterCards(bool isFoil = false, bool active = true)
    {
        List<CardObject> filteredCards = new List<CardObject>();
		FoilCards.Clear ();
        if (cardLibrary.Count > 0)
        {
            // Determine the current card type to check for, or simply return all cards.
            if (currentFilter == CardFilter.ALL)
            {
				foreach (var item in cardLibrary) 
				{
					if (item.IsActiveNow () == active) 
					{
						filteredCards.Add (item);
					} 
				}
            }
            else
            {
				CardType[] filters = CheckFilterType();
                foreach (CardObject card in cardLibrary)
                {
                    foreach (CardType filter in filters)
                    {
                        if (card.myCardType == filter)
                        {
							if (GamePlaySession.instance.GetInTutorialMode() && card.CardNumber == 700)
                                continue;
							
							if ((filter==CardType.FLAVOR || filter==CardType.MIC_SPOT || filter==CardType.SKIT || filter==CardType.WRESTLER) && (card.IsActiveNow ()!=active))
								continue;

                            if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_BAD)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                //int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
//								if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear || !isCardSelect)
//                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.cardAlignment == "Bad")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
                                        FoilCards.Add(wrestler);
                                    }
                                //}
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_TWEENER)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                //int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
//								if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear || !isCardSelect)
//                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
									if (wrestler.cardAlignment.ToUpper() == "TWEENER")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
										FoilCards.Add(wrestler);
                                    }
                                //}
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_GOOD)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                //int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
//								if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear || !isCardSelect)
//                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.cardAlignment == "Good")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
										FoilCards.Add(wrestler);
                                    }
                                //}
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_MAIN_EVENT)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                //int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
//								if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear || !isCardSelect)
//                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Main Eventer")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
										FoilCards.Add(wrestler);
                                    }
                                //}
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_MIDCARDER)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                //int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
//								if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear || !isCardSelect)
//                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Midcard")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
										FoilCards.Add(wrestler);
                                    }
                                //}
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_UPPER_MIDCARD)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                //int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
//								if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear || !isCardSelect)
//                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Upper Midcard")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
										FoilCards.Add(wrestler);
                                    }
                                //}
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_OPENER)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                //int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
//								if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear || !isCardSelect)
//                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Opener")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
										FoilCards.Add(wrestler);
                                    }
                                //}
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_JOBBER)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                
                                //int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
//								if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear || !isCardSelect)
//                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Jobber")
                                        {
                                           
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
										FoilCards.Add(wrestler);
                                    }
                                //}
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_LEGENDARY)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                
                                //int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
//								if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear || !isCardSelect)
//                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Legendary")
                                        {

                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
										FoilCards.Add(wrestler);
                                    }
                                //}
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_CHAMP)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;

                                //int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
//								if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear || !isCardSelect)
//                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
									if (wrestler.CardNumber == GamePlaySession.instance.tagChampions[0].CardNumber || 
										wrestler.CardNumber == GamePlaySession.instance.tagChampions[1].CardNumber || 
										wrestler.CardNumber == GamePlaySession.instance.GetTitleChampCardNumber() || 
										wrestler.CardNumber == GamePlaySession.instance.GetWorldChampCardNumber())
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
									if (wrestler.CardNumber == GamePlaySession.instance.tagChampions[0].CardNumber || 
										wrestler.CardNumber == GamePlaySession.instance.tagChampions[1].CardNumber || 
										wrestler.CardNumber == GamePlaySession.instance.GetTitleChampCardNumber() || 
										wrestler.CardNumber == GamePlaySession.instance.GetWorldChampCardNumber())
                                        {
                                            FoilCards.Add(wrestler);
                                        }
                                    }
                                //}
                            }
                            else if (filter == CardType.MERCH)
                            {
                                FlavorCardObject merch = (FlavorCardObject)card;
                                
                                string ParentName="";
                                if (selector.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.name != null)
                                {
                                    ParentName = selector.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.name;
                                }
                                int[] WrestlerCardNumber = { 0, 0, 0, 0, 0 };

                                //Debug.Log("Selector is: " + selector.name);

                                if ((selector.name == "Team1FlavorPanel") && (ParentName != "TagMatchSetupScreen"))
                                {
                                    GameObject WrestlerLogoDisplay = GameObject.Find("Wrestler1Logo");
									WrestlerCardNumber[0] = int.Parse(Regex.Replace(WrestlerLogoDisplay.GetComponent<Image>().sprite.name, "_banner", ""));
                                }
                                else if ((selector.name == "Team2FlavorPanel") && (ParentName != "TagMatchSetupScreen"))
                                {
                                    GameObject WrestlerLogoDisplay = GameObject.Find("Wrestler2Logo");
                                    WrestlerCardNumber[0] = int.Parse(Regex.Replace(WrestlerLogoDisplay.GetComponent<Image>().sprite.name, "_banner", ""));
                                }
                                else if ((selector.name == "Team1FlavorPanel") || (selector.name == "Team2FlavorPanel") && (ParentName == "TagMatchSetupScreen"))
                                {
                                    //Debug.Log("TEAM GAMEOBJECT IS: " + selector.transform.parent.gameObject.name);
                                    if (selector.transform.parent.gameObject.name == "Team1Options")
                                    {
                                        GameObject[] WrestlerLogoDisplays = { null, null };
                                        WrestlerLogoDisplays[0] = GameObject.Find("Wrestler1Logo");
                                        WrestlerLogoDisplays[1] = GameObject.Find("Wrestler2Logo");
                                        for (int i = 0; i < WrestlerLogoDisplays.Length; i++)
                                        {
                                            if (WrestlerLogoDisplays[i] != null)
                                            {
                                                WrestlerCardNumber[i] = int.Parse(Regex.Replace(WrestlerLogoDisplays[i].GetComponent<Image>().sprite.name, "_banner", ""));
                                            }
                                        }
                                    }
                                    else if (selector.transform.parent.gameObject.name == "Team2Options")
                                    {
                                        GameObject[] WrestlerLogoDisplays = { null, null };
                                        WrestlerLogoDisplays[0] = GameObject.Find("Wrestler3Logo");
                                        WrestlerLogoDisplays[1] = GameObject.Find("Wrestler4Logo");
                                        for (int i = 0; i < WrestlerLogoDisplays.Length; i++)
                                        {
                                            if (WrestlerLogoDisplays[i] != null)
                                            {
                                                WrestlerCardNumber[i] = int.Parse(Regex.Replace(WrestlerLogoDisplays[i].GetComponent<Image>().sprite.name, "_banner", ""));
                                            }
                                        }
                                    }
                                }

                                else if (selector.name == "FlavorPanel")
                                {
                                    if (selector.transform.parent.gameObject.name == "SkitFlavorOptions")
                                    {
                                        //Debug.Log("Parent is: SkitFlavorOptions");
                                        GameObject[] WrestlerLogoDisplays = { null, null, null, null, null };
                                        WrestlerLogoDisplays[0] = GameObject.Find("Wrestler1Logo");
                                        WrestlerLogoDisplays[1] = GameObject.Find("Wrestler2Logo");
                                        WrestlerLogoDisplays[2] = GameObject.Find("Wrestler3Logo");
                                        WrestlerLogoDisplays[3] = GameObject.Find("Wrestler4Logo");
                                        WrestlerLogoDisplays[4] = GameObject.Find("Wrestler5Logo");
                                        for (int i = 0; i < WrestlerCardNumber.Length; i++)
                                        {
                                            if (WrestlerLogoDisplays[i] != null)
                                            {
                                                WrestlerCardNumber[i] = int.Parse(Regex.Replace(WrestlerLogoDisplays[i].GetComponent<Image>().sprite.name, "_banner", ""));
                                            }
                                        }
                                    }

                                    else if (selector.transform.parent.gameObject.name == "MicSpotFlavorOptions")
                                    {
                                        //Debug.Log("Parent is: SkitFlavorOptions");
                                        GameObject[] WrestlerLogoDisplays = { null, null, null, null, null };
                                        WrestlerLogoDisplays[0] = GameObject.Find("Wrestler1Logo");
                                        WrestlerLogoDisplays[1] = GameObject.Find("Wrestler2Logo");
                                        WrestlerLogoDisplays[2] = GameObject.Find("Wrestler3Logo");
                                        WrestlerLogoDisplays[3] = GameObject.Find("Wrestler4Logo");
                                        WrestlerLogoDisplays[4] = GameObject.Find("Wrestler5Logo");
                                        for (int i = 0; i < WrestlerCardNumber.Length; i++)
                                        {
                                            if (WrestlerLogoDisplays[i] != null)
                                            {
                                                WrestlerCardNumber[i] = int.Parse(Regex.Replace(WrestlerLogoDisplays[i].GetComponent<Image>().sprite.name, "_banner", ""));
                                            }
                                        }
                                    }
                                }


                                foreach (int item in WrestlerCardNumber)
                                {
                                    if (item != 0)
                                    {
                                        if (item == merch.unlockRequirements.MaxOut)
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                }
                            }
                            else if (filter == CardType.WRESTLER && ((currentFilter != CardFilter.WRESTLERS_BAD) && (currentFilter != (CardFilter.WRESTLERS_GOOD)) && (currentFilter != (CardFilter.WRESTLERS_MAIN_EVENT) && (currentFilter != (CardFilter.WRESTLERS_MIDCARDER)))))
                            {
                                //if (card.CardNumber == 0)
                                //{
                                //    filteredCards.Add(card);
                                //}
                                WrestlerCardObject Wrestler = (WrestlerCardObject)card;
                                if (Wrestler.cardVersion.ToUpper() != "FOIL")
                                {
                                    filteredCards.Add(card);
                                }
                                else if (Wrestler.cardVersion.ToUpper() == "FOIL")
                                {
                                    FoilCards.Add(Wrestler);
                                }
                            }
                            else
							{
								bool addCard = false;
								if (filter == CardType.MANAGER || filter == CardType.SKIT || filter == CardType.MIC_SPOT) 
								{
									if (card.IsActiveNow () != active) 
									{
										addCard = false;
									} 
									else 
									{
										addCard = true;
									}
								} 
								else if (filter == CardType.FEUD || filter == CardType.SPONSOR || filter == CardType.TAG_TEAM || filter == CardType.VENUE || filter == CardType.MATCH || filter == CardType.ALL)
								{
									if (!active) 
									{
										addCard = false;
									} 
									else 
									{
										addCard = true;
									}

								}
								
								if (addCard) 
								{
									filteredCards.Add(card);	
								}
                            }
                        }
                    }
                }
            }

            if (filteredCards.Count == 0) 
			{
//                // If there's no cards when using this filter, switch to the next one and try again, unless we're in card select mode
//                if (!isCardSelect) 
//				{
//                    SwitchCardFilter();
//                    return;
//                }
            }
        }
		if (!isFoil) 
		{
			cardList = filteredCards;
		} 
		else 
		{
			cardList = FoilCards;
		}

		cardList.Sort ((x, y) => string.Compare (x.name, y.name));

		if (CheckFilterType () [0] == CardType.MATCH) 
		{
			int wrestlingMatchIndex = cardList.FindIndex (o => o.CardNumber == 189);
			if (cardList.Count > 0) 
			{
				var wrestlingMatchCard = cardList [wrestlingMatchIndex];
				cardList.RemoveAt (wrestlingMatchIndex);
				cardList.Insert (0, wrestlingMatchCard);
			}
		}


		string filterOutput = "Show: ";
		filterOutput += GetCurrentFilterName ();
		filterText.text = filterOutput;

		if (cardList.Count == 0) 
		{
			noCardsFound.SetActive (true);
		} 
		else 
		{
			ShowCurrentCard();
		}
    }
		

    public string GetCurrentFilterName()
    {
        string name = "";

        switch (currentFilter) 
		{
            case CardFilter.WRESTLERS_ALL:
                name = "Wrestlers";
                break;
            case CardFilter.WRESTLERS_BAD:
                name = "Wrestlers Bad";
                break;
            case CardFilter.WRESTLERS_GOOD:
                name = "Wrestlers Good";
                break;
            case CardFilter.WRESTLERS_TWEENER:
                name = "Wrestlers Tweener";
                break;
            case CardFilter.WRESTLERS_MAIN_EVENT:
                name = "Wrestlers Main Event";
                break;
            case CardFilter.WRESTLERS_OPENER:
                name = "Wrestlers Opener";
                break;
            case CardFilter.WRESTLERS_UPPER_MIDCARD:
                name = "Wrestlers Upper Midcard";
                break;
            case CardFilter.WRESTLERS_LEGENDARY:
                name = "Wrestlers Legendary";
                break;
            case CardFilter.WRESTLERS_MIDCARDER:
                name = "Wrestlers Midcarder";
                break;
            case CardFilter.WRESTLERS_CHAMP:
                name = "Wrestlers Champ";
                break;
            case CardFilter.WRESTLERS_JOBBER:
                name = "Wrestlers Jobber";
                break;
            case CardFilter.VENUES:
                name = "Venues";
                break;
            case CardFilter.MATCHES:
                name = "Matches";
                break;
            case CardFilter.FLAVORS:
                name = "Flavors";
                break;
            case CardFilter.MIC_SPOTS:
                name = "Mic Spots";
                break;
            case CardFilter.SKITS:
                name = "Skits";
                break;
            case CardFilter.MANAGERS:
                name = "Managers";
                break;
            case CardFilter.SPONSORS:
                name = "Sponsors";
                break;
            case CardFilter.MERCH:
                name = "Merch";
                break;
            case CardFilter.TAG_TEAMS:
                name = "Tag Teams";
                break;
            case CardFilter.FEUDS:
                name = "Feuds";
                break;
            default:
                name = "All";
                break;
        }

        return name;
    }

	public string GetCurrentFilterNameHeading()
	{
		string name = "";

		switch (currentFilter) 
		{
		case CardFilter.WRESTLERS_ALL:
			name = "Wrestlers";
			break;
		case CardFilter.WRESTLERS_BAD:
			name = "Wrestlers Bad";
			break;
		case CardFilter.WRESTLERS_GOOD:
			name = "Wrestlers Good";
			break;
		case CardFilter.WRESTLERS_TWEENER:
			name = "Wrestlers Tweener";
			break;
		case CardFilter.WRESTLERS_MAIN_EVENT:
			name = "Wrestlers Main Event";
			break;
		case CardFilter.WRESTLERS_OPENER:
			name = "Wrestlers Opener";
			break;
		case CardFilter.WRESTLERS_UPPER_MIDCARD:
			name = "Wrestlers Upper Midcard";
			break;
		case CardFilter.WRESTLERS_LEGENDARY:
			name = "Wrestlers Legendary";
			break;
		case CardFilter.WRESTLERS_MIDCARDER:
			name = "Wrestlers Midcarder";
			break;
		case CardFilter.WRESTLERS_CHAMP:
			name = "Wrestlers Champ";
			break;
		case CardFilter.WRESTLERS_JOBBER:
			name = "Wrestlers Jobber";
			break;
		case CardFilter.VENUES:
			name = "Venues";
			break;
		case CardFilter.MATCHES:
			name = "Matches";
			break;
		case CardFilter.FLAVORS:
			name = "Flavors";
			break;
		case CardFilter.MIC_SPOTS:
			name = "Mic Spots";
			break;
		case CardFilter.SKITS:
			name = "Skits";
			break;
		case CardFilter.MANAGERS:
			name = "Managers";
			break;
		case CardFilter.SPONSORS:
			name = "Sponsors";
			break;
		case CardFilter.MERCH:
			name = "Merch";
			break;
		case CardFilter.TAG_TEAMS:
			name = "Tag Teams";
			break;
		case CardFilter.FEUDS:
			name = "Feuds";
			break;
		default:
			name = "All";
			break;
		}

		return name;
	}

    public CardType[] CheckFilterType()
    {
        switch (currentFilter) 
		{
            case CardFilter.WRESTLERS_ALL:
            case CardFilter.WRESTLERS_BAD:
            case CardFilter.WRESTLERS_GOOD:
            case CardFilter.WRESTLERS_MAIN_EVENT:
            case CardFilter.WRESTLERS_MIDCARDER:
            case CardFilter.WRESTLERS_UPPER_MIDCARD:
            case CardFilter.WRESTLERS_OPENER:
            case CardFilter.WRESTLERS_JOBBER:
            case CardFilter.WRESTLERS_CHAMP:
            case CardFilter.WRESTLERS_LEGENDARY:        
                return new CardType[] { CardType.WRESTLER };
            case CardFilter.VENUES:
                return new CardType[] { CardType.VENUE };
            case CardFilter.MATCHES:
                return new CardType[] { CardType.MATCH };
            case CardFilter.FLAVORS:
                return new CardType[] { CardType.FLAVOR, CardType.MANAGER, CardType.SPONSOR, CardType.MERCH, CardType.TAG_TEAM, CardType.FEUD };
            case CardFilter.MIC_SPOTS:
                return new CardType[] { CardType.MIC_SPOT };
            case CardFilter.SKITS:
                return new CardType[] { CardType.SKIT };
            case CardFilter.MANAGERS:
                return new CardType[] { CardType.MANAGER };
            case CardFilter.SPONSORS:
                return new CardType[] { CardType.SPONSOR };
            case CardFilter.MERCH:
                return new CardType[] { CardType.MERCH };
            case CardFilter.TAG_TEAMS:
                return new CardType[] { CardType.TAG_TEAM };
            case CardFilter.FEUDS:
                return new CardType[] { CardType.FEUD };
            default:
                return new CardType[] { CardType.WRESTLER };
        }
    }

	public IEnumerator FlashMessage(GameObject messageString) 
	{
        messageString.SetActive(true);

        yield return new WaitForSeconds(2f);

        messageString.SetActive(false);
    }

    public void ShowCurrentCard()
    {
		noCardsFound.SetActive (false);
        if (cardList.Count > 0)
        {
            if (cardList[currentCardIndex].myCardType == CardType.WRESTLER)
            {
                WrestlerCardObject wrestler = (WrestlerCardObject)cardList[currentCardIndex];
                if (wrestler.cardVersion.ToUpper() != "FOIL")
                {
                    cardViewer.ShowCard(cardList[currentCardIndex]);
                }
                else if (wrestler.cardVersion.ToUpper() == "FOIL" && GamePlaySession.instance.CheckCard(wrestler.CardNumber - 1) == null)
                {
                    cardViewer.ShowCard(cardList[currentCardIndex]);
                }
            }
            else if (cardList[currentCardIndex].myCardType != CardType.WRESTLER)
            {
                cardViewer.ShowCard(cardList[currentCardIndex]);
            }
        }
    }

    public CardObject GetCurrentCard() {
        return cardList[currentCardIndex];
    }

    public void UserSwipeRight() {
        int modNum = cardList.Count;
        currentCardIndex--;

        if (currentCardIndex < 0) {
            currentCardIndex = modNum - 1;
        } else {
            currentCardIndex = currentCardIndex % modNum;
        }
        ShowCurrentCard();
    }

    public void Slide(Slider value)
    {
        var index = cardList.FindIndex(x => x.name.ToLower().StartsWith(alphabet[(int)value.value]));
        currentCardIndex = index >= 0 ? index : currentCardIndex;
        ShowCurrentCard();
    }

    public void UserSwipeLeft() {
        int modNum = cardList.Count;
        currentCardIndex++;
        currentCardIndex = currentCardIndex % modNum;
        ShowCurrentCard();
    }

    //ALI AMEEN
    public void PromoteWrestlerStatus()
    {
		CardObject c = cardViewer.myCard;

        WrestlerCardObject w = (WrestlerCardObject)c;
        if (c.CardNumber == 700)
        {
            UIManagerScript.instance.CantDoThisScreen.gameObject.SetActive(true);
            UIManagerScript.instance.CantDoThisScreen.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "You can not promote Local Talent!";
            UIManagerScript.instance.CantDoThisScreen.gameObject.transform.SetAsLastSibling();
        }
        else if (w.pushCurrent == 20)
        {
            UIManagerScript.instance.CantDoThisScreen.gameObject.SetActive(true);
            UIManagerScript.instance.CantDoThisScreen.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "This wrestler has been promoted to the maximum limit!";
            UIManagerScript.instance.CantDoThisScreen.gameObject.transform.SetAsLastSibling();
        }

        else
        {
			if (GamePlaySession.instance.GetMyTokens() < 1)
            {
                UIManagerScript.instance.CantDoThisScreen.gameObject.SetActive(true);
                UIManagerScript.instance.CantDoThisScreen.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "You don't have enough Tokens for this!";
                UIManagerScript.instance.CantDoThisScreen.gameObject.transform.SetAsLastSibling();
            }
            else
            {
                GameObject managerScript = GameObject.Find("UIManager");
				CardObject card = cardViewer.myCard;
                WrestlerCardObject W = (WrestlerCardObject)card;
                bool isChamp = false;
                GamePlaySession G = GamePlaySession.instance;
				if ((W.CardNumber == (G.tagChampCardNumbers[0])) || (W.CardNumber == (G.tagChampCardNumbers[1])) || (W.CardNumber == (G.GetTitleChampCardNumber())) || (W.CardNumber == G.GetWorldChampCardNumber()))
                {
                    isChamp = true;
                }

                if (isChamp == false)
                {
					if (GamePlaySession.instance.GetMyTokens() < 1)
                    {

                    }

					else if (GamePlaySession.instance.GetMyTokens() >= 1)
                    {
						//Debug.Log(UIManager.name);
                        UIManager.GetComponent<UIManagerScript>().PlayerHUD.SetActive(true);
                        UIManager.GetComponent<UIManagerScript>().PromoteVerifyScreen.gameObject.SetActive(true);
                        UIManager.GetComponent<UIManagerScript>().PromoteVerifyScreen.gameObject.transform.SetAsLastSibling();
                        GameObject.Find("WrestlerImage").GetComponent<Image>().sprite = card.headshotImage;
                    }
                }
                else if (isChamp == true)
                {
                    UIManagerScript.instance.CantDoThisScreen.gameObject.SetActive(true);
                    UIManagerScript.instance.CantDoThisScreen.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "YOU can not demote a wrestler who is a champ!";
                    UIManagerScript.instance.CantDoThisScreen.gameObject.transform.SetAsLastSibling();
                }
            }
        }
    }
    public void DemoteWrestlerStatus()
    {
		CardObject c = cardViewer.myCard;
        WrestlerCardObject w = (WrestlerCardObject)c;
        if (c.CardNumber == 700)
        {
            UIManagerScript.instance.CantDoThisScreen.gameObject.SetActive(true);
            UIManagerScript.instance.CantDoThisScreen.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "You can not demote Local Talent!";
            UIManagerScript.instance.CantDoThisScreen.gameObject.transform.SetAsLastSibling();
        }
        else if (w.pushCurrent == 1)
        {
            UIManagerScript.instance.CantDoThisScreen.gameObject.SetActive(true);
            UIManagerScript.instance.CantDoThisScreen.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "This wrestler has been demoted to the maximum limit!";
            UIManagerScript.instance.CantDoThisScreen.gameObject.transform.SetAsLastSibling();
        }
        else
        {
			if (GamePlaySession.instance.GetMyTokens() < 1)
            {
                UIManagerScript.instance.CantDoThisScreen.gameObject.SetActive(true);
                UIManagerScript.instance.CantDoThisScreen.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "You don't have enough Tokens for this!";
                UIManagerScript.instance.CantDoThisScreen.gameObject.transform.SetAsLastSibling();
            }
            else
            {
                GameObject managerScript = GameObject.Find("UIManager");
				CardObject card = cardViewer.myCard;
                WrestlerCardObject W = (WrestlerCardObject)card;
                bool isChamp = false;
                GamePlaySession G = GamePlaySession.instance;
				if ((W.CardNumber == (G.tagChampCardNumbers[0])) || (W.CardNumber == (G.tagChampCardNumbers[1])) || (W.CardNumber == (G.GetTitleChampCardNumber())) || (W.CardNumber == G.GetWorldChampCardNumber()))
                {
                    isChamp = true;
                    //Debug.Log("IS CHAMP");
                }

                if (isChamp == false)
                {
                    UIManagerScript.instance.PlayerHUD.SetActive(true);
                    UIManagerScript.instance.DemoteVerifyScreen.gameObject.SetActive(true);
                    UIManagerScript.instance.DemoteVerifyScreen.gameObject.transform.SetAsLastSibling();
                    GameObject.Find("WrestlerImage").GetComponent<Image>().sprite = card.headshotImage;
                }
                else if (isChamp == true)
                {
                    UIManagerScript.instance.CantDoThisScreen.gameObject.SetActive(true);
                    UIManagerScript.instance.CantDoThisScreen.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "YOU can not demote a wrestler who is a champ!";
                    UIManagerScript.instance.CantDoThisScreen.gameObject.transform.SetAsLastSibling();
                }
            }
        }
    }
    public void ClosePromoteWrestlerStatus()
    {
        UIManagerScript.instance.PromoteVerifyScreen.gameObject.SetActive(false);
        UIManagerScript.instance.PlayerHUD.SetActive(false);
    }
    public void ConfirmPromoteWrestlerStatus()
    {
        //Debug.Log("PROMOTE WRESTLER STATUS CLICKED...");
		WrestlerCardObject card = (WrestlerCardObject)cardViewer.myCard;
        foreach (CardObject item in GamePlaySession.instance.myCards)
        {
            if (item.myCardType == CardType.WRESTLER)
            {
                WrestlerCardObject W = (WrestlerCardObject)item;
                if (W.CardNumber == card.CardNumber)
                {
                    if (W.pushCurrent < 20)
                    {
                        W.pushCurrent += 3;
                        if (W.pushCurrent >= 20)
                        {
                            W.pushCurrent = 20;
                        }
                    }
                    if (W.pushCurrent == 1)
                    {
                        W.characterStatus = "Jobber";
                    }
                    else if (W.pushCurrent > 1 && W.pushCurrent < 5)
                    {
                        W.characterStatus = "Opener";
                    }
                    else if (W.pushCurrent >= 5 && W.pushCurrent < 10)
                    {
                        W.characterStatus = "Midcard";
                    }
                    else if (W.pushCurrent >= 10 && W.pushCurrent < 15)
                    {
                        W.characterStatus = "Upper Midcard";
                    }
                    else if (W.pushCurrent >= 15 && W.pushCurrent < 20)
                    {
                        W.characterStatus = "Main Eventer";
                    }
                    else if (W.pushCurrent == 20)
                    {
                        W.characterStatus = "Legendary";
                    }
                    CardViewBackWrestler.instance.statusData.text = W.characterStatus.ToString();
                    CardViewBackWrestler.instance.statPush.text = W.pushCurrent.ToString();
                    UIManagerScript.instance.PromoteVerifyScreen.gameObject.SetActive(false);
					GamePlaySession.instance.SetMyTokens(GamePlaySession.instance.GetMyTokens() - 1);
                    UIManagerScript.instance.PlayerHUD.GetComponent<HUDScript>().UpdateHUD();
                    
                    UIManagerScript.instance.PlayerHUD.SetActive(false);
                }
            }
        }
    }
    
    public void ShowFoilCard()
    {
        WrestlerCardObject currentWrestler = (WrestlerCardObject)cardViewer.myCard;
		if (currentWrestler.cardVersion == "FOIL") 
		{
			if (GamePlaySession.instance.CheckCard (cardViewer.myCard.CardNumber - 1) != null) 
			{
				ShowCurrentCard (cardViewer.myCard, false);
			}
		} 
		else if (currentWrestler.cardVersion != "FOIL") 
		{
			if (GamePlaySession.instance.CheckCard(cardViewer.myCard.CardNumber + 1) != null)
			{
				ShowCurrentCard(cardViewer.myCard, true);
			}
			else
			{
				UIManagerScript.instance.CantDoThisScreen.gameObject.SetActive(true);
				UIManagerScript.instance.CantDoThisScreen.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "You don't have this wrestler's foil card!";
				UIManagerScript.instance.CantDoThisScreen.gameObject.transform.SetAsLastSibling();
			}
		}
    }
    public void ShowCurrentCard(CardObject card, bool Foil)
    {
        if (Foil == true)
        {
			if (cardList.Count > 0 && FoilCards.Count == 0) 
			{
				foreach (CardObject item in cardList) 
				{
					if (item.CardNumber == card.CardNumber + 1) 
					{
						cardViewer.ShowCard (item);
					}
				}
			} 
			else if (cardList.Count > 0 && FoilCards.Count > 0) 
			{
				foreach (CardObject item in FoilCards) 
				{
					if (item.CardNumber == card.CardNumber + 1) 
					{
						cardViewer.ShowCard (item);
					}
				}
			}
        }
        else if (Foil == false)
        {
            if (cardList.Count > 0)
            {
                foreach (CardObject item in cardList)
                {
                    if (item.CardNumber == card.CardNumber - 1)
                    {
                        cardViewer.ShowCard(item);
                    }
                }
            }
        }
    }
    public void CloseDemoteWrestlerStatus()
    {
        UIManagerScript.instance.DemoteVerifyScreen.gameObject.SetActive(false);
        UIManagerScript.instance.PlayerHUD.SetActive(false);
    }
    public void ConfirmDemoteWrestlerStatus()
    {
		WrestlerCardObject card = (WrestlerCardObject)cardViewer.myCard;

        foreach (CardObject item in GamePlaySession.instance.myCards)
        {
            if (item.myCardType == CardType.WRESTLER)
            {
                WrestlerCardObject W = (WrestlerCardObject)item;
                //Debug.Log(W.name + W.characterStatus);
                if (W.CardNumber == card.CardNumber)
                {
                    //Debug.Log("CARD FOUND: " + item.name);
                    if (W.pushCurrent > 1)
                    {
                        W.pushCurrent -= 3;
                        if (W.pushCurrent <= 1)
                        {
                            W.pushCurrent = 1;
                        }
                        if (W.pushCurrent == 1)
                        {
                            W.characterStatus = "Jobber";
                        }
                        else if (W.pushCurrent > 1 && W.pushCurrent < 5)
                        {
                            W.characterStatus = "Opener";
                        }
                        else if (W.pushCurrent >= 5 && W.pushCurrent < 10)
                        {
                            W.characterStatus = "Midcard";
                        }
                        else if (W.pushCurrent >= 10 && W.pushCurrent < 15)
                        {
                            W.characterStatus = "Upper Midcard";
                        }
                        else if (W.pushCurrent >= 15 && W.pushCurrent < 20)
                        {
                            W.characterStatus = "Main Event";
                        }
                        else if (W.pushCurrent == 20)
                        {
                            W.characterStatus = "Legendary";
                        }
                        CardViewBackWrestler.instance.statusData.text = W.characterStatus.ToString();
                    }
                    //CardViewBackWrestler.instance.statusData.text = W.characterStatus.ToString();
                }
            }
        }

        int newPush = (int.Parse(CardViewBackWrestler.instance.statPush.text.ToString())) - 3;
        if (newPush <= 1)
        {
            newPush = 1;
        }
        
        CardViewBackWrestler.instance.statPush.text = newPush.ToString();
        UIManagerScript.instance.DemoteVerifyScreen.gameObject.SetActive(false);
		GamePlaySession.instance.SetMyTokens(GamePlaySession.instance.GetMyTokens() - 1);
        UIManagerScript.instance.PlayerHUD.GetComponent<HUDScript>().UpdateHUD();

        UIManagerScript.instance.PlayerHUD.SetActive(false);
    }
	IEnumerator WaitForActive()
	{
		yield return new WaitUntil (() => gameObject.activeSelf == true);
		UIManager = GameObject.Find ("UIManager");
	}

	bool isActiveNow(WrestlerCardObject wrestler)
	{
		int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName);
		if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear) 
		{
			return true;	
		}
		return false;

	}

}
