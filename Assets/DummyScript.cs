﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class DummyScript : MonoBehaviour 
{
	public string email;
	public string password;

	public void LoadFromOldServer()
	{
		StartCoroutine (LoadSession ());
	}

	IEnumerator LoadSession()
	{

		string url = "http://bugdevstudios.com/wrestling/server.php?REQUEST=GET_GPS_BY_CREDENTIALS&EMAIL="+email.ToString()+"&PASSWORD="+password.ToString();
		WWW www = new WWW (url);

		yield return www;

		if (www.error != null)
		{
			Debug.Log(www.error);
			yield break;
		}

		ProcessResponse (www.text);
	}


	void ProcessResponse(string response)
	{
		var N = JSON.Parse(response);
		var responseStatus = N["CODE"].AsInt;
		var data = N["GPS"].Value;
		var lastUpdate = N["GPS_UPDATED"].Value;
		var status = N["STATUS"].Value;

		GamePlaySession.instance = GamePlaySession.Load(data);
		GamePlaySession.instance.GSI.GetComponent<UIManagerScript>().PlayerHUD.GetComponent<HUDScript>().SetCommishImage();
		GamePlaySession.instance.GSI.GetComponent<UIManagerScript>().PlayerHUD.GetComponent<HUDScript>().SetFedImage();

		ParseManager.LogInUser (email, password);
		UIManagerScript.instance.SendToParse ();
	}

}
