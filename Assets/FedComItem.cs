using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

[System.Serializable]
public class FedComItem {
	public Sprite sprite;
	public Sprite littleSprite, littleSprite90s;
	public int fedCost;
	public int fedTokenCost;
	public BonusValue[] bonuses;

	[Multiline]
	public string NameString;
	[Multiline]
	public string BonusString;
    [Multiline]
    public string Acronym;
}
