﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SwipeInteraction : MonoBehaviour {

	public float comfortZoneVerticalSwipe = 50; // the vertical swipe will have to be inside a 50 pixels horizontal boundary
	public float comfortZoneHorizontalSwipe = 50; // the horizontal swipe will have to be inside a 50 pixels vertical boundary
	float minSwipeDistance = 14; // the swipe distance will have to be longer than this for it to be considered a swipe
	float maxSwipeTime = .75f;
	//the following 4 variables are used in some cases that I don’t want my character to be allowed to move on the board (it’s a board game)
	bool allowGoUp = true;
	bool allowGoRight = true;
	bool allowGoLeft = true;
	bool allowGoDown = true; 

	Vector2 startPos;
	Vector2 endPos;

	public GameObject cardViewer;

	float startTime;
	bool moving;

	public void Update () {
		/*if (!moving && (Input.touchCount > 0)) {
			//Debug.Log ("I am touching the screen");
			startPos = Input.GetTouch(0).position;	

			Debug.Log(startPos.ToString());
			moving = true;
		}
		else if (!moving && (Input.touchCount > 0 || Input.GetMouseButtonDown (0))) {
			//Debug.Log ("I am touching the screen");
			startPos = Input.mousePosition;
			Debug.Log(startPos.ToString());
			moving = true;
			
		} 
		else if (moving) {
			if(Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Ended){
				endPos = Input.GetTouch(0).position;
				GetSwipeDirection();

			}
			else if(Input.GetMouseButtonUp(0)){
				endPos = Input.mousePosition;
				GetSwipeDirection();
			}



		}*/
	}

	public void GetStartPosition(BaseEventData data){
		PointerEventData pointerData = data as PointerEventData;

		startPos = pointerData.position;
	}

	public void GetEndPosition(BaseEventData data){
		PointerEventData pointerData = data as PointerEventData;
		
		endPos = pointerData.position;
		GetSwipeDirection();
	}
	public void GetSwipeDirection(){

				Vector3 delta = endPos - startPos;
		if (Mathf.Abs(delta.normalized.x) > Mathf.Abs(delta.normalized.y)) {
			//horizontal swipe
			if(delta.x < 0 && Mathf.Abs(delta.x) > comfortZoneHorizontalSwipe){
				//Debug.Log("horixontal swipes LEFT:" +delta.x);
				cardViewer.SendMessage("UserSwipeLeft");
			}
			else if(delta.x > 0 && Mathf.Abs(delta.x) > comfortZoneHorizontalSwipe){
				//Debug.Log("horixontal swipes RIGHT:" +delta.x);
				cardViewer.SendMessage("UserSwipeRight");
			}

		} else {
			//vertical swipe
					//Debug.Log("vertical swipe");
		}
		//moving = false;
	}
}
