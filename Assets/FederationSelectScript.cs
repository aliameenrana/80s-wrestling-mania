﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FederationSelectScript : MonoBehaviour 
{
	public enum SetupMenuType{
		FEDERATIONS, 
		COMMISIONERS
	}
	private FedComItem[] federations;

	public SetupMenuType menuType;
	public GameObject fedDescription;
	public GameObject notEnoughString;

	private int currentFed;
	private GameObject hud;

	// Use this for initialization
	void Start () {
		hud = GameObject.Find("UIManager").GetComponent<UIManagerScript>().PlayerHUD;

		currentFed = 0;
		if(menuType == SetupMenuType.FEDERATIONS)
			federations = GameObject.Find("UIManager").GetComponent<GameSetupItems>().Federations;
		else
			federations = GameObject.Find("UIManager").GetComponent<GameSetupItems>().Commisioners;

			fedDescription.GetComponent<Image> ().sprite = federations[currentFed].sprite;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void UserSwipeRight(){
		int modNum = federations.Length;
		currentFed--;

		if (currentFed < 0) {
			currentFed = modNum - 1;
		} else {
			currentFed = currentFed % modNum;
		}
		fedDescription.GetComponent<Image> ().sprite = federations[currentFed].sprite;
	}

	public void UserSwipeLeft(){
		int modNum = federations.Length;
		currentFed++;
		currentFed = currentFed % modNum;

		fedDescription.GetComponent<Image> ().sprite = federations[currentFed].sprite;
	}

	public void SetSmallSprites(GameObject parent)
    {
		GraphicManipulation[] images = parent.GetComponentsInChildren<GraphicManipulation>();

		foreach (GraphicManipulation image in images)
        {
			image.SetImageSprite(federations[currentFed].littleSprite);
		}
	}

	public void SetCosts(GameObject parent){
		parent.BroadcastMessage("ItemCost", federations[currentFed].fedCost,SendMessageOptions.DontRequireReceiver);
		parent.BroadcastMessage("ItemTokenCost", federations[currentFed].fedTokenCost,SendMessageOptions.DontRequireReceiver);
		parent.BroadcastMessage("ShowMyCash", SendMessageOptions.DontRequireReceiver);
		parent.BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
	}

	public void BuyFederationCheck(){
		bool check1 = false;
		bool check2 = false;
		
		int currCash = GamePlaySession.instance.GetMyCash();
		int currTokens = GamePlaySession.instance.GetMyTokens();
		
		if(currCash >= federations[currentFed].fedCost || federations[currentFed].fedCost <= 0){
			check1 = true;
		}
		if(currTokens >= federations[currentFed].fedTokenCost || federations[currentFed].fedTokenCost <= 0){
			check2 = true;
		}
		
		if(check1 && check2){
			GameObject.Find("UIManager").GetComponent<UIManagerScript>().VerifyFed();
		}
		else{
			StartCoroutine(FlashMessage(notEnoughString));
		}
	}

	public void BuyComissionerCheck(){
		bool check1 = false;
		bool check2 = false;

		int currCash = GamePlaySession.instance.GetMyCash();
		int currTokens = GamePlaySession.instance.GetMyTokens();

		if(currCash >= federations[currentFed].fedCost || federations[currentFed].fedCost <= 0){
			check1 = true;
		}
		if(currTokens >= federations[currentFed].fedTokenCost || federations[currentFed].fedTokenCost <= 0){
			check2 = true;
		}

		if(check1 && check2){

			GameObject.Find("UIManager").GetComponent<UIManagerScript>().VerifyCommish();
		}
		else{
			StartCoroutine(FlashMessage(notEnoughString));
		}
	}

	public void BuyFederation()
    {
		int currCash = GamePlaySession.instance.GetMyCash();
		int currTokens = GamePlaySession.instance.GetMyTokens();
		
		GamePlaySession.instance.SetSelectedFederation(currentFed);
		GamePlaySession.instance.SetMyCash(currCash - federations[currentFed].fedCost);
		GamePlaySession.instance.SetMyTokens(currTokens - federations[currentFed].fedTokenCost);

        hud.GetComponent<HUDScript>().SetFedImage();
		hud.GetComponent<HUDScript>().UpdateHUD();
		//GamePlaySession.instance.SaveAll();

		SetCommishBonus();

		this.gameObject.SetActive(false);

	}

	public void BuyCommisioner()
    {
		int currCash = GamePlaySession.instance.GetMyCash();
		int currTokens = GamePlaySession.instance.GetMyTokens();

		GamePlaySession.instance.SetSelectedComissioner(currentFed);
		GamePlaySession.instance.SetMyCash(currCash - federations[currentFed].fedCost);
		GamePlaySession.instance.SetMyTokens(currTokens - federations[currentFed].fedTokenCost);

		hud.GetComponent<HUDScript>().SetCommishImage();
		hud.GetComponent<HUDScript>().UpdateHUD();
		//GamePlaySession.instance.SaveAll();

		SetCommishBonus();
	}

	IEnumerator FlashMessage(GameObject messageString){
		messageString.SetActive(true);

		yield return new WaitForSeconds (3f);

		messageString.SetActive(false);
	}

	public void SetCommishBonus(){
		foreach(BonusValue bonus in federations[currentFed].bonuses)
		{
			GamePlaySession.instance.myBonuses.Add(bonus);
			GamePlaySession.instance.SetBonusValues (GamePlaySession.instance.myBonuses);
		}
	}
}
