﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SoundToggle : MonoBehaviour
{

    public Text musicToggle;
    public Text sfxToggle;
    public Text TitleScreenMusicToggle;
    public Text TitleScreenSfxToggle;
    public Text MyFedScreenMusicToggle;
    public Text MyFedScreenSfxToggle;
    public AudioClip Theme80s;
    public AudioClip Theme90s;


    public GameObject UIManager;

    void Awake()
    {
        UIManager = GameObject.Find("UIManager");
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public void MusicToggle()
    {
        musicToggle = GameObject.Find("MusicButton").transform.GetChild(0).gameObject.GetComponent<Text>();
        sfxToggle = GameObject.Find("SFXButton").transform.GetChild(0).gameObject.GetComponent<Text>();
        if (musicToggle.text.Contains("ON"))
        {
            musicToggle.text = "Music OFF!";
            MyFedScreenMusicToggle.text = "Music OFF!";
            TitleScreenMusicToggle.text = "Music OFF!";
            UIManager.GetComponent<AudioSource>().enabled = false;
        }
        else
        {
            musicToggle.text = "Music ON!";
            MyFedScreenMusicToggle.text = "Music ON!";
            TitleScreenMusicToggle.text = "Music ON!";
            UIManager.GetComponent<AudioSource>().enabled = true;
            UIManager.GetComponent<AudioSource>().Play();
        }
    }

    // Update is called once per frame
    public void SFXToggle()
    {
        musicToggle = GameObject.Find("MusicButton").transform.GetChild(0).gameObject.GetComponent<Text>();
        sfxToggle = GameObject.Find("SFXButton").transform.GetChild(0).gameObject.GetComponent<Text>();
        if (sfxToggle.text.Contains("ON"))
        {
            sfxToggle.text = "SFX OFF!";
            TitleScreenSfxToggle.text = "SFX OFF!";
            MyFedScreenSfxToggle.text = "SFX OFF!";

            UIManager.GetComponent<UIManagerScript>().sfxEnabled = false;
        }
        else
        {
            sfxToggle.text = "SFX ON!";
            TitleScreenSfxToggle.text = "SFX ON!";
            MyFedScreenSfxToggle.text = "SFX ON!";
            UIManager.GetComponent<UIManagerScript>().sfxEnabled = true;
            //TODO: Play a click sound
        }
    }

    public void Play80sTheme()
    {
        UIManager.GetComponent<AudioSource>().clip = Theme80s;

        UIManager.GetComponent<AudioSource>().Play();
    }

    public void Play90sTheme()
    {
        UIManager.GetComponent<AudioSource>().clip = Theme90s;
        UIManager.GetComponent<AudioSource>().Play();
    }
}
