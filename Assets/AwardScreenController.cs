﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AwardScreenController : MonoBehaviour
{
    public static AwardScreenController instance;

    public GameObject WrestlerOfTheYear;
    public GameObject TagTeamOfTheYear;
    public GameObject MostPopularWrestler;
    public GameObject MostHatedWrestler;
    public GameObject GiveAwardsButton;
    public Text[] Texts;


    public bool WYSelected, TTSelected, MPWSelected, MHWSelected;

    public CardObject[] AwardHolders = new CardObject[4];

    void Awake()
    {
        instance = this;
    }

    public void ConfirmAwards()
    {
		int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.GetCurrentYear()].yearName);
        List<CardObject> ThisYearsAwards = new List<CardObject>();
        
        foreach (CardObject item in AwardScreenController.instance.AwardHolders)
        {
            ThisYearsAwards.Add(item);
        }
        GamePlaySession.instance.Awards[ThisYear - 1980] = ThisYearsAwards;
        List<CardObject> AwardCards = GamePlaySession.instance.Awards[ThisYear - 1980];
    }
}
