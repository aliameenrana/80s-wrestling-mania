using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using Parse;

public enum PurchaseResult{
	NOT_ENOUGH_CASH,
	NOT_ENOUGH_TOKENS,
	PURCHASE_COMPLETED,
	ERROR
}

[Serializable]
public class GamePlaySession
{
	[XmlIgnore, System.NonSerialized]
	public static GamePlaySession instance = null;

	[System.NonSerialized]
	[XmlIgnore]
	public GameSetupItems GSI;

	[System.NonSerialized]
	[XmlIgnore]
	public const short MAX_BLURBS = 500;


	public int selectedFederation;
	public int selectedComissioner;
	public int myCash;
	public int myTokens;
	public int myLevel;
	public int shownBlurbCount;
	public int storeCardsBought;
	public int currentYear;
	public int currentMonth;
	public int numberOfYears;
	public int worldChampCardNumber;
	public int titleChampCardNumber;
	public int worldChampLastCardNumber;
	public int titleChampLastCardNumber;
	public float averageRating;
	public bool inTutorialMode;
	public bool flavorTutorialClear;
	public bool calendarTutorialClear;
	public bool resultTutorialClear;
	public bool storePurchaseTutorialClear;
	public bool storeRefreshTutorialClear;
	public bool storeTutorialClear;
	public bool transitionMessageClear;
	public bool newGamePlus;
	public bool firstShopCards;
	public bool gameCompleted;
	public bool fantasyPopUpShown;
	public bool transferData;
	public bool isNewPlayer;
	public DateTime lastShopRefresh;

	public List<int> RatingPositionHolder;
	public List<float> FedRatings;
	public List<CardObject>[] Awards;


	public bool[] storePurchases;
	public int[] tagChampCardNumbers;
	public int[] tagChampLastCardNumbers;

	[System.NonSerialized]
	[XmlIgnore]
	public WrestlerCardObject worldChampion;

	[System.NonSerialized]
	[XmlIgnore]
	public WrestlerCardObject titleChampion;

	[System.NonSerialized]
	[XmlIgnore]
	public WrestlerCardObject[] tagChampions;

	[System.NonSerialized]
	[XmlIgnore]
	public WrestlerCardObject worldLastChampion;

	[System.NonSerialized]
	[XmlIgnore]
	public WrestlerCardObject titleLastChampion;

	[System.NonSerialized]
	[XmlIgnore]
	public WrestlerCardObject[] tagLastChampions;

	[XmlArray("MyBonuses"),XmlArrayItem("Bonus")]
	public List<BonusValue> myBonuses;

	public List<BonusValue> GetBonusValues()
	{
		return myBonuses;
	}	

	public void SetBonusValues(List<BonusValue> value)
	{
		int pop=0;
		int mic=0;
		int skill=0;
		int plusCash=0;

		foreach (var item in myBonuses) 
		{
			if (item.bType == BonusValue.BonusType.POP) 
			{
				pop = item.value;
			}

			if (item.bType == BonusValue.BonusType.MIC) 
			{
				mic = item.value;
			}

			if (item.bType == BonusValue.BonusType.SKILL) 
			{
				skill = item.value;
			}

			if (item.bType == BonusValue.BonusType.PLUS_CASH) 
			{
				plusCash = item.value;
			}
		}

		BonusValue bv1 = new BonusValue ();
		BonusValue bv2 = new BonusValue ();
		BonusValue bv3 = new BonusValue ();
		BonusValue bv4 = new BonusValue ();

		bv1.bType = BonusValue.BonusType.POP;bv1.value = pop;
		bv2.bType = BonusValue.BonusType.MIC;bv2.value = mic;
		bv3.bType = BonusValue.BonusType.SKILL;bv3.value = skill;
		bv4.bType = BonusValue.BonusType.PLUS_CASH;bv4.value = plusCash;

		myBonuses.Clear ();
		myBonuses.Add (bv1);myBonuses.Add (bv2);myBonuses.Add (bv3);myBonuses.Add (bv4);
		List<int> bonuses = new List<int> ();
		bonuses.Add (bv1.value);
		bonuses.Add (bv2.value);
		bonuses.Add (bv3.value);
		bonuses.Add (bv4.value);
		ParseManager.UpdateGameSessionData ("myBonuses", bonuses);
	}

	[XmlArray("MyCards"),XmlArrayItem("Card")]
	public List<CardObject> myCards;

	[XmlArray("MyShows"),XmlArrayItem("Show")]
	public List<ShowObject> myShows;

	[XmlElement("MyCalendar")]
	public CalendarObject myCalendar;

	[XmlArray("ShownBlurbs"),XmlArrayItem("Blurb")]
	public List<int> shownBlurbs;

	[XmlArray("StoreCards"),XmlArrayItem("Card")]
	public List<CardObject> storeCards = new List<CardObject>();

	[System.NonSerialized]
	[XmlIgnore]
	public SegmentObject currentSegment;

	public void UploadGameSession()
	{
		var dic = new Dictionary<string, object>();

		dic.Add("selectedFederation", selectedFederation);
		dic.Add("selectedComissioner", selectedComissioner);
		dic.Add("myCash", myCash);
		dic.Add("myTokens", myTokens);
		dic.Add("myLevel", myLevel);
		dic.Add("shownBlurbCount", shownBlurbCount);
		dic.Add("isNewPlayer", isNewPlayer);
		dic.Add("inTutorialMode", inTutorialMode);
		dic.Add("flavorTutorialClear", flavorTutorialClear);
		dic.Add("calendarTutorialClear", calendarTutorialClear);
		dic.Add("resultTutorialClear", resultTutorialClear);
		dic.Add("storePurchaseTutorialClear", storePurchaseTutorialClear);
		dic.Add("storeRefreshTutorialClear", storeRefreshTutorialClear);
		dic.Add("storeTutorialClear", storeTutorialClear);
		dic.Add("transitionMessageClear", transitionMessageClear);
		dic.Add("storeCardsBought", storeCardsBought);
		dic.Add("currentMonth", currentMonth);
		dic.Add("currentYear", currentYear);
		dic.Add("numberOfYears", numberOfYears);
		dic.Add("worldChampCardNumber", worldChampCardNumber);
		dic.Add("titleChampCardNumber", titleChampCardNumber);
		dic.Add("worldChampLastCardNumber", worldChampLastCardNumber);
		dic.Add("titleChampLastCardNumber", titleChampLastCardNumber);
		dic.Add("newGamePlus", newGamePlus);
		dic.Add("firstShopCards", firstShopCards);
		dic.Add("gameCompleted", gameCompleted);
		dic.Add("fantasyPopUpShown", fantasyPopUpShown);
		dic.Add("transferData", transferData);
		dic.Add("averageRating", averageRating);
		dic.Add("lastShopRefresh", lastShopRefresh);
		dic.Add("tagChampCardNumbers", tagChampCardNumbers);

		int startingYear = int.Parse(GamePlaySession.instance.myCalendar.years[0].yearName);
		dic.Add ("startingYear", startingYear);

		List<string> awards = new List<string>();

		if (GamePlaySession.instance.Awards!=null)
		{
			if (GamePlaySession.instance.Awards.Length!=0)
			{
				for(int i=0;i<20;i++)
				{
					string thisYearsAwards = "";
					if (GamePlaySession.instance.Awards[i]!=null)
					{
						foreach (var item in GamePlaySession.instance.Awards[i]) 
						{
							if (thisYearsAwards=="")
							{
								thisYearsAwards += item.CardNumber.ToString();
							}
							else
							{
								thisYearsAwards += "," + item.CardNumber.ToString();
							}
						}
					}
					awards.Add(thisYearsAwards);
				}
			}
		}

		List<int> myCardIDs = new List<int> ();

		foreach (var item in myCards) 
		{
			myCardIDs.Add (item.CardNumber);
		}




		dic.Add ("myCardIDs", myCardIDs);
		dic.Add("awards",awards);



		int pop=0;
		int mic=0;
		int skill=0;
		int plusCash=0;

		foreach (var item in GamePlaySession.instance.myBonuses) 
		{
			if (item.bType == BonusValue.BonusType.POP) 
			{
				pop = item.value;
			}

			if (item.bType == BonusValue.BonusType.MIC) 
			{
				mic = item.value;
			}

			if (item.bType == BonusValue.BonusType.SKILL) 
			{
				skill = item.value;
			}

			if (item.bType == BonusValue.BonusType.PLUS_CASH) 
			{
				plusCash = item.value;
			}
		}

		BonusValue bv1 = new BonusValue ();
		BonusValue bv2 = new BonusValue ();
		BonusValue bv3 = new BonusValue ();
		BonusValue bv4 = new BonusValue ();

		bv1.bType = BonusValue.BonusType.POP;bv1.value = pop;
		bv2.bType = BonusValue.BonusType.MIC;bv2.value = mic;
		bv3.bType = BonusValue.BonusType.SKILL;bv3.value = skill;
		bv4.bType = BonusValue.BonusType.PLUS_CASH;bv4.value = plusCash;

		GamePlaySession.instance.myBonuses.Clear ();
		GamePlaySession.instance.myBonuses.Add (bv1);
		GamePlaySession.instance.myBonuses.Add (bv2);
		GamePlaySession.instance.myBonuses.Add (bv3);
		GamePlaySession.instance.myBonuses.Add (bv4);
		List<int> bonuses = new List<int> ();
		bonuses.Add (bv1.value);
		bonuses.Add (bv2.value);
		bonuses.Add (bv3.value);
		bonuses.Add (bv4.value);

		dic.Add("myBonuses",bonuses);

		PlayerPrefs.SetString ("parseUserID", Parse.ParseUser.CurrentUser.ObjectId);

		ParseManager.UpdateGameSessionData(dic);


	}

	public int GetSelectedFederation(){
		return selectedFederation;
	}
	public void SetSelectedFederation(int value){
		selectedFederation = value;
//		ParseManager.UpdateGameSessionData ("selectedFederation", value);
	}
	public int GetSelectedComissioner(){
		return selectedComissioner;
	}
	public void SetSelectedComissioner(int value){
		selectedComissioner = value;
//		ParseManager.UpdateGameSessionData ("selectedComissioner", value);
	}
	public int GetMyCash(){
		return myCash;
	}
	public void SetMyCash(int value){
		myCash = value;
	}
	public int GetMyTokens(){
		return myTokens;
	}
	public void SetMyTokens(int value){
		myTokens = value;
	}
	public int GetMyLevel(){
		return myLevel;
	}
	public void SetMyLevel(int value){
		myLevel = value;
//		ParseManager.UpdateGameSessionData ("myLevel", value);
	}
	public int GetShownBlurbCount(){
		return shownBlurbCount;
	}
	public void SetShownBlurbCount(int value){
		shownBlurbCount = value;
//		ParseManager.UpdateGameSessionData ("shownBlurbCount", value);
	}
	public bool GetIsNewPlayer(){
		return isNewPlayer;
	}
	public void SetIsNewPlayer(bool value){
		isNewPlayer = value;
//		ParseManager.UpdateGameSessionData ("isNewPlayer", value);
	}
	public bool GetInTutorialMode(){
		return inTutorialMode;
	}
	public void SetInTutorialMode(bool value){
		inTutorialMode = value;
//		ParseManager.UpdateGameSessionData ("inTutorialMode", value);
	}
	public bool GetFlavorTutorialClear(){
		return flavorTutorialClear;
	}
	public void SetFlavorTutorialClear(bool value){
		flavorTutorialClear = value;
//		ParseManager.UpdateGameSessionData ("flavorTutorialClear", value);
	}
	public bool GetCalendarTutorialClear(){
		return calendarTutorialClear;
	}
	public void SetCalendarTutorialClear(bool value){
		calendarTutorialClear = value;
//		ParseManager.UpdateGameSessionData ("calendarTutorialClear", value);
	}
	public bool GetResultTutorialClear(){
		return resultTutorialClear;
	}
	public void SetResultTutorialClear(bool value){
		resultTutorialClear = value;
//		ParseManager.UpdateGameSessionData ("resultTutorialClear", value);
	}
	public bool GetStorePurchaseTutorialClear(){
		return storePurchaseTutorialClear;
	}
	public void SetStorePurchaseTutorialClear(bool value){
		storePurchaseTutorialClear = value;
//		ParseManager.UpdateGameSessionData ("storePurchaseTutorialClear", value);
	}
	public bool GetStoreRefreshTutorialClear(){
		return storeRefreshTutorialClear;
	}
	public void SetStoreRefreshTutorialClear(bool value){
		storeRefreshTutorialClear = value;
//		ParseManager.UpdateGameSessionData ("storeRefreshTutorialClear", value);
	}
	public bool GetStoreTutorialClear(){
		return storeTutorialClear;
	}
	public void SetStoreTutorialClear(bool value){
		storeTutorialClear = value;
//		ParseManager.UpdateGameSessionData ("storeTutorialClear", value);
	}
	public bool GetTransitionMessageClear(){
		return transitionMessageClear;
	}
	public void SetTransitionMessageClear(bool value){
		transitionMessageClear = value;
//		ParseManager.UpdateGameSessionData ("transitionMessageClear", value);
	}
	public int GetStoreCardsBought(){
		return storeCardsBought;
	}
	public void SetStoreCardsBought(int value){
		storeCardsBought = value;
//		ParseManager.UpdateGameSessionData ("storeCardsBought", value);
	}
	public int GetCurrentYear(){
		return currentYear;
	}
	public void SetCurrentYear(int value){
		currentYear = value;
//		ParseManager.UpdateGameSessionData ("currentYear", value);
	}
	public int GetCurrentMonth(){
		return currentMonth;
	}
	public void SetCurrentMonth(int value){
		currentMonth = value;
//		ParseManager.UpdateGameSessionData ("currentMonth", value);
	}
	public int GetNumberOfYears(){
		return numberOfYears;
	}
	public void SetNumberOfYears(int value){
		numberOfYears = value;
//		ParseManager.UpdateGameSessionData ("numberOfYears", value);
	}
	public int GetWorldChampCardNumber(){
		return worldChampCardNumber;
	}
	public void SetWorldChampCardNumber(int value){
		worldChampCardNumber = value;
//		ParseManager.UpdateGameSessionData ("worldChampCardNumber", value);
	}
	public int GetTitleChampCardNumber(){
		return titleChampCardNumber;
	}
	public void SetTitleChampCardNumber(int value){
		titleChampCardNumber = value;
//		ParseManager.UpdateGameSessionData ("titleChampCardNumber", value);
	}
	public int GetWorldChampLastCardNumber(){
		return worldChampLastCardNumber;
	}
	public void SetWorldChampLastCardNumber(int value){
		worldChampLastCardNumber = value;
//		ParseManager.UpdateGameSessionData ("worldChampLastCardNumber", value);
	}
	public int GetTitleChampLastCardNumber(){
		return titleChampLastCardNumber;
	}
	public void SetTitleChampLastCardNumber(int value){
		titleChampLastCardNumber = value;
//		ParseManager.UpdateGameSessionData ("titleChampLastCardNumber", value);
	}
	public bool GetNewGamePlus(){
		return newGamePlus;
	}
	public void SetNewGamePlus(bool value){
		newGamePlus = value;
//		ParseManager.UpdateGameSessionData ("newGamePlus", value);
	}
	public bool GetFirstShopCards(){
		return firstShopCards;
	}
	public void SetFirstShopCards(bool value){
		firstShopCards = value;
//		ParseManager.UpdateGameSessionData ("firstShopCards", value);
	}
	public bool GetGameCompleted(){
		return gameCompleted;
	}
	public void SetGameCompleted(bool value){
		gameCompleted = value;
//		ParseManager.UpdateGameSessionData ("gameCompleted", value);
	}
	public bool GetFantasyPopUpShown(){
		return fantasyPopUpShown;
	}
	public void SetFantasyPopUpShown(bool value){
		fantasyPopUpShown = value;
//		ParseManager.UpdateGameSessionData ("fantasyPopUpShown", value);
	}
	public bool GetTransferData(){
		return transferData;
	}
	public void SetTransferData(bool value){
		transferData = value;
//		ParseManager.UpdateGameSessionData ("transferData", value);
	}
	public DateTime GetLastShopRefresh(){
		return lastShopRefresh;
	}
	public void SetLastShopRefresh(DateTime value){
		lastShopRefresh= value;
//		ParseManager.UpdateGameSessionData ("lastShopRefresh", value);
	}
	public float GetAverageRating(){
		averageRating = 0f;
		if (myShows.Count > 0) {
			foreach (ShowObject show in myShows) {
				averageRating += show.showRating;
			}
			averageRating /= myShows.Count;
		}
		return averageRating;
	}
	public void SetAverageRating(float value){
		averageRating = value;
//		ParseManager.UpdateGameSessionData ("averageRating", value);
	}

	public void SetTagChampCardNumbers(int[] value)
	{
		tagChampCardNumbers [0] = value [0];
		tagChampCardNumbers [1] = value [1];
	}

		
    public GamePlaySession()
    {
		selectedFederation = -1;
		selectedComissioner = -1;
		myCash = 0;
		myTokens = 0;
		myLevel = 0;
		shownBlurbCount = 0;
		storeCardsBought = 0;
		currentYear = 0;
		currentMonth = 0;
		averageRating = 0;
		numberOfYears = 20;
		worldChampCardNumber = -1;
		titleChampCardNumber = -1;
		worldChampLastCardNumber = -1;
		titleChampLastCardNumber = -1;
		flavorTutorialClear = false;
		calendarTutorialClear = false;
		resultTutorialClear = false;
		storePurchaseTutorialClear = false;
		storeRefreshTutorialClear = false;
		storeTutorialClear = false;
		transitionMessageClear = false;
		newGamePlus = false;
		firstShopCards = false;
		gameCompleted = false;
		fantasyPopUpShown = false;
		transferData = false;
		inTutorialMode = true;
		isNewPlayer = true;
		lastShopRefresh = DateTime.Now;

		storePurchases = new bool[3];
		tagChampCardNumbers = new int[2];
		tagChampLastCardNumbers = new int[2];
        shownBlurbs = new List<int>();
		RatingPositionHolder = new List<int>();
		FedRatings = new List<float>();
        myCards = new List<CardObject>();
        myShows = new List<ShowObject>();
        myBonuses = new List<BonusValue>();

        tagChampCardNumbers = new int[2];
        tagChampLastCardNumbers = new int[2];

        storeCards = new List<CardObject>();
        Awards = new List<CardObject>[20];
    }

	public void InitializeSession()
	{
		myCalendar = new CalendarObject();
		numberOfYears = myCalendar.GetNumberOfYears();
		myCalendar = myCalendar.GenerateCalendar();

		UploadGameSession();
	}

	public GamePlaySession (GamePlaySession other)
	{
		selectedFederation = other.selectedFederation;
		selectedComissioner = other.selectedComissioner;
		myBonuses = other.myBonuses;
		isNewPlayer = other.isNewPlayer;
		inTutorialMode = other.inTutorialMode;
		storeCardsBought = other.storeCardsBought;
		storePurchases = other.storePurchases;
		averageRating = other.averageRating;
		myLevel = other.myLevel;

		lastShopRefresh = other.lastShopRefresh;

		shownBlurbs = other.shownBlurbs;

		myCards = other.myCards;
		myShows = other.myShows;
		myBonuses = other.myBonuses;

		myCalendar = other.myCalendar;
		numberOfYears = other.numberOfYears;
		myCalendar = other.myCalendar;

		tagChampCardNumbers = other.tagChampCardNumbers;
		tagChampLastCardNumbers = other.tagChampLastCardNumbers;

		storeCards = other.storeCards;

		RatingPositionHolder = other.RatingPositionHolder;
		FedRatings = other.FedRatings;

		Awards = other.Awards;
		fantasyPopUpShown = other.fantasyPopUpShown;
	}
    
	public void SaveAll(bool upload=false, bool overrideSaving = false, bool fromButton = false)
	{
		try
		{
			if (worldChampion != null) 
			{
				SetTitleChampCardNumber (titleChampion.CardNumber);
				SetWorldChampCardNumber (worldChampion.CardNumber);
				SetTagChampCardNumbers (new int[2]{ tagChampions [0].CardNumber, tagChampions [1].CardNumber });
			}
		}
		catch 
		{
			SetTitleChampCardNumber (151);
			SetWorldChampCardNumber (45);
			SetTagChampCardNumbers (new int[2]{ 73, 25 });

		}

		WriteToBinaryFile ();
		GamePlaySession.instance.UploadGameSession ();
	}


	public string SaveToString()
	{
		string myStrTemp = "";
		try 
		{
			if (worldChampion != null) {
				worldChampCardNumber = worldChampion.CardNumber;
				titleChampCardNumber = titleChampion.CardNumber;
				tagChampCardNumbers [0] = tagChampions [0].CardNumber;
				tagChampCardNumbers [1] = tagChampions [1].CardNumber;
			}

			if (worldLastChampion != null)
				worldChampLastCardNumber = worldLastChampion.CardNumber;
			if (titleLastChampion != null)
				titleChampLastCardNumber = titleLastChampion.CardNumber;

			if (tagLastChampions != null) {
				if (tagLastChampions [0] != null)
					tagChampLastCardNumbers [0] = tagLastChampions [0].CardNumber;
				if (tagLastChampions [1] != null)
					tagChampLastCardNumbers [1] = tagLastChampions [1].CardNumber;
			} else {
				tagChampLastCardNumbers [0] = -1;
				tagChampLastCardNumbers [1] = -1;
			}

			var serializer = new XmlSerializer (typeof(GamePlaySession));
			MemoryStream memStream = new MemoryStream ();	

			XmlWriterSettings settings = new XmlWriterSettings ();
			settings.Indent = false;
			settings.NewLineHandling = NewLineHandling.None;

			using (XmlWriter writer = XmlWriter.Create (memStream, settings)) {
				serializer.Serialize (writer, this);
			}

			memStream.Flush ();
			memStream.Position = 0;

			var sr = new StreamReader (memStream);
			myStrTemp = sr.ReadToEnd ();

			memStream.Close ();


		} 
		catch (Exception e) 
		{
			Debug.Log ("ERROR: " + e.ToString ());
		}
		return myStrTemp;
	}
		
	public bool Equals(GamePlaySession other)
	{
		if (selectedFederation != other.selectedFederation)
			return false;
		else if (selectedComissioner != other.selectedComissioner)
			return false;
		else if (myCash != other.myCash)
			return false;
		else if (myTokens != other.myTokens)
			return false;
		else if (myBonuses.Count != other.myBonuses.Count)
			return false;
		else if (isNewPlayer != other.isNewPlayer)
			return false;
		else if (inTutorialMode != other.inTutorialMode)
			return false;
		else if (storeCardsBought != other.storeCardsBought)
			return false;
		else if (!storePurchases.Equals (other.storePurchases))
			return false;
		else if (averageRating != other.averageRating)
			return false;
		else if (myLevel != other.myLevel)
			return false;
		else if (!lastShopRefresh.Equals (other.lastShopRefresh))
			return false;
		else if (!shownBlurbs.Equals (other.shownBlurbs))
			return false;
		else if (myCards.Count != other.myCards.Count)
			return false;
		else if (myShows.Count != other.myShows.Count)
			return false;
		else if (myBonuses.Count != other.myBonuses.Count)
			return false;
		else if (!tagChampCardNumbers.Equals (other.tagChampCardNumbers))
			return false;
		else if (!tagChampLastCardNumbers.Equals (other.tagChampLastCardNumbers))
			return false;
		else if (!RatingPositionHolder.Equals (other.RatingPositionHolder))
			return false;
		else if (!FedRatings.Equals (other.FedRatings))
			return false;
		else if (fantasyPopUpShown != other.fantasyPopUpShown)
			return false;
		//storeCards = other.storeCards;



		//AllTagChampions = other.AllTagChampions;
		//AllTitleChampions = other.AllTitleChampions;
		//AllWorldChampions = other.AllWorldChampions;
		//Awards = other.Awards;
		//fantasyPopUpShown = other.fantasyPopUpShown;
		//myCalendar = other.myCalendar;
		//numberOfYears = other.numberOfYears;
		//myCalendar = other.myCalendar;

		Debug.Log ("GAMEPLAYSESSION IS THE SAME!");

		return true;
	}

	public static GamePlaySession Load(string data)
	{
		var serializer = new XmlSerializer(typeof(GamePlaySession), new XmlRootAttribute("GamePlaySession"));
		var reader = XmlReader.Create(new StringReader(data));

		GamePlaySession s = serializer.Deserialize(reader) as GamePlaySession;
		s.tagChampions = new WrestlerCardObject[2];
		s.tagLastChampions = new WrestlerCardObject[2];
		foreach (CardObject card in s.myCards)
		{
			if(card.CardNumber == s.worldChampCardNumber)
			{
				s.worldChampion = (WrestlerCardObject)card;
			}
			if(card.CardNumber == s.titleChampCardNumber)
			{
				s.titleChampion = (WrestlerCardObject)card;
			}
			if(card.CardNumber == s.tagChampCardNumbers[0])
			{
				s.tagChampions[0] = (WrestlerCardObject)card;
			}
			if(card.CardNumber == s.tagChampCardNumbers[1])
			{
				s.tagChampions[1] = (WrestlerCardObject)card;
			}

			if(card.CardNumber == s.worldChampLastCardNumber)
			{
				s.worldLastChampion = (WrestlerCardObject)card;
			}
			if(card.CardNumber == s.titleChampLastCardNumber)
			{
				s.titleLastChampion = (WrestlerCardObject)card;
			}
			if(card.CardNumber == s.tagChampLastCardNumbers[0])
			{
				s.tagLastChampions[0] = (WrestlerCardObject)card;
			}
			if(card.CardNumber == s.tagChampLastCardNumbers[1])
			{
				s.tagLastChampions[1] = (WrestlerCardObject)card;
			}
		}
        GameObject UIManager = GameObject.Find("UIManager");
        s.GSI = UIManager.GetComponent<GameSetupItems>();
		return s;
		//}
	//	catch (Exception e){
	//		Debug.Log(e.StackTrace);
	//	}
		//return new GamePlaySession();
	}

	public bool GiveInitialCards() {
		return (myCards.Count == 0);
	}

	public CardObject CheckCard(int cardNum) {
		return myCards.Find(card => card.CardNumber == cardNum);
	}

    public CardObject CheckCard(int cardNum, List<WrestlerCardObject> List)
    {
        return List.Find(card => card.CardNumber == cardNum);
    }

    public WeekItem GetWeekInCurrentMonth(int weekNum) {
		return myCalendar.years[currentYear].months[currentMonth].weeks[weekNum];
	}

    public void AdvanceMonth() 
	{
        // If we cleared month 1 we've beaten the tutorial

		if (GameSaveController.instance.firstSave) 
		{
			GameSaveController.instance.UploadUserData ();
		}

        inTutorialMode = false;
        
        //SaveAll();
        currentMonth += 1;
        
		if (currentMonth >= 12) 
		{
			if (currentYear >= numberOfYears - 1) 
			{
				// Go back to the month we were on and complete the game
				currentMonth -= 1;
				CompleteGame();
			} 
			else
			{
				currentMonth = 0;
				currentYear += 1;
			}
			AdvanceChampions ();
		}
	}

	public void CompleteGame() {
		gameCompleted = true;
	}
    
	public float GetAverageStarRating() {
		float starRating = GetAverageRating();

		if (starRating >= 19f) {
			// 5 stars
			return 1f;
		} else if (starRating >= 15f) {
			// 4 stars
			return 0.8f;
		} else if (starRating >= 11f) {
			// 3 stars
			return 0.6f;
		} else if (starRating >= 7f) {
			// 2 stars
			return 0.4f;
		} else if (starRating >= 3f) {
			// 1 star
			return 0.2f;
		} else {
			// 0 stars
			return 0f;
		}
	}

	public static PurchaseResult AttemptPurchase(int cashCost, int tokenCost){
		int currCash = GamePlaySession.instance.GetMyCash();
		int currTokens = GamePlaySession.instance.GetMyTokens();
		
		if(currCash < cashCost && cashCost > 0){
			return PurchaseResult.NOT_ENOUGH_CASH;
		}
		if(currTokens < tokenCost && tokenCost > 0){
			return PurchaseResult.NOT_ENOUGH_TOKENS;
		}

		GamePlaySession.instance.SetMyCash(currCash - cashCost);
		GamePlaySession.instance.SetMyTokens(currTokens - tokenCost);

		return PurchaseResult.PURCHASE_COMPLETED;
	}

	//purchase things using real money


	//simulation functions
	public int GetCommishBonus()
    {
		int i = 0;
		while(i < myBonuses.Count)
        {
			if(myBonuses[i].bType == BonusValue.BonusType.PLUS_CASH)
            {
				return myBonuses[i].value;
			}
			i++;
		}
		return 0;
	}

    public string[] DecodeChamp(string Champ)
    {
        char[] Delimiters = { 'q' };
        return (Champ.Split(Delimiters[0]));
    }

	void AdvanceChampions()
	{
		if (titleChampion.LastActiveYear <= currentYear + 1980 && titleChampion.LastActiveYear!=1999) 
		{
			titleChampion.LastActiveYear += 1;
		}

		if (worldChampion.LastActiveYear <= currentYear + 1980 && worldChampion.LastActiveYear!=1999) 
		{
			worldChampion.LastActiveYear += 1;
		}

		foreach (var item in tagChampions) 
		{
			if (item.LastActiveYear <= currentYear + 1980 && item.LastActiveYear!=1999) 
			{
				item.LastActiveYear += 1;
			}
		}
	}


	public int GetDate(string champ)
	{
		int monthNum;
		string crowningDate = DecodeChamp (champ) [2];
		if (crowningDate.ToUpper () == "DEFAULT") 
		{
			monthNum = 1;
		} 
		else 
		{
			char[] cDate = crowningDate.ToCharArray ();
			int year = int.Parse(cDate[cDate.Length-4].ToString() + cDate[cDate.Length-3].ToString() + cDate[cDate.Length-2].ToString() + cDate[cDate.Length-1].ToString());
			int month;
			if (cDate [7].ToString() == ",") 
			{
				month = int.Parse (cDate [6].ToString ());
			} 
			else 
			{
				month = int.Parse (cDate [6].ToString () + cDate [7].ToString ());	
			}
			monthNum = (year - int.Parse (GamePlaySession.instance.myCalendar.years [0].yearName))*12 + month;
		}
		return monthNum;
	}

	public List<string> SortByDate(List<string> champList)
	{
		for (int j = 0; j < champList.Count; j++) 
		{
			for (int i = 0; i < champList.Count - 1; i++) 
			{
				if (GetDate (champList [i]) < GetDate (champList [i + 1])) 
				{
					string temp = champList [i + 1];
					champList [i + 1] = champList [i];
					champList [i] = temp;
				}
			}
		}

		return champList;
	}
	public List<List<string>> SortByDateTag(List<List<string>> champList)
	{
		for (int j = 0; j < champList.Count; j++) 
		{
			for (int i = 0; i < champList.Count - 1; i++) 
			{
				if (GetDate (champList [i][0]) < GetDate (champList [i + 1][0])) 
				{
					List<string> temp = champList [i + 1];
					champList [i + 1] = champList [i];
					champList [i] = temp;
				}
			}
		}

		return champList;
	}

	/// <summary>
	/// returns world Championships if world is true, otherwise returns Title Championships.
	/// </summary>
	/// <returns>The championships.</returns>
	/// <param name="World">If set to <c>true</c> then World Championships, otherwise Title Championships</param>


	public List<Championship> GetWorldChampionships()
	{
		
		int firstChampID = 45;
//		foreach (var item in AllWorldChampions) 
//		{
//			if ((DecodeChamp (item) [2]).ToUpper () == "DEFAULT") 
//			{
//				firstChampID =  int.Parse((DecodeChamp (item) [0]));
//			}
//		}

		Championship firstChampionship = new Championship (firstChampID,0);
		Championship latestChampionship = firstChampionship;
		List<Championship> championships = new List<Championship> ();
		List<ShowObject> championshipShows = new List<ShowObject> ();
		List<int> championshipShowsIndexes = new List<int> ();
		int latestChampID = firstChampID;

		championshipShows = GetTitleShows (SegmentObject.TitleMatchType.WORLD);
		championshipShowsIndexes = GetTitleShowIndexes (SegmentObject.TitleMatchType.WORLD);

		for (int i = 0; i < GamePlaySession.instance.myShows.Count; i++) 
		{
			if (GamePlaySession.instance.myShows [i].showFinished) 
			{
				if (GamePlaySession.instance.myShows [i].segments != null) 
				{
					foreach (var seg in GamePlaySession.instance.myShows [i].segments) 
					{
						if (seg.isTitleMatch && seg.titleMatchType == SegmentObject.TitleMatchType.WORLD && (seg.DisQualify!=1 && seg.DisQualify!=3 && seg.DisQualify!=4)) 
						{
							if (seg.winnerIDs [0] == latestChampionship.cardNumber) 
							{
								latestChampionship.defenses++;
							} 
							else 
							{
								Championship newChampionship = new Championship (seg.winnerIDs [0], i);
								championships.Add (latestChampionship);
								latestChampionship = newChampionship;
							}
						}
					}
				}
			}
		}

		championships.Add (latestChampionship);

		return championships;
	}

	public List<Championship> GetTitleChampionships()
	{
		int firstChampID = 151;
//		foreach (var item in AllTitleChampions) 
//		{
//			if ((DecodeChamp (item) [2]).ToUpper () == "DEFAULT") 
//			{
//				firstChampID =  int.Parse((DecodeChamp (item) [0]));
//			}
//		}

		Championship firstChampionship = new Championship (firstChampID,0);
		Championship latestChampionship = firstChampionship;
		List<Championship> championships = new List<Championship> ();
		List<ShowObject> championshipShows = new List<ShowObject> ();
		List<int> championshipShowsIndexes = new List<int> ();
		int latestChampID = firstChampID;

		championshipShows = GetTitleShows (SegmentObject.TitleMatchType.CONTINENTAL);
		championshipShowsIndexes = GetTitleShowIndexes (SegmentObject.TitleMatchType.CONTINENTAL);

		for (int i = 0; i < GamePlaySession.instance.myShows.Count; i++) 
		{
			if (GamePlaySession.instance.myShows [i].showFinished) 
			{
				if (GamePlaySession.instance.myShows [i].segments != null) {
					foreach (var seg in GamePlaySession.instance.myShows[i].segments) {
						if (seg.isTitleMatch && seg.titleMatchType == SegmentObject.TitleMatchType.CONTINENTAL && (seg.DisQualify != 1 && seg.DisQualify != 3 && seg.DisQualify != 4)) {
							if (seg.winnerIDs [0] == latestChampionship.cardNumber) {
								latestChampionship.defenses++;
							} else {
								Championship newChampionship = new Championship (seg.winnerIDs [0], i);
								championships.Add (latestChampionship);
								latestChampionship = newChampionship;
							}
						}	
					}
				}
			}
		}

		championships.Add (latestChampionship);

		return championships;
	}

	public List<TagChampionship> GetTagChampionships()
	{
		int firstChampIDA = 73;
		int firstChampIDB = 25;
//		foreach (var item in AllTagChampions) 
//		{
//			if ((DecodeChamp (item [0])[2]).ToUpper () == "DEFAULT") 
//			{
//				firstChampIDA =  int.Parse(DecodeChamp (item [0])[0]);
//				firstChampIDB =  int.Parse(DecodeChamp (item [1])[0]);
//			}
//		}

		TagChampionship firstChampionship = new TagChampionship (firstChampIDA, firstChampIDB,0);
		TagChampionship latestChampionship = firstChampionship;
		List<TagChampionship> championships = new List<TagChampionship> ();
		List<ShowObject> championshipShows = new List<ShowObject> ();
		int latestChampIDA = firstChampIDA;
		int latestChampIDB = firstChampIDB;

		championshipShows = GetTitleShows (SegmentObject.TitleMatchType.TAG);

		for (int i = 0; i < GamePlaySession.instance.myShows.Count; i++) 
		{
			if (GamePlaySession.instance.myShows [i].segments != null) 
			{
				foreach (var seg in GamePlaySession.instance.myShows[i].segments) 
				{
					if (GamePlaySession.instance.myShows [i].showFinished) 
					{
						if (seg.isTitleMatch && seg.titleMatchType == SegmentObject.TitleMatchType.TAG && (seg.DisQualify != 1 && seg.DisQualify != 3 && seg.DisQualify != 4)) 
						{
							List<int> segWinners = seg.winnerIDs.ToList ();
							segWinners.Sort ();
							List<int> myWrestlerIDs = new List<int> ();
							myWrestlerIDs.Add (latestChampionship.cardA);
							myWrestlerIDs.Add (latestChampionship.cardB);
							myWrestlerIDs.Sort ();

							if (myWrestlerIDs.SequenceEqual (segWinners)) 
							{
								latestChampionship.defenses++;
							} 
							else 
							{
								TagChampionship newChampionship = new TagChampionship (seg.winnerIDs [0], seg.winnerIDs [1], i);
								championships.Add (latestChampionship);
								latestChampionship = newChampionship;
							}
						}
					}
				}
			}
		}
		championships.Add (latestChampionship);

		return championships;
	}

	List<ShowObject> GetTitleShows(SegmentObject.TitleMatchType type)
	{
		List<ShowObject> shows = new List<ShowObject> ();
		foreach (var show in myShows) 
		{
			if (show.showFinished)
			{
				if (show.segments == null)
					continue;
				foreach (var seg in show.segments) 
				{
					if (seg.isTitleMatch && seg.titleMatchType == type)
						shows.Add (show);
				}
			}
		}
		return shows;
	}

	List<int> GetTitleShowIndexes(SegmentObject.TitleMatchType type)
	{
		List<int> showIndexes = new List<int> ();
		int i = 0;
		foreach (var show in myShows) 
		{
			if (show.showFinished)
			{
				if (show.segments == null)
					continue;
				foreach (var seg in show.segments) 
				{
					if (seg.isTitleMatch && seg.titleMatchType == type)
						showIndexes.Add (i);
				}
			}
			i++;
		}
		return showIndexes;
	}

	string ListToString(List<int> list)
	{
		string finalString = "";
		for (int i = 0; i < list.Count; i++) 
		{
			if (finalString != "") 
			{
				finalString = finalString + "," + list [i].ToString ();
			} 
			else 
			{
				finalString = finalString + list [i].ToString ();
			}
		}
		return finalString;
	}

	public void OnParseLogin(bool success)
	{
		if (success) 
		{
			PlayerPrefs.SetString ("parseObjectID", ParseUser.CurrentUser.ObjectId.ToString ());
			Debug.Log ("Login Successful");
		} 
		else 
		{
			Debug.Log ("Parse Login Failed");
			// Do Something
		}
	}

	public void WriteToBinaryFile()
	{
		string path = "";
		#if UNITY_EDITOR
		path = Application.dataPath + "/userdata.dat";
		#elif UNITY_ANDROID || UNITY_IOS
		path = Application.persistentDataPath + "/userdata.dat";
		#endif

		using (Stream stream = File.Open (path, FileMode.Create)) 
		{
			var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
			binaryFormatter.Serialize (stream, GamePlaySession.instance);
		}
	}


	public void MakeCalendar()
	{
		myCalendar = myCalendar.GenerateCalendar ();
	}


	public void AddCardObjectToList(CardObject card,List<CardObject> cards)
	{
		cards.Add (card);

		//Also upload to Parse Server...
		List<int> myCardIDs = new List<int>();
		foreach (var item in myCards) 
		{
			myCardIDs.Add (item.CardNumber);
		}

		List<int> wrestlerStats = new List<int>();
		if (GamePlaySession.instance.CheckCard (card.CardNumber) == null) 
		{
			if (card.myCardType == CardType.WRESTLER) 
			{
				WrestlerCardObject wrestler = (WrestlerCardObject)card;

				wrestlerStats.Add (wrestler.popCurrent);
				wrestlerStats.Add (wrestler.micCurrent);
				wrestlerStats.Add (wrestler.skillCurrent);
				wrestlerStats.Add (wrestler.pushCurrent);
				ParseManager.UpdateOrCreateWrestlerCardData (card.CardNumber, wrestlerStats);
			}
		}
	}


	public void AddSeveralCardObjectsToList(List<CardObject> cardsInput, List<CardObject> cardsList)
	{
		Dictionary<int,List<int>> wrestlersDictionary = new Dictionary<int, List<int>> ();
		List<int> myCardIDs = new List<int>();

		foreach (var item in cardsInput) 
		{
			if (GamePlaySession.instance.CheckCard (item.CardNumber) == null) 
			{
				//cardsList.Add(item);
				if (item.myCardType == CardType.WRESTLER) 
				{
					WrestlerCardObject wrestler = (WrestlerCardObject)item;
					List<int> wrestlerStats = new List<int>();
					wrestlerStats.Add (wrestler.popCurrent);
					wrestlerStats.Add (wrestler.micCurrent);
					wrestlerStats.Add (wrestler.skillCurrent);
					wrestlerStats.Add (wrestler.pushCurrent);
					wrestlersDictionary.Add (wrestler.CardNumber, wrestlerStats);
				}
			}	
		}
		//Also upload to Parse Server...

		foreach (var item in cardsList) 
		{
			myCardIDs.Add (item.CardNumber);
		}

		if(wrestlersDictionary.Count>0)
			ParseManager.UpdateOrCreateWrestlerCardData (wrestlersDictionary);

		if(myCardIDs.Count>0)
			ParseManager.UpdateGameSessionData ("myCardIDs", myCardIDs);
	}
		

//	public void GameSessionLoaded(bool success,ParseObject session)
//	{
//		UIManagerScript.instance.BuildGamePlaySession (0,session);
//	}
//
//	public void ShowDataLoaded(bool success, IEnumerable<ParseObject> showData)
//	{
//		UIManagerScript.instance.BuildGamePlaySession (1,null,showData);
//	}
//
//	public void SegmentDataLoaded(bool success, IEnumerable<ParseObject> segmentData)
//	{
//		UIManagerScript.instance.BuildGamePlaySession (2,null,segmentData);
//	}
//
//	public void WrestlerDataLoaded(bool success, IEnumerable<ParseObject> wrestlerData)
//	{
//		UIManagerScript.instance.BuildGamePlaySession (3,null,wrestlerData);
//	}



}
		
		