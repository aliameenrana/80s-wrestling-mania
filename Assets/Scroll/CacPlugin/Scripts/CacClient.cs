﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LitJson;
using System.Text.RegularExpressions;

public class CacClient : MonoBehaviour {

	static private CacClient instance = null;
	//public static CacClient Instance;
	public delegate void CompleteAction(JsonData returnedJson = null);
	public static event CompleteAction OnCompleteAction;
	public delegate void UIAction() ;
	public static event  UIAction OnClickAction;


	Dictionary<string, string> data = new Dictionary<string, string>();
	public Dictionary<string, string> gameAchievements = new Dictionary<string, string>();
	public List<string> playerAchievements = new List<string> ();
	string error;
	private string UrlPlayer = "http://45.55.220.66:9369/playersController";
	private string UrlGames = "http://45.55.220.66:9369/gamesController";
	public string userId;

	private string userName;
	public string _userName
	{
		get
		{ 
			return userName;
		}
		set
		{
			userName = value;
		}

	}

	private string playerEmail;
	public string _playerEmail
	{
		get
		{ 
			return playerEmail;
		}
		set
		{
			playerEmail = value;
		}
	}
	private string playerPassword;
	public string _playerPassword
	{
		get
		{ 
			return playerPassword;
		}
		set
		{
			playerPassword = value;
		}
	}

	public string gameId;
	public InputField LoginuseridLabel;
	public InputField LoginpasswordLabel;
	public InputField SignUpuserName;
	public InputField SignUpEmail;
	public InputField SignUppassword;
	public InputField SignUppassword2;
	public GameObject LoadingIcon;
	public GameObject CacPopup;
	public GameObject LoginScreenPanel;
	public GameObject SignUpScreenPanel;
	public GameObject AchievementsPanel;
	public Transform AchievementsParent;
	public GameObject CacPanel;
	public GameObject PopupBackButton;
	public GameObject achievementButton;
	public GameObject warningImage;
	public GameObject tickImage;
	public GameObject crossImage;


	public bool isLoggedIn = false;

	public Text CacText;
	private const string statusCode0 = "200";
	private const string statusCode1 = "100";
	private const string statusCode2 = "206";
	private const string statusCode3 = "201";
	private const string statusCode4 = "202";
	public const string MatchEmailPattern =
		@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
              + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";


	void Awake()
	{
	
		instance = this;
//		if (isLoggedin ()) {
//		
//			ChangeLoginStatus (true);
//			isLoggedIn = true;
//			Debug.Log ("User value found");
//		}else
//			ChangeLoginStatus (false);

	
	
	}


	void Start()
	{
		if (PlayerPrefs.GetInt ("LoggedIn") == 1) 
		{
			userId = PlayerPrefs.GetString ("LastUserId");
		
		}
//		getUserData ();
	
	
	}
	static public CacClient getInstance()
	{
		return instance;
	}


	#region CallsToServer

	// method checks for null fields, validates email and passwords and sends request to cac server for signup
	public void SignUp()
	{
		data.Clear();
		data.Add("firstName", "Dummy1");
		data.Add("lastName", "Dummy");
		data.Add ("userName", SignUpuserName.text);
		data.Add ("email", SignUpEmail.text);
		data.Add ("password", SignUppassword.text);
		data.Add ("age", "25");
		data.Add ("image","base64Data");
		data.Add ("gender", "male");
		data.Add ("CAC_Points","0");
		data.Add ("social", "CAC");

		if(String.IsNullOrEmpty(SignUpEmail.text) || string.IsNullOrEmpty(SignUpuserName.text)|| string.IsNullOrEmpty(SignUppassword.text))
		{
			ChangeObjectState (SignUpScreenPanel, false);
			ChangeObjectState (CacPopup, true);
			ChangeObjectState (LoadingIcon, false);
			ChangeObjectState (tickImage, false);
			ChangeObjectState (warningImage, true);
			CacText.text = "One or more of fields you have entered are empty. Please re-enter your sign up information";
			OnClickAction += SignUpFailed;
			return;
		}
	
		if (!IsEmail (SignUpEmail.text))
		{
			ChangeObjectState (SignUpScreenPanel, false);
			ChangeObjectState (CacPopup, true);
			ChangeObjectState (warningImage, true);
			ChangeObjectState (tickImage, false);
			ChangeObjectState (LoadingIcon, false);
			CacText.text = "Invalid email. Please try again";
			OnClickAction += SignUpFailed;
			return;
		} 
		if (!ValidPassword (SignUppassword.text)) 
		{
			ChangeObjectState (SignUpScreenPanel, false);
			ChangeObjectState (CacPopup, true);
			ChangeObjectState (LoadingIcon, false);
			ChangeObjectState (warningImage, true);
			CacText.text = "The password you have entered is invalid, please make sure that it is atleast 6 letters long";
			OnClickAction += SignUpFailed;
			return;
		}
		if (SignUppassword.text != SignUppassword2.text) 
		{
			ChangeObjectState (SignUpScreenPanel, false);
			ChangeObjectState (CacPopup, true);
			ChangeObjectState (LoadingIcon, false);
			ChangeObjectState (warningImage, true);
			CacText.text = "The passwords you have entered do not match. Please try again";
			OnClickAction += SignUpFailed;
			return;
					
		}
		StartCoroutine (DoAction ("SIGNUP", data, UrlPlayer));
		SignUpScreenPanel.SetActive (false);
		ChangeObjectState (CacPopup, true);
		CacText.text = "Signing in....";
		ChangeObjectState (warningImage, false);
		ChangeObjectState (tickImage, false);
		ChangeObjectState (crossImage, false);
		ChangeObjectState (LoadingIcon, true);
		ChangeObjectState (PopupBackButton, false);
		OnCompleteAction += OnSignUp;

	}

	// method send login info to server ,checks for null field , validates email ,a then sends request to server

	public void LoginClick()
	{
		data.Clear();
		data.Add ("email", LoginuseridLabel.text);
		data.Add ("password", LoginpasswordLabel.text);
		data.Add ("social", "CAC");
		if (String.IsNullOrEmpty (LoginuseridLabel.text) || string.IsNullOrEmpty (LoginpasswordLabel.text))
		{
			ChangeObjectState (LoginScreenPanel, false);
			ChangeObjectState (CacPopup, true);
			CacText.text = "One or more of fields you have entered are empty. Please re-enter your sign up information";
			OnClickAction += LoginFailed;
			return;
		}

		if (!IsEmail (LoginuseridLabel.text)) 
		{
			ChangeObjectState (LoginScreenPanel, false);
			ChangeObjectState (CacPopup, true);
			CacText.text = "Invalid email. Please try again";
			OnClickAction += LoginFailed;
			return;
		}
			playerEmail = LoginuseridLabel.text;
			playerPassword = LoginpasswordLabel.text;
			
			ChangeObjectState (CacPopup, true);
			ChangeObjectState (PopupBackButton, false);
			ChangeObjectState (warningImage, false);
			ChangeObjectState (tickImage, false);
			ChangeObjectState (crossImage, false);
			ChangeObjectState (LoadingIcon, true);
			CacText.text ="Logging in ....";
			ChangeObjectState (warningImage, false);
			ChangeObjectState (tickImage, false);
			ChangeObjectState (crossImage, false);
			StartCoroutine (DoAction ("LOGIN", data, UrlPlayer));
			OnCompleteAction += OnLogin;
		}

	// methods adds a game to servers admin panel
	public void AddGameToServer()
	{
		data.Clear ();
		data.Add ("gameName","Don't mess with my head");
		data.Add ("gameImage","121232252");
		data.Add ("version", "1.0");
		StartCoroutine(DoAction ("ADD_NEW_GAME",data,UrlGames));
		OnCompleteAction += OnGameAddedToServer;
	}

	// method to call login at first time
	public void LoginFirstTime(string email, string password)
	{
		data.Clear();
		data.Add ("email", email);
		data.Add ("password", password);
		playerPassword = password;
		StartCoroutine(DoAction ("LOGIN",data,UrlPlayer));
		OnCompleteAction += OnLogin;
	}

	// method that sends request for checking if email has been verified
	public void VerifyEmail()
	{
		data.Clear();
		StartCoroutine(DoAction ("VERIFY_EMAIL",data,UrlPlayer));
		OnCompleteAction += OnVerification;
	}

	// method sends a request to server add the player to the game's database
	public void AddPlayerToGame()
	{
		data.Clear ();
		data.Add ("gameId",gameId);
		data.Add ("userId", userId);
		StartCoroutine(DoAction ("ADD_PLAYER_TO_GAME",data,UrlGames));
		OnCompleteAction += OnAddedPlayerToTheGame;
	}

	//method sends a request to server add the game into the player's databse
	public void AddGameToPlayer()
	{
		data.Clear ();
		data.Add ("userId", userId);
		data.Add ("gameId",gameId);
		StartCoroutine(DoAction ("ADD_GAME",data,UrlPlayer));
		OnCompleteAction += OnAddedGameToPlayer;
	}

	// method sends a request to server ,to check if player is already added to 
	public void CheckIfUserIsAddedTothisGame(string userid, string gameid)
	{
		data.Clear ();
		data.Add ("gameId",gameid);
		data.Add ("userId", userId);
		StartCoroutine(DoAction ("CHECK_IF_USER_HAS_THIS_GAME",data,UrlPlayer));
		OnCompleteAction += OnCheckedIfUserIsAdded;
	}

	// methods add the player to the leaderboard, by sending in the highest score ,the game id and the user id
	public void AddUserToLeaderBoard(string gameId, string userId, string score)
	{
		data.Clear ();
		data.Add ("gameId",gameId);
		data.Add ("userId", userId);
		data.Add ("score", score);
		StartCoroutine(DoAction ("ADD_USER_TO_LEADER_BOARD",data,UrlPlayer));
		OnCompleteAction += OnAddedPlayerToTheGame;
	}

	// method sends request to the server to retrieve the list of achivements claimed by the player
	public void ListPlayerAchievements(string userId, string gameId )
	{
		data.Clear ();
		data.Add ("userId", userId);
		data.Add ("gameId",gameId);
		StartCoroutine(DoAction ("GET_THIS_PLAYERS_ACHIEVEMENTS_IN_THIS_GAME",data,UrlPlayer));
		OnCompleteAction += OnGetListOfPlayerAchievements;
		ChangeObjectState (CacPopup, true);
		ChangeObjectState (LoadingIcon, true);
		ChangeObjectState (warningImage, false);
		ChangeObjectState (tickImage, false);
		ChangeObjectState (crossImage, false);
		CacText.text = "Fetching Achievements. Please wait..";
	}

	// method sends request to the server to retrieve the list of achivements of the game
	public void ListAchievements(string gameId )
	{
		data.Clear ();
		data.Add ("gameId",gameId);
		StartCoroutine(DoAction ("LIST_ACHIEVEMENTS_OF_THIS_GAME",data,UrlGames));
		ChangeObjectState (CacPopup, true);
		ChangeObjectState (PopupBackButton, false);
		ChangeObjectState (LoadingIcon, true);
		ChangeObjectState (warningImage, false);
		ChangeObjectState (tickImage, false);
		ChangeObjectState (crossImage, false);
		CacText.text = "Fetching Achievements. Please wait..";
		OnCompleteAction += OnGetListOfGameAchievements;
	}

	public void DeleteAchievement(string _gameID, string achievement)
	{
		data.Clear ();
		data.Add ("gameId",_gameID);
		data.Add ("achievementId",achievement);
		StartCoroutine(DoAction ("DELETE_ALL_ACHIEVEMENTS_OF_GAME",data,UrlGames));
	}

	public void AddAchievementToGame(string _gameId, string achievementName,string achievementDescription)
	{
		data.Clear ();
		data.Add ("gameId",_gameId);
		data.Add ("achievementName", achievementName);
		data.Add ("achievementImage","base64");
		data.Add ("achievementDescription", achievementDescription);
		StartCoroutine(DoAction ("ADD_ACHIEVEMENT_TO_GAME",data,UrlGames));
		ChangeObjectState (CacPopup, true);
		ChangeObjectState (LoadingIcon, true);
		ChangeObjectState (warningImage, false);
		ChangeObjectState (tickImage, false);
		ChangeObjectState (crossImage, false);
		CacText.text = "loading Achievements. Please wait..";
		OnCompleteAction += OnAchievementAddedToGame;
	}

	public void SendUserData(String jsonString)
	{
		data.Clear ();

		data.Add ("userId",userId);
		data.Add ("gameId",gameId);
		data.Add ("userData", jsonString);
		StartCoroutine (DoAction("UPDATE_PLAYER_DATA_FOR_THIS_GAME",data,UrlPlayer));
		OnCompleteAction += OnSendUserData;
		SendUserDataSuccessful ();
	}
	public void SendUserDataSuccessful()
	{
		// Sent complete
		// now close the windows

		Debug.Log ("SendUserDataSuccessful");

	}

	public void getUserData()
	{
		data.Add ("userId",userId);
		data.Add ("gameId",gameId);
		StartCoroutine (DoAction("GET_PLAYER_DATA_FOR_THIS_GAME",data,UrlPlayer));
//		Debug - waleed
		OnCompleteAction += OnRetrievedUserData;

	}
	#endregion

	#region JsonMapping

	public string BuildCustomJsonString(Dictionary<string, string> _data )
	{
		StringBuilder db = new StringBuilder();
		JsonWriter writer = new JsonWriter (db);

		writer.WriteObjectStart ();
		foreach(var item in _data)
		{
			writer.WritePropertyName (item.Key);
			writer.Write (item.Value);
		}
		writer.WriteObjectEnd ();
		return db.ToString();
	}
		



	// builds the json string , with the keys in dictionary 
	public string buildJson(string action,Dictionary<string, string> data )
	{
		StringBuilder sb = new StringBuilder();
		JsonWriter writer = new JsonWriter (sb);
		writer.WriteObjectStart ();
		writer.WritePropertyName ("ACTION");
		writer.Write (action);
		writer.WritePropertyName ("DATA");
		writer.WriteObjectStart ();
		foreach(var item in data)
		{
			writer.WritePropertyName (item.Key);
			writer.Write (item.Value);
		}
		writer.WriteObjectEnd ();
		writer.WriteObjectEnd ();
		return sb.ToString ();
	}
		// send the json data to the server , calls processjson on returned string
	IEnumerator DoAction(string action, Dictionary<string,string> data,string url)
	{
		WWWForm form = new WWWForm();
		Dictionary<string, string> headers = form.headers;
	
		headers["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
		headers["Accept"] = "application/json, text/javascript, */*; q=0.01";

		Debug.Log (buildJson(action, data));
		byte[] bytes = Encoding.UTF8.GetBytes(buildJson(action, data));
		WWW www = new WWW(url, bytes,headers);
		yield return www;
			
//		LastUserId
		if (www.error == null) 
			{
				Debug.Log (www.text);
			// Debug - waleed overriding functionality
			if(action== "GET_PLAYER_DATA_FOR_THIS_GAME")
			{

			//	GameController.Instance.DecodeJSON (www.text);
			}
				Processjson (www.text);
			} 
			else 
			{
				Debug.Log (www.text);
				Debug.Log ("Error found");
				PopupBackButton.SetActive (true);
			    CacText.text = "Unable to establish connection, please try again";
				ChangeObjectState (LoadingIcon, false);
				ChangeObjectState (warningImage, true);
				OnClickAction += OnInternetConnectionFailed ;
			}

		Debug.Log(action +"      "+ www.text);
		Debug.Log(action + "      "+ www.error);

		}


	// method converts the recieved string from the server and converts it into a json object
	private void Processjson(string jsonString)
	{
		Debug.Log ("Process json String is here" +jsonString);
//		Debug - WALEED
		JsonData jsonData = JsonMapper.ToObject (jsonString);
		if(OnCompleteAction!=null)
			OnCompleteAction (jsonData);
	}







	#endregion

	#region CallBack
	// callback event that is triggered, on player signup 
	public void OnSignUp(JsonData jsonObj= null)
	{
		string status = jsonObj ["STATUS"].ToString ();
		string errorCode = jsonObj ["CODE"].ToString();
		Debug.Log (errorCode);

		if (status.Contains ("SUCCESS")) 
		{
			if (errorCode == statusCode0) 
			{
				ChangeObjectState (CacPopup, true);
				userId = jsonObj ["DATA"] ["_id"].ToString ();
				CacText.text = "Adding player to CAC. Please wait...";
				CheckIfUserIsAddedTothisGame (userId, gameId);
			}
		}
		else 
		{
			if(errorCode ==statusCode4)
			CacText.text = "Could not sign in. Username already taken";
			if(errorCode==statusCode3)
			CacText.text = "Could not sign in. An account is already registered with this email adress ";	
			ChangeObjectState (LoadingIcon,false);
			ChangeObjectState (crossImage, true);
			OnClickAction+=SignUpFailedAction;
			ChangeObjectState (PopupBackButton,true);
		
		}
		OnCompleteAction -= OnSignUp;
	}
	// callback event that is triggered, on verification
	public void OnVerification(JsonData jsonObj= null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{
			
	
		}
	}
	// callback event that is triggered, on login 
	public void OnLogin(JsonData jsonObj= null)
	{
		
		string status = jsonObj ["STATUS"].ToString ();
		string errorCode = jsonObj ["CODE"].ToString();
		Debug.Log (errorCode);
		Debug.Log ("On Login Called");

		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{
			if (errorCode == statusCode0) 
			{
				userId = jsonObj ["DATA"] ["_id"].ToString ();
				PlayerPrefs.SetString ("LastUserId", userId);
				CacText.text = "Logged in successfully !";
				OnClickAction += LoginSuccessfullAction;
				ChangeObjectState (LoadingIcon,false);
				ChangeObjectState (PopupBackButton, true);
				ChangeObjectState (tickImage,true);
				ChangeObjectState (warningImage,false);
				isLoggedIn = true;

				if (PlayerPrefs.GetInt ("LoggedIn") != 1) {
					PlayerPrefs.SetInt ("LoggedIn", 1);
					PlayerPrefs.SetString ("emailAdress", playerEmail);
					PlayerPrefs.SetString ("password", playerPassword);
				}

			}
		} 
		else 
		{
			if(errorCode == statusCode2)
			CacText.text = "Invalid email or password. Please re-enter your credentials again, or create a new account";
			ChangeObjectState (CacPopup,true);
			ChangeObjectState (PopupBackButton,true);
			ChangeObjectState (LoadingIcon,false);
			ChangeObjectState (warningImage,true);
			OnClickAction += LoginFailedAction;
		}
		OnCompleteAction -= OnLogin;
	}
	// callback event that is triggered, on game is added to the server
	public void OnGameAddedToServer (JsonData jsonObj= null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{
			gameId = jsonObj ["DATA"] ["_id"].ToString ();

		}
		OnCompleteAction -= OnGameAddedToServer;
	}
	// callback event that is triggered, on checking if user is added to the game , if not it will call addplayertogame method

	public void OnCheckedIfUserIsAdded(JsonData jsonObj=null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{

			AddPlayerToGame ();

		} 
		else 
		{
			string error = jsonObj ["STATUS"].ToString ();
			if(error.Contains("ERROR"))
			{
				AddGameToPlayer ();
			}

		}
		OnCompleteAction -= OnCheckedIfUserIsAdded;
	}


	public void OnAchievementAddedToGame(JsonData jsonObj= null)
	{
	
		OnCompleteAction -= OnAchievementAddedToGame;
			
	}

	public void OnGetListOfPlayerAchievements(JsonData jsonObj= null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{
			for (int i = 0; i < jsonObj ["DATA"].Count; i++) 
			{
				Debug.Log (jsonObj ["DATA"] [i]["achievement_name"].ToString());
				playerAchievements.Add (jsonObj ["DATA"] [i] ["achievement_name"].ToString ());
			}

			ChangeObjectState (CacPopup,true);
			ChangeObjectState (PopupBackButton,true);
			ChangeObjectState (tickImage, true);
			ChangeObjectState (LoadingIcon,false);
			CacText.text = "Achievements fetched Successfully";
			OnClickAction += GameAchievementsFetchedActions;
		}
		else
		{
			
			CacText.text = "Could not fetch achievements. Please try again";
			ChangeObjectState (CacPopup,true);
			ChangeObjectState (PopupBackButton,true);
			ChangeObjectState (LoadingIcon,false);
			ChangeObjectState (warningImage,true);
			ChangeObjectState (PopupBackButton,true);
			OnClickAction += LoginFailedAction;
		}
		OnCompleteAction -= OnGetListOfPlayerAchievements;
	
	}
	
	public void OnGetListOfGameAchievements (JsonData jsonObj= null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{
			ChangeObjectState (PopupBackButton,false);
			Debug.Log (jsonObj["DATA"].Count);
			Debug.Log (jsonObj ["DATA"] [1]["achievement_id"].ToString());
			for (int i = 0; i < jsonObj ["DATA"].Count; i++) 
			{
				Debug.Log (jsonObj ["DATA"] [i]["achievement_name"].ToString());
				gameAchievements.Add (jsonObj ["DATA"] [i]["achievement_name"].ToString(),jsonObj ["DATA"] [i]["achievement_description"].ToString());
			}
			if (userId == null) 
			{

			
			}

		}
		else
		{			
			CacText.text = "Could not fetch achievements. Please try again";
			ChangeObjectState (CacPopup,true);
			ChangeObjectState (PopupBackButton,true);
			ChangeObjectState (PopupBackButton,true);
			ChangeObjectState (LoadingIcon,false);
			ChangeObjectState (warningImage,true);
			OnClickAction += LoginFailedAction;
		}

		OnCompleteAction -= OnGetListOfGameAchievements;
	}


	public void OnClaimPlayerAchievement (JsonData jsonObj= null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") {
			
		// Watoo Code
		
		}

		else
		{
			
					
		}
		OnCompleteAction -= OnClaimPlayerAchievement;
	}

	public void OnAddedGameToPlayer(JsonData jsonObj= null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{
			AddPlayerToGame ();

		}
		else
		{
			CacText.text = "Couldn't add player to the game. Please login in again";
			ChangeObjectState (CacPopup,true);
			ChangeObjectState (PopupBackButton,true);
			ChangeObjectState (LoadingIcon,false);
			ChangeObjectState (warningImage,true);
			OnClickAction += SignUpFailed;
		}

		OnCompleteAction -= OnAddedGameToPlayer;
	}

	public void OnSendUserData(JsonData jsonObj= null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{
			
			SendUserDataSuccessful ();
		}
	
		OnCompleteAction -= OnSendUserData;
	}

	// callback event that is triggered, when player is added to the game
	public void OnAddedPlayerToTheGame(JsonData jsonObj= null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{
			ChangeObjectState (LoginScreenPanel, true);
			CacText.text = "You have successfully Registered, please login";
			ChangeObjectState (LoadingIcon, false);
			ChangeObjectState (tickImage, true);
			OnClickAction += AfterSuccessfullAction;
			ChangeObjectState (PopupBackButton, true);
		}
		OnCompleteAction -= OnAddedPlayerToTheGame;

	}
	// callback event that is triggered, when player's score is add to the leaderboard
	public void OnAddedPlayerScoreToLeaderBoard(JsonData jsonObj= null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{
		}

	}
	public void OnRetrievedUserData(JsonData jsonObj= null)
	{
		if ((string)jsonObj ["STATUS"] == "SUCCESS") 
		{
			


			///CACBridge.Instance.UserDataReceieved ((string)jsonObj ["DATA"].ToString ());

		}
		OnCompleteAction -= OnRetrievedUserData;
	}
	#endregion

	#region UIActions
	public void AfterSuccessfullAction()
	{
		PlayerPrefs.SetInt ("LoggedIn", 0);

		ChangeObjectState (CacPopup,false);
		ChangeObjectState (SignUpScreenPanel,false);
		ChangeObjectState (LoginScreenPanel,true);
		ClearText ();
		OnClickAction -= AfterSuccessfullAction;
	}



	public void GameAchievementsFetchedActions()
	{
		ChangeObjectState (CacPopup,false);
		ChangeObjectState (AchievementsPanel, true);
		foreach (string key in gameAchievements.Keys) 
		{
			GameObject ach = Instantiate (achievementButton, AchievementsParent);
			ach.transform.localScale = new Vector3 (1, 1, 1);
			ach.transform.Find ("Name").GetComponent<Text> ().text = key;
			ach.transform.Find ("Description").GetComponent<Text> ().text = gameAchievements [key];
			if (playerAchievements.Contains(key))
			{
				ach.GetComponent<AchievementsManager> ().activateTick ();
						
			}
		    
		}
		OnClickAction -= GameAchievementsFetchedActions;
	}


	public void OpenAchievementPanel()
	{
		OpenCACPanel ();
		if (isLoggedIn) 
		{
			ListAchievements (gameId);
		}

		else 
		{

			ChangeObjectState (CacPopup, true);
			ChangeObjectState (LoadingIcon, false);
			ChangeObjectState (warningImage, true);
			CacText.text = "Unable to Load achievements, Please make sure your logged in";
			OnClickAction += OnAchievementsNotOpened;
		}


	}


	public void OnAchievementsNotOpened()
	{
		ChangeObjectState (CacPopup, false);
		ChangeObjectState (LoginScreenPanel, true);
		CloseCacPanel ();
		OnClickAction -= OnAchievementsNotOpened;
	}

	public void GameAchievementsFailedActions()
	{
		ChangeObjectState (CacPopup,false);
	}

	public void SignUpFailedAction()
	{
		ChangeObjectState (warningImage, false);
		ChangeObjectState (tickImage, false);
		ChangeObjectState (crossImage, false);
		ChangeObjectState (CacPopup,false);
		ChangeObjectState (SignUpScreenPanel, true);
		OnClickAction -= SignUpFailedAction;
	}
	public void LoginFailedAction()
	{
		ChangeObjectState (CacPopup, false);
		ChangeObjectState (LoginScreenPanel, true);
	}



	public void LoginSuccessfullAction()
	{
		
	


		ChangeObjectState (CacPopup, false);
		ChangeObjectState (LoginScreenPanel, false);
		CloseCacPanel ();
		OnClickAction -= LoginSuccessfullAction;
	}

	public void SignUpFailed()
	{
		ChangeObjectState (CacPopup,false);
		ChangeObjectState(SignUpScreenPanel,true);
		OnClickAction -= SignUpFailed;
	}

	public void LoginFailed()
	{
		ChangeObjectState (CacPopup,false);
		ClearText ();
		ChangeObjectState (LoginScreenPanel, true);
		OnClickAction -= LoginFailed;
	}

	public void OnInternetConnectionFailed()
	{
		Debug.Log ("OnInternetConnectionFailed");
		ChangeObjectState (CacPopup,false);
		ChangeObjectState (LoadingIcon, false);
		ChangeObjectState (SignUpScreenPanel,false);
		ChangeObjectState (LoginScreenPanel, true);
		OnClickAction -= OnInternetConnectionFailed;
	}

	public void ButtonClick()
	{
		OnClickAction();

	}
	#endregion

	#region UICalls

	public static bool ValidPassword(string password)
	{
		if (password.Length < 6) 
		{
			return false;	
		} 
		else 
		{
			return true;
		}

	}

	public void ClearText()
	{
		LoginuseridLabel.text = "";
		LoginpasswordLabel.text = "";
		SignUppassword.text = "";
		SignUpuserName.text = "";
		SignUpEmail.text = "";
	}

	public void ChangeObjectState(GameObject obj, bool state)
	{
		if (obj == warningImage) {
		
			Debug.Log ("Warning Image Passed");
		}
		if (state) 
		{
			obj.SetActive (true);
		}
		else 
		{
			obj.SetActive (false);

		}
	}

	public bool isLoggedin()
	{
		if (PlayerPrefs.GetInt ("LoggedIn") == 1) 
		{
			return true;
		} 
		else 
		{
			
			return false;
		}
	}


	public static bool IsEmail(string email)
	{
		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
		else return false;
	}

	// ui call for opening the signup panel
	public void OpenSignupPanel()
	{
		ClearText ();
		ChangeObjectState (LoginScreenPanel, false);
		ChangeObjectState (SignUpScreenPanel, true);
	}
	public void OpenLoginPanel()
	{


		if(isLoggedIn)
		{
			
			CloseCacPanel ();
		}
		else
		{
			ChangeObjectState (SignUpScreenPanel, false);
			ChangeObjectState (LoginScreenPanel, true);

		}

	}

	// ui call for closing the cac panel
	public void CloseCacPanel()
	{
		ClearText ();
		ChangeObjectState(AchievementsPanel, false);
		ChangeObjectState(SignUpScreenPanel, false);
		ChangeObjectState(LoginScreenPanel, true);
		//Debug - Waleed
		ChangeObjectState(CacPanel, false);

	}
	//ui call for opening the cac panel
	public void OpenCACPanel()
	{
		ChangeObjectState (CacPanel, true);
		ClearText ();

		if (isLoggedIn) 
		{
			ChangeObjectState (SignUpScreenPanel, true);
			ChangeObjectState (LoginScreenPanel, false);
				
		}

		if (PlayerPrefs.GetInt("LoggedIn") == 1) 
		{
			LoginuseridLabel.text = PlayerPrefs.GetString ("emailAdress");
			LoginpasswordLabel.text =PlayerPrefs.GetString ("password"); 

		}
	}
	public void Logout(){
	
		PlayerPrefs.DeleteKey ("LoggedIn");

		PlayerPrefs.DeleteKey ("emailAdress");

		PlayerPrefs.DeleteKey ("password"); 
		// Logout is successfull and we don't need to override the data
		// we just need to call the required fu
		//CACBridge.Instance.ChangeLoginStatus(false);
	}




	//ui call for turning off a gameobject
	public void TurnThisOff(GameObject Obj)
	{
		Obj.SetActive (false);
	}
	#endregion
}
